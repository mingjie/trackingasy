#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TAxis.h>

int plotDiff(const std::string& charge = "Dsm") {
    gROOT->ProcessLine(".x ~/lhcbStyle.C");

    std::vector<double> Muon, MuonErr, Pion, PionErr;

    std::ifstream f1("kileReweight/final_muon" + charge + ".txt");
    if (!f1) {
        std::cerr << "Cannot open kileReweight/final_muon" + charge + ".txt" << std::endl;
        return 0;
    }

    std::string line;
    while (std::getline(f1, line)) {
        size_t semi_pos = line.find(" ; ");
        size_t tab_pos = line.find('\t');
        Muon.push_back(std::stod(line.substr(tab_pos + 1, semi_pos - tab_pos - 1)));
        MuonErr.push_back(std::stod(line.substr(semi_pos + 3)));
    }

    std::ifstream f2("kileReweight/final_pion" + charge + ".txt");
    if (!f2) {
        std::cerr << "Cannot open kileReweight/final_pion" + charge + ".txt" << std::endl;
        return 0;
    }

    while (std::getline(f2, line)) {
        size_t semi_pos = line.find(" ; ");
        size_t tab_pos = line.find('\t');
        Pion.push_back(std::stod(line.substr(tab_pos + 1, semi_pos - tab_pos - 1)));
        PionErr.push_back(std::stod(line.substr(semi_pos + 3)));
    }

    TCanvas* c1 = new TCanvas("c1", "c1", 800, 600);

    TGraphErrors* Kst = new TGraphErrors();
    TGraphErrors* phi = new TGraphErrors();
    TGraphErrors* NR = new TGraphErrors();
    TMultiGraph* mg = new TMultiGraph();

    const char* stat[] = {"Kst", "phi", "NR"};

    for (int s = 0; s < 3; ++s) {
        for (int i = 0; i < 6; ++i) {
            int x = s * 3 + i;

            if (x >= Pion.size() || x >= Muon.size()) continue;

            TGraphErrors* currentGraph = nullptr;
            if (s == 0) currentGraph = Kst;
            else if (s == 1) currentGraph = phi;
            else if (s == 2) currentGraph = NR;

            double diff = Pion[x] - Muon[x];
            double err = sqrt(MuonErr[x]*MuonErr[x] + PionErr[x]*PionErr[x]);
            currentGraph->SetPoint(i, i + 1, diff);
            currentGraph->SetPointError(i, 0, err);

            std::ofstream outfile("diff_PiMu_" + charge + ".txt", std::ios::app);
            outfile << stat[s] << "\t" << diff << "\t" << err << "\n";
        }
    }

    Kst->SetMarkerColor(kBlue);
    Kst->SetLineColor(kBlue);
    Kst->SetMarkerStyle(20);
    Kst->SetMarkerSize(1);

    phi->SetMarkerColor(kRed);
    phi->SetLineColor(kRed);
    phi->SetMarkerStyle(20);
    phi->SetMarkerSize(1);

    NR->SetMarkerColor(kBlack);
    NR->SetLineColor(kBlack);
    NR->SetMarkerStyle(20);
    NR->SetMarkerSize(1);

    mg->Add(Kst, "AP");
    mg->Add(phi, "AP");
    mg->Add(NR, "AP");
    mg->GetYaxis()->SetTitle("A_{Track} difference (#times 10^{-2})");

    TAxis *xAxis = mg->GetXaxis();

    xAxis->SetLimits(0.5, 6.5);
    xAxis->SetNdivisions(6, kTRUE);
    xAxis->SetBinLabel(1, "16UP");
    xAxis->SetBinLabel(2, "16Down");
    xAxis->SetBinLabel(3, "17UP");
    xAxis->SetBinLabel(4, "17Down");
    xAxis->SetBinLabel(5, "18Up");
    xAxis->SetBinLabel(6, "18Down");

    mg->Draw("A");

    TText* text = new TText(4, 0.7, charge.c_str());
    text->Draw("same");

    TLegend* leg = new TLegend(0.65, 0.7, 0.9, 0.85);
    leg->SetFillColor(10);
    leg->SetLineColor(10);
    leg->SetLineWidth(3);
    leg->SetTextSize(0.045);
    leg->SetTextFont(22);

    leg->AddEntry(Kst, "Kst", "Ple");
    leg->AddEntry(phi, "phi", "Ple");
    leg->AddEntry(NR, "NR", "Ple");
    leg->Draw("same");

    c1->SaveAs(("diff_" + charge + ".png").c_str());

    delete c1;
    delete Kst;
    delete phi;
    delete NR;
    delete mg;
    delete leg;
    return 0;
}

