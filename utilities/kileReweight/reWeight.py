import ROOT
import math,sys

muonPath = "/publicfs/lhcb/user/mjfeng200004/SCA/muon_track_withP/fit/results/"
finalPionPath = "/publicfs/lhcb/user/mjfeng200004/SCA/pion_track/ans/promptD/results/"
weightMuonPath = "/publicfs/lhcb/user/mjfeng200004/SCA/utilities/plotDistribution/results_muon/"
weightPionPath = "/publicfs/lhcb/user/mjfeng200004/SCA/utilities/plotDistribution/results_pion/"

muonP = [2,10,20,30,40,50,100]
pionP = [2,5,10,20,30,40,50,100]
mag = ["Up", "Down"]

def drawTool (mg):
    mg.GetXaxis().SetLabelFont(22)
    mg.GetXaxis().SetLabelSize(0.04)
    mg.GetXaxis().SetTitleSize(0.050)
    mg.GetXaxis().SetTitleFont(22)
    mg.GetYaxis().SetLabelFont(22)
    mg.GetYaxis().SetLabelSize(0.04)
    mg.GetYaxis().SetTitleSize(0.05)
    mg.GetYaxis().SetTitleFont(22)
    mg.GetXaxis().SetTickLength(0.050)
    mg.GetYaxis().SetTickLength(0.033)
    mg.GetXaxis().SetTitleOffset(0.85)
    mg.GetYaxis().SetTitleOffset(1.1)


def getWeight(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    if tag == "muon":
       plist = muonP
       weightPath = weightMuonPath
    else:
       plist = pionP
       weightPath = weightPionPath

    wUp    = []
    wDown  = []
    
    for m in mag:
        f = ROOT.TFile(weightPath+year+"_"+m+"_Ds"+charge+"_"+mode+".root","read")
        hdiff= f.Get("hdiff")
        for i in range(len(plist)-1):
            diff = float(hdiff.GetBinContent(i+1))
            diffE = float(hdiff.GetBinError(i+1))
            exec("w"+m+".append(diff)")

    #print("wMuonUp: ", wUp)
    #print("wMuonDown: ", wDown)
    return wUp, wDown


def getAsy(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    if tag == "muon":
       plist = muonP
    else:
       plist = pionP
    
    aUp    = []
    aDown  = []
    aUpE   = []
    aDownE = []
    if tag == "muon":
        f1     = ROOT.TFile(muonPath+"ASYRESULT_"+year.split("20")[1]+".root", "r")
        hUp    = f1.Get("h"+year.split("20")[1]+"UpLong")
        hDown  = f1.Get("h"+year.split("20")[1]+"DownLong")

        for i in range(len(plist)-1):
            aUp.append(float(hUp.GetBinContent(i+1)))
            aDown.append(float(hDown.GetBinContent(i+1)))
            aUpE.append(float(hUp.GetBinError(i+1)))
            aDownE.append(float(hDown.GetBinError(i+1)))
    else:
        f1     = ROOT.TFile(finalPionPath+"ans_"+year+".root", "r")
        hUp    = f1.Get("hAns1")
        hDown  = f1.Get("hAns0")

        for i in range(len(plist)-1):
            aUp.append(float(hUp.GetBinContent(i+1))*100)
            aDown.append(float(hDown.GetBinContent(i+1))*100)
            aUpE.append(float(hUp.GetBinError(i+1))*100)
            aDownE.append(float(hDown.GetBinError(i+1))*100)
    print(tag, year)
    print("aMuonUp: ", aUp)
    print("aMuonDown: ", aDown)
    print("aMuonUpError: ", aUpE)
    print("aMuonDownError: ", aDownE)

    return aUp, aUpE, aDown, aDownE


def genGauss(mean, sigma, seed = 1):
    random = []
    for i in range(len(mean)):
        ROOT.gRandom.SetSeed(seed*20+i)
        random_numbers1 =  [ROOT.gRandom.Gaus(mean[i], sigma[i]) for x in range(10000)]
        random.append(random_numbers1)
    return random


def genToyMC(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    aaaa      = getAsy(tag, year, charge, mode)
    meanUp    = aaaa[0]
    sigmaUp   = aaaa[1]
    meanDown  = aaaa[2]    
    sigmaDown = aaaa[3]

    randomUp = genGauss(meanUp, sigmaUp, 1)
    randomDown = genGauss(meanDown, sigmaDown, 2)

    return randomUp, randomDown


def reWeight(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    a     = getWeight(tag, year, charge, mode)
    wUp   = a[0]
    wDown = a[1]
    
    aa    = genToyMC(tag, year, charge, mode)
    randomUp = aa[0]
    randomDown = aa[1]

    asyUp = []
    for x in range(len(randomUp[0])):
        iAsyUp = 0
        for y in range(len(randomUp)):
            iAsyUp += randomUp[y][x]*wUp[y]   
        asyUp.append(iAsyUp)

    asyDown = []
    for x in range(len(randomDown[0])):
        iAsyDown = 0
        for y in range(len(randomDown)):
            iAsyDown += randomDown[y][x]*wDown[y]
        asyDown.append(iAsyDown)

    return asyUp, asyDown


def checkReWeight(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    a       = reWeight(tag, year, charge, mode)
    asyUp   = a[0]
    asyDown = a[1]
 
    c1 = ROOT.TCanvas()
    h1 = ROOT.TH1F("h1","h1", 100, -1.5, 1.5)
    h2 = ROOT.TH1F("h2","h2", 100, -1.5, 1.5)

    for i in range(len(asyUp)):
        h1.Fill(asyUp[i])
 
    for i in range(len(asyDown)):
        h2.Fill(asyDown[i])

    h1.SetLineColor(ROOT.kRed)
    h2.SetLineColor(ROOT.kBlue)

    h1.Draw()
    h2.Draw("same")
   
    c1.SaveAs("results/checkReWeight_"+tag+year+"Ds"+charge+mode+".png")


def singleFit(h1, tag = "muon", year = "2016", charge = "m", mode = "Kst", mag="MagUp"):
    gaussian_func = ROOT.TF1("gaussian_func", "gaus", -2, 2)
    gaussian_func.SetParameters(1000, 0, 1)

    fit_result = h1.Fit("gaussian_func", "S")

    fit_status = fit_result.Status()
    fit_parameters = [gaussian_func.GetParameter(i) for i in range(3)]

    print(tag+year+mag+"Ds"+charge+mode, "Fit Status: ", fit_status)
    print(tag+year+mag+"Ds"+charge+mode, "Fit Parameters: ", fit_parameters)

    c1 = ROOT.TCanvas("c"+mag, mag+" Gaussian Fit", 800, 600)
    drawTool(h1)
    h1.Draw()
    gaussian_func.Draw("Same")
    c1.SaveAs("results/fit_"+tag+year+mag+"Ds"+charge+mode+".png")

    return fit_parameters[1], fit_parameters[2], fit_status


def fit(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    aaa     = reWeight(tag, year, charge, mode)
    asyUp   = aaa[0]
    asyDown = aaa[1]

    h1 = ROOT.TH1F("h1","h1", 100, -1.5, 1.5)
    h2 = ROOT.TH1F("h2","h2", 100, -1.5, 1.5)

    for i in range(len(asyUp)):
        h1.Fill(asyUp[i])

    for i in range(len(asyDown)):
        h2.Fill(asyDown[i])

    Up = singleFit(h1, tag, year, charge, mode, "MagUp")
    Down = singleFit(h2, tag, year, charge, mode, "MagDown")

    with open("results/fit_"+tag+year+"Ds"+charge+mode+'.txt', 'w') as file:
        file.write("MagUp	" + str(Up[0]) + " ; " + str(Up[1]) + " ; " + str(Up[2]) + "\n")
        file.write("MagDown       " + str(Down[0]) + " ; " + str(Down[1]) + " ; " + str(Down[2]))
 
    print(Up[0], Up[1], Down[0], Down[1])
    return Up[0], Up[1], Down[0], Down[1]


def run(tag = "muon", charge = "m"):
    Kst = []
    NR = []
    phi = []
    for y in ["2016","2017","2018"]:
        kk = fit(tag, y, charge, "Kst")
        nn = fit(tag, y, charge, "NR")
        pp = fit(tag, y, charge, "phi")
        Kst.append([kk[0], kk[1]])
        Kst.append([kk[2], kk[3]])
        NR.append([nn[0], nn[1]])
        NR.append([nn[2], nn[3]])
        phi.append([pp[0], pp[1]])
        phi.append([pp[2], pp[3]])

    KstAsy = 0
    KstAsyErr2 = 0
    NRAsy = 0
    NRAsyErr2 = 0
    phiAsy = 0
    phiAsyErr2 = 0
    with open("results/final_"+tag+"Ds"+charge+'.txt', 'w') as file0:

        for x in range(len(Kst)):
            KstAsy += float(Kst[x][0])/6
            NRAsy += float(NR[x][0])/6
            phiAsy += float(phi[x][0])/6
            KstAsyErr2 += float(Kst[x][1])**2
            NRAsyErr2 += float(NR[x][1])**2
            phiAsyErr2 += float(phi[x][1])**2
            file0.write("Kst\t"+ str(float(Kst[x][0])) + " ; " +str(float(Kst[x][1]))+"\n")
            file0.write("NR\t"+ str(float(NR[x][0])) + " ; " +str(float(NR[x][1]))+"\n")
            file0.write("phi\t"+ str(float(phi[x][0])) + " ; " +str(float(phi[x][1]))+"\n")

    KstAsyErr = math.sqrt(KstAsyErr2)
    NRAsyErr = math.sqrt(NRAsyErr2)
    phiAsyErr = math.sqrt(phiAsyErr2)

    with open("final_"+tag+"Ds"+charge+'.txt', 'w') as file:
        file.write("Kst\t"+ str(KstAsy) + " ; " +str(KstAsyErr)+"\n")
        file.write("NR\t"+ str(NRAsy) + " ; " +str(NRAsyErr)+"\n")
        file.write("phi\t"+ str(phiAsy) + " ; " +str(phiAsyErr)+"\n")
   
    return Kst, NR, phi
        

def plot(tag = "muon", charge = "m", isRun = False):
    if (isRun):
        aa = run(tag, charge)
        Kst = aa[0]
        NR  = aa[1]
        phi = aa[2]
    else:
        Kst = []
        NR  = []
        phi = []
        for y in ["2016","2017","2018"]:
            with open("results/fit_"+tag+y+"Ds"+charge+"Kst.txt") as kk:
                lines = kk.readlines()
                for l in lines:
                    Kst.append([(l.split(" ; ")[0].split("\t")[1]), float(l.split(" ; ")[1].split(" ; ")[0])])
        
            with open("results/fit_"+tag+y+"Ds"+charge+"NR.txt") as nn:
                lines = nn.readlines()
                for l in lines:
                    NR.append([(l.split(" ; ")[0].split("\t")[1]), float(l.split(" ; ")[1].split(" ; ")[0])])             
    
            with open("results/fit_"+tag+y+"Ds"+charge+"phi.txt") as pp:
                lines = pp.readlines()
                for l in lines:
                    phi.append([(l.split(" ; ")[0].split("\t")[1]), float(l.split(" ; ")[1].split(" ; ")[0])])

    c1  = ROOT.TCanvas()

    grKst = ROOT.TGraphErrors()
    grPhi = ROOT.TGraphErrors()
    grNR = ROOT.TGraphErrors()
    mg = ROOT.TMultiGraph()

    for i in range(len(Kst)):
        grKst.SetPoint(i,i+1,float(Kst[i][0]))
        grKst.SetPointError(i,0,float(Kst[i][1]))
        grPhi.SetPoint(i,i+1,float(phi[i][0]))
        grPhi.SetPointError(i,0,float(phi[i][1]))
        grNR.SetPoint(i,i+1,float(NR[i][0]))
        grNR.SetPointError(i,0,float(NR[i][1]))

    grKst.SetMarkerColor(ROOT.kBlue)
    grKst.SetLineColor(ROOT.kBlue)
    grKst.SetMarkerStyle(20)
    grKst.SetMarkerSize(1)
    grKst.GetYaxis().SetTitle("A_{Track} (#times 10^{-2})")

    grPhi.SetMarkerColor(ROOT.kRed)
    grPhi.SetLineColor(ROOT.kRed)
    grPhi.SetMarkerStyle(20)
    grPhi.SetMarkerSize(1)
    grPhi.GetYaxis().SetTitle("A_{Track} (#times 10^{-2})")

    grNR.SetMarkerColor(ROOT.kBlack)
    grNR.SetLineColor(ROOT.kBlack)
    grNR.SetMarkerStyle(20)
    grNR.SetMarkerSize(1)
    grNR.GetYaxis().SetTitle("A_{Track} (#times 10^{-2})")

    drawTool(grKst)
    drawTool(grPhi) 
    drawTool(grNR)
    mg.Add(grKst,"AP")
    mg.Add(grPhi,"AP")
    mg.Add(grNR,"AP")
    mg.GetYaxis().SetTitle("A_{Track} (#times 10^{-2})")

    mg.Draw("A")

    leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
    leg.SetFillColor(10)
    leg.SetLineColor(10)
    leg.SetLineWidth(3)
    leg.SetTextSize(0.045)
    leg.SetTextFont(22)

    leg.AddEntry(grKst," Kst","Ple")
    leg.AddEntry(grPhi," phi ","ple")
    leg.AddEntry(grNR," NR ","ple")
    leg.Draw("same")

    c1.SaveAs(tag+"Ds"+charge+".png")


def plotFinal(charge = "m"):
    grMuon = ROOT.TGraphErrors()
    grPion = ROOT.TGraphErrors()
    mg = ROOT.TMultiGraph()
    
    muon = []
    with open("final_muonDs"+charge+".txt") as f1:
        lines = f1.readlines()
        for l in lines:
            muon.append([float(l.split("\t")[1].split(" ; ")[0]), float(l.split(" ; ")[1].split("\n")[0])])

    pion = []
    with open("final_pionDs"+charge+".txt") as f1:
        lines = f1.readlines()
        for l in lines:
            pion.append([float(l.split("\t")[1].split(" ; ")[0]), float(l.split(" ; ")[1].split("\n")[0])])

    for i in range(3):
        grMuon.SetPoint(i,i+1,muon[i][0])
        grMuon.SetPointError(i,i+1,muon[i][1])
        grPion.SetPoint(i,i+1,pion[i][0])
        grPion.SetPointError(i,i+1,pion[i][1])

    grMuon.SetMarkerColor(ROOT.kBlue)
    grMuon.SetLineColor(ROOT.kBlue)
    grMuon.SetMarkerStyle(20)
    grMuon.SetMarkerSize(1)
    
    grPion.SetMarkerColor(ROOT.kRed)
    grPion.SetLineColor(ROOT.kRed)
    grPion.SetMarkerStyle(20)
    grPion.SetMarkerSize(1)

    drawTool(grMuon)
    drawTool(grPion)

    mg.Add(grMuon,"AP")
    mg.Add(grPion,"AP")
    mg.GetYaxis().SetTitle("A_{Track} differnence (#times 10^{-2})")

    c1 = ROOT.TCanvas("c1", "c1")

    mg.Draw("A")

    leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
    leg.SetFillColor(10)
    leg.SetLineColor(10)
    leg.SetLineWidth(3)
    leg.SetTextSize(0.045)
    leg.SetTextFont(22)

    leg.AddEntry(grMuon," muon","Ple")
    leg.AddEntry(grPion," pion ","ple")
    leg.Draw("same")

    c1.SaveAs("finalDiff_MuAndPi_Ds"+charge+".png")

#fit()
# first time should be true
isRun = True
plot("muon", "m", isRun)
plot("muon", "p", isRun)
plot("pion", "m", isRun)
plot("pion", "p", isRun)
plotFinal()






