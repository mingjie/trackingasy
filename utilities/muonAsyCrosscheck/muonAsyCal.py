import ROOT
import math,sys

muonPath = "/eos/home-m/mingjie/SCA/trackingasy/muon_track/oldSetup_baseline/ana/results/"
muonPhiPath = "/eos/home-m/mingjie/SCA/trackingasy/muon_track/oldSetup_baseline/fit_phi/results/"
weightMuonPath = "/eos/home-m/mingjie/SCA/trackingasy/utilities/distribution/results_muon/"
weightPhiPath = "/eos/home-m/mingjie/SCA/trackingasy/utilities/distribution/results_phi/"
muonP = [2,10,20,30,40,50,100]
PI = 3.1415
muonPhi = [0, 0.25*PI, 0.5*PI, 0.75*PI, PI, 1.25*PI, 1.5*PI, 1.75*PI, 2*PI]
mag = ["Up", "Down"]

def cal(tag = "momentum", year = "2016", charge = "m", mode = "Kst"):
    if tag == "momentum":
        plist = muonP
        asyPath = muonPath
        weightPath = weightMuonPath
        tname = "hmup"
    elif tag == "phi":
        plist = muonPhi
        asyPath = muonPhiPath
        weightPath = weightPhiPath
        tname = "hmuphi"

    wUp    = []
    wDown  = []
    
    for m in mag:
        f = ROOT.TFile(weightPath+year+"_"+m+"_Ds"+charge+"_"+mode+".root","read")
        hdiff= f.Get(tname)
        for i in range(len(plist)-1):
            diff = float(hdiff.GetBinContent(i+1))
            diffE = float(hdiff.GetBinError(i+1))
            exec("w"+m+".append(diff)")

    print("wMuonUp: ", wUp)
    print("wMuonDown: ", wDown)

    aUp         = []
    aDown       = []
    aUpE_Stat   = []
    aDownE_Stat = []
    if tag == "momentum":
        f1     = ROOT.TFile(asyPath+"ASYRESULT_"+year.split("20")[1]+".root", "r")
        hUp    = f1.Get("hDefaultUp")
        hDown  = f1.Get("hDefaultDown")
        hSysUp   = f1.Get("hSysUp")
        hSysDown = f1.Get("hSysDown")

        for i in range(len(plist)-1):
            aUp.append(float(hUp.GetBinContent(i+1)))
            aDown.append(float(hDown.GetBinContent(i+1)))
            aUpE_Stat.append(float(hUp.GetBinError(i+1)))
            aDownE_Stat.append(float(hDown.GetBinError(i+1)))

    elif tag == "phi":
        f1       = ROOT.TFile(asyPath+"ASYRESULT_"+year.split("20")[1]+".root", "r")
        hUp      = f1.Get("h"+year.split("20")[1]+"UpLong")
        hDown    = f1.Get("h"+year.split("20")[1]+"DownLong")

        for i in range(len(plist)-1):
            aUp.append(float(hUp.GetBinContent(i+1)))
            aDown.append(float(hDown.GetBinContent(i+1)))
            aUpE_Stat.append(float(hUp.GetBinError(i+1)))
            aDownE_Stat.append(float(hDown.GetBinError(i+1)))

    print(aUp, aDown, aUpE_Stat, aDownE_Stat)

cal("phi", "2016", "m", "Kst")
