import ROOT
import math,sys

muonPath = "/eos/home-m/mingjie/SCA/trackingasy/muon_track/oldSetup_baseline/ana/results/"
muonPhiPath = "/eos/home-m/mingjie/SCA/trackingasy/muon_track/oldSetup_baseline/fit_phi/results/"
finalPionPath = "/eos/home-m/mingjie/SCA/trackingasy/pion_track/oldSetup_baseline/ana/promptD/results/"
weightMuonPath = "/eos/home-m/mingjie/SCA/trackingasy/utilities/distribution/results_muon/"
weightPionPath = "/eos/home-m/mingjie/SCA/trackingasy/utilities/distribution/results_pion/"
weightPhiPath = "/eos/home-m/mingjie/SCA/trackingasy/utilities/distribution/results_phi/"

muonP = [2,10,20,30,40,50,100]
pionP = [2,5,10,20,30,40,50,100]
PI = 3.1415
muonPhi = [0, 0.25*PI, 0.5*PI, 0.75*PI, PI, 1.25*PI, 1.5*PI, 1.75*PI, 2*PI]

mag = ["Up", "Down"]
nEvents = 10000

def drawTool (mg):
    mg.GetXaxis().SetLabelFont(22)
    mg.GetXaxis().SetLabelSize(0.04)
    mg.GetXaxis().SetTitleSize(0.050)
    mg.GetXaxis().SetTitleFont(22)
    mg.GetYaxis().SetLabelFont(22)
    mg.GetYaxis().SetLabelSize(0.04)
    mg.GetYaxis().SetTitleSize(0.05)
    mg.GetYaxis().SetTitleFont(22)
    mg.GetXaxis().SetTickLength(0.050)
    mg.GetYaxis().SetTickLength(0.033)
    mg.GetXaxis().SetTitleOffset(0.85)
    mg.GetYaxis().SetTitleOffset(0.9)


def getWeight(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    if tag == "muon":
       plist = muonP
       weightPath = weightMuonPath
       tname = "hmup"
    elif tag == "phi":
       plist = muonPhi
       weightPath = weightPhiPath   
       tname = "hmuphi"
 
    wUp    = []
    wDown  = []
    
    for m in mag:
        f = ROOT.TFile(weightPath+year+"_"+m+"_Ds"+charge+"_"+mode+".root","read")
        hdiff= f.Get(tname)
        for i in range(len(plist)-1):
            diff = float(hdiff.GetBinContent(i+1))
            diffE = float(hdiff.GetBinError(i+1))
            exec("w"+m+".append(diff)")

    print("wMuonUp: ", wUp)
    print("wMuonDown: ", wDown)
    return wUp, wDown


def getAsy(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    if tag == "muon":
       plist = muonP
    else:
       plist = muonPhi
    
    aUp         = []
    aDown       = []
    aUpE_Stat   = []
    aDownE_Stat = []
    aUpE_Sys    = []
    aDownE_Sys = []
    if tag == "muon":
        f1     = ROOT.TFile(muonPath+"ASYRESULT_"+year.split("20")[1]+".root", "r")
        hUp    = f1.Get("hDefaultUp")
        hDown  = f1.Get("hDefaultDown")
        hSysUp   = f1.Get("hSysUp")
        hSysDown = f1.Get("hSysDown")

        for i in range(len(plist)-1):
            aUp.append(float(hUp.GetBinContent(i+1)))
            aDown.append(float(hDown.GetBinContent(i+1)))
            aUpE_Stat.append(float(hUp.GetBinError(i+1)))
            aDownE_Stat.append(float(hDown.GetBinError(i+1)))
            aUpE_Sys.append(float(hSysUp.GetBinContent(i+1)))
            aDownE_Sys.append(float(hSysDown.GetBinContent(i+1)))
    elif tag == "phi":
        f1       = ROOT.TFile(muonPhiPath+"ASYRESULT_"+year.split("20")[1]+".root", "r")
        hUp      = f1.Get("h"+year.split("20")[1]+"UpLong")
        hDown    = f1.Get("h"+year.split("20")[1]+"DownLong")

        for i in range(len(plist)-1):
            aUp.append(float(hUp.GetBinContent(i+1)))
            aDown.append(float(hDown.GetBinContent(i+1)))
            aUpE_Stat.append(float(hUp.GetBinError(i+1)))
            aDownE_Stat.append(float(hDown.GetBinError(i+1)))
            aUpE_Sys.append(0.0)
            aDownE_Sys.append(0.0)

    print(tag, year)
    print("aUp: ", aUp)
    print("aDown: ", aDown)
    print("aUpStatError: ", aUpE_Stat)
    print("aDownStatError: ", aDownE_Stat)
    print("aUpSysError: ", aUpE_Sys)
    print("aDownSysError: ", aDownE_Sys)

    return aUp, aUpE_Stat, aUpE_Sys, aDown, aDownE_Stat, aDownE_Sys


def genGauss(mean, sigma, seed = 1):
    random = []
    for i in range(len(mean)):
        ROOT.gRandom.SetSeed(seed*20+i)
        random_numbers1 =  [ROOT.gRandom.Gaus(mean[i], sigma[i]) for x in range(nEvents)]
        random.append(random_numbers1)
    return random


def genToyMC(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    aaaa          = getAsy(tag, year, charge, mode)
    meanUp        = aaaa[0]
    sigmaUpStat   = aaaa[1]
    sigmaUpSys    = aaaa[2]
    meanDown      = aaaa[3]    
    sigmaDownStat = aaaa[4]
    sigmaDownSys  = aaaa[5]

    sigmaUpT   = [math.sqrt((x**2 + y**2)) for x, y in zip(sigmaUpStat, sigmaUpSys)]
    sigmaDownT = [math.sqrt((x**2 + y**2)) for x, y in zip(sigmaDownStat, sigmaDownSys)]

    randomUp       = genGauss(meanUp, sigmaUpT, 1)
    randomDown     = genGauss(meanDown, sigmaDownT, 2)
    randomUpStat   = genGauss(meanUp, sigmaUpStat, 3)
    randomDownStat = genGauss(meanDown, sigmaDownStat, 4)
    randomUpSys    = genGauss(meanUp, sigmaUpSys, 5)
    randomDownSys  = genGauss(meanDown, sigmaDownSys, 6)

    return randomUp, randomDown, randomUpStat, randomDownStat, randomUpSys, randomDownSys


def reWeight(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    a     = getWeight(tag, year, charge, mode)
    wUp   = a[0]
    wDown = a[1]
    
    aa    = genToyMC(tag, year, charge, mode)
    randomUp       = aa[0]
    randomDown     = aa[1]
    randomUpStat   = aa[2]
    randomDownStat = aa[3]
    randomUpSys    = aa[4]
    randomDownSys  = aa[5]

    asyUp     = []
    asyUpStat = []
    asyUpSys  = []
    for x in range(len(randomUp[0])):
        iAsyUp     = 0
        iAsyUpStat = 0
        iAsyUpSys  = 0
        for y in range(len(randomUp)):
            iAsyUp += randomUp[y][x]*wUp[y]   
            iAsyUpStat += randomUpStat[y][x]*wUp[y]
            iAsyUpSys  += randomUpSys[y][x]*wUp[y]

        asyUp.append(iAsyUp)
        asyUpStat.append(iAsyUpStat)
        asyUpSys.append(iAsyUpSys)

    asyDown     = []
    asyDownStat = []
    asyDownSys  = []
    for x in range(len(randomDown[0])):
        iAsyDown     = 0
        iAsyDownStat = 0
        iAsyDownSys  = 0
        for y in range(len(randomDown)):
            iAsyDown += randomDown[y][x]*wDown[y]
            iAsyDownStat += randomDownStat[y][x]*wDown[y]
            iAsyDownSys  += randomDownSys[y][x]*wDown[y]
        asyDown.append(iAsyDown)
        asyDownStat.append(iAsyDownStat)
        asyDownSys.append(iAsyDownSys)

    return asyUp, asyDown, asyUpStat, asyDownStat, asyUpSys, asyDownSys


def checkReWeight(tag = "muon", year = "2016", charge = "m", mode = "Kst", error = "total"):
    a       = reWeight(tag, year, charge, mode)
    if error == "total":
        asyUp   = a[0]
        asyDown = a[1]
    elif error == "stat":
        asyUp   = a[2]
        asyDown = a[3]
    elif error == "sys":
        asyUp   = a[4]
        asyDown = a[5]
    else:
        print("==================CHECK==================")
        print("error should be one of [total, stat, sys]")
 
    c1 = ROOT.TCanvas()
    h1 = ROOT.TH1F("h1","h1", 100, -1.5, 1.5)
    h2 = ROOT.TH1F("h2","h2", 100, -1.5, 1.5)

    for i in range(len(asyUp)):
        h1.Fill(asyUp[i])
 
    for i in range(len(asyDown)):
        h2.Fill(asyDown[i])

    h1.SetLineColor(ROOT.kRed)
    h2.SetLineColor(ROOT.kBlue)

    h1.Draw()
    h2.Draw("same")
   
    c1.SaveAs("results/checkReWeight_"+error+"_"+tag+year+"Ds"+charge+mode+".png")


def singleFit(h1, tag = "muon", year = "2016", charge = "m", mode = "Kst", mag="MagUp"):
    if h1.GetMaximum() == nEvents: 
        max_bin = h1.GetMaximumBin()
        max_bin_center = h1.GetBinCenter(max_bin)
        c1 = ROOT.TCanvas("c"+mag, mag+" Gaussian Fit", 800, 600)
        drawTool(h1)
        h1.Draw()
        c1.SaveAs("results/fit_"+tag+year+mag+"Ds"+charge+mode+".png")
        return max_bin_center, 0.0, 0
    else:
        gaussian_func = ROOT.TF1("gaussian_func", "gaus", -2, 2)
        gaussian_func.SetParameters(1000, 0, 1)

        fit_result = h1.Fit("gaussian_func", "S")

        fit_status = fit_result.Status()
        fit_parameters = [gaussian_func.GetParameter(i) for i in range(3)]

        print(tag+year+mag+"Ds"+charge+mode, "Fit Status: ", fit_status)
        print(tag+year+mag+"Ds"+charge+mode, "Fit Parameters: ", fit_parameters)

        c1 = ROOT.TCanvas("c"+mag, mag+" Gaussian Fit", 800, 600)
        drawTool(h1)
        h1.Draw()
        gaussian_func.Draw("Same")
        c1.SaveAs("results/fit_"+tag+year+mag+"Ds"+charge+mode+".png")

        return fit_parameters[1], fit_parameters[2], fit_status


def fit(tag = "muon", year = "2016", charge = "m", mode = "Kst"):
    aaa         = reWeight(tag, year, charge, mode)
    asyUp       = aaa[0]
    asyDown     = aaa[1]
    asyUpStat   = aaa[2]
    asyDownStat = aaa[3]
    asyUpSys    = aaa[4]
    asyDownSys  = aaa[5]

    if tag == "phi": xx = 1.5
    else: xx = 1.5
    h1 = ROOT.TH1F("h1","h1", 100, -xx, xx)
    h2 = ROOT.TH1F("h2","h2", 100, -xx, xx)
    h3 = ROOT.TH1F("h3","h3", 100, -xx, xx)
    h4 = ROOT.TH1F("h4","h4", 100, -xx, xx)
    h5 = ROOT.TH1F("h5","h5", 100, -xx, xx)
    h6 = ROOT.TH1F("h6","h6", 100, -xx, xx)

    for i in range(len(asyUp)):
        h1.Fill(asyUp[i])
        h3.Fill(asyUpStat[i])
        h5.Fill(asyUpSys[i])

    for i in range(len(asyDown)):
        h2.Fill(asyDown[i])
        h4.Fill(asyDownStat[i])
        h6.Fill(asyDownSys[i])

    Up       = singleFit(h1, tag, year, charge, mode, "MagUp")
    Down     = singleFit(h2, tag, year, charge, mode, "MagDown")
    UpStat   = singleFit(h3, tag, year, charge, mode, "MagUp_Stat_")
    DownStat = singleFit(h4, tag, year, charge, mode, "MagDown_Stat_")
    UpSys    = singleFit(h5, tag, year, charge, mode, "MagUp_Sys_")
    DownSys  = singleFit(h6, tag, year, charge, mode, "MagDown_Sys_")

    with open("results/fit_"+tag+year+"Ds"+charge+mode+'.txt', 'w') as file:
        file.write("Up\t" + str(Up[0]) + "\t" + str(UpStat[1]) + "\t" + str(UpSys[1])+"\n")
        file.write("Down\t" + str(Down[0]) + "\t" + str(DownStat[1]) + "\t" + str(DownSys[1])+"\n")
 
    print(Up[0], UpStat[1], UpSys[1], Down[0], DownStat[1], DownSys[1])
    print(Up[1], UpStat[1], UpSys[1])
    print(Down[1], DownStat[1], DownSys[1])
    return Up[0], UpStat[1], UpSys[1], Down[0], DownStat[1], DownSys[1]


def run(tag = "muon", charge = "m"):
    Kst = []
    NR = []
    phi = []
    for y in ["2016","2017","2018"]:
        kk = fit(tag, y, charge, "Kst")
        nn = fit(tag, y, charge, "NR")
        pp = fit(tag, y, charge, "phi")
        yy = y.split("20")[1]
        Kst.append([yy+"Up  ", kk[0], kk[1], kk[2]])
        Kst.append([yy+"Down", kk[3], kk[4], kk[5]])
        NR.append([yy+"Up  ", nn[0], nn[1], nn[2]])
        NR.append([yy+"Down", nn[3], nn[4], nn[5]])
        phi.append([yy+"Up  ", pp[0], pp[1], pp[2]])
        phi.append([yy+"Down", pp[3], pp[4], pp[5]])

    KstAsy = 0
    KstAsyStatErr2 = 0
    KstAsySysErr2 = 0
    NRAsy = 0
    NRAsyStatErr2 = 0
    NRAsySysErr2 = 0
    phiAsy = 0
    phiAsyStatErr2 = 0
    phiAsySysErr2 = 0
    with open("results/final_"+tag+"Ds"+charge+'.txt', 'w') as file0:

        for x in range(len(Kst)):
            KstAsy += float(Kst[x][1])/6
            NRAsy += float(NR[x][1])/6
            phiAsy += float(phi[x][1])/6
            KstAsyStatErr2 += float(Kst[x][2])**2
            NRAsyStatErr2 += float(NR[x][2])**2
            phiAsyStatErr2 += float(phi[x][2])**2
            KstAsySysErr2 += float(Kst[x][3])**2
            NRAsySysErr2 += float(NR[x][3])**2
            phiAsySysErr2 += float(phi[x][3])**2
            file0.write("Kst"+Kst[x][0] +"\t"+ "{:.3g}".format(float(Kst[x][1])) + "\t" +"{:.3g}".format(float(Kst[x][2]))+"\t"+"{:.3g}".format(float(Kst[x][3]))+"\n")
            file0.write("NR"+NR[x][0]+"\t"+ "{:.3g}".format(float(NR[x][1])) + "\t" +"{:.3g}".format(float(NR[x][2]))+"\t"+"{:.3g}".format(float(NR[x][3]))+"\n")
            file0.write("phi"+phi[x][0]+"\t"+ "{:.3g}".format(float(phi[x][1])) + "\t" +"{:.3g}".format(float(phi[x][2]))+"\t"+"{:.3g}".format(float(phi[x][3]))+"\n")

    '''
    KstAsyStatErr = math.sqrt(KstAsyStatErr2)
    NRAsyStatErr = math.sqrt(NRAsyStatErr2)
    phiAsyStatErr = math.sqrt(phiAsyStatErr2)
    KstAsySysErr = math.sqrt(KstAsySysErr2)
    NRAsySysErr = math.sqrt(NRAsySysErr2)
    phiAsySysErr = math.sqrt(phiAsySysErr2)

    with open("final_"+tag+"Ds"+charge+'.txt', 'w') as file:
        file.write("Kst\t"+ str(KstAsy) + "\t" +str(KstAsyStatErr)+"\t"+str(KstAsySysErr)+"\n")
        file.write("NR\t"+ str(NRAsy) + "\t" +str(NRAsyStatErr)+"\t"+str(NRAsySysErr)+"\n")
        file.write("phi\t"+ str(phiAsy) + "\t" +str(phiAsyStatErr)+"\t"+str(phiAsySysErr)+"\n")
    '''

    return Kst, NR, phi
        

def plot(tag = "muon", charge = "m", isRun = False):
    if (isRun):
        aa = run(tag, charge)
        Kst = aa[0]
        NR  = aa[1]
        phi = aa[2]
    else:
        Kst = []
        NR  = []
        phi = []
        for y in ["2016","2017","2018"]:
            yy = y.split("20")[1]
            with open("results/fit_"+tag+y+"Ds"+charge+"Kst.txt") as kk:
                lines = kk.readlines()
                for l in lines:
                    Kst.append([yy+l.split("\t")[0], float(l.split("\t")[1]), float(l.split("\t")[2]), float(l.split("\t")[3])])
        
            with open("results/fit_"+tag+y+"Ds"+charge+"NR.txt") as nn:
                lines = nn.readlines()
                for l in lines:
                    NR.append([yy+l.split("\t")[0], float(l.split("\t")[1]), float(l.split("\t")[2]), float(l.split("\t")[3])])
    
            with open("results/fit_"+tag+y+"Ds"+charge+"phi.txt") as pp:
                lines = pp.readlines()
                for l in lines:
                    phi.append([yy+l.split("\t")[0], float(l.split("\t")[1]), float(l.split("\t")[2]), float(l.split("\t")[3])])


    grKst = ROOT.TGraphErrors()
    grPhi = ROOT.TGraphErrors()
    grNR = ROOT.TGraphErrors()
    mg = ROOT.TMultiGraph()


    grKstStatErr = ROOT.TGraph()
    grKstSysErr  = ROOT.TGraph()
    grPhiStatErr = ROOT.TGraph()
    grPhiSysErr  = ROOT.TGraph()
    grNRStatErr = ROOT.TGraph()
    grNRSysErr  = ROOT.TGraph()

    for i in range(len(Kst)):
        grKst.SetPoint(i,i+1,float(Kst[i][1]))
        grKst.SetPointError(i,0,math.sqrt(float(Kst[i][2])**2+float(Kst[i][3])**2))
        grPhi.SetPoint(i,i+1,float(phi[i][1]))
        grPhi.SetPointError(i,0,math.sqrt(float(phi[i][2])**2+float(phi[i][3])**2))
        grNR.SetPoint(i,i+1,float(NR[i][1]))
        grNR.SetPointError(i,0,math.sqrt(float(NR[i][2])**2+float(NR[i][3])**2))
        grKstStatErr.SetPoint(i,i+1,float(Kst[i][2]))
        grKstSysErr.SetPoint(i,i+1,float(Kst[i][3]))
        grPhiStatErr.SetPoint(i,i+1,float(phi[i][2]))
        grPhiSysErr.SetPoint(i,i+1,float(phi[i][3]))
        grNRStatErr.SetPoint(i,i+1,float(NR[i][2]))
        grNRSysErr.SetPoint(i,i+1,float(NR[i][3]))


    grKst.SetMarkerColor(ROOT.kBlue)
    grKst.SetMarkerStyle(20)
    grKst.SetMarkerSize(1)

    grPhi.SetMarkerColor(ROOT.kRed)
    grPhi.SetMarkerStyle(20)
    grPhi.SetMarkerSize(1)

    grNR.SetMarkerColor(ROOT.kBlack)
    grNR.SetMarkerStyle(20)
    grNR.SetMarkerSize(1)

    grKst.SetTitle("Kst Graph;Samples;A_{Track} (#times 10^{-2})")
    grPhi.SetTitle("Phi Graph;Samples;A_{Track} (#times 10^{-2})")
    grNR.SetTitle("NR Graph;Samples;A_{Track} (#times 10^{-2})")

    drawTool(grKst)
    drawTool(grPhi) 
    drawTool(grNR)
    mg.Add(grKst,"AP")
    mg.Add(grPhi,"AP")
    mg.Add(grNR,"AP")
    mg.GetYaxis().SetTitle("A_{Track} (#times 10^{-2})")

    c1 = ROOT.TCanvas("c1", "c1")

    drawTool(mg)

    mg.GetXaxis().SetLabelSize(0)
    mg.GetXaxis().SetLabelColor(0)

    mg.Draw("A")

    custom_labels = [kk[0] for kk in Kst ]
    print(custom_labels)
    nPoints = len(custom_labels)

    latex = ROOT.TLatex()
    latex.SetTextSize(0.04)
    latex.SetTextAlign(22)
    latex.SetTextFont(22)
    
    for i in range(1, nPoints+1):
      latex.DrawLatex(i, -0.53, custom_labels[i-1]);

    leg = ROOT.TLegend(0.75,0.72,0.87,0.87)
    leg.SetFillColor(10)
    leg.SetLineColor(10)
    leg.SetLineWidth(3)
    leg.SetTextSize(0.035)
    leg.SetTextFont(22)

    leg.AddEntry(grKst," Kst","Ple")
    leg.AddEntry(grPhi," phi ","ple")
    leg.AddEntry(grNR," NR ","ple")
    leg.Draw("same")

    c1.SaveAs("results/"+tag+"Ds"+charge+".png")

    file = ROOT.TFile("results/"+tag+"Ds"+charge+".root", "RECREATE")

    grKstStatErr.SetMarkerStyle(20)
    grKstSysErr.SetMarkerStyle(20)
    grPhiStatErr.SetMarkerStyle(20)
    grPhiSysErr.SetMarkerStyle(20)
    grNRStatErr.SetMarkerStyle(20)
    grNRSysErr.SetMarkerStyle(20)
    grKstStatErr.SetTitle("Kst Stat Uncertainty;Samples;StatErr (#times 10^{-2})")
    grPhiStatErr.SetTitle("Phi Stat Uncertainty;Samples;StatErr (#times 10^{-2})")
    grNRStatErr.SetTitle("NR Stat Uncertainty;Samples;StatErr (#times 10^{-2})")
    grKstSysErr.SetTitle("Kst Sys Uncertainty;Samples;SysErr (#times 10^{-2})")
    grPhiSysErr.SetTitle("Phi Sys Uncertainty;Samples;SysErr (#times 10^{-2})")
    grNRSysErr.SetTitle("NR Sys Uncertainty;Samples;SysErr (#times 10^{-2})")

    drawTool(grKstStatErr)
    drawTool(grPhiStatErr)
    drawTool(grNRStatErr)
    drawTool(grKstSysErr)
    drawTool(grPhiSysErr)
    drawTool(grNRSysErr)

    grKstStatErr.Draw("AP")
    grKstSysErr.Draw("AP")
    grPhiStatErr.Draw("AP")
    grPhiSysErr.Draw("AP")
    grNRStatErr.Draw("AP")
    grNRSysErr.Draw("AP")

    grKst.Write("KstTotle")
    grPhi.Write("PhiTotle")
    grNR.Write("NRTotle")
    grKstStatErr.Write("KstStatErr")
    grKstSysErr.Write("KstSysErr")
    grPhiStatErr.Write("PhiStatErr")
    grPhiSysErr.Write("PhiSysErr")
    grNRStatErr.Write("NRStatErr")
    grNRSysErr.Write("NRSysErr")


    file.Close()


def plotFinal(charge = "m"):
    grMuon = ROOT.TGraphErrors()
    grPion = ROOT.TGraphErrors()
    mg = ROOT.TMultiGraph()
    
    muon = []
    with open("final_muonDs"+charge+".txt") as f1:
        lines = f1.readlines()
        for l in lines:
            muon.append([float(l.split("\t")[1].split(" ; ")[0]), float(l.split(" ; ")[1].split("\n")[0])])

    pion = []
    with open("final_pionDs"+charge+".txt") as f1:
        lines = f1.readlines()
        for l in lines:
            pion.append([float(l.split("\t")[1].split(" ; ")[0]), float(l.split(" ; ")[1].split("\n")[0])])

    for i in range(3):
        grMuon.SetPoint(i,i+1,muon[i][0])
        grMuon.SetPointError(i,i+1,muon[i][1])
        grPion.SetPoint(i,i+1,pion[i][0])
        grPion.SetPointError(i,i+1,pion[i][1])

    grMuon.SetMarkerColor(ROOT.kBlue)
    grMuon.SetLineColor(ROOT.kBlue)
    grMuon.SetMarkerStyle(20)
    grMuon.SetMarkerSize(1)
    
    grPion.SetMarkerColor(ROOT.kRed)
    grPion.SetLineColor(ROOT.kRed)
    grPion.SetMarkerStyle(20)
    grPion.SetMarkerSize(1)

    drawTool(grMuon)
    drawTool(grPion)

    mg.Add(grMuon,"AP")
    mg.Add(grPion,"AP")
    mg.GetYaxis().SetTitle("A_{Track} differnence (#times 10^{-2})")

    c1 = ROOT.TCanvas("c1", "c1")

    mg.Draw("A")

    leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
    leg.SetFillColor(10)
    leg.SetLineColor(10)
    leg.SetLineWidth(3)
    leg.SetTextSize(0.045)
    leg.SetTextFont(22)

    leg.AddEntry(grMuon," muon","Ple")
    leg.AddEntry(grPion," pion ","ple")
    leg.Draw("same")

    c1.SaveAs("finalDiff_MuAndPi_Ds"+charge+".png")

#fit()
# first time should be true
isRun = True
#plot("phi", "m", isRun)
plot("phi", "p", isRun)
#plot("pion", "m", isRun)
#plot("pion", "p", isRun)
#plotFinal()






