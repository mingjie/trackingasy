import pandas as pd
from array import array
import numpy as np
import ROOT
import os, re, math
import sys
sys.path.append("/publicfs/lhcb/user/mjfeng200004/utilities")
import lhcbStyle

path  = "/publicfs/lhcb/user/mjfeng200004/SCA/pion_track/fit/promptD/"
pionP = ["2-5","5-10","10-20","20-30","30-40","40-50","50-100"]

# the name of file name as "FullMom_i_j/" and the i means detect P and j means infer P
def calWij(year = "2016", mag = "Down", charge="M"):
    infer0 = []
    infer1 = []
    infer2 = []  
    infer3 = []
    infer4 = []
    infer5 = []
    infer6 = []  
    
    df = pd.DataFrame(columns=["inferP_i/detP_j (GeV)", "2-5", "5-10", "10-20", "20-30", "30-40", "40-50", "50-100"])
    for i in range(7):
        for j in range(7):
            fname = path+year+"/FullMom_"+str(i)+"_"+str(j)+"/results/para_full_"+mag+".txt"
            with open(fname) as f:
                 for line in f.readlines():
                     if charge == "M":
                         N = float(line.split("\t")[2])
                     elif charge == "P":
                         N = float(line.split("\t")[3])
                 infer = "infer"+str(j)
                 exec(infer+".append(N)")
                 
    
    si0 = sum(infer0)
    si1 = sum(infer1)
    si2 = sum(infer2)
    si3 = sum(infer3)
    si4 = sum(infer4)
    si5 = sum(infer5)
    si6 = sum(infer6)

    for a in range(7):
        exec("infer"+str(a)+".clear()")

    for i in range(7):
        for j in range(7):
            fname = path+year+"/FullMom_"+str(i)+"_"+str(j)+"/results/para_full_"+mag+".txt"
            with open(fname) as f:
                 for line in f.readlines():
                     if charge == "M":
                         N = float(line.split("\t")[2])
                     elif charge == "P":
                         N = float(line.split("\t")[3])
                 infer = "infer"+str(j)
                 exec(infer+".append(str('{:.3f}'.format(N/si"+str(j)+")))")
                 #exec(infer+".append(N)")

    print(infer0)
    df["inferP_i/detP_j (GeV)"] = pionP
    df["2-5"] = infer0
    df["5-10"] = infer1
    df["10-20"] = infer2
    df["20-30"] = infer3
    df["30-40"] = infer4
    df["40-50"] = infer5
    df["50-100"] = infer6

    with open("results/wij"+year+mag+charge+".tex", "w") as f:
        f.write(df.to_latex(column_format='c'* len(df.columns), caption= "Wij table",  label="tab:Wij table", index=False, escape=False) \
                .replace('\\toprule', '\\hline')        \
                .replace('\\midrule', '\\hline')        \
                .replace('\\bottomrule', '\\hline'))
    return df


def drawWij(year,mag,charge):
    ROOT.gStyle.SetOptStat(0)
    df=calWij(year,mag,charge)
    print(df)
    
    bins = [2, 5, 10, 20, 30, 40, 50, 100] 

    c1 = ROOT.TCanvas("c1", "2D Histogram", 1200, 800)
    #h1 = ROOT.TH2F("histogram", "2D Histogram",7,0,100,7,0,100)
    h1 = ROOT.TH2F("histogram", "2D Histogram", len(bins)-1, array("d",bins), len(bins)-1, array("d",bins))
    h1.GetXaxis().SetTitle("Detected P")
    h1.GetYaxis().SetTitle("Inferred P")
    
    bb = [2,5,10,20,30,40,50,100]

    for i in range(7):
        for j in range(7):
            binx = h1.GetXaxis().FindBin(bb[j]+0.5)
            biny = h1.GetYaxis().FindBin(bb[i]+0.5)
            h1.SetBinContent(binx, biny, float(df.iloc[i][pionP[binx-1]])) 
    h1.GetZaxis().SetRangeUser(0,0.8)
    h1.Draw("colz")

    latex = ROOT.TLatex()
    latex.SetTextColor(ROOT.kWhite)  
    latex.SetTextAlign(22)
    latex.SetTextSize(0.02)

    for binx in range(1, h1.GetNbinsX()+1):
        for biny in range(1, h1.GetNbinsY()+1):
            content = h1.GetBinContent(binx, biny)
            latex.DrawLatex(h1.GetXaxis().GetBinCenter(binx), h1.GetYaxis().GetBinCenter(biny), str("{:.3f}".format(float(content))))

    c1.Update()
    #c1.WaitPrimitive()
    c1.SaveAs("results/wij"+year+mag+charge+".pdf")
    

df1=drawWij("2016","Up","P")
df2=drawWij("2016","Up","M")
df3=drawWij("2016","Down","P")
df4=drawWij("2016","Down","M")
                

