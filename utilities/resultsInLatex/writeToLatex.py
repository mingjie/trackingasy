import pandas as pd
import ROOT
import os, re, math

muonPath = "/eos/home-m/mingjie/SCA/trackingasy/muon_track/oldSetup_baseline/ana/results/"
pionPath = "/eos/home-m/mingjie/SCA/trackingasy/pion_track/oldSetup_baseline/ana/promptD/results/"
kile     = "/eos/home-m/mingjie/SCA/trackingasy/utilities/kileReweight/results/"
blue     = "/eos/home-m/mingjie/SCA/trackingasy/utilities/blue/results/"

def muonAsyTolatex( caption, label, method = "Long"):
    P = ["2-10","10-20","20-30","30-40","40-50","50-100"]
    Up16 = []
    Down16 = []
    Up17 = []
    Down17 = []
    Up18 = []
    Down18 = []

    df = pd.DataFrame(columns=["$P(GeV)$", "2016, MagUp", "2016, MagDown", "2017, MagUp", "2017, MagDown", "2018, MagUp", "2018, MagDown"])

    for y in ["16", "17", "18"]:
        file = ROOT.TFile.Open(muonPath+"ASYRESULT_"+y+".root")
        for mag in ["Up", "Down"]:
            h1 = file.Get("hDefault"+mag)
            h2 = file.Get("hSys"+mag)
            ltag = mag + y
            for ip in range(6): 
                asy = str("{:.3f}".format(round(float(h1.GetBinContent(ip+1)),3))) + " $\\pm$ " + str("{:.3f}".format(round(float(h1.GetBinError(ip+1)),3)) + " $\\pm$ " + str("{:.3f}".format(round(float(h2.GetBinContent(ip+1)),3))))
                exec(ltag+".append(asy)")
    
    df["$P(GeV)$"] = P
    df["2016, MagUp"] = Up16
    df["2016, MagDown"] = Down16
    df["2017, MagUp"] = Up17
    df["2017, MagDown"] = Down17
    df["2018, MagUp"] = Up18
    df["2018, MagDown"] = Down18
    
    with open("results/muonAsy.tex", "w") as f:
        f.write(df.to_latex(column_format='c'* len(df.columns), caption= caption,  label=label, index=False, escape=False) \
                .replace('\\toprule', '\\hline')        \
                .replace('\\midrule', '\\hline')        \
                .replace('\\bottomrule', '\\hline'))


def pionAsyTolatex( caption, label ):
    P = ["2-5","5-10","10-20","20-30","30-40","40-50","50-100"]
    Up16 = []
    Down16 = []
    Up17 = []
    Down17 = []
    Up18 = []
    Down18 = []

    df = pd.DataFrame(columns=["$P(GeV)$", "2016, MagUp", "2016, MagDown", "2017, MagUp", "2017, MagDown", "2018, MagUp", "2018, MagDown"])

    for y in ["16", "17", "18"]:
        file = ROOT.TFile.Open(pionPath+"ans_20"+y+".root")
        for mag in ["Up", "Down"]:
            h1 = file.Get("hDefault"+mag)
            h2 = file.Get("hSys"+mag)
            ltag = mag + y
            for ip in range(7):
                asy = str("{:.3f}".format(round(float(h1.GetBinContent(ip+1))*100,3))) + " $\\pm$ " + str("{:.3f}".format(round(float(h1.GetBinError(ip+1))*100,3)) + " $\\pm$ " + str("{:.3f}".format(round(float(h2.GetBinContent(ip+1))*100,3))))
                exec(ltag+".append(asy)")

    df["$P(GeV)$"] = P
    df["2016, MagUp"] = Up16
    df["2016, MagDown"] = Down16
    df["2017, MagUp"] = Up17
    df["2017, MagDown"] = Down17
    df["2018, MagUp"] = Up18
    df["2018, MagDown"] = Down18

    with open("results/pionAsy.tex", "w") as f:
        f.write(df.to_latex(column_format='c'* len(df.columns), caption= caption,  label=label, index=False, escape=False) \
                .replace('\\toprule', '\\hline')        \
                .replace('\\midrule', '\\hline')        \
                .replace('\\bottomrule', '\\hline'))


def afterKile(caption, label, tag="muon", charge="Dsp"):
    proc = ['2016, MagUp', '2016, MagDown', '2017, MagUp', '2017, MagDown', '2018, MagUp', '2018, MagDown']

    Kst = []
    NR = []
    phi = []
   
    df = pd.DataFrame(columns=["sample", "Kst ($\\times 10^{-2}$)", "NR ($\\times 10^{-2}$)", "phi ($\\times 10^{-2}$)"])

    with open(kile+"final_"+tag+charge+'.txt', 'r') as file0:
        for line in file0:
            line = line.strip()

            if "Kst" in line.split("\t")[0]:
                Kst.append(str("{:.3f}".format(round(float(line.split("\t")[1]),3))) + " $\pm$ " + str("{:.3f}".format(round(float(line.split("\t")[2]),3))) + " $\pm$ " + str("{:.3f}".format(round(float(line.split("\t")[3]),3))))
            elif "NR" in line.split("\t")[0]:
                NR.append(str("{:.3f}".format(round(float(line.split("\t")[1]),3))) + " $\pm$ " + str("{:.3f}".format(round(float(line.split("\t")[2]),3))) + " $\pm$ " + str("{:.3f}".format(round(float(line.split("\t")[3]),3))))
            elif "phi" in line.split("\t")[0]:
                phi.append(str("{:.3f}".format(round(float(line.split("\t")[1]),3))) + " $\pm$ " + str("{:.3f}".format(round(float(line.split("\t")[2]),3))) + " $\pm$ " + str("{:.3f}".format(round(float(line.split("\t")[3]),3))))

    df["sample"] = proc
    df["Kst ($\\times 10^{-2}$)"] = Kst
    df["NR ($\\times 10^{-2}$)"] = NR
    df["phi ($\\times 10^{-2}$)"] = phi

    with open("results/using_"+tag+charge+"Asy.tex", "w") as f:
        f.write(df.to_latex(column_format='c'* len(df.columns), caption= caption,  label=label, index=False, escape=False) \
                .replace('\\toprule', '\\hline')        \
                .replace('\\midrule', '\\hline')        \
                .replace('\\bottomrule', '\\hline'))
    

def afterBlue( caption, label, tag = "muon", charge = "Dsm"):
    proc = ["2016, MagUp", "2016, MagDown", "2017, MagUp", "2017, MagDown", "2018, MagUp", "2018, MagDown"]
    Apair = []

    df = pd.DataFrame(columns=["sample", "$A_{\mu^{\pm}\pi^{\mp}} (\\times 10^{-2})$"])

    with open(blue+tag+charge+"_ave_usingBLUE.txt", "r") as file0:
         for line in file0:
            line = line.strip()

            Apair.append(str("{:.3f}".format(round(float(line.split(" ; ")[1]),3)) + " $\pm$ " + str("{:.3f}".format(round(float(line.split(" ; ")[2]),3))) + " $\pm$ " + str("{:.3f}".format(round(float(line.split(" ; ")[3]),3)))))

    df["sample"] = proc
    df["$A_{\mu^{\pm}\pi^{\mp}} (\\times 10^{-2})$"] = Apair

    with open("results/"+tag+charge+"_ave_usingBLUE.tex", "w") as f:
        f.write(df.to_latex(column_format='{c' + 'c' * (len(df.columns) - 1) + 'c}', caption= caption,  label=label, index=False, escape=False) \
                .replace('\\toprule', '\\hline')        \
                .replace('\\midrule', '\\hline')        \
                .replace('\\bottomrule', '\\hline'))


def aveCharge( caption, label, tag = "muon"):
    proc = ["2016, MagUp", "2016, MagDown", "2017, MagUp", "2017, MagDown", "2018, MagUp", "2018, MagDown"]
    Apair = []

    df = pd.DataFrame(columns=["sample", "$A_{\mu^{\pm}\pi^{\mp}} (\\times 10^{-2})$"])

    with open(blue+"final_"+tag+"_ave_Charge.txt", "r") as file0:
         for line in file0:
            line = line.strip()

            Apair.append(str("{:.3f}".format(round(float(line.split(" ; ")[1]),3)) + " $\pm$ " + str("{:.3f}".format(round(float(line.split(" ; ")[2]),3))) + " $\pm$ " + str("{:.3f}".format(round(float(line.split(" ; ")[3]),3)))))

    df["sample"] = proc
    df["$A_{\mu^{\pm}\pi^{\mp}} (\\times 10^{-2})$"] = Apair

    with open("results/final_"+tag+"_ave_Charge.tex", "w") as f:
        f.write(df.to_latex(column_format='{c' + 'c' * (len(df.columns) - 1) + 'c}', caption= caption,  label=label, index=False, escape=False) \
                .replace('\\toprule', '\\hline')        \
                .replace('\\midrule', '\\hline')        \
                .replace('\\bottomrule', '\\hline'))
    

#muonAsyTolatex("Track Asymmetry $A_{\mu}$ ($\\times 10^{-2}$)", "tab:mu-trackAsy")
pionAsyTolatex("Track Asymmetry $A_{\pi}$ ($\\times 10^{-2}$)", "tab:pi-trackAsy")
#afterKile("using $A_\mu$ for $A_{\mu^{\pm}\pi^{\mp}} (D_s^+)$", "tab:mu-trackAsy", "muon", "Dsp")
#afterKile("using $A_\mu$ for $A_{\mu^{\pm}\pi^{\mp}} (D_s^-)$", "tab:mu-trackAsy", "muon", "Dsm")
afterKile("using $A_\pi$ for $A_{\mu^{\pm}\pi^{\mp}} (D_s^+)$", "tab:pi-trackAsy", "pion", "Dsp")
afterKile("using $A_\pi$ for $A_{\mu^{\pm}\pi^{\mp}} (D_s^-)$", "tab:pi-trackAsy", "pion", "Dsm")
#afterBlue("using $A_\mu$ for $A_{\mu^{\pm}\pi^{\mp}} (D_s^+)$", "tab:mu-trackAsy", "muon", "Dsp")
#afterBlue("using $A_\mu$ for $A_{\mu^{\pm}\pi^{\mp}} (D_s^-)$", "tab:mu-trackAsy", "muon", "Dsm")
afterBlue("using $A_\pi$ for $A_{\mu^{\pm}\pi^{\mp}} (D_s^+)$", "tab:pi-trackAsy", "pion", "Dsp")
afterBlue("using $A_\pi$ for $A_{\mu^{\pm}\pi^{\mp}} (D_s^-)$", "tab:pi-trackAsy", "pion", "Dsm")
#aveCharge("using $A_\mu$ for $A_{\mu^{\pm}\pi^{\mp}}$", "tab:mu-trackAsy", "muon")
aveCharge("using $A_\pi$ for $A_{\mu^{\pm}\pi^{\mp}}$", "tab:mu-trackAsy", "pion")
