import math

tag = "pion"
inFile1 = "results/final_"+tag+"_ave_Charge.txt"

meas16 = []
meas17 = []
meas18 = []

with open(inFile1, 'r') as file:
    for line in file:
        line = line.strip()
        if "16Up" in line or "16Down" in line:
            meas16.append([line.split(" ; ")[0], float(line.split(" ; ")[1]), float(line.split(" ; ")[2]), float(line.split(" ; ")[3])])
        elif "17Up" in line or "17Down" in line:
            meas17.append([line.split(" ; ")[0], float(line.split(" ; ")[1]), float(line.split(" ; ")[2]), float(line.split(" ; ")[3])])
        elif "18Up" in line or "18Down" in line:
            meas18.append([line.split(" ; ")[0], float(line.split(" ; ")[1]), float(line.split(" ; ")[2]), float(line.split(" ; ")[3])])

mean16 = mean17 = mean18 = 0
measStatE2_16 = measStatE2_17 = measStatE2_18 = 0
measSystE2_16 = measSystE2_17 = measSystE2_18 = 0

for i in range(len(meas16)):
    mean16 += meas16[i][1]
    measStatE2_16 += meas16[i][2]**2    
    measSystE2_16 += meas16[i][3]**2

    mean17 += meas17[i][1]
    measStatE2_17 += meas17[i][2]**2
    measSystE2_17 += meas17[i][3]**2

    mean18 += meas18[i][1]
    measStatE2_18 += meas18[i][2]**2
    measSystE2_18 += meas18[i][3]**2

ave16 = mean16/2
aveStatE16 = math.sqrt(measStatE2_16)/2
aveSystE16 = math.sqrt(measSystE2_16)/2

ave17 = mean17/2
aveStatE17 = math.sqrt(measStatE2_17)/2
aveSystE17 = math.sqrt(measSystE2_17)/2

ave18 = mean18/2
aveStatE18 = math.sqrt(measStatE2_18)/2
aveSystE18 = math.sqrt(measSystE2_18)/2

ave_all_arithmatic = (ave16+ave17+ave18)/3
aveStatE_all_arithmatic = math.sqrt(aveStatE16**2+aveStatE17**2+aveStatE18**2)/3
aveSystE_all_arithmatic = math.sqrt(aveSystE16**2+aveSystE17**2+aveSystE18**2)/3

weighted_mean = (ave16/(aveStatE16**2)+ave17/(aveStatE17**2)+ave18/(aveStatE18**2))/(1/(aveStatE16**2)+1/(aveStatE17**2)+1/(aveStatE18**2))
weight_stat_sum = math.sqrt(1/(1/(aveStatE16**2)+1/(aveStatE17**2)+1/(aveStatE18**2)))
weight_syst_sum = math.sqrt(1/(1/(aveSystE16**2)+1/(aveSystE17**2)+1/(aveSystE18**2)))

with open("results/"+tag+'_Final.txt', 'a') as file0:
    file0.write(f"Arithmatic 16: ; {round(ave16, 3):.3f} ; {round(aveStatE16,3):.3f} ; {round(aveSystE16,3):.3f} \n")
    file0.write(f"Arithmatic 17: ; {round(ave17, 3):.3f} ; {round(aveStatE17,3):.3f} ; {round(aveSystE17,3):.3f} \n")
    file0.write(f"Arithmatic 18: ; {round(ave18, 3):.3f} ; {round(aveStatE18,3):.3f} ; {round(aveSystE18,3):.3f} \n")
    file0.write(f"Arithmatic all: ; {round(ave_all_arithmatic, 3):.3f} ; {round(aveStatE_all_arithmatic,3):.3f} ; {round(aveSystE_all_arithmatic,3):.3f} \n")
    file0.write(f"Inverse Variance all: ; {round(weighted_mean, 3):.3f} ; {round(weight_stat_sum,3):.3f} ; {round(weight_syst_sum,3):.3f} \n")




