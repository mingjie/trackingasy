
"""Defines class BLUE.

Based on https://github.com/jolange/BLUE-py.

See
https://www.sciencedirect.com/science/article/pii/0168900288900186
and
https://gitlab.cern.ch/lhcb-slb/asls3fb-copy/-/blob/master/scripts/Average/Average.py
"""
import numpy as np
from uncertainties import ufloat
from uncertainties import covariance_matrix
from scipy import optimize

class BLUE:
    """Class for determining the best linear unbiased estimate of the mean."""

    def __init__(self, measurements, stat_error, syst_error):
        """Calculate the BLUE of a set of (correlated) measurements.

        The result can be accessed via "value".

        arguments:
        measurements -- list of uncertainties with correlations preserved
        """
        import numpy as np

        from uncertainties import covariance_matrix
        from scipy import optimize

        def error(args):
            """Calculate sigma for a given set of weights.

            arguments:
            args -- list of weights
                len(args) should be self.Ndf
                the final weight is assigned so that they sum to 1
            """
            # check their sum
            if sum(args) > 1:
                return 9999  # far from minimum
            # check they are positive
            if any(x < 0 for x in args):
                return 9999  # far from minimum
            weights = np.matrix([[x] for x in args] + [[1 - sum(args)]])
            return float(np.sqrt(weights.T * self.E * weights))

        self.Ndf = len(measurements) - 1
        self.E = np.matrix(covariance_matrix(measurements))

        _start_weights = [1.0 / self.Ndf] * self.Ndf
        _opt_weights = optimize.fmin(error, _start_weights)
        opt_weights = np.append(_opt_weights, 1 - sum(_opt_weights))
        assert sum(opt_weights) == 1, opt_weights
        print(opt_weights)

        # # BLUE notation
        # self.y = np.matrix([[x.n] for x in measurements])
        # self.alpha = np.matrix(opt_weights).T
        # self.mean = (self.alpha.T * self.y)[0, 0]
        # self.sigma = (np.sqrt(self.alpha.T * self.E * self.alpha))[0, 0]

        # ufloat notation (should be the same)
        self.value = sum(measurements * opt_weights)  # numpy array multiplication
        # # assert agreement to acceptable precision (should be machine error)
        # assert abs(self.value.n / self.mean - 1) < 10e-15, (self.value, self.mean)
        # assert abs(self.value.s / self.sigma - 1) < 10e-15, (self.value, self.sigma)

        self.stat = np.sqrt(1 / np.sum(1 / np.array(stat_error)**2))
        self.syst = np.sqrt(1 / np.sum(1 / np.array(syst_error)**2))
        print(self.value, self.stat, self.syst)


tag = "pion"
charge = "Dsp"

inFile = "/eos/home-m/mingjie/SCA/trackingasy/utilities/kileReweight/results/final_"+tag+charge+".txt"
print(inFile)

Kst = []
phi = []
NR  = []
with open(inFile, 'r') as file:
    for line in file:
        line = line.strip()

        if "Kst" in line.split("\t")[0]:
            Kst.append([line.split("\t")[0].split("Kst")[1], float(line.split("\t")[1]), float(line.split("\t")[2]), float(line.split("\t")[3])])
        elif "NR" in line.split("\t")[0]:
            NR.append([line.split("\t")[0].split("NR")[1], float(line.split("\t")[1]), float(line.split("\t")[2]), float(line.split("\t")[3])])
        elif "phi" in line.split("\t")[0]:
            phi.append([line.split("\t")[0].split("phi")[1], float(line.split("\t")[1]), float(line.split("\t")[2]), float(line.split("\t")[3])])

print(Kst)
print(phi)
print(NR)

for i in range(6):
    meas = [
        ufloat(Kst[i][1], (Kst[i][2]**2 + Kst[i][3]**2)**0.5),  
        ufloat(phi[i][1], (phi[i][2]**2 + phi[i][3]**2)**0.5),  
        ufloat(NR[i][1], (NR[i][2]**2 + NR[i][3]**2)**0.5)      
    ]

    stat = [Kst[i][2], phi[i][2], NR[i][2]]
    sys = [Kst[i][3], phi[i][3], NR[i][3]]
       
    blue = BLUE(meas, stat, sys)

    print(i, blue.value, blue.stat, blue.syst)
    with open("results/"+tag+charge+'_ave_usingBLUE.txt', 'a') as file0:
        file0.write(str(Kst[i][0])+ f" ; {round(blue.value.n, 3):.3f} ; {round(blue.stat,3):.3f} ; {round(blue.syst,3):.3f} \n") 

