import math

tag = "pion"
inFile1 = "results/"+tag+"Dsm_ave_usingBLUE.txt"
inFile2 = "results/"+tag+"Dsp_ave_usingBLUE.txt"

Dsm = []
Dsp = []

with open(inFile1, 'r') as file:
    for line in file:
        line = line.strip()
        Dsm.append([line.split(" ; ")[0], float(line.split(" ; ")[1]), float(line.split(" ; ")[2]), float(line.split(" ; ")[3])])

with open(inFile2, 'r') as file:
    for line in file:
        line = line.strip()
        Dsp.append([line.split(" ; ")[0], float(line.split(" ; ")[1]), float(line.split(" ; ")[2]), float(line.split(" ; ")[3])])

print(Dsm)
print(Dsp)
for i in range(6):
    meas = (Dsm[i][1]+Dsp[i][1])/2.0
    measStatE = math.sqrt(Dsm[i][2]*Dsm[i][2] + Dsp[i][2]*Dsp[i][2])/2
    measSystE = math.sqrt(Dsm[i][3]*Dsm[i][3] + Dsp[i][3]*Dsp[i][3])/2
    
    with open("results/final_"+tag+'_ave_Charge.txt', 'a') as file0:
        file0.write(str(Dsm[i][0])+" ; "+str(meas)+" ; "+str(measStatE) + " ; " +str(measSystE)+"\n")




