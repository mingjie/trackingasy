'''Produce TrackCalib ntuples on 2016 real data, stripping outout.'''

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from TrackCalibProduction.Tuple import set_Tuples

#######################
### INPUT           ###
#######################
datatype = "2016"        # FOR ALL CATEGORIES, SEE cond_db_dict IN Tuple_Turbo_test.py
sample = "Data_stripping" 
methods = ["VeloMuon","MuonTT","Downstream"]

# CALL FUNCTION THAT SETS DAVINCI OPTIONS WHICH DEPEND ON DATATYPE AND SAMPLE
set_Tuples(datatype, sample, methods, MDST=False, Test=False, isTurbo=False)

from Configurables import DaVinci
# Necessary DaVinci parameters #################
DaVinci().SkipEvents = 0
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "dummy.root"
DaVinci().PrintFreq = 1000

#from Gaudi.Configuration import *
#from GaudiConf import IOHelper

#IOHelper('ROOT').inputFiles(['/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/DIMUON.DST/00103400/0000/00103400_00009938_1.dimuon.dst'], clear=True)

