#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <vector>
#pragma once

std::vector<TString> getname(int fileSec=0, TString year="18", TString polarity ="Down" ){

   std::vector<TString> vecstring;

   std::string rootList = (std::string) ("ls LHCb_Collision"+year+"_Beam6500GeVVeloClosedMag"+polarity+"_Real*.xml");
   std::cout<<rootList<<std::endl;
   FILE *stream;
   char buf[1024];
   //memset( buf, '/0', sizeof(buf) );

   if (system((rootList).c_str())!=0){
	std::cout<<"no RootList file found"<<std::endl;
	return vecstring;
   }
   stream = popen(rootList.data(), "r");
   fread( buf, sizeof(char), sizeof(buf), stream);
   pclose(stream);

   std::string tmp = buf;
   tmp = tmp.substr(0,tmp.find_last_of("xml")+1);
   std::cout<<tmp<<std::endl;

   std::string temp;
   ifstream READfile;
   READfile.open(tmp.data(), ios::in);
   while (!READfile.eof()){
	getline(READfile, temp);
	if (temp.find("root")==std::string::npos){
	   break;
	}
	vecstring.push_back(temp);
   }

   if (fileSec<10){
	std::vector<TString>::const_iterator first = vecstring.begin()+fileSec*(vecstring.size()/10+1);
	std::vector<TString>::const_iterator last  = vecstring.begin()+(fileSec+1)*(vecstring.size()/10+1);

	if (fileSec*(vecstring.size()/10+1)>=vecstring.size()) first = vecstring.end();
	if ((fileSec+1)*(vecstring.size()/10+1)>=vecstring.size()) last = vecstring.end();

	std::vector<TString> vec1(first, last);

	return vec1;
   } else {
	return vecstring;
   }
}
