#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "TString.h"

using namespace RooFit ;

int mass(TString year="18", TString polarity="Down", TString method="T", bool isBin=false, unsigned int etabin=0, unsigned int pbin=0, unsigned int phibin = 0, double parafitP[]={}, double parafitM[]={}){
   gROOT->ProcessLine(".x ~/lhcbStyle.C");

   std::cout << "aaaaaaa " << etabin << " ; " << pbin << " ; " << phibin << std::endl; 
   double PI = 3.14159;
   double eta[] = {1.6,2.5,3.5,5.5};
   double p[]  = {2,10,20,30,40,50,100};
   double phi[] = {0, 0.25*PI, 0.5*PI, 0.75*PI, PI, 1.25*PI, 1.5*PI, 1.75*PI, 2*PI};

   double pmax = 100;
   double pmin = 0;
   double etamax = 10;
   double etamin = 0;

   double phimax  = (isBin) ?  phi[phibin+1]  : 2*PI;
   double phimin  = (isBin) ?  phi[phibin]    :  0;

   using chargemap = std::pair<int, TString>;

   std::array<chargemap,2> charge = {{
	{0,"Plus"}, {1,"Minus"}
   }};

   std::array<TChain,2> chain;


   double min = 2650., max = 3550.;
   if (method.Contains("Velo")) {min = 2950.; max = 3250.;}
   unsigned int nbin = 90;

   RooRealVar *x = new RooRealVar("x","m(J/#psi(1S)) [MeV]", min, max);
   x->setBins(nbin);

   RooDataHist *dataTOT[2], *dataMATCH[2];
   RooChebychev *bkgpdfTOT[2], *bkgpdfMATCH[2];
   RooAbsPdf *modelMATCH[2], *modelTOT[2];
   RooAbsReal *nllDataMATCH[2], *nllDataTOT[2];
   RooRealVar *cTOT[2], *cMATCH[2];
   RooAbsReal *nsigTOT[2], *nbkgTOT[2], *nsigMATCH[2], *nbkgMATCH[2], *eff[2];
   RooChi2Var *chi2MATCH[2], *chi2TOT[2];

   RooRealVar* mean1P = new RooRealVar("mean1P","mean1P",parafitP[0],parafitP[1],parafitP[2]);
   RooRealVar* mean2P = new RooRealVar("mean2P","mean2P",parafitP[3],parafitP[4],parafitP[5]);
   RooRealVar* sigma1P = new RooRealVar("sigma1P","sigma1P",parafitP[6],parafitP[7],parafitP[8]);
   RooRealVar* sigma2P = new RooRealVar("sigma2P","sigma2P",parafitP[9],parafitP[10],parafitP[11]);
   RooRealVar* frac0P = new RooRealVar("frac0P","frac0P",parafitP[12],parafitP[13],parafitP[14]);

   RooRealVar* mean1M = new RooRealVar("mean1M","mean1M",parafitM[0],parafitM[1],parafitM[2]);
   RooRealVar* mean2M = new RooRealVar("mean2M","mean2M",parafitM[3],parafitM[4],parafitM[5]);
   RooRealVar* sigma1M = new RooRealVar("sigma1M","sigma1M",parafitM[6],parafitM[7],parafitM[8]);
   RooRealVar* sigma2M = new RooRealVar("sigma2M","sigma2M",parafitM[9],parafitM[10],parafitM[11]);
   RooRealVar* frac0M = new RooRealVar("frac0M","frac0M",parafitM[12],parafitM[13],parafitM[14]);  

   RooAbsPdf *sig1Plus = new RooGaussian("sig1Plus","sig1Plus",*x, *mean1P, *sigma1P);
   RooAbsPdf *sig2Plus = new RooGaussian("sig2Plus","sig2Plus",*x, *mean2P, *sigma2P);
   RooAbsPdf *sig1Minus = new RooGaussian("sig1Minus","sig1Minus",*x, *mean1M, *sigma1M);
   RooAbsPdf *sig2Minus = new RooGaussian("sig2Minus","sig2Minus",*x, *mean2M, *sigma2M);

   cTOT[0] = new RooRealVar("cTOTPlus","cTOTPlus",parafitP[15],parafitP[16],parafitP[17]);
   cMATCH[0] = new RooRealVar("cMATCHPlus","cMATCHPlus",parafitP[18],parafitP[19],parafitP[20]);
   cTOT[1] = new RooRealVar("cTOTMinus","cTOTMinus",parafitM[15],parafitM[16],parafitM[17]);
   cMATCH[1] = new RooRealVar("cMATCHMinus","cMATCHMinus",parafitM[18],parafitM[19],parafitM[20]);

   RooAbsPdf *sigpdf[2];
   sigpdf[0] = new RooAddPdf("sigpdfPlus","sigpdfPlus",RooArgList(*sig1Plus,*sig2Plus),RooArgList(*frac0P));
   sigpdf[1] = new RooAddPdf("sigpdfMinus","sigpdfMinus",RooArgList(*sig1Minus,*sig2Minus),RooArgList(*frac0M));

   eff[0] = new RooRealVar("effPlus","effPlus",0.8,0,1);
   eff[1] = new RooRealVar("effMinus","effMinus",0.8,0,1);

   //mean1->setConstant(true);

   TCanvas c1;
   TString texName[2];
   for (auto i : charge){
	chain[i.first].Add("/eos/home-m/mingjie/SCA/trackingasy/muon_track/selection/results_phi/tuple_"+year+polarity+"_withPhi_10.root/Tree"+method+i.second);

	TString TagMu = (i.second.Contains("Plus"))? "muplus" : "muminus";
	TString pCut  = Form(TagMu+"_P/1000.>%g&&"+TagMu+"_P/1000.<%g&&",pmin,pmax);
	TString etaCut = Form(TagMu+"_ETA>%g&&"+TagMu+"_ETA<%g&&",etamin,etamax);
        TString phiCut = Form(TagMu+"_Phi>%g&&"+TagMu+"_Phi<%g&&",phimin,phimax);
	TString ptCut = TagMu+"_PT>1000&&"; 
	TString ipCut = TagMu+"_IPCHI2_OWNPV>9.0&&"; 
	TString mmCut  = Form("J_psi_1S_MM>%g&&J_psi_1S_MM<%g",min,max);
	TagMu = "&&"+TagMu+"_Assoc==1";

	texName[i.first] = (i.second.Contains("Plus"))? "{+}" : "{-}";

	int NumTOT = chain[i.first].GetEntries(pCut+ptCut+phiCut+mmCut);
	int NumMATCH = chain[i.first].GetEntries(pCut+ptCut+phiCut+mmCut+TagMu);
	std::cout<<pCut+ptCut+phiCut+mmCut<<std::endl;
	std::cout<<NumTOT<<" ; "<<NumMATCH<<std::endl;

	//TCanvas c1;
	TH1F hTOT("hTOT","hTOT", nbin, min, max);
	TH1F hMATCH("hMATCH","hMATCH", nbin, min, max);

	chain[i.first].Draw("J_psi_1S_MM>>hTOT",pCut+ptCut+phiCut+mmCut);
	chain[i.first].Draw("J_psi_1S_MM>>hMATCH",pCut+ptCut+phiCut+mmCut+TagMu);

	dataTOT[i.first] = new RooDataHist("dataTOT"+i.second,"dataTOT"+i.second,RooArgList(*x),&hTOT);
	dataMATCH[i.first] = new RooDataHist("dataMATCH"+i.second,"dataMATCH"+i.second,RooArgList(*x),&hMATCH);

	bkgpdfTOT[i.first] = new RooChebychev("bkgpdfTOT"+i.second,"bkgpdfTOT"+i.second,*x,*cTOT[i.first]);
	bkgpdfMATCH[i.first] = new RooChebychev("bkgpdfMATCH"+i.second,"bkgpdfMATCH"+i.second,*x,*cMATCH[i.first]);

	nsigTOT[i.first] = new RooRealVar("nsigTOT"+i.second,"nsigTOT"+i.second,0.5*NumTOT,0,NumTOT);
	nbkgTOT[i.first] = new RooRealVar("nbkgTOT"+i.second,"nbkgTOT"+i.second,0.5*NumTOT,0,NumTOT);

	//nsigMATCH[i.first] = new RooRealVar("nsigMATCH"+i.second,"nsigMATCH"+i.second,0.9*NumMATCH,0,NumMATCH);
	nsigMATCH[i.first] = new RooFormulaVar("nsigMATCH"+i.second,"@0*@1",RooArgSet(*nsigTOT[i.first],*eff[i.first]));
	nbkgMATCH[i.first] = new RooRealVar("nbkgMATCH"+i.second,"nbkgMATCH"+i.second,0.5*NumMATCH,0,NumMATCH);

	// Total PDF
	modelMATCH[i.first] = new RooAddPdf("modelMATCH"+i.second,"modelMATCH"+i.second,RooArgList(*sigpdf[i.first], *bkgpdfMATCH[i.first]),RooArgList(*nsigMATCH[i.first],*nbkgMATCH[i.first]));
	modelTOT[i.first] = new RooAddPdf("modelTOT"+i.second,"modelTOT"+i.second,RooArgList(*sigpdf[i.first], *bkgpdfTOT[i.first]),RooArgList(*nsigTOT[i.first],*nbkgTOT[i.first]));

        std::cout << "aaa " << bkgpdfTOT[i.first]->getParameters(RooArgSet(*x))->getSize() << std::endl; 
        std::cout << "bbb " << bkgpdfMATCH[i.first]->getParameters(RooArgSet(*x))->getSize() << std::endl;

	nllDataMATCH[i.first] = modelMATCH[i.first]->createNLL(*dataMATCH[i.first],Extended(kTRUE));
	nllDataTOT[i.first] = modelTOT[i.first]->createNLL(*dataTOT[i.first],Extended(kTRUE));
        
        chi2MATCH[i.first] = new RooChi2Var("chi2MATCH"+i.second, "chi2MATCH"+i.second, *modelMATCH[i.first], *dataMATCH[i.first], RooFit::Extended(kTRUE));
        chi2TOT[i.first] = new RooChi2Var("chi2TOT"+i.second, "chi2TOT"+i.second, *modelTOT[i.first], *dataTOT[i.first], RooFit::Extended(kTRUE));
        //chi2MATCH[i.first] = new RooChi2Var("chi2MATCH"+i.second, *nllDataMATCH[i.first]);
        //chi2TOT[i.first] = new RooChi2Var("chi2TOT"+i.second, *nllDataTOT[i.first]);
   }

   int paraSize[2] = {0,0};
   RooAddition * nllPlus = new RooAddition("nllPlus","nllPlus",RooArgSet(*nllDataMATCH[0],*nllDataTOT[0]));
   RooMinuit mPlus(*nllPlus);
   mPlus.setVerbose(kFALSE);
   int migradstatusP(0), hessestatusP(0);
   migradstatusP = mPlus.migrad();
   hessestatusP = mPlus.hesse();

   RooFitResult* fitResultPlus = mPlus.save();
   RooArgList fitParamsPlus = fitResultPlus->floatParsFinal();
   paraSize[0] = fitParamsPlus.getSize();
   int ndfPlus = nbin-paraSize[0];

   RooAddition * nllMinus = new RooAddition("nllMinus","nllMinus",RooArgSet(*nllDataMATCH[1],*nllDataTOT[1]));
   RooMinuit mMinus(*nllMinus);
   mMinus.setVerbose(kFALSE);
   int migradstatusM(0), hessestatusM(0);
   migradstatusM = mMinus.migrad();
   hessestatusM = mMinus.hesse();

   RooFitResult* fitResultMinus = mMinus.save();
   RooArgList fitParamsMinus = fitResultMinus->floatParsFinal();
   paraSize[1] = fitParamsMinus.getSize();
   int ndfMinus= nbin-paraSize[1];

   double chi2PlusMATCHVal = chi2MATCH[0]->getVal();
   double chi2PlusTOTVal = chi2TOT[0]->getVal();
   double chi2MinusMATCHVal = chi2MATCH[1]->getVal();
   double chi2MinusTOTVal = chi2TOT[1]->getVal();
   std::cout << "PLUS: " << ndfPlus << " ; " << chi2PlusTOTVal/ndfPlus << " ; " << chi2PlusMATCHVal/ndfPlus << std::endl;
   std::cout << "MINUS: " << ndfMinus << " ; " << chi2MinusTOTVal/ndfMinus << " ; " << chi2MinusMATCHVal/ndfMinus << std::endl;

   RooRealVar* effP = dynamic_cast<RooRealVar*>(fitParamsPlus.find("effPlus")); 
   RooRealVar* effM = dynamic_cast<RooRealVar*>(fitParamsMinus.find("effMinus"));

   ofstream outEff(Form("results/"+year+polarity+method+"EFF__%d_%d_%d.txt",etabin, pbin, phibin));
   outEff<<effP->getVal()<<"\t"<<effP->getError()<<"\t"<<effM->getVal()<<"\t"<<effM->getError()<<std::endl;

   float asy = (effP->getVal()-effM->getVal())/(effP->getVal()+effM->getVal());
   float asyError = 2/std::pow((effP->getVal()+effM->getVal()),2)*std::sqrt(std::pow(effM->getVal()*effM->getError(),2)+std::pow(effP->getVal()*effP->getError(),2));
   ofstream outAsy(Form("results/"+year+polarity+method+"ASY__%d_%d_%d.txt",etabin, pbin, phibin));
   outAsy<<asy<<"\t"<<asyError<<std::endl;

   ofstream outAFit(year+"FIT.txt",std::ios::app);
   outAFit<<year<<polarity<<method<<"phibin" << phibin << "\t" <<migradstatusP<<"\t"<<hessestatusP<<"\t" <<migradstatusM<<"\t"<<hessestatusM<<"\t"<<"PlusTOT: "<<chi2PlusTOTVal/ndfPlus<<"\t"<<"PlusMATCH: "<<chi2PlusMATCHVal/ndfPlus<<"\t"<<"MinusTOT: "<<chi2MinusTOTVal/ndfMinus<<"\t" << "MinusMATCH: "<<chi2MinusMATCHVal/ndfMinus<<std::endl;

   TCanvas *canTOT[2], *canMATCH[2];

   for (auto i : charge){
	canTOT[i.first] = new TCanvas("canTOT"+i.second,"canTOT"+i.second);
	canMATCH[i.first] = new TCanvas("canMATCH"+i.second,"canMATCH"+i.second);

	RooPlot *frameTOT = x->frame();
	//data->plotOn(frameTOT,MarkerSize(0.8),Name("data_fit"));
	dataTOT[i.first]->plotOn(frameTOT,Name("dataTOT"),MarkerSize(0.8));
	modelTOT[i.first]->plotOn(frameTOT,Components("sigpdf"+i.second),LineStyle(7),LineColor(8));
	modelTOT[i.first]->plotOn(frameTOT,Components("sig1"+i.second),LineStyle(7),LineColor(6));
	modelTOT[i.first]->plotOn(frameTOT,Components("sig2"+i.second),LineStyle(7),LineColor(7));
	modelTOT[i.first]->plotOn(frameTOT,Components("bkgpdfTOT"+i.second),LineStyle(2),LineColor(2));
	modelTOT[i.first]->plotOn(frameTOT,Name("TOT"),LineColor(4));
   
	RooHist* hpullTOT=frameTOT->pullHist();
	hpullTOT->SetFillStyle(3001);
	RooPlot* pullTOT=x->frame();
	pullTOT->addPlotable(hpullTOT,"l3");
	pullTOT->SetTitle("");
	pullTOT->GetYaxis()->SetLabelSize(0.20);
	pullTOT->GetYaxis()->SetNdivisions(206);

	RooPlot *frameMATCH = x->frame();
	//data->plotOn(frameMATCH,MarkerSize(0.8),Name("data_fit"));
	dataMATCH[i.first]->plotOn(frameMATCH,Name("dataMATCH"),MarkerSize(0.8));
	modelMATCH[i.first]->plotOn(frameMATCH,Components("sigpdf"+i.second),LineStyle(7),LineColor(8));
	modelMATCH[i.first]->plotOn(frameMATCH,Components("sig1"+i.second),LineStyle(7),LineColor(6));
	modelMATCH[i.first]->plotOn(frameMATCH,Components("sig2"+i.second),LineStyle(7),LineColor(7));
	modelMATCH[i.first]->plotOn(frameMATCH,Components("bkgpdfMATCH"+i.second),LineStyle(2),LineColor(2));
	modelMATCH[i.first]->plotOn(frameMATCH,Name("MATCH"),LineColor(4));

        double chi2_1 = frameTOT->chiSquare("TOT","dataTOT",paraSize[i.first]);
        double chi2_2 = frameMATCH->chiSquare("MATCH","dataMATCH",paraSize[i.first]);
        std::cout << i.second << "; TOT chi2: " << chi2_1  << " ; MATCH: " << chi2_2 << std::endl;

        ofstream outBFit(year+"FIT2.txt",std::ios::app);
        outBFit<<year<<polarity<<method<<"phibin" << phibin << "\t" << i.second << ":\t" << " TOT chi2: " << chi2_1 << "\t"  << " ; MATCH: " << chi2_2 << std::endl;


	RooHist* hpullMATCH=frameMATCH->pullHist();
	hpullMATCH->SetFillStyle(3001);
	RooPlot* pullMATCH=x->frame();
	pullMATCH->addPlotable(hpullMATCH,"l3");
	pullMATCH->SetTitle("");
	pullMATCH->GetYaxis()->SetLabelSize(0.20);
	pullMATCH->GetYaxis()->SetNdivisions(206);

	TLatex *textot = new TLatex(0.7,0.85,"#splitline{#mu^"+texName[i.first]+"_{total}}{#splitline{"+year+"/"+polarity+"/"+method+"}{#splitline{"+Form("%.2g<#eta<%.2g",etamin,etamax)+"}{#splitline{"+Form("%.2g<p<%.3g (GeV)",pmin,pmax)+"}{"+Form("%.3g<phi<%.3g",phimin,phimax)+"}}}}");
	textot->SetNDC();textot->SetTextSize(0.07);
	TLatex *texmatch = new TLatex(0.7,0.85,"#splitline{#mu^"+texName[i.first]+"_{matched}}{#splitline{"+year+"/"+polarity+"/"+method+"}{#splitline{"+Form("%.2g<#eta<%.2g",etamin,etamax)+"}{#splitline{"+Form("%.2g<p<%.3g (GeV)",pmin,pmax)+"}{"+Form("%.3g<phi<%.3g",phimin,phimax)+"}}}}");
	texmatch->SetNDC();texmatch->SetTextSize(0.07);

	frameTOT->addObject(textot);
	frameMATCH->addObject(texmatch);

	canTOT[i.first]->Divide(1,2,0,0,0);
	canTOT[i.first]->cd(2);
	gPad->SetTopMargin(0.0);
	gPad->SetLeftMargin(0.15);
	gPad->SetPad(0.03,0.02,0.97,0.77);
	frameTOT->Draw() ;
	canTOT[i.first]->cd(1);
	gPad->SetBottomMargin(0.0);
	gPad->SetLeftMargin(0.15);
	gPad->SetPad(0.03,0.77,0.97,0.97);
	pullTOT->Draw();
	canTOT[i.first]->Print("results/"+year+polarity+method+i.second+Form("TOT__%d_%d_%d.pdf",etabin,pbin,phibin));
	canTOT[i.first]->Print("results/"+year+polarity+method+i.second+Form("TOT__%d_%d_%d.png",etabin,pbin,phibin));

	canMATCH[i.first]->Divide(1,2,0,0,0);
	canMATCH[i.first]->cd(2);
	gPad->SetTopMargin(0.0);
	gPad->SetLeftMargin(0.15);
	gPad->SetPad(0.03,0.02,0.97,0.77);
	frameMATCH->Draw() ;
	canMATCH[i.first]->cd(1);
	gPad->SetBottomMargin(0.0);
	gPad->SetLeftMargin(0.15);
	gPad->SetPad(0.03,0.77,0.97,0.97);
	pullMATCH->Draw();
	canMATCH[i.first]->Print("results/"+year+polarity+method+i.second+Form("MATCH__%d_%d_%d.pdf",etabin, pbin,phibin));
	canMATCH[i.first]->Print("results/"+year+polarity+method+i.second+Form("MATCH__%d_%d_%d.png",etabin, pbin,phibin));
   }

   return 0;

}
