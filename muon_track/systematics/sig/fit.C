#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "include/mass.h"
#include <iostream>
#include <fstream> 
#include <string>

using namespace RooFit ;

int fit(TString year="18", TString polarity="Down", TString method="T"){
   gROOT->ProcessLine(".x ~/lhcbStyle.C");

   if (system("ls -d results")!=0){ system("mkdir -p results");}//if no results create one

   std::string parafile = "PARALIST.txt";

   if (system(((string)"ls "+parafile).c_str())!=0){
	std::cout<<"no "+parafile+" is found. I will create one and then please run script again"<<std::endl;
	double parafit[]={
	   3096, 2650, 3550, 
	   3090, 2650, 3550, 
	   10, 0, 999,
	   15, 0, 999,
	   0.5,0, 1,
	   -0.4,-999,999,
	   -0.4,-999,999,
	};
	std::ofstream outfile(parafile);
	for (auto i=0; i<21; i++){
	   outfile<<parafit[i]<<"\t";
	   if (i%3==2) outfile<<"\n";
	}
	return 0;
   }


   double parafit[21] = {};
   int ieta = 0;
   for (auto ip = 0; ip<6; ip++ ){
	std::string parafilebin = Form("results/PARALIST"+year+polarity+method+"_%d_%d.txt",ieta,ip);

	if (system(((string)"ls "+parafilebin).c_str())!=0){
	   system(((string)"cp "+parafile+" "+parafilebin).c_str());
	}

	std::ifstream inputfile(parafilebin);
	//for (auto i=0; i<21; i++){inputfile>>parafit[i];}
        int index = 0;
        std::string line;
	while (std::getline(inputfile, line) && index < 21) {
            std::istringstream iss(line); 
            double value;

            while (iss >> value && index < 21) {
                parafit[index++] = value;
            }
        }

	//std::cout<<eta[ieta]<<" ; "<<p[ip]<<std::endl;
	mass(year, polarity, method, true, ieta, ip, parafit);
   }
#if 0
   for (auto ieta = 0; ieta <3; ieta++ ){
	for (auto ip = 0; ip<6; ip++ ){
	   std::string parafilebin = Form("results/PARALIST"+year+polarity+method+"_%d_%d.txt",ieta,ip);

	   if (system(((string)"ls "+parafilebin).c_str())!=0){
		system(((string)"cp "+parafile+" "+parafilebin).c_str());
	   }

	   std::ifstream inputfile(parafilebin);
	   for (auto i=0; i<21; i++){inputfile>>parafit[i];}

	   //std::cout<<eta[ieta]<<" ; "<<p[ip]<<std::endl;
	   mass(year, polarity, method, true, ieta, ip, parafit);
	}
   }
#endif

   return 0;

}
