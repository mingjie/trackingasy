#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "TString.h"
#include <memory>

using namespace RooFit ;

int mass(TString year="18", TString polarity="Down", TString method="T", bool isBin=false, unsigned int etabin=0, unsigned int pbin=0, double parafit[]={}){
   std::cout << year << " ; " << polarity << " ; " << method << " ; " << isBin << " ; " << etabin << " ; " << pbin << std::endl;

   for (size_t i = 0; i < 27; ++i) {
        std::cout << parafit[i] << " ";
        if ((i + 1) % 3 == 0) { // Assuming 3 columns per row in your data
           std::cout << std::endl;
        }
   }

   double eta[] = {1.6,2.5,3.5,5.5};
   double p[]  = {2,10,20,30,40,50,100};

#if 0
   double etamax = (isBin) ? eta[etabin+1] : 10;
   double etamin = (isBin) ? eta[etabin]   :  0;
#endif
   double etamax = 6;
   double etamin = 1.5;
   double pmax  = (isBin) ?  p[pbin+1]  : 100;
   double pmin  = (isBin) ?  p[pbin]    :  0;

   using chargemap = std::pair<int, TString>;

   std::array<chargemap,2> charge = {{
	{0,"Plus"}, {1,"Minus"}
   }};

   std::array<TChain,2> chain;


   double min = 2650., max = 3550.;
   if (method.Contains("Velo")) {min = 2950.; max = 3250.;}
   unsigned int nbin = 90;

   RooRealVar *x = new RooRealVar("x","m(J/#psi(1S)) [MeV]", min, max);
   x->setBins(nbin);

   RooDataHist *dataTOT[2], *dataMATCH[2];
   //RooChebychev *bkgpdfTOT[2], *bkgpdfMATCH[2];
   RooPolynomial *bkgpdfTOT[2], *bkgpdfMATCH[2];
   RooAbsPdf *modelMATCH[2], *modelTOT[2];
   RooAbsReal *nllDataMATCH[2], *nllDataTOT[2];
   RooRealVar *kTOT[2], *kMATCH[2], *bTOT[2], *bMATCH[2];
   RooAbsReal *nsigTOT[2], *nbkgTOT[2], *nsigMATCH[2], *nbkgMATCH[2], *eff[2];
   RooRealVar asy("asy","asy", 0, -1., 1.);
   RooChi2Var *chi2MATCH[2], *chi2TOT[2];

   RooRealVar* mean1 = new RooRealVar("mean1","mean1",parafit[0],parafit[1],parafit[2]);
   RooRealVar* mean2 = new RooRealVar("mean2","mean2",parafit[3],parafit[4],parafit[5]);
   RooRealVar* sigma1 = new RooRealVar("sigma1","sigma1",parafit[6],parafit[7],parafit[8]);
   RooRealVar* sigma2 = new RooRealVar("sigma2","sigma2",parafit[9],parafit[10],parafit[11]);

   RooRealVar* frac0 = new RooRealVar("frac0","frac0",parafit[12],parafit[13],parafit[14]);

   RooAbsPdf *sig1 = new RooGaussian("sig1","sig1",*x, *mean1, *sigma1);
   RooAbsPdf *sig2 = new RooGaussian("sig2","sig2",*x, *mean2, *sigma2);
   RooAbsPdf *sigpdf = new RooAddPdf("sigpdf","sigpdf",RooArgList(*sig1,*sig2),RooArgList(*frac0));

   //mean1->setConstant(true);
   //mean2->setConstant(true);
   //sigma1->setConstant(true);
   //sigma2->setConstant(true);

   eff[0] = new RooRealVar("effPlus","effPlus",0.8,0,1);
   eff[1] = new RooFormulaVar("effMinus","((1-@0)/(1+@0))*@1",RooArgSet(asy, *eff[0]));

   TCanvas c1;
   TString texName[2];
   for (auto i : charge){
        std::cout << "charge"  << i.second << std::endl;
	std::cout << year << polarity << std::endl;
	chain[i.first].Add("/eos/home-m/mingjie/SCA/trackingasy/muon_track/selection/results/tuple_"+year+polarity+"_10.root/Tree"+method+i.second);

	TString TagMu = (i.second.Contains("Plus"))? "muplus" : "muminus";
	TString pCut  = Form(TagMu+"_P/1000.>%g&&"+TagMu+"_P/1000.<%g&&",pmin,pmax);
	TString etaCut = Form(TagMu+"_ETA>%g&&"+TagMu+"_ETA<%g&&",etamin,etamax);
	TString ptCut = TagMu+"_PT>1000&&"; 
	TString ipCut = TagMu+"_IPCHI2_OWNPV>9.0&&"; 
	TString mmCut  = Form("J_psi_1S_MM>%g&&J_psi_1S_MM<%g",min,max);
	TagMu = "&&"+TagMu+"_Assoc==1";

	texName[i.first] = (i.second.Contains("Plus"))? "{+}" : "{-}";

	int NumTOT = chain[i.first].GetEntries(pCut+ptCut+mmCut);
	int NumMATCH = chain[i.first].GetEntries(pCut+ptCut+mmCut+TagMu);
	std::cout<<pCut+ptCut+mmCut<<std::endl;
	std::cout<<NumTOT<<" ; "<<NumMATCH<<std::endl;

	//TCanvas c1;
	TH1F hTOT("hTOT","hTOT", nbin, min, max);
	TH1F hMATCH("hMATCH","hMATCH", nbin, min, max);

	chain[i.first].Draw("J_psi_1S_MM>>hTOT",pCut+ptCut+mmCut);
	chain[i.first].Draw("J_psi_1S_MM>>hMATCH",pCut+ptCut+mmCut+TagMu);

	dataTOT[i.first] = new RooDataHist("dataTOT"+i.second,"dataTOT"+i.second,RooArgList(*x),&hTOT);
	dataMATCH[i.first] = new RooDataHist("dataMATCH"+i.second,"dataMATCH"+i.second,RooArgList(*x),&hMATCH);

	kTOT[i.first] = new RooRealVar("kTOT"+i.second,"kTOT"+i.second,parafit[21],parafit[22],parafit[23]);
	kMATCH[i.first] = new RooRealVar("kMATCH"+i.second,"kMATCH"+i.second,parafit[24],parafit[25],parafit[26]);
        bTOT[i.first] = new RooRealVar("bTOT"+i.second,"bTOT"+i.second,parafit[15],parafit[16],parafit[17]);
        bMATCH[i.first] = new RooRealVar("bMATCH"+i.second,"bMATCH"+i.second,parafit[18],parafit[19],parafit[20]);
	bkgpdfTOT[i.first] = new RooPolynomial("bkgpdfTOT"+i.second,"bkgpdfTOT"+i.second,*x,RooArgList(*bTOT[i.first],*kTOT[i.first]));
        bkgpdfMATCH[i.first] = new RooPolynomial("bkgpdfMATCH"+i.second,"bkgpdfMATCH"+i.second,*x,RooArgList(*bMATCH[i.first],*kMATCH[i.first]));

	nsigTOT[i.first] = new RooRealVar("nsigTOT"+i.second,"nsigTOT"+i.second,0.5*NumTOT,0,NumTOT);
	nbkgTOT[i.first] = new RooRealVar("nbkgTOT"+i.second,"nbkgTOT"+i.second,0.5*NumTOT,0,NumTOT);

	//nsigMATCH[i.first] = new RooRealVar("nsigMATCH"+i.second,"nsigMATCH"+i.second,0.9*NumMATCH,0,NumMATCH);
	nsigMATCH[i.first] = new RooFormulaVar("nsigMATCH"+i.second,"@0*@1",RooArgSet(*nsigTOT[i.first],*eff[i.first]));
	nbkgMATCH[i.first] = new RooRealVar("nbkgMATCH"+i.second,"nbkgMATCH"+i.second,0.5*NumMATCH,0,NumMATCH);

	// Total PDF
	modelMATCH[i.first] = new RooAddPdf("modelMATCH"+i.second,"modelMATCH"+i.second,RooArgList(*sigpdf, *bkgpdfMATCH[i.first]),RooArgList(*nsigMATCH[i.first],*nbkgMATCH[i.first]));
        modelTOT[i.first] = new RooAddPdf("modelTOT"+i.second,"modelTOT"+i.second,RooArgList(*sigpdf, *bkgpdfTOT[i.first]),RooArgList(*nsigTOT[i.first],*nbkgTOT[i.first]));

	nllDataMATCH[i.first] = modelMATCH[i.first]->createNLL(*dataMATCH[i.first],Extended(kTRUE));
	nllDataTOT[i.first] = modelTOT[i.first]->createNLL(*dataTOT[i.first],Extended(kTRUE));

        chi2MATCH[i.first] = new RooChi2Var("chi2MATCH"+i.second, "chi2MATCH"+i.second, *modelMATCH[i.first], *dataMATCH[i.first], RooFit::Extended(kTRUE));
        chi2TOT[i.first] = new RooChi2Var("chi2TOT"+i.second, "chi2TOT"+i.second, *modelTOT[i.first], *dataTOT[i.first], RooFit::Extended(kTRUE));
   }

   RooAddition * nll = new RooAddition("nll","nll",RooArgList(*nllDataMATCH[1],*nllDataTOT[1],*nllDataMATCH[0],*nllDataTOT[0]));

   RooMinuit m(*nll);
   m.setVerbose(kFALSE);
   int migradstatus(0), hessestatus(0);
   migradstatus = m.migrad();
   hessestatus = m.hesse();

   RooFitResult* fitResult = m.save();
   RooArgList fitParams = fitResult->floatParsFinal();
   int paraSize = fitParams.getSize();
   int ndf = nbin-paraSize;

   double chi2PlusMATCHVal = chi2MATCH[0]->getVal();
   double chi2PlusTOTVal = chi2TOT[0]->getVal();
   double chi2MinusMATCHVal = chi2MATCH[1]->getVal();
   double chi2MinusTOTVal = chi2TOT[1]->getVal();
   std::cout << ndf << " ; " << chi2PlusTOTVal/ndf  << " ; " << chi2PlusMATCHVal/ndf  << " ; " << chi2MinusTOTVal/ndf << " ; "  << chi2MinusMATCHVal/ndf << std::endl;

   ofstream outAsy(Form("results/"+year+polarity+method+"ASY__%d_%d.txt",etabin, pbin));
   outAsy<<asy.getVal()<<"\t"<<asy.getError()<<std::endl;

   ofstream outFit(Form("results/"+year+polarity+method+"FIT__%d_%d.txt",etabin, pbin));
   outFit<<migradstatus<<"\t"<<hessestatus<<"\t"<<chi2PlusTOTVal/ndf<<"\t"<<chi2PlusMATCHVal/ndf<<"\t"<<chi2MinusTOTVal/ndf<<"\t"<<chi2MinusMATCHVal/ndf<<std::endl;

   ofstream outAFit(year+"FIT.txt",std::ios::app);
   outAFit<<year<<polarity<<method<<"pbin" << pbin << "\t" <<migradstatus<<"\t"<<hessestatus<<"\t"<<"PlusTOT: "<<chi2PlusTOTVal/ndf<<"\t"<<"PlusMATCH: "<<chi2PlusMATCHVal/ndf<<"\t"<<"MinusTOT: "<<chi2MinusTOTVal/ndf<<"\t" << "MinusMATCH: "<<chi2MinusMATCHVal/ndf<<std::endl;
    

   TCanvas *canTOT[2], *canMATCH[2];


   for (auto i : charge){
	canTOT[i.first] = new TCanvas("canTOT"+i.second,"canTOT"+i.second);
	canMATCH[i.first] = new TCanvas("canMATCH"+i.second,"canMATCH"+i.second);

	RooPlot *frameTOT = x->frame();
	dataTOT[i.first]->plotOn(frameTOT, Name("dataTOT"), MarkerSize(0.8));
	modelTOT[i.first]->plotOn(frameTOT,Components("sigpdf"),LineStyle(7),LineColor(8));
	modelTOT[i.first]->plotOn(frameTOT,Components("sig1"),LineStyle(7),LineColor(6));
	modelTOT[i.first]->plotOn(frameTOT,Components("sig2"),LineStyle(7),LineColor(7));
        modelTOT[i.first]->plotOn(frameTOT,Components("bkgpdfTOT"+i.second),LineStyle(2),LineColor(2));
	modelTOT[i.first]->plotOn(frameTOT, Name("TOT"), LineColor(4));
   
	RooHist* hpullTOT=frameTOT->pullHist();
	hpullTOT->SetFillStyle(3001);
	RooPlot* pullTOT=x->frame();
	pullTOT->addPlotable(hpullTOT,"l3");
	pullTOT->SetTitle("");
	pullTOT->GetYaxis()->SetLabelSize(0.20);
	pullTOT->GetYaxis()->SetNdivisions(206);

	RooPlot *frameMATCH = x->frame();
	dataMATCH[i.first]->plotOn(frameMATCH, Name("dataMATCH"), MarkerSize(0.8));
	modelMATCH[i.first]->plotOn(frameMATCH,Components("sigpdf"),LineStyle(7),LineColor(8));
	modelMATCH[i.first]->plotOn(frameMATCH,Components("sig1"),LineStyle(7),LineColor(6));
	modelMATCH[i.first]->plotOn(frameMATCH,Components("sig2"),LineStyle(7),LineColor(7));
        modelMATCH[i.first]->plotOn(frameMATCH,Components("bkgpdfMATCH"+i.second),LineStyle(2),LineColor(2));
	modelMATCH[i.first]->plotOn(frameMATCH, Name("MATCH"),LineColor(4));


        double chi2_1 = frameTOT->chiSquare("TOT","dataTOT",paraSize);
        double chi2_2 = frameMATCH->chiSquare("MATCH","dataMATCH",paraSize);
        std::cout << i.second << " ; " << migradstatus << " ; " << hessestatus  << " ; TOT chi2: " << chi2_1  << " ; MATCH: " << chi2_2 << std::endl;


        ofstream outBFit(year+"FIT2.txt",std::ios::app);
        outBFit<<year<<polarity<<method<<"pbin" << pbin << "\t" << i.second << ":\t" << " TOT chi2: " << chi2_1 << "\t"  << " ; MATCH: " << chi2_2 << std::endl;


	RooHist* hpullMATCH=frameMATCH->pullHist();
	hpullMATCH->SetFillStyle(3001);
	RooPlot* pullMATCH=x->frame();
	pullMATCH->addPlotable(hpullMATCH,"l3");
	pullMATCH->SetTitle("");
	pullMATCH->GetYaxis()->SetLabelSize(0.20);
	pullMATCH->GetYaxis()->SetNdivisions(206);

	TLatex *textot = new TLatex(0.7,0.85,"#splitline{#mu^"+texName[i.first]+"_{total}}{#splitline{"+year+"/"+polarity+"/"+method+"}{#splitline{"+Form("%.2g<#eta<%.2g",etamin,etamax)+"}{"+Form("%.2g<p<%.3g (GeV)",pmin,pmax)+"}}}");
	textot->SetNDC();textot->SetTextSize(0.07);
	TLatex *texmatch = new TLatex(0.7,0.85,"#splitline{#mu^"+texName[i.first]+"_{matched}}{#splitline{"+year+"/"+polarity+"/"+method+"}{#splitline{"+Form("%.2g<#eta<%.2g",etamin,etamax)+"}{"+Form("%.2g<p<%.3g (GeV)",pmin,pmax)+"}}}");
	texmatch->SetNDC();texmatch->SetTextSize(0.07);

	frameTOT->addObject(textot);
	frameMATCH->addObject(texmatch);

	canTOT[i.first]->Divide(1,2,0,0,0);
	canTOT[i.first]->cd(2);
	gPad->SetTopMargin(0.0);
	gPad->SetLeftMargin(0.15);
	gPad->SetPad(0.03,0.02,0.97,0.77);
	frameTOT->Draw() ;
	canTOT[i.first]->cd(1);
	gPad->SetBottomMargin(0.0);
	gPad->SetLeftMargin(0.15);
	gPad->SetPad(0.03,0.77,0.97,0.97);
	pullTOT->Draw();
	canTOT[i.first]->Print("results/"+year+polarity+method+i.second+Form("TOT__%d_%d.pdf",etabin,pbin));
	canTOT[i.first]->Print("results/"+year+polarity+method+i.second+Form("TOT__%d_%d.png",etabin,pbin));

	canMATCH[i.first]->Divide(1,2,0,0,0);
	canMATCH[i.first]->cd(2);
	gPad->SetTopMargin(0.0);
	gPad->SetLeftMargin(0.15);
	gPad->SetPad(0.03,0.02,0.97,0.77);
	frameMATCH->Draw() ;
	canMATCH[i.first]->cd(1);
	gPad->SetBottomMargin(0.0);
	gPad->SetLeftMargin(0.15);
	gPad->SetPad(0.03,0.77,0.97,0.97);
	pullMATCH->Draw();
	canMATCH[i.first]->Print("results/"+year+polarity+method+i.second+Form("MATCH__%d_%d.pdf",etabin, pbin));
	canMATCH[i.first]->Print("results/"+year+polarity+method+i.second+Form("MATCH__%d_%d.png",etabin, pbin));
   } 

   for(auto i : charge){
        delete dataTOT[i.first];
        delete dataMATCH[i.first];
        delete bkgpdfTOT[i.first];
        delete bkgpdfMATCH[i.first];
        delete modelMATCH[i.first];
        delete modelTOT[i.first];
        delete nllDataMATCH[i.first];
        delete nllDataTOT[i.first];
        delete kTOT[i.first];
        delete kMATCH[i.first];
        delete bTOT[i.first];
        delete bMATCH[i.first];
        delete nsigTOT[i.first];
        delete nbkgTOT[i.first];
        delete nsigMATCH[i.first];
        delete nbkgMATCH[i.first];
        delete eff[i.first];
   }

   return 0;

}
