MagDown PARAMs START ====>
am, 2, -20, 20, 0
bm, -1.3, -50, 0, 0
cm, 6, -10, 200, 0
coeffsig, 0.3, 0, 1, 0
dm0, 139.4, 130, 150, 1
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.3, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, -3, -20, 20, 0
bm, -3, -50, 0, 0
cm, 13, -10, 200, 0
coeffsig, 0.7, 0, 1, 0
dm0, 139.4, 130, 150, 1
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.3, 0, 10, 0
MagUp PARAMs END <====
