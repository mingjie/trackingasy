#include "READ.h"
using namespace RooFit;

void ws_part(int ibin=0, TString polarity="Down", TString year="2016"){
   double bin[] = {2,5,10,20,30,40,50,100};
   double pmin = bin[ibin];
   double pmax = bin[ibin+1];
   gROOT->ProcessLine(".x ~/lhcbStyle.C");

   auto t = getDirectory();

   TString parafile("../parttempt/PARAMs/PartMom_"+TString::Format("%d.txt",ibin));

   //ReadPara para;
   auto Para = READParameter((char*)parafile.Data(), (string)polarity);

   TChain *chRS = new TChain();
   TChain *chWS = new TChain();
   chRS->Add("/eos/home-m/mingjie/SCA/trackingasy/pion_track/selection/promptD/results/"+year+"Mag"+polarity+"PartRS.root/DecayTree");
   chWS->Add("/eos/home-m/mingjie/SCA/trackingasy/pion_track/selection/promptD/results/"+year+"Mag"+polarity+"PartWS.root/DecayTree");

   TString cutKine = "(PMiss_Fit/1000<100&&PMiss_Fit/PMiss_Err>2.5&&PtMiss_Fit/PtMiss_Err>2)&&";
   TString cutPRS = cutKine+"(PiS_ID==-211)"+TString::Format("&&PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g",pmin,pmax),
	     cutMRS = cutKine+"(PiS_ID==211)"+TString::Format("&&PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g",pmin,pmax),
	     cutPWS = cutKine+"(PiS_ID==211)"+TString::Format("&&PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g",pmin,pmax),
	     cutMWS = cutKine+"(PiS_ID==-211)"+TString::Format("&&PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g",pmin,pmax);

   double xmin(139), xmax(174);
   double binWidth = 0.5;
   const int nBin = (int) ((xmax-xmin)/binWidth);
   RooRealVar x("x","#DeltaM(MeV)",xmin,xmax);
   x.setBins(nBin);

   TH1F *hPRS = new TH1F("hPRS","hPRS",nBin,xmin,xmax);
   TH1F *hMRS = new TH1F("hMRS","hMRS",nBin,xmin,xmax);
   TH1F *hPWS = new TH1F("hPWS","hPWS",nBin,xmin,xmax);
   TH1F *hMWS = new TH1F("hMWS","hMWS",nBin,xmin,xmax);

   TCanvas *c1 = new TCanvas("c1","c1");
   chRS->Draw("Dst_MM-D0_MM>>hPRS",cutPRS);
   chRS->Draw("Dst_MM-D0_MM>>hMRS",cutMRS);
   chWS->Draw("Dst_MM-D0_MM>>hPWS",cutPWS);
   chWS->Draw("Dst_MM-D0_MM>>hMWS",cutMWS);

   RooDataHist dataPRS("dataPRS", "dataPRS", x, hPRS),
		   dataMRS("dataMRS", "dataMRS", x, hMRS),
		   dataPWS("dataPWS", "dataPWS", x, hPWS),
		   dataMWS("dataMWS", "dataMWS", x, hMWS);

   RooRealVar NsigPRS("NsigPRS","NsigPRS",0.7*hPRS->Integral(),0,1.5*hPRS->Integral()),
		  NsigMRS("NsigMRS","NsigMRS",0.7*hMRS->Integral(),0,1.5*hMRS->Integral()),
		  NbkgPRS("NbkgPRS","NbkgPRS",0.3*hPRS->Integral(),0,1.5*hPRS->Integral()),
		  NbkgMRS("NbkgMRS","NbkgMRS",0.3*hMRS->Integral(),0,1.5*hMRS->Integral()),
		  NbkgPWS("NbkgPWS","NbkgPWS",hPWS->Integral(),0.8*hPWS->Integral(),1.2*hPWS->Integral()),
		  NbkgMWS("NbkgMWS","NbkgMWS",hMWS->Integral(),0.8*hPWS->Integral(),1.2*hMWS->Integral());

   RooRealVar mean("mean","mean",143,139,148);
   RooRealVar sigma("sigma","sigma",2.3,0.5,4.);
   RooRealVar sigma2("sigma2","", 8.2,7.,9.);
   RooBifurGauss* gaubifur = new RooBifurGauss("gaubifur","",x,mean,sigma,sigma2);

   RooRealVar *acc_p = new RooRealVar("acc_p","acc_p",1.0,0.1,1.5);
   RooRealVar *acc_s = new RooRealVar("acc_s","acc_s",0.5,0.1,1.5);
   RooRealVar *offset = new RooRealVar("offset","offset",139,130,150);
   RooAbsReal *acc =  new RooFormulaVar("acc","(@0-@3<0) ? 0 : (abs((@0-@3)*@1)^@2/(1.0+abs((@0-@3)*@1)^@2))",RooArgList(x,*acc_s,*acc_p,*offset));

   RooAbsPdf *modelSig = new RooEffProd("modelSig","modelSig",*gaubifur,*acc);

   //parameters for DstD0BG
   RooRealVar dm0("dm0","dm0",Para.value("dm0",0),Para.value("dm0",1),Para.value("dm0",2)),
		  am("am","am",Para.value("am",0),Para.value("am",1),Para.value("am",2)),
		  bm("bm","bm",Para.value("bm",0),Para.value("bm",1),Para.value("bm",2)),
		  cm("cm","cm",Para.value("cm",0),Para.value("cm",1),Para.value("cm",2));
   dm0.setConstant(Para.value("dm0",3));
   am.setConstant(Para.value("am",3));
   bm.setConstant(Para.value("bm",3));
   cm.setConstant(Para.value("cm",3));

   //parameters for Eff func
   RooRealVar poly_a("poly_a","poly_a",Para.value("poly_a",0),Para.value("poly_a",1),Para.value("poly_a",2)),
		  poly_b("poly_b","poly_b",Para.value("poly_b",0),Para.value("poly_b",1),Para.value("poly_b",2)),
		  poly_c("poly_c","poly_c",Para.value("poly_c",0),Para.value("poly_c",1),Para.value("poly_c",2));
   poly_a.setConstant(Para.value("poly_a",3));
   poly_b.setConstant(Para.value("poly_b",3));
   poly_c.setConstant(Para.value("poly_c",3));

   //P.D.F.
   RooDstD0BG pdfD0BG("pdfD0BG","DstD0BKG",x,dm0,cm,am,bm);
   RooChebychev poly("poly","", x, RooArgList(poly_a,poly_b,poly_c));

   RooProdPdf modelBkg("modelBkg", "", pdfD0BG, poly);
   //pdfMWS("pdfMWS", "", pdfD0BG, poly);

   RooAbsPdf *modelPWS = new RooAddPdf("modelPWS","modelPWS",RooArgList(modelBkg),RooArgList(NbkgPWS));
   RooAbsPdf *modelMWS = new RooAddPdf("modelMWS","modelMWS",RooArgList(modelBkg),RooArgList(NbkgMWS));

   RooAbsPdf *modelPRS = new RooAddPdf("modelPRS","modelPRS",RooArgList(*modelSig,modelBkg),RooArgList(NsigPRS,NbkgPRS));
   RooAbsPdf *modelMRS = new RooAddPdf("modelMRS","modelMRS",RooArgList(*modelSig,modelBkg),RooArgList(NsigMRS,NbkgMRS));

   RooAbsReal *nllDataPWS = modelPWS->createNLL(dataPWS,Extended());
   RooAbsReal *nllDataMWS = modelMWS->createNLL(dataMWS,Extended());
   RooAddition *nll = new RooAddition("nll","nll",RooArgSet(*nllDataPWS,*nllDataMWS));

   RooMinuit m(*nll);
   m.setVerbose(kFALSE);
   int migradestatus = m.migrad();
   int hessestatus = m.hesse();

#if 0
   RooPlot *MRSframe = x.frame();
   modelMRS->plotOn(MRSframe, Components("modelSig"), FillColor(2),VLines(),DrawOption("F"));
   dataMRS.plotOn(MRSframe, MarkerSize(0.8));
   modelMRS->plotOn(MRSframe, Components("modelBkg"), LineColor(8));
   modelMRS->plotOn(MRSframe, LineColor(4));

   RooHist* hMRSpull=MRSframe->pullHist();
   hMRSpull->SetFillStyle(3001);
   RooPlot* MRSpull=x.frame();
   MRSpull->addPlotable(hMRSpull,"l3");
   MRSpull->SetTitle("");
   MRSpull->GetYaxis()->SetLabelSize(0.20);
   MRSpull->GetYaxis()->SetNdivisions(206);

   RooPlot *PRSframe = x.frame();
   modelPRS->plotOn(PRSframe, Components("modelSig"), FillColor(2),VLines(),DrawOption("F"));
   dataPRS.plotOn(PRSframe, MarkerSize(0.8));
   modelPRS->plotOn(PRSframe, Components("modelBkg"), LineColor(8));
   modelPRS->plotOn(PRSframe, LineColor(4));

   RooHist* hPRSpull=PRSframe->pullHist();
   hPRSpull->SetFillStyle(3001);
   RooPlot* PRSpull=x.frame();
   PRSpull->addPlotable(hPRSpull,"l3");
   PRSpull->SetTitle("");
   PRSpull->GetYaxis()->SetLabelSize(0.20);
   PRSpull->GetYaxis()->SetNdivisions(206);
#endif

   RooPlot *MWSframe = x.frame();
   dataMWS.plotOn(MWSframe, MarkerSize(0.8));
   modelMWS->plotOn(MWSframe, LineColor(8));

   RooHist* hMWSpull=MWSframe->pullHist();
   hMWSpull->SetFillStyle(3001);
   RooPlot* MWSpull=x.frame();
   MWSpull->addPlotable(hMWSpull,"l3");
   MWSpull->SetTitle("");
   MWSpull->GetYaxis()->SetLabelSize(0.20);
   MWSpull->GetYaxis()->SetNdivisions(206);

   RooPlot *PWSframe = x.frame();
   dataPWS.plotOn(PWSframe, Name("data_fit"), MarkerSize(0.8));
   modelPWS->plotOn(PWSframe, Name("data_all"),LineColor(8));

   RooHist* hPWSpull=PWSframe->pullHist();
   hPWSpull->SetFillStyle(3001);
   RooPlot* PWSpull=x.frame();
   PWSpull->addPlotable(hPWSpull,"l3");
   PWSpull->SetTitle("");
   PWSpull->GetYaxis()->SetLabelSize(0.20);
   PWSpull->GetYaxis()->SetNdivisions(206);

   gROOT->Reset();
   TCanvas *c2 = new TCanvas("c2","c2");
   c2->Divide(1,2,0,0,0);
   c2->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   TLatex *TeXM = new TLatex(0.7,0.8, "#splitline{#pi^{-} in "+year+" Mag"+polarity+"}{#splitline{ WS Data}"
	   +TString::Format("{p_{Infer}#in(%d,%d) GeV}}",
		(int)pmin,(int)pmax));
   TeXM->SetNDC();
   TeXM->SetTextSize(0.07);
   MWSframe->addObject(TeXM);
   MWSframe->Draw();

   c2->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   MWSpull->Draw();

   TCanvas *c3 = new TCanvas("c3","c3");
   c3->Divide(1,2,0,0,0);
   c3->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   TLatex *TeXP = new TLatex(0.7,0.8, "#splitline{#pi^{+} in "+year+" Mag"+polarity+"}{#splitline{ WS Data}"
	   +TString::Format("{p_{Infer}#in(%d,%d) GeV}}",
		(int)pmin,(int)pmax));
   TeXP->SetNDC();
   TeXP->SetTextSize(0.07);
   PWSframe->addObject(TeXP);
   PWSframe->Draw();

   c3->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   PWSpull->Draw();

#if 0
   TCanvas *c4 = new TCanvas("c4","c4");
   c4->Divide(1,2,0,0,0);
   c4->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   MRSframe->Draw();

   c4->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   MRSpull->Draw();

   TCanvas *c5 = new TCanvas("c5","c5");
   c5->Divide(1,2,0,0,0);
   c5->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   PRSframe->Draw();

   c5->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   PRSpull->Draw();
#endif

#if 1
   c2->Print(TString(t.FIG)+"ws_part_"+year+"_"+polarity+Form("_Bin%d",ibin)+"_PIM_promtD.pdf");
   c3->Print(TString(t.FIG)+"ws_part_"+year+"_"+polarity+Form("_Bin%d",ibin)+"_PIP_promtD.pdf");
   c2->Print("results/ws_part_"+polarity+"_PIM.pdf");
   c3->Print("results/ws_part_"+polarity+"_PIP.pdf");

#if 0
   RooArgSet* setws   = model_bkg->getParameters(data);
   setws->writeToFile("results/ws_part_"+polarity+"_char_"+charge+".txt");
#endif

   ofstream outfile("results/ws_para"+polarity+".txt");
   outfile<<am.getVal()<< "\t";
   outfile<<bm.getVal()<< "\t";
   outfile<<cm.getVal()<< "\t";
   outfile<<dm0.getVal()<< "\t";
   outfile<<poly_a.getVal()<< "\t";
   outfile<<poly_b.getVal()<< "\t";
   outfile<<poly_c.getVal()<< "\t";
#endif

}
