MagDown PARAMs START ====>
am, 10, -10, 20, 0
bm, -22, -50, 0, 0
cm, 0.5, -10, 200, 0
coeffsig, 0.9, 0, 1, 0
dm0, 139, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.7, 0, 10, 0
sigma3, 3, 0, 50, 1
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 10, -10, 20, 0
bm, -22, -50, 0, 0
cm, 0.5, -10, 200, 0
coeffsig, 0.9, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.6, 0, 10, 0
sigma2, 0.7, 0, 10, 0
sigma3, 3, 0, 50, 1
MagUp PARAMs END <====
