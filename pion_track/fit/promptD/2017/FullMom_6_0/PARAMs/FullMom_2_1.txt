MagDown PARAMs START ====>
am, 5, -20, 10, 0
bm, -10, -30, 0, 0
cm, 5, -10, 200, 0
coeffsig, 0.3, 0, 1, 0
dm0, 139.2, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.5, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 2.5, -10, 10, 1
bm, -5, -20, 0, 1
cm, 7, -10, 200, 0
coeffsig, 0.9, 0, 1, 0
dm0, 139.4, 130, 150, 1
mean, 145.5, 138, 159, 1
sigma1, 0.3, 0, 10, 0
sigma2, 0.3, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagUp PARAMs END <====
