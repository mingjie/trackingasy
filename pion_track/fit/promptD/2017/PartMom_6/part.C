#include "READ.h"
using namespace RooFit;

void part(int ibin=0, TString polarity="Down", TString year="2016"){
   double bin[] = {2,5,10,20,30,40,50,100};
   double pmin = bin[ibin];
   double pmax = bin[ibin+1];
   gROOT->ProcessLine(".x ~/lhcbStyle.C");

   auto t = getDirectory();

   TString parafile("../parttempt/PARAMs/PartMom_"+TString::Format("%d.txt",ibin));

   //ReadPara para;
   auto Para = READParameter((char*)parafile.Data(), (string)polarity);

   TChain *chRS = new TChain();
   TChain *chWS = new TChain();
   chRS->Add("/eos/home-m/mingjie/SCA/trackingasy/pion_track/selection/promptD/results/"+year+"Mag"+polarity+"PartRS.root/DecayTree");
   chWS->Add("/eos/home-m/mingjie/SCA/trackingasy/pion_track/selection/promptD/results/"+year+"Mag"+polarity+"PartWS.root/DecayTree");

   TString cutKine = "(PMiss_Fit/1000<100&&PMiss_Fit/PMiss_Err>2.5&&PtMiss_Fit/PtMiss_Err>2)&&";
   TString cutPRS = cutKine+"((PiS_ID==211&&pi1_ID*pi2_ID<0)||(PiS_ID==-211&&pi1_ID*pi2_ID>0))"+TString::Format("&&PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g",pmin,pmax),
	     cutMRS = cutKine+"((PiS_ID==-211&&pi1_ID*pi2_ID<0)||(PiS_ID==211&&pi1_ID*pi2_ID>0))"+TString::Format("&&PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g",pmin,pmax),
	     cutPWS = cutKine+"(PiS_ID==-211)"+TString::Format("&&PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g",pmin,pmax),
	     cutMWS = cutKine+"(PiS_ID==211)"+TString::Format("&&PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g",pmin,pmax);

   double xmin(139), xmax(174);
   double binWidth = 0.5;
   const int nBin = (int) ((xmax-xmin)/binWidth);
   RooRealVar x("x","#DeltaM(MeV)",xmin,xmax);
   x.setBins(nBin);

   TH1F *hPRS = new TH1F("hPRS","hPRS",nBin,xmin,xmax);
   TH1F *hMRS = new TH1F("hMRS","hMRS",nBin,xmin,xmax);
   TH1F *hPWS = new TH1F("hPWS","hPWS",nBin,xmin,xmax);
   TH1F *hMWS = new TH1F("hMWS","hMWS",nBin,xmin,xmax);

   TCanvas *c1 = new TCanvas("c1","c1");
   chRS->Draw("Dst_MM-D0_MM>>hPRS",cutPRS);
   chRS->Draw("Dst_MM-D0_MM>>hMRS",cutMRS);
   chWS->Draw("Dst_MM-D0_MM>>hPWS",cutPWS);
   chWS->Draw("Dst_MM-D0_MM>>hMWS",cutMWS);
   std::cout<<"====>"<<chRS->GetEntries(cutPRS)<<std::endl;

   RooDataHist dataPRS("dataPRS", "dataPRS", x, hPRS),
		   dataMRS("dataMRS", "dataMRS", x, hMRS),
		   dataPWS("dataPWS", "dataPWS", x, hPWS),
		   dataMWS("dataMWS", "dataMWS", x, hMWS);

   RooRealVar NsigPRS("NsigPRS","NsigPRS",0.74*hPRS->Integral(),0,hPRS->Integral()),
		  NsigMRS("NsigMRS","NsigMRS",0.74*hMRS->Integral(),0,hMRS->Integral()),
		  NbkgPRS("NbkgPRS","NbkgPRS",0.26*hPRS->Integral(),0,hPRS->Integral()),
		  NbkgMRS("NbkgMRS","NbkgMRS",0.26*hMRS->Integral(),0,hMRS->Integral()),
		  NbkgPWS("NbkgPWS","NbkgPWS",hPWS->Integral(),0.8*hPWS->Integral(),1.2*hPWS->Integral()),
		  NbkgMWS("NbkgMWS","NbkgMWS",hMWS->Integral(),0.8*hPWS->Integral(),1.2*hMWS->Integral());

   RooRealVar mean("mean","mean",Para.value("mean",0),Para.value("mean",1),Para.value("mean",2)),
		  sigma("sigma","sigma",Para.value("sigma",0),Para.value("sigma",1),Para.value("sigma",2)),
		  sigma2("sigma2","sigma2",Para.value("sigma2",0),Para.value("sigma2",1),Para.value("sigma2",2));
   mean.setConstant(Para.value("mean",3));
   sigma.setConstant(Para.value("sigma",3));
   sigma2.setConstant(Para.value("sigma2",3));

   RooBifurGauss* gaubifur = new RooBifurGauss("gaubifur","",x,mean,sigma,sigma2);

   RooRealVar acc_p("acc_p","acc_p",Para.value("acc_p",0),Para.value("acc_p",1),Para.value("acc_p",2)),
		  acc_s("acc_s","acc_s",Para.value("acc_s",0),Para.value("acc_s",1),Para.value("acc_s",2)),
		  offset("offset","offset",Para.value("offset",0),Para.value("offset",1),Para.value("offset",2));
   acc_p.setConstant(Para.value("acc_p",3));
   acc_s.setConstant(Para.value("acc_s",3));
   offset.setConstant(Para.value("offset",3));
   RooAbsReal *acc =  new RooFormulaVar("acc","(@0-@3<0) ? 0 : (abs((@0-@3)*@1)^@2/(1.0+abs((@0-@3)*@1)^@2))",RooArgList(x,acc_s,acc_p,offset));

   RooAbsPdf *modelSig = new RooEffProd("modelSig","modelSig",*gaubifur,*acc);

   ifstream input("results/ws_para"+polarity+".txt");
   double para[10];
   for (int i=0; i<7; i++){
	input>>para[i];
   }
   //parameters for DstD0BG
   RooRealVar dm0("dm0","dm0",para[3]),
		  am("am","am",para[0]),
		  bm("bm","bm",para[1]),
		  cm("cm","cm",para[2]);

   //parameters for Eff func
   RooRealVar poly_a("poly_a","poly_a",para[4]),
		  poly_b("poly_b","poly_b",para[5]),
		  poly_c("poly_c","poly_c",para[6]);

   //P.D.F.
   RooDstD0BG pdfD0BG("pdfD0BG","DstD0BKG",x,dm0,cm,am,bm);
   RooChebychev poly("poly","", x, RooArgList(poly_a,poly_b,poly_c));

   RooProdPdf modelBkg("modelBkg", "", pdfD0BG, poly);
   //pdfMWS("pdfMWS", "", pdfD0BG, poly);

   RooAbsPdf *modelPWS = new RooAddPdf("modelPWS","modelPWS",RooArgList(modelBkg),RooArgList(NbkgPWS));
   RooAbsPdf *modelMWS = new RooAddPdf("modelMWS","modelMWS",RooArgList(modelBkg),RooArgList(NbkgMWS));

   RooAbsPdf *modelPRS = new RooAddPdf("modelPRS","modelPRS",RooArgList(*modelSig,modelBkg),RooArgList(NsigPRS,NbkgPRS));
   RooAbsPdf *modelMRS = new RooAddPdf("modelMRS","modelMRS",RooArgList(*modelSig,modelBkg),RooArgList(NsigMRS,NbkgMRS));

   RooChi2Var *chi2PRS = new RooChi2Var("chi2PRS", "chi2PRS", *modelPRS, dataPRS, RooFit::Extended(kTRUE));
   RooChi2Var *chi2MRS = new RooChi2Var("chi2MRS", "chi2MRS", *modelMRS, dataMRS, RooFit::Extended(kTRUE));

   RooAbsReal *nllDataPRS = modelPRS->createNLL(dataPRS,Extended());
   RooAbsReal *nllDataMRS = modelMRS->createNLL(dataMRS,Extended());
   RooAddition *nll = new RooAddition("nll","nll",RooArgSet(*nllDataPRS,*nllDataMRS));

   RooMinuit m(*nll);
   m.setVerbose(kFALSE);
   int migradstatus = m.migrad();
   int hessestatus = m.hesse();

   RooFitResult* fitResult = m.save();
   RooArgList fitParams = fitResult->floatParsFinal();
   int paraSize = fitParams.getSize();
   int ndf = nBin-paraSize;

   double chi2PRSVal = chi2PRS->getVal();
   double chi2MRSVal = chi2MRS->getVal();
   std::cout << ndf << " ; " << chi2PRSVal/ndf << " ; " << chi2MRSVal/ndf << std::endl;

   ofstream outAFit("../part"+year+"FIT.txt",std::ios::app);
   outAFit<<year<<polarity<< ibin << "\t" <<migradstatus<<"\t"<<hessestatus<<"\t"<< "chi2PRS: " << chi2PRSVal/ndf<< "\t" << " chi2MRS: " << chi2MRSVal/ndf << std::endl;

   ofstream outFit(Form("results/"+year+polarity+"FIT__%d.txt", ibin));
   outFit<<migradstatus<<"\t"<<hessestatus<<std::endl;

   RooPlot *MRSframe = x.frame();
   dataMRS.plotOn(MRSframe, Name("dataMRS"), MarkerSize(0.8));
   modelMRS->plotOn(MRSframe, Components("modelBkg"), LineColor(8));
   modelMRS->plotOn(MRSframe, Components("modelSig"), LineColor(2));
   modelMRS->plotOn(MRSframe, Name("modelMRS"), LineColor(4));

   RooHist* hMRSpull=MRSframe->pullHist();
   hMRSpull->SetFillStyle(3001);
   RooPlot* MRSpull=x.frame();
   MRSpull->addPlotable(hMRSpull,"l3");
   MRSpull->SetTitle("");
   MRSpull->GetYaxis()->SetLabelSize(0.20);
   MRSpull->GetYaxis()->SetNdivisions(206);

   RooPlot *PRSframe = x.frame();
   dataPRS.plotOn(PRSframe, Name("dataPRS"), MarkerSize(0.8));
   modelPRS->plotOn(PRSframe, Components("modelBkg"), LineColor(8));
   modelPRS->plotOn(PRSframe, Components("modelSig"), LineColor(2));
   modelPRS->plotOn(PRSframe, Name("modelPRS"), LineColor(4));

   double chi2_1 = PRSframe->chiSquare("modelPRS","dataPRS",paraSize);
   double chi2_2 = MRSframe->chiSquare("modelMRS","dataMRS",paraSize);
   std::cout << "PRS chi2: " << chi2_1  << " ; MPS chi2: " << chi2_2 << std::endl;
   ofstream outBFit("../part"+year+"FIT2.txt",std::ios::app);
   outBFit<<year<<polarity<< ibin << "\t" << migradstatus << "\t" << hessestatus << "\t" <<" chi2PRS: "<<chi2_1<<"\t"<< " chi2MRS: " << chi2_2 << std::endl;

   RooHist* hPRSpull=PRSframe->pullHist();
   hPRSpull->SetFillStyle(3001);
   RooPlot* PRSpull=x.frame();
   PRSpull->addPlotable(hPRSpull,"l3");
   PRSpull->SetTitle("");
   PRSpull->GetYaxis()->SetLabelSize(0.20);
   PRSpull->GetYaxis()->SetNdivisions(206);

#if 0
   RooPlot *MWSframe = x.frame();
   dataMWS.plotOn(MWSframe, MarkerSize(0.8));
   modelMWS->plotOn(MWSframe, LineColor(8));

   RooHist* hMWSpull=MWSframe->pullHist();
   hMWSpull->SetFillStyle(3001);
   RooPlot* MWSpull=x.frame();
   MWSpull->addPlotable(hMWSpull,"l3");
   MWSpull->SetTitle("");
   MWSpull->GetYaxis()->SetLabelSize(0.20);
   MWSpull->GetYaxis()->SetNdivisions(206);

   RooPlot *PWSframe = x.frame();
   dataPWS.plotOn(PWSframe, Name("data_fit"), MarkerSize(0.8));
   modelPWS->plotOn(PWSframe, Name("data_all"),LineColor(8));

   RooHist* hPWSpull=PWSframe->pullHist();
   hPWSpull->SetFillStyle(3001);
   RooPlot* PWSpull=x.frame();
   PWSpull->addPlotable(hPWSpull,"l3");
   PWSpull->SetTitle("");
   PWSpull->GetYaxis()->SetLabelSize(0.20);
   PWSpull->GetYaxis()->SetNdivisions(206);

   gROOT->Reset();
   TCanvas *c2 = new TCanvas("c2","c2");
   c2->Divide(1,2,0,0,0);
   c2->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   MWSframe->Draw();

   c2->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   MWSpull->Draw();

   TCanvas *c3 = new TCanvas("c3","c3");
   c3->Divide(1,2,0,0,0);
   c3->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   PWSframe->Draw();

   c3->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   PWSpull->Draw();
#endif

   TCanvas *c4 = new TCanvas("c4","c4");
   c4->Divide(1,2,0,0,0);
   c4->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   TLatex *TeXM = new TLatex(0.7,0.8, "#splitline{#pi^{-} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{p_{Infer}#in(%d,%d) GeV}}",
		(int)pmin,(int)pmax));
   TeXM->SetNDC();
   TeXM->SetTextSize(0.07);
   MRSframe->addObject(TeXM);
   MRSframe->Draw();

   c4->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   MRSpull->Draw();

   TCanvas *c5 = new TCanvas("c5","c5");
   c5->Divide(1,2,0,0,0);
   c5->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   TLatex *TeXP = new TLatex(0.7,0.8, "#splitline{#pi^{+} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{p_{Infer}#in(%d,%d) GeV}}",
		(int)pmin,(int)pmax));
   TeXP->SetNDC();
   TeXP->SetTextSize(0.07);
   PRSframe->addObject(TeXP);
   PRSframe->Draw();

   c5->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   PRSpull->Draw();

#if 0
   c2->Print("results/ws_part_"+polarity+"_PIM.pdf");
   c3->Print("results/ws_part_"+polarity+"_PIP.pdf");
#endif
   c4->Print(TString(t.FIG)+"part_"+year+"_"+polarity+Form("_Bin%d",ibin)+"_PIM_promptD.pdf");
   c5->Print(TString(t.FIG)+"part_"+year+"_"+polarity+Form("_Bin%d",ibin)+"_PIP_promptD.pdf");
   c4->Print("results/part_"+polarity+"_PIM.pdf");
   c5->Print("results/part_"+polarity+"_PIP.pdf");

   ofstream outfile("results/para"+polarity+".txt");
   outfile<<NbkgMRS.getVal()<< "\t";
   outfile<<NbkgPRS.getVal()<< "\t";
   outfile<<NsigMRS.getVal()<< "\t";
   outfile<<NsigPRS.getVal()<< "\t";
   outfile<<acc_p.getVal()<< "\t";
   outfile<<acc_s.getVal()<< "\t";
   outfile<<mean.getVal()<< "\t";
   outfile<<offset.getVal()<< "\t";
   outfile<<sigma.getVal()<< "\t";
   outfile<<sigma2.getVal()<< "\t";

}
