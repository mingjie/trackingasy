MagDown PARAMs START ====>
mean, 143, 139, 148, 0
sigma, 3.3, 0.5, 10, 0
sigma2, 4.3, 2, 9, 0
acc_p, 4, 0.1, 5.5, 0
acc_s, 0.4, -5.5, 5.5, 0
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 1.88, -2, 10, 0
bm, -3.7, -20, 10, 0
cm, 37, 30, 45, 0
poly_a, -0.76, -1, 1, 0
poly_b, 0.48, -1, 1, 0
poly_c, -0.18, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 144, 139, 148, 0
sigma, 2.6, 0.5, 10, 0
sigma2, 5.4, 2, 9, 0
acc_p, 0.5, 0.1, 5.5, 0
acc_s, 0.001, -5.5, 5.5, 1
offset, 138, 130, 150, 1
dm0, 139, 130, 145, 0
am, 1.88, -2, 10, 0
bm, -3.7, -20, 10, 0
cm, 37, 30, 45, 0
poly_a, -0.76, -1, 1, 0
poly_b, 0.48, -1, 1, 0
poly_c, -0.18, -1, 1, 0
MagUp PARAMs END <====
