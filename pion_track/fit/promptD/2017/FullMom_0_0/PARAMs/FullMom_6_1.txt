MagDown PARAMs START ====>
am, 1, -10, 10, 0
bm, -0.8, -20, 0, 0
cm, 2, -10, 200, 0
coeffsig, 0.5, 0, 1, 0
dm0, 138.5, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.3, 0, 10, 0
sigma2, 0.3, 0, 10, 0
sigma3, 1.5, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 1, -10, 20, 0
bm, -1.1, -25, 0, 0
cm, 5, -10, 200, 0
coeffsig, 0.9, 0, 1, 0
dm0, 138.6, 130, 150, 1
mean, 145.5, 138, 159, 1
sigma1, 0.6, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.0, 0, 10, 0
MagUp PARAMs END <====
