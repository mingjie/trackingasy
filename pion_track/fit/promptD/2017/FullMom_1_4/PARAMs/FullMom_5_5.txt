MagDown PARAMs START ====>
am, 3.5, -20, 10, 0
bm, -6.2, -20, 0, 0
cm, 4, -10, 200, 0
coeffsig, 0.9, 0, 1, 1
dm0, 139.8, 130, 150, 1
mean, 145.5, 138, 159, 1
sigma1, 0.3, 0, 10, 0
sigma2, 0.3, 0, 10, 0
sigma3, 1.0, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, -9.18, -30, 10, 0
bm, -1.1, -20, 0, 0
cm, 16, -10, 200, 0
coeffsig, 0.6, 0, 1, 0
dm0, 139.2, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.56, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.27, 0, 10, 0
MagUp PARAMs END <====
