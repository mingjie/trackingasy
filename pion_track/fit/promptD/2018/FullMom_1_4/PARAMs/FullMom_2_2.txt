MagDown PARAMs START ====>
am, 1, -10, 10, 0
bm, 0.8, -20, 0, 0
cm, 5, -10, 200, 0
coeffsig, 0.5, 0, 1, 0
dm0, 138.8, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.4, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 3.5, -10, 20, 0
bm, -7.5, -30, 0, 0
cm, 3.1, -10, 200, 0
coeffsig, 0.7, 0, 1, 1
dm0, 140, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagUp PARAMs END <====
