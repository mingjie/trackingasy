MagDown PARAMs START ====>
am, 1, -10, 10, 0
bm, -1, -20, 0, 0
cm, 5, -10, 200, 0
coeffsig, 0.7, 0, 1, 0
dm0, 139.2, 130, 150, 0
mean, 145.6, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 1, -10, 10, 0
bm, -1, -20, 0, 0
cm, 5, -10, 200, 0
coeffsig, 0.5, 0, 1, 0
dm0, 139.2, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagUp PARAMs END <====
