MagDown PARAMs START ====>
am, 2.7, -10, 10, 0
bm, -5.9, -20, 0, 0
cm, 3.7, -10, 200, 0
coeffsig, 0.1, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 1.2, 0, 10, 1
sigma2, 0.3, 0, 10, 1
sigma3, 1.1, 0, 10, 1
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 3.6, -10, 10, 0
bm, -7.4, -20, 0, 0
cm, 4.8, -10, 200, 0
coeffsig, 0.15, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.45, 0, 10, 0
sigma2, 0.57, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagUp PARAMs END <====
