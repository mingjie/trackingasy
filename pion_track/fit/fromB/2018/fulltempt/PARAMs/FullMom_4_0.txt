MagDown PARAMs START ====>
am, 8, -10, 10, 0
bm, -19, -20, 0, 0
cm, 2, -10, 200, 0
coeffsig, 0.4, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.8, 0, 10, 0
sigma2, 0.2, 0, 10, 0
sigma3, 0.9, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 2, -10, 10, 0
bm, -6, -20, 0, 0
cm, 1, -10, 200, 0
coeffsig, 0.4, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.7, 0, 10, 0
sigma2, 0.1, 0, 10, 0
sigma3, 1.0, 0, 10, 0
MagUp PARAMs END <====
