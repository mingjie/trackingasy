MagDown PARAMs START ====>
am, -5, -10, 10, 0
bm, -4, -20, 0, 0
cm, 1, -10, 200, 0
coeffsig, 0.6, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 6.5, -10, 10, 0
bm, -14, -20, 0, 0
cm, 0.1, -10, 200, 0
coeffsig, 0.6, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.7, 0, 10, 0
sigma2, 0.9, 0, 10, 0
sigma3, 1.5, 0, 10, 0
MagUp PARAMs END <====
