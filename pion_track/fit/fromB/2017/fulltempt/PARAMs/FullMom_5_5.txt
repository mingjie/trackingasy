MagDown PARAMs START ====>
am, -4, -50, 10, 0
bm, -3, -20, 0, 0
cm, 16, -10, 200, 0
coeffsig, 0.8, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.6, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.4, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 2, -50, 10, 0
bm, -5, -20, 0, 0
cm, 3, -10, 200, 0
coeffsig, 0.6, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.4, 0, 10, 0
sigma3, 0.9, 0, 10, 0
MagUp PARAMs END <====
