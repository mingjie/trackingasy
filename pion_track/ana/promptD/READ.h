#include <iostream>
#include <string>
#include <cmath>
#include "stdlib.h"
#pragma once

struct dir{
   std::string FIG;
   std::string TAB;
   std::string PWD;
};

dir getDirectory(){
   dir t;
   std::string AnaName = "SCA";
   char *buffer;
   if((buffer = getcwd(NULL,0)) == NULL){
	perror("getcwd error"); return t;
   } else {
	std::string tmpdir = buffer;
	TString tt(tmpdir);
	t.PWD = tmpdir.substr(0,tmpdir.find(AnaName)+4);
	t.FIG = t.PWD+"/FIGURES/";
	t.TAB = t.PWD+"/TABLES/";
	return t;
   }

}

