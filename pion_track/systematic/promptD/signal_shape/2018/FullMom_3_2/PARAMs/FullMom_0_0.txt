MagDown PARAMs START ====>
am, 1, -10, 10, 0
bm, -1, -20, 0, 0
cm, 5, -10, 200, 1
coeffsig, 0.1, 0, 1, 0
dm0, 139.4, 130, 150, 1
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.4, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 2.5, -10, 10, 0
bm, -5.2, -20, 0, 0
cm, 26, -10, 200, 1
coeffsig, 0.5, 0, 1, 0
dm0, 139.4, 130, 150, 1
mean, 140.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.4, 0, 10, 0
MagUp PARAMs END <====
