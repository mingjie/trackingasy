MagDown PARAMs START ====>
mean, 141, 139, 148, 0
sigma, 2.4, 0.5, 4, 0
sigma2, 6.2, 6, 9, 0
acc_p, 1.1, 0.1, 1.5, 0
acc_s, 0.1, 0.1, 1.5, 0
offset, 138, 130, 150, 0
dm0, 137, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 5, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 141.5, 139, 148, 0
sigma, 3.3, 0.5, 4, 0
sigma2, 7.2, 1, 9, 0
acc_p, 1.3, 0.1, 1.5, 0
acc_s, 0.1, 0.1, 1.5, 0
offset, 138, 130, 150, 0
dm0, 137, 130, 145, 0
am, 5, -2, 10, 0
bm, -8, -20, 10, 0
cm, 5, -10, 10, 0
poly_a, 0.2, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagUp PARAMs END <====
