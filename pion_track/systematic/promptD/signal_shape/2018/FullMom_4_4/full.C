#include "MyStudent.cxx"
#include "MyCB.cxx"
#include "READ.h"

using namespace RooFit;

void full(int ibin=0, int jbin=0, TString polarity="Down", TString year="2016"){
   double bin[] = {2,5,10,20,30,40,50,100};
   double ipmin = bin[ibin], ipmax = bin[ibin+1];
   double jpmin = bin[jbin], jpmax = bin[jbin+1];
   gROOT->ProcessLine(".x ~/lhcbStyle.C");

   auto t = getDirectory();

   TString parafile("../fulltempt/PARAMs/FullMom_"+TString::Format("%d_%d.txt",ibin,jbin));

   auto Para = READParameter((char*)parafile.Data(), (string)polarity);

   cout<<"../../../../../selection/promptD/results/"+year+"Mag"+polarity+"FullRS.root/DecayTree"<<"\n";

#if 1
   TChain *chRS = new TChain();
   chRS->Add("/eos/home-m/mingjie/SCA/trackingasy/pion_track/selection/promptD/results/"+year+"Mag"+polarity+"FullRS.root/DecayTree");

   TString cutKine = "(PMiss_Fit/1000<100&&PMiss_Fit/PMiss_Err>2.5&&PtMiss_Fit/PtMiss_Err>2)&&";
    TString cutPRS = cutKine+"((PiS_ID==211&&pi1_ID*pi2_ID<0)||(PiS_ID==-211&&pi1_ID*pi2_ID>0))"+TString::Format("&&(PiMiss_P/1000>%g&&PiMiss_P/1000<%g)",ipmin,ipmax)+TString::Format("&&(PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g)",jpmin,jpmax),
	      cutMRS = cutKine+"((PiS_ID==-211&&pi1_ID*pi2_ID<0)||(PiS_ID==211&&pi1_ID*pi2_ID>0))"+TString::Format("&&(PiMiss_P/1000>%g&&PiMiss_P/1000<%g)",ipmin,ipmax)+TString::Format("&&(PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g)",jpmin,jpmax);

   double xmin(138), xmax(159);
   double binWidth = 0.25;
   const int nBin = (int) ((xmax-xmin)/binWidth);
   RooRealVar x("x","#DeltaM(MeV)",xmin,xmax);
   x.setBins(nBin);

   TH1F *hPRS = new TH1F("hPRS","hPRS",nBin,xmin,xmax);
   TH1F *hMRS = new TH1F("hMRS","hMRS",nBin,xmin,xmax);

   TCanvas *c1 = new TCanvas("c1","c1");
   chRS->Draw("Dst_MM-D0_MM>>hPRS",cutPRS);
   chRS->Draw("Dst_MM-D0_MM>>hMRS",cutMRS);
   RooDataHist dataPRS("dataPRS", "dataPRS", x, hPRS),
		   dataMRS("dataMRS", "dataMRS", x, hMRS);

   RooRealVar mean("mean","mean",Para.value("mean",0),Para.value("mean",1),Para.value("mean",2)),
		  sigma1("sigma1","sigma1",Para.value("sigma1",0),Para.value("sigma1",1),Para.value("sigma1",2)),
		  sigma2("sigma2","sigma2",Para.value("sigma2",0),Para.value("sigma2",1),Para.value("sigma2",2)),
		  sigma3("sigma3","sigma3",Para.value("sigma3",0),Para.value("sigma3",1),Para.value("sigma3",2)),
		  coeffsig("coeffsig","coeffsig",Para.value("coeffsig",0),Para.value("coeffsig",1),Para.value("coeffsig",2)),
		  dm0("dm0","dm0",Para.value("dm0",0),Para.value("dm0",1),Para.value("dm0",2)),
		  am("am","am",Para.value("am",0),Para.value("am",1),Para.value("am",2)),
		  bm("bm","bm",Para.value("bm",0),Para.value("bm",1),Para.value("bm",2)),
		  cm("cm","cm",Para.value("cm",0),Para.value("cm",1),Para.value("cm",2));
   mean.setConstant(Para.value("mean",3));
   sigma1.setConstant(Para.value("sigma1",3));
   sigma2.setConstant(Para.value("sigma2",3));
   sigma3.setConstant(Para.value("sigma3",3));
   coeffsig.setConstant(Para.value("coeffsig",3));
   dm0.setConstant(Para.value("dm0",3));
   am.setConstant(Para.value("am",3));
   bm.setConstant(Para.value("bm",3));
   cm.setConstant(Para.value("cm",3));

   RooBifurGauss *sig_pdf1 = new RooBifurGauss("sig_pdf1","sig_pdf1",x,mean,sigma1,sigma2);
   RooAbsPdf *sig_pdf2 = new RooGaussian("sig_pdf2","sig_pdf2",x,mean,sigma3);

   RooAbsPdf* modelSig_default= new RooAddPdf("modelSig_default","modelSig_default",RooArgList(*sig_pdf1,*sig_pdf2),RooArgList(coeffsig));

   RooDstD0BG* modelBkg_default= new RooDstD0BG("modelBkg_default","modelBkg_default",x,dm0,cm,am,bm);


   RooRealVar NsigPRS_d("NsigPRS_d","NsigPRS_d",0.9*hPRS->Integral(),0,hPRS->Integral()),
		  NbkgPRS_d("NbkgPRS_d","NbkgPRS_d",0.1*hPRS->Integral(),0,hPRS->Integral()),
		  NsigMRS_d("NsigMRS_d","NsigMRS_d",0.9*hMRS->Integral(),0,hMRS->Integral()),
		  NbkgMRS_d("NbkgMRS_d","NbkgMRS_d",0.1*hMRS->Integral(),0,hMRS->Integral());

   RooAbsPdf *modelPRS_d = new RooAddPdf("modelPRS_d","modelPRS_d",RooArgList(*modelSig_default,*modelBkg_default),RooArgList(NsigPRS_d,NbkgPRS_d));
   RooAbsPdf *modelMRS_d = new RooAddPdf("modelMRS_d","modelMRS_d",RooArgList(*modelSig_default,*modelBkg_default),RooArgList(NsigMRS_d,NbkgMRS_d));

   RooAbsReal *nllDataPRS_d = modelPRS_d->createNLL(dataPRS,Extended());
   RooAbsReal *nllDataMRS_d = modelMRS_d->createNLL(dataMRS,Extended());
   RooAddition *nll_d = new RooAddition("nll_d","nll_d",RooArgSet(*nllDataPRS_d,*nllDataMRS_d));

   RooMinuit m_d(*nll_d);
   m_d.setVerbose(kFALSE);
   int migradstatus_d = m_d.migrad();
   int hessestatus_d = m_d.hesse();

   RooDataSet* all_data = (RooDataSet*)dataPRS.Clone();

   RooDataSet* sampled_data = modelBkg_default->generate(x, static_cast<int>(NbkgPRS_d.getVal()));
   RooDataHist* sampled_hist = sampled_data->binnedClone();

   RooDataHist all_hist("all_hist", "Signal Data Hist", RooArgSet(x), *all_data);
   RooDataHist* signal_hist = new RooDataHist("signal_hist", "Difference Hist", RooArgSet(x));

   for (Int_t i = 0; i < all_hist.numEntries(); ++i) {
     RooArgSet* all_bin = (RooArgSet*)all_hist.get(i);
     RooArgSet* sampled_bin = (RooArgSet*)sampled_hist->get(i);
     Double_t all_val = all_hist.weight(*all_bin);
     Double_t sampled_val = sampled_hist->weight(*sampled_bin);

     std::cout << "aaaa" << i << " ; " << all_val<< " ; " << sampled_val << std::endl;
     Double_t diff = all_val-sampled_val;
     RooArgSet signal_bin(x);
     signal_hist->add(*all_bin, diff);
   }
   RooHistPdf *modelSig = new RooHistPdf("modelSig","modelSig",x,*signal_hist,0);

   RooPlot *sigframe = x.frame();
   signal_hist->plotOn(sigframe);
   TCanvas *cc = new TCanvas("cc", "cc");
   sigframe->Draw();
   cc->SaveAs("results/signalShape.pdf");

   RooPlot *bkgframe = x.frame();
   sampled_hist->plotOn(bkgframe);
   TCanvas *cc1 = new TCanvas("cc1", "cc1");
   bkgframe->Draw();
   cc1->SaveAs("results/bkgShape.pdf");

   RooDstD0BG* modelBkg= new RooDstD0BG("modelBkg","modelBkg",x,dm0,cm,am,bm);

   RooRealVar NsigPRS("NsigPRS","NsigPRS",NsigPRS_d.getVal(),NsigPRS_d.getVal()-100, NsigPRS_d.getVal()+100),
                  NbkgPRS("NbkgPRS","NbkgPRS",NbkgPRS_d.getVal(),NbkgPRS_d.getVal()-100,NbkgPRS_d.getVal()+100),
                  NsigMRS("NsigMRS","NsigMRS",NsigMRS_d.getVal(),NsigMRS_d.getVal()-100,NsigMRS_d.getVal()+100),
                  NbkgMRS("NbkgMRS","NbkgMRS",NbkgMRS_d.getVal(),NbkgMRS_d.getVal()-100,NbkgMRS_d.getVal()+100);

   RooAbsPdf *modelPRS = new RooAddPdf("modelPRS","modelPRS",RooArgList(*modelSig,*modelBkg),RooArgList(NsigPRS,NbkgPRS));
   RooAbsPdf *modelMRS = new RooAddPdf("modelMRS","modelMRS",RooArgList(*modelSig,*modelBkg),RooArgList(NsigMRS,NbkgMRS));

   RooChi2Var *chi2PRS = new RooChi2Var("chi2PRS", "chi2PRS", *modelPRS, dataPRS, RooFit::Extended(kTRUE));
   RooChi2Var *chi2MRS = new RooChi2Var("chi2MRS", "chi2MRS", *modelMRS, dataMRS, RooFit::Extended(kTRUE));

   RooAbsReal *nllDataPRS = modelPRS->createNLL(dataPRS,Extended());
   RooAbsReal *nllDataMRS = modelMRS->createNLL(dataMRS,Extended());
   RooAddition *nll = new RooAddition("nll","nll",RooArgSet(*nllDataPRS,*nllDataMRS));

   RooMinuit m(*nll);
   m.setVerbose(kFALSE);
   int migradstatus = m.migrad();
   int hessestatus = m.hesse();

   RooFitResult* fitResult = m.save();
   RooArgList fitParams = fitResult->floatParsFinal();
   int paraSize = fitParams.getSize();
   int ndf = nBin-paraSize;

   double chi2PRSVal = chi2PRS->getVal();
   double chi2MRSVal = chi2MRS->getVal();
   std::cout << ndf << " ; " << chi2PRSVal/ndf << " ; " << chi2MRSVal/ndf << std::endl;

   ofstream outAFit("../full"+year+"FIT.txt",std::ios::app);
   outAFit<<year<<polarity<<ibin<<"-"<<jbin<< "\t" << migradstatus<<"\t"<<hessestatus<<"\t" << " chi2PRS: " << chi2PRSVal/ndf <<"\t"<< " chi2MRS: " << chi2MRSVal/ndf << std::endl;

   ofstream outFit(Form("results/"+year+polarity+"FIT__%d_%d.txt", ibin,jbin));
   outFit<<migradstatus<<"\t"<<hessestatus<<std::endl;

   RooPlot *MRSframe = x.frame();
   dataMRS.plotOn(MRSframe, Name("dataMRS"), MarkerSize(0.8));
   modelMRS->plotOn(MRSframe, Components("modelBkg"), LineColor(8));
   modelMRS->plotOn(MRSframe, Components("modelSig"), LineColor(2));
   modelMRS->plotOn(MRSframe, Name("modelMRS"), LineColor(4));

   RooHist* hMRSpull=MRSframe->pullHist();
   hMRSpull->SetFillStyle(3001);
   RooPlot* MRSpull=x.frame();
   MRSpull->addPlotable(hMRSpull,"l3");
   MRSpull->SetTitle("");
   MRSpull->GetYaxis()->SetLabelSize(0.20);
   MRSpull->GetYaxis()->SetNdivisions(206);

   RooPlot *PRSframe = x.frame();
   dataPRS.plotOn(PRSframe, Name("dataPRS"), MarkerSize(0.8));
   modelPRS->plotOn(PRSframe, Components("modelBkg"), LineColor(8));
   modelPRS->plotOn(PRSframe, Components("modelSig"), LineColor(2));
   modelPRS->plotOn(PRSframe, Name("modelPRS"), LineColor(4));
 
   double chi2_1 = PRSframe->chiSquare("modelPRS","dataPRS",paraSize);
   double chi2_2 = MRSframe->chiSquare("modelMRS","dataMRS",paraSize);
   std::cout << "PRS chi2: " << chi2_1  << " ; MPS chi2: " << chi2_2 << std::endl;
   ofstream outBFit("../full"+year+"FIT2.txt",std::ios::app);
   outBFit<<year<<polarity<<ibin<<"-"<<jbin<< "\t"<<migradstatus<<"\t"<<hessestatus<<"\t"<<" chi2PRS: "<<chi2_1<<"\t"<< " chi2MRS: " << chi2_2 << std::endl;

   RooHist* hPRSpull=PRSframe->pullHist();
   hPRSpull->SetFillStyle(3001);
   RooPlot* PRSpull=x.frame();
   PRSpull->addPlotable(hPRSpull,"l3");
   PRSpull->SetTitle("");
   PRSpull->GetYaxis()->SetLabelSize(0.20);
   PRSpull->GetYaxis()->SetNdivisions(206);

   gROOT->Reset();
   TCanvas *c4 = new TCanvas("c4","c4");
   c4->Divide(1,2,0,0,0);
   c4->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   gPad->SetLogy();
   /*
   TLatex *TeXM = new TLatex(0.7,0.8, "#splitline{#pi^{-} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{#splitline{p_{Det}#in(%d,%d) GeV}{p_{Infer}#in(%d,%d) GeV}}}",
		(int)ipmin,(int)ipmax,(int)jpmin,(int)jpmax));
   */
   TLatex *TeXM = new TLatex(0.6,0.9, "#splitline{#pi^{-} in " + year + " Mag" + polarity + ", RS Data}"
                                       "{#splitline{p_{Det}#in(" + TString::Format("%d,%d", (int)ipmin, (int)ipmax) + ") GeV}"
                                       "{p_{Infer}#in(" + TString::Format("%d,%d", (int)jpmin, (int)jpmax) + ") GeV}}");
   TeXM->SetNDC();
   TeXM->SetTextSize(0.06);
   MRSframe->addObject(TeXM);
   MRSframe->Draw();

   c4->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   MRSpull->Draw();

   TCanvas *c5 = new TCanvas("c5","c5");
   c5->Divide(1,2,0,0,0);
   c5->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   gPad->SetLogy();
   /*
   TLatex *TeXP = new TLatex(0.7,0.8, "#splitline{#pi^{+} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{#splitline{p_{Det}#in(%d,%d) GeV}{p_{Infer}#in(%d,%d) GeV}}}",
		(int)ipmin,(int)ipmax,(int)jpmin,(int)jpmax));
   */
   TLatex *TeXP = new TLatex(0.6,0.9, "#splitline{#pi^{+} in " + year + " Mag" + polarity + ", RS Data}"
                                       "{#splitline{p_{Det}#in(" + TString::Format("%d,%d", (int)ipmin, (int)ipmax) + ") GeV}"
                                       "{p_{Infer}#in(" + TString::Format("%d,%d", (int)jpmin, (int)jpmax) + ") GeV}}");
   TeXP->SetNDC();
   TeXP->SetNDC();
   TeXP->SetTextSize(0.06);
   PRSframe->addObject(TeXP);
   PRSframe->Draw();

   c5->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   PRSpull->Draw();

   c4->Print(TString(t.FIG)+"syst_signalshape_full_"+year+"_"+polarity+Form("_Bin%d_%d",ibin,jbin)+"_PIM_promptD.pdf");
   c5->Print(TString(t.FIG)+"syst_signalshape_full_"+year+"_"+polarity+Form("_Bin%d_%d",ibin,jbin)+"_PIP_promptD.pdf");
   c4->Print("results/full_"+polarity+"_PIM.pdf");
   c5->Print("results/full_"+polarity+"_PIP.pdf");

   ofstream outfile("results/para_full_"+polarity+".txt");
   outfile<<NbkgMRS.getVal()<< "\t";
   outfile<<NbkgPRS.getVal()<< "\t";
   outfile<<NsigMRS.getVal()<< "\t";
   outfile<<NsigPRS.getVal()<< "\t";
   outfile<<NsigMRS.getVal()<< "\t";
   outfile<<NsigPRS.getVal()<< "\t";
#endif

}

