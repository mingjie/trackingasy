MagDown PARAMs START ====>
am, 1, -10, 10, 0
bm, -1.5, -20, 0, 0
cm, 7, -10, 200, 1
coeffsig, 0.5, 0, 1, 0
dm0, 139.9, 130, 150, 0
mean, 146.0, 138, 159, 0
sigma1, 0.6, 0, 10, 1
sigma2, 0.4, 0, 10, 0
sigma3, 1.2, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 4, -10, 20, 0
bm, -8, -30, 0, 0
cm, 3, -10, 200, 0
coeffsig, 0.8, 0, 1, 0
dm0, 139., 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.3, 0, 10, 0
sigma2, 0.3, 0, 10, 0
sigma3, 1.5, 0, 10, 0
MagUp PARAMs END <====
