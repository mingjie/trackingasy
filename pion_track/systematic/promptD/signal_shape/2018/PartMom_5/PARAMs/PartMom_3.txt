MagDown PARAMs START ====>
mean, 142.6, 139, 148, 0
sigma, 2.2, 0.5, 4, 0
sigma2, 7.0, 6, 9, 0
acc_p, 0.7, 0.0, 1.5, 1
acc_s, 0.1, 0.0, 1.5, 1
offset, 139, 130, 150, 1
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.5, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 143, 139, 148, 0
sigma, 3.3, 0.5, 4, 0
sigma2, 7.0, 6, 9, 0
acc_p, 0.5, 0.0, 3.5, 1
acc_s, 0.1, 0.0, 1.5, 1
offset, 139, 130, 150, 1
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.5, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagUp PARAMs END <====
