MagDown PARAMs START ====>
mean, 143.5, 139, 148, 0
sigma, 3.4, 0.5, 500, 0
sigma2, 5.8, 2, 9, 0
acc_p, 5.3, 0, 7, 0
acc_s, 0.6, 0, 0.5, 0
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 1.11, 0, 10, 0
bm, -3.8, -10, 0, 0
cm, 21, 15, 25, 0
poly_a, -0.4, -1, 0, 0
poly_b, 0.1, -1, 1, 0
poly_c, -0.18, -5, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 143.5, 139, 148, 0
sigma, 3.6, 0.5, 10, 0
sigma2, 5.9, 2, 9, 0
acc_p, 3.8, 0.1, 5.5, 0
acc_s, 0.45, -5.5, 5.5, 0
offset, 136, 130, 150, 1
dm0, 139, 130, 145, 0
am, 1.88, -2, 10, 0
bm, -3.7, -20, 10, 0
cm, 37, 30, 45, 0
poly_a, -0.76, -1, 1, 0
poly_b, 0.48, -1, 1, 0
poly_c, -0.18, -1, 1, 0
MagUp PARAMs END <====
