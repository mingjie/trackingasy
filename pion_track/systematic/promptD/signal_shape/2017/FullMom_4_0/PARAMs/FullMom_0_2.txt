MagDown PARAMs START ====>
am, 11, -20, 20, 0
bm, -21, -50, 50, 0
cm, 1.6, -10, 200, 0
coeffsig, 0.8, 0, 1, 0
dm0, 138.5, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 11, -20, 20, 0
bm, -22, -50, 50, 0
cm, 1.5, -10, 200, 0
coeffsig, 0.6, 0, 1, 0
dm0, 139.2, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.6, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagUp PARAMs END <====
