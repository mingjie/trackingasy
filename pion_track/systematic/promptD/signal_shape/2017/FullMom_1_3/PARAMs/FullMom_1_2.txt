MagDown PARAMs START ====>
am, 11, -10, 10, 0
bm, -22, -20, 0, 0
cm, 1.3, -10, 200, 0
coeffsig, 0.3, 0, 1, 0
dm0, 139, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.6, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.3, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 2, -10, 10, 0
bm, -3.5, -20, 0, 0
cm, 4, -10, 200, 0
coeffsig, 0.4, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.4, 0, 10, 0
MagUp PARAMs END <====
