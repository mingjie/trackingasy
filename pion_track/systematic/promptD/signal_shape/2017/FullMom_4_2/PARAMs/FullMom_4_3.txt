MagDown PARAMs START ====>
am, 3, -10, 10, 0
bm, -2.3, -20, 0, 0
cm, 6, -10, 200, 0
coeffsig, 0.8, 0, 1, 0
dm0, 139.6, 130, 150, 1
mean, 145.5, 138, 159, 1
sigma1, 0.6, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 1.5, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 1, -10, 10, 0
bm, -1.5, -20, 0, 0
cm, 4, -10, 200, 0
coeffsig, 0.66, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.7, 138, 159, 0
sigma1, 0.6, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.4, 0, 10, 0
MagUp PARAMs END <====
