MagDown PARAMs START ====>
am, 3.7, -20, 20, 0
bm, -10, -50, 10, 0
cm, 6, -10, 200, 1
coeffsig, 0.7, 0, 1, 0
dm0, 139.7, 130, 150, 1
mean, 145.5, 138, 159, 0
sigma1, 0.7, 0, 10, 0
sigma2, 0.7, 0, 10, 0
sigma3, 1.75, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 2.5, -20, 20, 0
bm, -5, -50, 0, 0
cm, 5, -10, 200, 0
coeffsig, 0.9, 0, 1, 0
dm0, 139.8, 130, 150, 1
mean, 145.5, 138, 159, 0
sigma1, 0.4, 0, 10, 0
sigma2, 0.4, 0, 10, 0
sigma3, 1.2, 0, 10, 0
MagUp PARAMs END <====
