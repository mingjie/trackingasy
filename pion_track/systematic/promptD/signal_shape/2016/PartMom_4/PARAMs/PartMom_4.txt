MagDown PARAMs START ====>
mean, 142, 139, 148, 0
sigma, 2.6, 0.5, 4, 0
sigma2, 5.8, 0, 9, 0
acc_p, 0.8, 0.1, 1.5, 0
acc_s, 0.1, -1, 1.5, 0
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -10, -20, 10, 0
cm, 5, -10, 10, 0
poly_a, 0.2, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 142, 139, 148, 0
sigma, 3.0, 0.5, 6, 0
sigma2, 4.2, 0, 9, 0
acc_p, 0.8, 0.1, 1.5, 0
acc_s, 0.3, -0.1, 1.5, 0
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 5, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagUp PARAMs END <====
