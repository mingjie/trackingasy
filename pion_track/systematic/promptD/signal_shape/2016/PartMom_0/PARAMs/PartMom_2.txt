MagDown PARAMs START ====>
mean, 141, 100, 148, 0
sigma, 3.9, 0, 4, 0
sigma2, 7.1, 0, 20, 0
acc_p, 0.9, 0, 100, 1
acc_s, 0.003, 0., 5.5, 1
offset, 137, 100, 150, 1
dm0, 139, 130, 145, 0
am, 4.6, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.7, -10, 20, 0
poly_a, 0.3, -1, 1, 0
poly_b, -0.04, -1, 1, 0
poly_c, -0.14, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 141, 100, 148, 0
sigma, 3.9, 0, 4, 0
sigma2, 7.1, 0, 20, 0
acc_p, 0.9, 0, 100, 1
acc_s, 0.003, 0., 5.5, 1
offset, 137, 100, 150, 1
dm0, 139, 130, 145, 0
am, 4.6, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.7, -10, 20, 0
poly_a, 0.3, -1, 1, 0
poly_b, -0.04, -1, 1, 0
poly_c, -0.14, -1, 1, 0
MagUp PARAMs END <====
