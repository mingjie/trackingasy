MagDown PARAMs START ====>
am, 2.5, -10, 10, 1
bm, -8.1, -20, 0, 1
cm, 1.2, -10, 200, 1
coeffsig, 0.5, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 1
sigma2, 0.4, 0, 10, 1
sigma3, 1.4, 0, 10, 1
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 2.5, -10, 10, 1
bm, -8.1, -20, 0, 1
cm, 1.2, -10, 200, 1
coeffsig, 0.5, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 1
sigma2, 0.4, 0, 10, 1
sigma3, 1.4, 0, 10, 1
MagUp PARAMs END <====
