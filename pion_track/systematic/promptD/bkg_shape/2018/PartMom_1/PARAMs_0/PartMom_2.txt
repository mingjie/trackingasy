MagDown PARAMs START ====>
mean, 140, 100, 148, 0
sigma, 5.5, 0, 6, 1
sigma2, 11, 0, 20, 1
acc_p, 2.5, 0, 100, 1
acc_s, 0.3, 0.1, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 4.6, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.7, -10, 20, 0
poly_a, 0.3, -1, 1, 0
poly_b, -0.04, -1, 1, 0
poly_c, -0.14, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 139, 100, 148, 0
sigma, 3, 0, 4, 1
sigma2, 13, 0, 20, 1
acc_p, 2.5, 0, 100, 1
acc_s, 0.3, 0.1, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 4.6, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.7, -10, 20, 0
poly_a, 0.3, -1, 1, 0
poly_b, -0.04, -1, 1, 0
poly_c, -0.14, -1, 1, 0
MagUp PARAMs END <====
