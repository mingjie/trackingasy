MagDown PARAMs START ====>
mean, 145, 0, 148, 0
sigma, 3.0, 0, 4, 0
sigma2, 7, 0, 100, 0
acc_p, 1.0, 0, 5.5, 1
acc_s, 0.4, 0, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.5, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 145, 0, 148, 0
sigma, 3.0, 0, 4, 0
sigma2, 7, 0, 100, 0
acc_p, 1.0, 0, 5.5, 1
acc_s, 0.4, 0, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 3, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagUp PARAMs END <====
