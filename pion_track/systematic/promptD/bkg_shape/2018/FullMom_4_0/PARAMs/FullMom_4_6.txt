MagDown PARAMs START ====>
am, 7, -10, 10, 0
bm, -13, -20, 0, 0
cm, 2, -10, 200, 0
coeffsig, 0.9, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.8, 0, 10, 0
sigma2, 0.9, 0, 10, 0
sigma3, 2.7, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 7, -10, 10, 0
bm, -15, -20, 0, 0
cm, 3, -10, 200, 0
coeffsig, 0.8, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.6, 0, 10, 0
sigma2, 0.6, 0, 10, 0
sigma3, 3, 0, 10, 0
MagUp PARAMs END <====
