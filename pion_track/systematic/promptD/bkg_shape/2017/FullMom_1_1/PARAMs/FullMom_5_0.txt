MagDown PARAMs START ====>
am, 9.6, -10, 10, 1
bm, -19, -20, 0, 1
cm, 1, -10, 200, 1
coeffsig, 0.1, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.9, 0, 10, 0
sigma2, 0.9, 0, 10, 0
sigma3, 2.5, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 9.6, -10, 10, 1
bm, -19, -20, 0, 1
cm, 1, -10, 200, 1
coeffsig, 0.1, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.6, 0, 10, 1
sigma2, 0.4, 0, 10, 1
sigma3, 1.3, 0, 10, 1
MagUp PARAMs END <====
