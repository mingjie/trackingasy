MagDown PARAMs START ====>
am, 2, -10, 10, 0
bm, -3.5, -20, 0, 0
cm, 5, -10, 200, 0
coeffsig, 0.1, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, -1.1, -10, 10, 1
bm, -2.4, -20, 0, 1
cm, 8.7, -10, 200, 1
coeffsig, 0.999, 0, 1, 1
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.55, 0, 10, 1
sigma2, 0.56, 0, 10, 1
sigma3, 6.8, 0, 10, 1
MagUp PARAMs END <====
