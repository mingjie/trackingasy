MagDown PARAMs START ====>
mean, 60, 0, 148, 0
sigma, 1.5, 0, 4, 1
sigma2, 25, 0, 100, 0
acc_p, 2.6, 0, 5.5, 1
acc_s, 1.0, 0, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 8, -2, 10, 0
bm, -8, -20, 10, 0
cm, 0.3, -10, 10, 0
poly_a, 0.3, -1, 1, 0
poly_b, -0.08, -1, 1, 0
poly_c, -0.2, -1, 1, 1
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 60, 0, 148, 0
sigma, 1.5, -4, 4, 1
sigma2, 35, 0, 100, 0
acc_p, 2.0, 0, 5.5, 1
acc_s, 2.1, 0, 5.5, 1
offset, 139, 100, 150, 0
dm0, 139, 130, 145, 0
am, 6, -2, 10, 0
bm, -13, -20, 10, 0
cm, 0.9, -10, 10, 0
poly_a, 0.4, -1, 1, 0
poly_b, 0.1, -1, 1, 0
poly_c, -0.2, -1, 1, 1
MagUp PARAMs END <====
