MagDown PARAMs START ====>
am, 6.2, -10, 10, 0
bm, -14.2, -20, 0, 0
cm, 8.8, -10, 200, 0
coeffsig, 0.1, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.25, 0, 10, 1
sigma2, 0.6, 0, 10, 1
sigma3, 1.0, 0, 10, 1
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 6.2, -10, 10, 0
bm, -14.2, -20, 0, 0
cm, 8.8, -10, 200, 0
coeffsig, 0.1, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.25, 0, 10, 1
sigma2, 0.6, 0, 10, 1
sigma3, 1.0, 0, 10, 1
MagUp PARAMs END <====
