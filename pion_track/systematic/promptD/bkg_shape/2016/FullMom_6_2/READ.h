#include <iostream>
#include <string>
#include <cmath>
#include "stdlib.h"
#pragma once

class ReadPara
{
   public:
	using ReadMap = std::map<string, std::vector<double>>;
	ReadPara( ReadMap map ) : m_map{ map} {}
	double value( const string& t, const int ival ) {
	   return m_map.find(t)->second[ival];
	}
   private:
	ReadMap m_map;
};

struct dir{
   std::string FIG;
   std::string TAB;
   std::string PWD;
};

dir getDirectory(){
   dir t;
   std::string AnaName = "SCA";
   char *buffer;
   if((buffer = getcwd(NULL,0)) == NULL){
	perror("getcwd error"); return t;
   } else {
	std::string tmpdir = buffer;
	t.PWD = tmpdir.substr(0,tmpdir.find(AnaName)+4);
	t.FIG = tmpdir.substr(0,tmpdir.find(AnaName)+4)+"/FIGURES/";
	t.TAB = tmpdir.substr(0,tmpdir.find(AnaName)+4)+"/TABLES/";
	return t;
   }

}

ReadPara READParameter(char *filename, string Polarity){

   ReadPara::ReadMap ans;
   TString folderPath(filename);
   std::string templatePath = ((string)folderPath).replace(((string)folderPath).find_last_of("_"),
	   ((string)folderPath).find_last_of(".txt") - 3 - ((string)folderPath).find_last_of("_"),
	   "");
   templatePath = (templatePath).replace(((string)templatePath).find_last_of("_"),
	   ((string)templatePath).find_last_of(".txt") - 3 - ((string)templatePath).find_last_of("_"),
	   "");
   if (system(((string)"ls -d "+(string)folderPath).c_str())!=0){
	system(((string)"ls -d "+(string)folderPath).c_str());
	system(((string)"cp " + templatePath + (string)" " + (string)folderPath).c_str());
   }

   ifstream READFILE;
   READFILE.open(filename,ios::in);

   string temp, varname;
   int nline=0;
   bool isRead = false;

   while(!READFILE.eof()){
	getline(READFILE, temp);
	nline++;
	if (nline>100||temp.length()<1) break;
	if ( temp.find(Polarity)!=string::npos ){
	   if ( temp.find("START")!=string::npos ) isRead = true;
	   if ( temp.find("END")!=string::npos ) isRead = false;
	   continue;
	}
	if ( !isRead ) continue;

	varname = temp.substr(0,temp.find_first_of(","));

	std::vector<double> varvalue; 
	for (int i=0; i<4; i++){
	   temp = temp.substr(temp.find_first_of(",") + 1);
	   if (temp.find(",")==string::npos) varvalue.push_back(atof(temp.c_str()));
	   else {
		varvalue.push_back(atof(temp.substr(0,temp.find_first_of(",")).c_str()));
	   }
	}
	ans.insert(ReadPara::ReadMap::value_type(varname, varvalue));
   }

   return ReadPara{ans};

}
