#include "MyStudent.cxx"
#include "MyCB.cxx"
#include "READ.h"

using namespace RooFit;

void full(int ibin=0, int jbin=0, TString polarity="Down", TString year="2016"){
   double bin[] = {2,5,10,20,30,40,50,100};
   double ipmin_ws= ((ibin==0) ? bin[ibin] : bin[ibin-1]), ipmax_ws = ((ibin==0) ? bin[ibin+2] : bin[ibin+1]);
   double jpmin_ws= ((jbin==0) ? bin[jbin] : bin[jbin-1]), jpmax_ws = ((jbin==0) ? bin[jbin+2] : bin[jbin+1]);
   double ipmin = bin[ibin], ipmax = bin[ibin+1];
   double jpmin = bin[jbin], jpmax = bin[jbin+1];
   gROOT->ProcessLine(".x ~/lhcbStyle.C");

   auto t = getDirectory();

   TString parafile("../fulltempt/PARAMs/FullMom_"+TString::Format("%d_%d.txt",ibin,jbin));

   auto Para = READParameter((char*)parafile.Data(), (string)polarity);

   TChain *chRS = new TChain();
   chRS->Add("/eos/home-m/mingjie/SCA/trackingasy/pion_track/selection/promptD/results/"+year+"Mag"+polarity+"FullRS.root/DecayTree");

   TChain *chWS = new TChain();
   chWS->Add("/eos/home-m/mingjie/SCA/trackingasy/pion_track/selection/promptD/results/"+year+"Mag"+polarity+"FullWS.root/DecayTree");

   TString cutKine = "(PMiss_Fit/1000<100&&PMiss_Fit/PMiss_Err>2.5&&PtMiss_Fit/PtMiss_Err>2)&&";
   TString cutPRS = cutKine+"(PiS_ID==211)"+TString::Format("&&(PiMiss_P/1000>%g&&PiMiss_P/1000<%g)",ipmin,ipmax)+TString::Format("&&(PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g)",jpmin,jpmax),
              cutMRS = cutKine+"(PiS_ID==-211)"+TString::Format("&&(PiMiss_P/1000>%g&&PiMiss_P/1000<%g)",ipmin,ipmax)+TString::Format("&&(PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g)",jpmin,jpmax);


   double xmin(138), xmax(159);
   double binWidth = 0.25;
   const int nBin = (int) ((xmax-xmin)/binWidth);
   RooRealVar x("x","#DeltaM(MeV)",xmin,xmax);
   x.setBins(nBin);

   TH1F *hPRS = new TH1F("hPRS","hPRS",nBin,xmin,xmax);
   TH1F *hMRS = new TH1F("hMRS","hMRS",nBin,xmin,xmax);

   TCanvas *c1 = new TCanvas("c1","c1");
   chRS->Draw("Dst_MM-D0_MM>>hPRS",cutPRS);
   chRS->Draw("Dst_MM-D0_MM>>hMRS",cutMRS);
   RooDataHist dataPRS("dataPRS", "dataPRS", x, hPRS),
		   dataMRS("dataMRS", "dataMRS", x, hMRS);
   RooDataSet dataPWS("dataPWS", "dataPWS", RooArgSet(x)), 
		  dataMWS("dataMWS", "dataMWS", RooArgSet(x));

   int PiS_ID, pi1_ID, pi2_ID;
   double PiMiss_P, PMiss_Fit, Dst_MM, D0_MM;
   chWS->SetBranchAddress("PiS_ID",&PiS_ID);
   chWS->SetBranchAddress("pi1_ID",&pi1_ID);
   chWS->SetBranchAddress("pi2_ID",&pi2_ID);
   chWS->SetBranchAddress("PiMiss_P",&PiMiss_P);
   chWS->SetBranchAddress("PMiss_Fit",&PMiss_Fit);
   chWS->SetBranchAddress("Dst_MM",&Dst_MM);
   chWS->SetBranchAddress("D0_MM",&D0_MM);
   for (int i=0; i<chWS->GetEntries(); i++){
	chWS->GetEntry(i);
	if (Dst_MM-D0_MM<=xmin || Dst_MM-D0_MM>=xmax) continue;
	if (PiMiss_P/1000<=ipmin_ws||PiMiss_P/1000>=ipmax_ws) continue;
	if (PMiss_Fit/1000<=jpmin_ws||PMiss_Fit/1000>=jpmax_ws) continue;
	x= Dst_MM-D0_MM;
	dataPWS.add(RooArgSet(x));
   }

   RooRealVar mean("mean","mean",Para.value("mean",0),Para.value("mean",1),Para.value("mean",2)),
		  sigma1("sigma1","sigma1",Para.value("sigma1",0),Para.value("sigma1",1),Para.value("sigma1",2)),
		  sigma2("sigma2","sigma2",Para.value("sigma2",0),Para.value("sigma2",1),Para.value("sigma2",2)),
		  sigma3("sigma3","sigma3",Para.value("sigma3",0),Para.value("sigma3",1),Para.value("sigma3",2)),
		  coeffsig("coeffsig","coeffsig",Para.value("coeffsig",0),Para.value("coeffsig",1),Para.value("coeffsig",2)),
		  dm0("dm0","dm0",Para.value("dm0",0),Para.value("dm0",1),Para.value("dm0",2)),
		  am("am","am",Para.value("am",0),Para.value("am",1),Para.value("am",2)),
		  bm("bm","bm",Para.value("bm",0),Para.value("bm",1),Para.value("bm",2)),
		  cm("cm","cm",Para.value("cm",0),Para.value("cm",1),Para.value("cm",2));
   mean.setConstant(Para.value("mean",3));
   sigma1.setConstant(Para.value("sigma1",3));
   sigma2.setConstant(Para.value("sigma2",3));
   sigma3.setConstant(Para.value("sigma3",3));
   coeffsig.setConstant(Para.value("coeffsig",3));
   dm0.setConstant(Para.value("dm0",3));
   am.setConstant(Para.value("am",3));
   bm.setConstant(Para.value("bm",3));
   cm.setConstant(Para.value("cm",3));

   RooBifurGauss *sig_pdf1 = new RooBifurGauss("sig_pdf1","sig_pdf1",x,mean,sigma1,sigma2);
   RooAbsPdf *sig_pdf2 = new RooGaussian("sig_pdf2","sig_pdf2",x,mean,sigma3);

   RooAbsPdf* modelSig= new RooAddPdf("modelSig","modelSig",RooArgList(*sig_pdf1,*sig_pdf2),RooArgList(coeffsig));

   RooKeysPdf* modelBkg= new RooKeysPdf("modelBkg","modelBkg",x,dataPWS,RooKeysPdf::MirrorBoth);

   RooRealVar NsigPRS("NsigPRS","NsigPRS",0.8*hPRS->Integral(),0,hPRS->Integral()),
		  NbkgPRS("NbkgPRS","NbkgPRS",0.2*hPRS->Integral(),0,hPRS->Integral()),
		  NsigMRS("NsigMRS","NsigMRS",0.8*hMRS->Integral(),0,hMRS->Integral()),
		  NbkgMRS("NbkgMRS","NbkgMRS",0.2*hMRS->Integral(),0,hMRS->Integral());

   RooAbsPdf *modelPRS = new RooAddPdf("modelPRS","modelPRS",RooArgList(*modelSig,*modelBkg),RooArgList(NsigPRS,NbkgPRS));
   RooAbsPdf *modelMRS = new RooAddPdf("modelMRS","modelMRS",RooArgList(*modelSig,*modelBkg),RooArgList(NsigMRS,NbkgMRS));

   RooChi2Var *chi2PRS = new RooChi2Var("chi2PRS", "chi2PRS", *modelPRS, dataPRS, RooFit::Extended(kTRUE));
   RooChi2Var *chi2MRS = new RooChi2Var("chi2MRS", "chi2MRS", *modelMRS, dataMRS, RooFit::Extended(kTRUE));

   RooAbsReal *nllDataPRS = modelPRS->createNLL(dataPRS,Extended());
   RooAbsReal *nllDataMRS = modelMRS->createNLL(dataMRS,Extended());
   RooAddition *nll = new RooAddition("nll","nll",RooArgSet(*nllDataPRS,*nllDataMRS));

   RooMinuit m(*nll);
   m.setVerbose(kFALSE);
   int migradstatus = m.migrad();
   int hessestatus = m.hesse();

   RooFitResult* fitResult = m.save();
   RooArgList fitParams = fitResult->floatParsFinal();
   int paraSize = fitParams.getSize();
   int ndf = nBin-paraSize;

   double chi2PRSVal = chi2PRS->getVal();
   double chi2MRSVal = chi2MRS->getVal();
   std::cout << ndf << " ; " << chi2PRSVal/ndf << " ; " << chi2MRSVal/ndf << std::endl;

   ofstream outAFit("../full"+year+"FIT.txt",std::ios::app);
   outAFit<<year<<polarity<<ibin<<"-"<<jbin<< "\t" << migradstatus<<"\t"<<hessestatus<<"\t" << " chi2PRS: " << chi2PRSVal/ndf <<"\t"<< " chi2MRS: " << chi2MRSVal/ndf << std::endl;

   ofstream outFit(Form("results/"+year+polarity+"FIT__%d_%d.txt", ibin,jbin));
   outFit<<migradstatus<<"\t"<<hessestatus<<std::endl;
  
   RooPlot *MRSframe = x.frame();
   dataMRS.plotOn(MRSframe, Name("dataMRS"), MarkerSize(0.8));
   modelMRS->plotOn(MRSframe, Components("modelBkg"), LineColor(8));
   modelMRS->plotOn(MRSframe, Components("modelSig"), LineColor(2));
   modelMRS->plotOn(MRSframe, Name("modelMRS"), LineColor(4));

   RooHist* hMRSpull=MRSframe->pullHist();
   hMRSpull->SetFillStyle(3001);
   RooPlot* MRSpull=x.frame();
   MRSpull->addPlotable(hMRSpull,"l3");
   MRSpull->SetTitle("");
   MRSpull->GetYaxis()->SetLabelSize(0.20);
   MRSpull->GetYaxis()->SetNdivisions(206);

   RooPlot *PRSframe = x.frame();
   dataPRS.plotOn(PRSframe, Name("dataPRS"), MarkerSize(0.8));
   modelPRS->plotOn(PRSframe, Components("modelBkg"), LineColor(8));
   modelPRS->plotOn(PRSframe, Components("modelSig"), LineColor(2));
   modelPRS->plotOn(PRSframe, Name("modelPRS"), LineColor(4));

   RooHist* hPRSpull=PRSframe->pullHist();
   hPRSpull->SetFillStyle(3001);
   RooPlot* PRSpull=x.frame();
   PRSpull->addPlotable(hPRSpull,"l3");
   PRSpull->SetTitle("");
   PRSpull->GetYaxis()->SetLabelSize(0.20);
   PRSpull->GetYaxis()->SetNdivisions(206);

   double chi2_1 = PRSframe->chiSquare("modelPRS","dataPRS",paraSize);
   double chi2_2 = MRSframe->chiSquare("modelMRS","dataMRS",paraSize);
   std::cout << "PRS chi2: " << chi2_1  << " ; MPS chi2: " << chi2_2 << std::endl;
   ofstream outBFit("../full"+year+"FIT2.txt",std::ios::app);
   outBFit<<year<<polarity<<ibin<<"-"<<jbin<< "\t"<<migradstatus<<"\t"<<hessestatus<<"\t"<<" chi2PRS: "<<chi2_1<<"\t"<< " chi2MRS: " << chi2_2 << std::endl;

   gROOT->Reset();
   TCanvas *c4 = new TCanvas("c4","c4");
   c4->Divide(1,2,0,0,0);
   c4->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   gPad->SetLogy();
   /*
   TLatex *TeXM = new TLatex(0.75,0.9, "#splitline{#pi^{-} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{#splitline{p_{Det}#in(%d,%d) GeV}{p_{Infer}#in(%d,%d) GeV}}}",
		(int)ipmin,(int)ipmax,(int)jpmin,(int)jpmax));
   */
   TLatex *TeXM = new TLatex(0.6,0.9, "#splitline{#pi^{-} in " + year + " Mag" + polarity + ", RS Data}"
                                       "{#splitline{p_{Det}#in(" + TString::Format("%d,%d", (int)ipmin, (int)ipmax) + ") GeV}"
                                       "{p_{Infer}#in(" + TString::Format("%d,%d", (int)jpmin, (int)jpmax) + ") GeV}}");
   TeXM->SetNDC();
   TeXM->SetTextSize(0.06);
   MRSframe->addObject(TeXM);
   MRSframe->Draw();

   c4->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   MRSpull->Draw();

   TCanvas *c5 = new TCanvas("c5","c5");
   c5->Divide(1,2,0,0,0);
   c5->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   gPad->SetLogy();
   /*
   TLatex *TeXP = new TLatex(0.75,0.9, "#splitline{#pi^{+} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{#splitline{p_{Det}#in(%d,%d) GeV}{p_{Infer}#in(%d,%d) GeV}}}",
		(int)ipmin,(int)ipmax,(int)jpmin,(int)jpmax));
   */
   TLatex *TeXP = new TLatex(0.6,0.9, "#splitline{#pi^{+} in " + year + " Mag" + polarity + ", RS Data}"
                                       "{#splitline{p_{Det}#in(" + TString::Format("%d,%d", (int)ipmin, (int)ipmax) + ") GeV}"
                                       "{p_{Infer}#in(" + TString::Format("%d,%d", (int)jpmin, (int)jpmax) + ") GeV}}");
   TeXP->SetNDC();
   TeXP->SetTextSize(0.06);
   PRSframe->addObject(TeXP);
   PRSframe->Draw();

   c5->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   PRSpull->Draw();

   TCanvas *c3 = new TCanvas("c3","c3");
   RooPlot *WSframe = x.frame();
   dataPWS.plotOn(WSframe, MarkerSize(0.8), MarkerColor(4), LineColor(4));
   c3->SetLogy();
   modelBkg->plotOn(WSframe, MarkerSize(0.8), LineColor(8));
   TLatex *TeXWS = new TLatex(0.6,0.4, "#splitline{"+year+" Mag"+polarity+"}{#splitline{ WS Data}"
	   +TString::Format("{#splitline{p_{Det}#in(%d,%d) GeV}{p_{Infer}#in(%d,%d) GeV}}}",
		(int)ipmin_ws,(int)ipmax_ws,(int)jpmin_ws,(int)jpmax_ws));
   WSframe->SetMaximum(WSframe->GetMaximum()*10);
   WSframe->SetMinimum(0.9);
   TeXWS->SetNDC();
   TeXWS->SetTextSize(0.06);
   WSframe->addObject(TeXWS);
   WSframe->Draw();

   c3->Print(TString(t.FIG)+"syst_bkgshape_full_"+year+"_"+polarity+Form("_Bin%d_%d",ibin,jbin)+"_promptD_PIM_WS_forSystBKG.pdf");
   c4->Print(TString(t.FIG)+"syst_bkgshape_full_"+year+"_"+polarity+Form("_Bin%d_%d",ibin,jbin)+"_promptD_PIM.pdf");
   c5->Print(TString(t.FIG)+"syst_bkgshape_full_"+year+"_"+polarity+Form("_Bin%d_%d",ibin,jbin)+"_promptD_PIP.pdf");
   c3->Print("results/full_"+polarity+"_WS_forSystBKG.pdf");
   c4->Print("results/full_"+polarity+"_PIM.pdf");
   c5->Print("results/full_"+polarity+"_PIP.pdf");

   ofstream outfile("results/para_full_"+polarity+".txt");
   outfile<<NbkgMRS.getVal()<< "\t";
   outfile<<NbkgPRS.getVal()<< "\t";
   outfile<<NsigMRS.getVal()<< "\t";
   outfile<<NsigPRS.getVal()<< "\t";
   outfile<<NsigMRS.getVal()<< "\t";
   outfile<<NsigPRS.getVal()<< "\t";

}

