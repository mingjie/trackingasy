MagDown PARAMs START ====>
mean, 128, 0, 148, 0
sigma, 0.1, 0, 4, 1
sigma2, 25, 0, 100, 0
acc_p, 2.6, 0, 5.5, 1
acc_s, 1.0, 0, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 6, -2, 10, 0
bm, -13, -20, 10, 0
cm, 0.9, -10, 10, 0
poly_a, 0.8, -1, 1, 0
poly_b, -0.1, -1, 1, 0
poly_c, -0.3, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 121, 0, 148, 0
sigma, 0.5, -4, 4, 1
sigma2, 15, 0, 100, 0
acc_p, 2.6, 0, 5.5, 1
acc_s, 1.1, 0, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 6, -2, 10, 0
bm, -13, -20, 10, 0
cm, 0.9, -10, 10, 0
poly_a, 0.4, -1, 1, 0
poly_b, 0.1, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagUp PARAMs END <====
