MagDown PARAMs START ====>
mean, 128, 0, 148, 0
sigma, 0.5, 0, 4, 1
sigma2, 14, 0, 100, 1
acc_p, 2.1, 0, 5.5, 1
acc_s, 0.6, 0, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.5, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 128, 0, 148, 0
sigma, 0.5, -4, 4, 1
sigma2, 15, 0, 100, 1
acc_p, 2.2, 0, 5.5, 1
acc_s, 0.8, 0, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 3, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, 0.02, -1, 1, 0
MagUp PARAMs END <====
