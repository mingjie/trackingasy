MagDown PARAMs START ====>
mean, 147, 0, 150, 0
sigma, 3, 0, 10, 0
sigma2, 5.3, 0, 20, 0
acc_p, 1.5, 0, 100, 1
acc_s, 0.7, 0.1, 1.5, 1
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.5, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 147, 0, 148, 0
sigma, 2.5, 0, 10, 0
sigma2, 5.5, 0, 20, 0
acc_p, 1.5, 0, 100, 1
acc_s, 0.5, 0.1, 1.5, 1
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 2.5, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagUp PARAMs END <====
