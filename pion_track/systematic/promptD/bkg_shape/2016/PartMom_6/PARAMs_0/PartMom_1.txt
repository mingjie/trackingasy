MagDown PARAMs START ====>
mean, 145, 135, 148, 0
sigma, 4, 0.5, 4, 0
sigma2, 8, 7, 9, 0
acc_p, 2.5, 0, 100, 0
acc_s, 0.5, 0.1, 1.5, 0
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 0.5, -2, 10, 0
bm, 1.3, -20, 10, 0
cm, 1.5, -10, 10, 0
poly_a, 0.15, -1, 1, 0
poly_b, -0.3, -1, 1, 0
poly_c, 0.01, -1, 1, 1
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 139, 135, 148, 0
sigma, 4, 0.5, 4, 0
sigma2, 8, 7, 9, 0
acc_p, 2.5, 0, 100, 0
acc_s, 0.5, 0.1, 5.5, 0
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 5, -2, 10, 0
bm, -9, -20, 10, 0
cm, 4.8, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, 0.01, -1, 1, 1
MagUp PARAMs END <====
