#include "READ.h"
using namespace RooFit;

void part(int ibin=0, TString polarity="Down", TString year="2018"){
   double bin[] = {2,5,10,20,30,40,50,100};
   double pmin = bin[ibin];
   double pmax = bin[ibin+1];
   gROOT->ProcessLine(".x ~/lhcbStyle.C");

   auto t = getDirectory();

   TString parafile("../parttempt/PARAMs/PartMom_"+TString::Format("%d.txt",ibin));

   //ReadPara para;
   auto Para = READParameter((char*)parafile.Data(), (string)polarity);

   TChain *chRS = new TChain();
   chRS->Add(t.PWD+"/pion_track/selection/fromB/results/B"+year+"Mag"+polarity+"PartRS.root/DecayTree");
   TChain *chMC = new TChain();
   chMC->Add(t.PWD+"/pion_track/selection/fromB/results/B2018Mag"+polarity+"RS_MC.root/TuplePartialRS/DecayTree");

   TString cutKine = "(PMiss_Fit/1000<100&&PMiss_Fit/PMiss_Err>2.5&&PtMiss_Fit/PtMiss_Err>2)&&";
   TString cutPRS = cutKine+"((PiS_ID==211&&pi1_ID*pi2_ID<0)||(PiS_ID==-211&&pi1_ID*pi2_ID>0))"+TString::Format("&&(PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g)",pmin,pmax),
	     cutMRS = cutKine+"((PiS_ID==-211&&pi1_ID*pi2_ID<0)||(PiS_ID==211&&pi1_ID*pi2_ID>0))"+TString::Format("&&(PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g)",pmin,pmax);

   double xmin(139), xmax(174);
   double binWidth = 0.5;
   const int nBin = (int) ((xmax-xmin)/binWidth);
   RooRealVar x("x","#DeltaM(MeV)",xmin,xmax);
   x.setBins(nBin);

   TH1F *hPRS = new TH1F("hPRS","hPRS",nBin,xmin,xmax);
   TH1F *hMRS = new TH1F("hMRS","hMRS",nBin,xmin,xmax);
   TH1F *hMC = new TH1F("hMC","hMC",nBin,xmin,xmax);

   TCanvas *c1 = new TCanvas("c1","c1");
   chRS->Draw("Dst_M-D0_M>>hPRS",cutPRS);
   chRS->Draw("Dst_M-D0_M>>hMRS",cutMRS);

   RooDataHist dataPRS("dataPRS", "dataPRS", x, hPRS),
		   dataMRS("dataMRS", "dataMRS", x, hMRS);

   RooDataSet dataMC("dataMC", "dataMC", RooArgSet(x));

   int Dst_BKGCAT, D0_BKGCAT;
   double PMiss_Fit, Dst_M, D0_M, PMiss_Err, PtMiss_Fit, PtMiss_Err;
   chMC->SetBranchAddress("Dst_BKGCAT",&Dst_BKGCAT);
   chMC->SetBranchAddress("D0_BKGCAT",&D0_BKGCAT);
   chMC->SetBranchAddress("PMiss_Fit",&PMiss_Fit);
   chMC->SetBranchAddress("PMiss_Err",&PMiss_Err);
   chMC->SetBranchAddress("PtMiss_Fit",&PtMiss_Fit);
   chMC->SetBranchAddress("PtMiss_Err",&PtMiss_Err);
   chMC->SetBranchAddress("Dst_M",&Dst_M);
   chMC->SetBranchAddress("D0_M",&D0_M);

   for (int i=0; i<chMC->GetEntries(); i++){
	chMC->GetEntry(i);
	if (D0_BKGCAT!=50) continue;
	if (Dst_BKGCAT!=50) continue;
	if (PMiss_Fit/1000<=pmin||PMiss_Fit/1000>=pmax) continue;
	if (Dst_M-D0_M<=xmin||Dst_M-D0_M>=xmax) continue;
	if (PMiss_Fit/1000>100) continue;
	if (PMiss_Fit/PMiss_Err<=2.5) continue;
	//if (PtMiss_Fit/PtMiss_Err<=2) continue;
	x = Dst_M-D0_M;
	hMC->Fill(Dst_M-D0_M);
	dataMC.add(RooArgSet(x));
   }
   TCanvas* canvas = new TCanvas("canvas", "Histogram Canvas", 800, 600);
   hMC->Draw();
   canvas->SaveAs("hMC.pdf");

   RooRealVar NsigPRS("NsigPRS","NsigPRS",0.74*hPRS->Integral(),0,hPRS->Integral()),
		  NsigMRS("NsigMRS","NsigMRS",0.74*hMRS->Integral(),0,hMRS->Integral()),
		  NbkgPRS("NbkgPRS","NbkgPRS",0.26*hPRS->Integral(),0,hPRS->Integral()),
		  NbkgMRS("NbkgMRS","NbkgMRS",0.26*hMRS->Integral(),0,hMRS->Integral());

   RooRealVar mean("mean","mean",Para.value("mean",0),Para.value("mean",1),Para.value("mean",2)),
		  sigma("sigma","sigma",Para.value("sigma",0),Para.value("sigma",1),Para.value("sigma",2)),
		  sigma2("sigma2","sigma2",Para.value("sigma2",0),Para.value("sigma2",1),Para.value("sigma2",2));
   mean.setConstant(Para.value("mean",3));
   sigma.setConstant(Para.value("sigma",3));
   sigma2.setConstant(Para.value("sigma2",3));

   RooBifurGauss* gaubifur = new RooBifurGauss("gaubifur","",x,mean,sigma,sigma2);

   RooRealVar acc_p("acc_p","acc_p",Para.value("acc_p",0),Para.value("acc_p",1),Para.value("acc_p",2)),
		  acc_s("acc_s","acc_s",Para.value("acc_s",0),Para.value("acc_s",1),Para.value("acc_s",2)),
		  offset("offset","offset",Para.value("offset",0),Para.value("offset",1),Para.value("offset",2));
   acc_p.setConstant(Para.value("acc_p",3));
   acc_s.setConstant(Para.value("acc_s",3));
   offset.setConstant(Para.value("offset",3));
   RooAbsReal *acc =  new RooFormulaVar("acc","(@0-@3<0) ? 0 : (abs((@0-@3)*@1)^@2/(1.0+abs((@0-@3)*@1)^@2))",RooArgList(x,acc_s,acc_p,offset));

#if 0
   RooHistPdf *modelSig = new RooHistPdf("modelSig","modelSig",x,dataMC,0);
#else
   RooKeysPdf *HistSig = new RooKeysPdf("HistSig","HistSig",x,dataMC,RooKeysPdf::MirrorBoth,2);
   RooRealVar mean_gaus("mean_gaus","mean_gaus",0.1,-5,5);
   RooRealVar sigma_gaus("sigma_gaus","sigma_gaus",0.5,0.01,100);
   RooGaussian gauss("gauss","gauss",x,mean_gaus,sigma_gaus);
   RooAbsPdf *modelSig = new RooFFTConvPdf("modelSig","KeysPdf(*)Gauss",x,*HistSig,gauss);
#endif

   ifstream input("results/ws_para"+polarity+".txt");
   double para[10];
   for (int i=0; i<7; i++){
	input>>para[i];
   }
   //parameters for DstD0BG
   RooRealVar dm0("dm0","dm0",para[3]),
		  am("am","am",para[0]),
		  bm("bm","bm",para[1]),
		  cm("cm","cm",para[2]);

   //parameters for Eff func
   RooRealVar poly_a("poly_a","poly_a",para[4]),
		  poly_b("poly_b","poly_b",para[5]),
		  poly_c("poly_c","poly_c",para[6]);

   //P.D.F.
   RooDstD0BG pdfD0BG("pdfD0BG","DstD0BKG",x,dm0,cm,am,bm);
   RooChebychev poly("poly","", x, RooArgList(poly_a,poly_b,poly_c));

   RooProdPdf modelBkg("modelBkg", "", pdfD0BG, poly);

   RooAbsPdf *modelPRS = new RooAddPdf("modelPRS","modelPRS",RooArgList(*modelSig,modelBkg),RooArgList(NsigPRS,NbkgPRS));
   RooAbsPdf *modelMRS = new RooAddPdf("modelMRS","modelMRS",RooArgList(*modelSig,modelBkg),RooArgList(NsigMRS,NbkgMRS));

   RooAbsReal *nllDataPRS = modelPRS->createNLL(dataPRS,Extended());
   RooAbsReal *nllDataMRS = modelMRS->createNLL(dataMRS,Extended());
   RooAddition *nll = new RooAddition("nll","nll",RooArgSet(*nllDataPRS,*nllDataMRS));

   RooMinuit m(*nll);
   m.setVerbose(kFALSE);
   int migradstatus = m.migrad();
   int hessestatus = m.hesse();

   ofstream outFit(Form("results/"+year+polarity+"FIT__%d.txt", ibin));
   outFit<<migradstatus<<"\t"<<hessestatus<<std::endl;   

   RooPlot *MRSframe = x.frame();
   dataMRS.plotOn(MRSframe, MarkerSize(0.8));
   modelMRS->plotOn(MRSframe, Components("modelBkg"), LineColor(8));
   modelMRS->plotOn(MRSframe, Components("modelSig"), LineColor(2));
   modelMRS->plotOn(MRSframe, LineColor(4));

   RooHist* hMRSpull=MRSframe->pullHist();
   hMRSpull->SetFillStyle(3001);
   RooPlot* MRSpull=x.frame();
   MRSpull->addPlotable(hMRSpull,"l3");
   MRSpull->SetTitle("");
   MRSpull->GetYaxis()->SetLabelSize(0.20);
   MRSpull->GetYaxis()->SetNdivisions(206);

   RooPlot *PRSframe = x.frame();
   dataPRS.plotOn(PRSframe, MarkerSize(0.8));
   modelPRS->plotOn(PRSframe, Components("modelBkg"), LineColor(8));
   modelPRS->plotOn(PRSframe, Components("modelSig"), LineColor(2));
   modelPRS->plotOn(PRSframe, LineColor(4));

   RooHist* hPRSpull=PRSframe->pullHist();
   hPRSpull->SetFillStyle(3001);
   RooPlot* PRSpull=x.frame();
   PRSpull->addPlotable(hPRSpull,"l3");
   PRSpull->SetTitle("");
   PRSpull->GetYaxis()->SetLabelSize(0.20);
   PRSpull->GetYaxis()->SetNdivisions(206);

   TCanvas *c4 = new TCanvas("c4","c4");
   c4->Divide(1,2,0,0,0);
   c4->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   TLatex *TeXM = new TLatex(0.7,0.8, "#splitline{#pi^{-} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{p_{Infer}#in(%d,%d) GeV}}",
		(int)pmin,(int)pmax));
   TeXM->SetNDC();
   TeXM->SetTextSize(0.07);
   MRSframe->addObject(TeXM);
   MRSframe->Draw();

   c4->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   MRSpull->Draw();

   TCanvas *c5 = new TCanvas("c5","c5");
   c5->Divide(1,2,0,0,0);
   c5->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   TLatex *TeXP = new TLatex(0.7,0.8, "#splitline{#pi^{+} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{p_{Infer}#in(%d,%d) GeV}}",
	(int)pmin,(int)pmax));
   TeXP->SetNDC();
   TeXP->SetTextSize(0.07);
   PRSframe->addObject(TeXP);
   PRSframe->Draw();

   c5->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   PRSpull->Draw();

   c4->Print(t.FIG+"syst_signalshape_part_"+year+"_"+polarity+Form("_Bin%d",ibin)+"_PIM.pdf");
   c5->Print(t.FIG+"syst_signalshape_part_"+year+"_"+polarity+Form("_Bin%d",ibin)+"_PIP.pdf");
   c4->Print("results/part_"+polarity+"_PIM.pdf");
   c5->Print("results/part_"+polarity+"_PIP.pdf");

   ofstream outfile("results/para"+polarity+".txt");
   outfile<<NbkgMRS.getVal()<< "\t";
   outfile<<NbkgPRS.getVal()<< "\t";
   outfile<<NsigMRS.getVal()<< "\t";
   outfile<<NsigPRS.getVal()<< "\t";
   outfile<<acc_p.getVal()<< "\t";
   outfile<<acc_s.getVal()<< "\t";
   outfile<<mean.getVal()<< "\t";
   outfile<<offset.getVal()<< "\t";
   outfile<<sigma.getVal()<< "\t";
   outfile<<sigma2.getVal()<< "\t";

}
