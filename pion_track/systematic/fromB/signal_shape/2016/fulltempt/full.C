#include "MyStudent.cxx"
#include "MyCB.cxx"
#include "READ.h"

using namespace RooFit;

void full(int ibin=0, int jbin=0, TString polarity="Down", TString year="2016"){
   double bin[] = {2,5,10,20,30,40,50,100};
   double ipmin_ws= ((ibin==0) ? bin[ibin] : bin[ibin-1]), ipmax_ws = ((ibin==0) ? bin[ibin+2] : bin[ibin+1]);
   double jpmin_ws= ((jbin==0) ? bin[jbin] : bin[jbin-1]), jpmax_ws = ((jbin==0) ? bin[jbin+2] : bin[jbin+1]);
   double ipmin = bin[ibin], ipmax = bin[ibin+1];
   double jpmin = bin[jbin], jpmax = bin[jbin+1];
   gROOT->ProcessLine(".x ~/lhcbStyle.C");

   auto t = getDirectory();

   TString parafile("../fulltempt/PARAMs/FullMom_"+TString::Format("%d_%d.txt",ibin,jbin));

   auto Para = READParameter((char*)parafile.Data(), (string)polarity);

   TChain *chRS = new TChain();
   chRS->Add(t.PWD+"/pion_track/selection/fromB/results/B"+year+"Mag"+polarity+"FullRS.root/DecayTree");
   TChain *chMC = new TChain();
   chMC->Add(t.PWD+"/pion_track/selection/fromB/results/B2018Mag"+polarity+"RS_MC.root/TupleFullRS/DecayTree");

   TString cutKine = "(PMiss_Fit/1000<100&&PMiss_Fit/PMiss_Err>2.5&&PtMiss_Fit/PtMiss_Err>2)&&";
   TString cutPRS = cutKine+"((PiS_ID==211&&pi1_ID*pi2_ID<0)||(PiS_ID==-211&&pi1_ID*pi2_ID>0))"+TString::Format("&&(PiMiss_P/1000>%g&&PiMiss_P/1000<%g)",ipmin,ipmax)+TString::Format("&&(PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g)",jpmin,jpmax),
	     cutMRS = cutKine+"((PiS_ID==-211&&pi1_ID*pi2_ID<0)||(PiS_ID==211&&pi1_ID*pi2_ID>0))"+TString::Format("&&(PiMiss_P/1000>%g&&PiMiss_P/1000<%g)",ipmin,ipmax)+TString::Format("&&(PMiss_Fit/1000>%g&&PMiss_Fit/1000<%g)",jpmin,jpmax);

   double xmin(138), xmax(159);
   double binWidth = 0.25;
   const int nBin = (int) ((xmax-xmin)/binWidth);
   RooRealVar x("x","#DeltaM(MeV)",xmin,xmax);
   x.setBins(nBin);

   TH1F *hPRS = new TH1F("hPRS","hPRS",nBin,xmin,xmax);
   TH1F *hMRS = new TH1F("hMRS","hMRS",nBin,xmin,xmax);

   TCanvas *c1 = new TCanvas("c1","c1");
   chRS->Draw("Dst_M-D0_M>>hPRS",cutPRS);
   chRS->Draw("Dst_M-D0_M>>hMRS",cutMRS);
   RooDataHist dataPRS("dataPRS", "dataPRS", x, hPRS),
		   dataMRS("dataMRS", "dataMRS", x, hMRS);
   RooDataSet dataMC("dataMC", "dataMC", RooArgSet(x));

   int D0_BKGCAT, Dst_BKGCAT;
   double PiMiss_P, PMiss_Fit, Dst_M, D0_M;
   chMC->SetBranchAddress("D0_BKGCAT",&D0_BKGCAT);
   chMC->SetBranchAddress("Dst_BKGCAT",&Dst_BKGCAT);
   chMC->SetBranchAddress("PiMiss_P",&PiMiss_P);
   chMC->SetBranchAddress("PMiss_Fit",&PMiss_Fit);
   chMC->SetBranchAddress("Dst_M",&Dst_M);
   chMC->SetBranchAddress("D0_M",&D0_M);
   for (int i=0; i<chMC->GetEntries(); i++){
	chMC->GetEntry(i);
	if (Dst_M-D0_M<=xmin||Dst_M-D0_M>=xmax) continue;
	if (D0_BKGCAT!=20) continue;
	if (Dst_BKGCAT!=10) continue;
	if (PiMiss_P/1000<=ipmin_ws||PiMiss_P/1000>=ipmax_ws) continue;
	if (PMiss_Fit/1000<=jpmin_ws&&PMiss_Fit/1000>=jpmax_ws) continue;
	x = Dst_M-D0_M;
	dataMC.add(RooArgSet(x));
   }

   RooRealVar mean("mean","mean",Para.value("mean",0),Para.value("mean",1),Para.value("mean",2)),
		  sigma1("sigma1","sigma1",Para.value("sigma1",0),Para.value("sigma1",1),Para.value("sigma1",2)),
		  sigma2("sigma2","sigma2",Para.value("sigma2",0),Para.value("sigma2",1),Para.value("sigma2",2)),
		  sigma3("sigma3","sigma3",Para.value("sigma3",0),Para.value("sigma3",1),Para.value("sigma3",2)),
		  coeffsig("coeffsig","coeffsig",Para.value("coeffsig",0),Para.value("coeffsig",1),Para.value("coeffsig",2)),
		  dm0("dm0","dm0",Para.value("dm0",0),Para.value("dm0",1),Para.value("dm0",2)),
		  am("am","am",Para.value("am",0),Para.value("am",1),Para.value("am",2)),
		  bm("bm","bm",Para.value("bm",0),Para.value("bm",1),Para.value("bm",2)),
		  cm("cm","cm",Para.value("cm",0),Para.value("cm",1),Para.value("cm",2));
   mean.setConstant(Para.value("mean",3));
   sigma1.setConstant(Para.value("sigma1",3));
   sigma2.setConstant(Para.value("sigma2",3));
   sigma3.setConstant(Para.value("sigma3",3));
   coeffsig.setConstant(Para.value("coeffsig",3));
   dm0.setConstant(Para.value("dm0",3));
   am.setConstant(Para.value("am",3));
   bm.setConstant(Para.value("bm",3));
   cm.setConstant(Para.value("cm",3));

   RooBifurGauss *sig_pdf1 = new RooBifurGauss("sig_pdf1","sig_pdf1",x,mean,sigma1,sigma2);
   RooAbsPdf *sig_pdf2 = new RooGaussian("sig_pdf2","sig_pdf2",x,mean,sigma3);

#if 0
   RooHistPdf *modelSig = new RooHistPdf("modelSig","modelSig",x,dataMC,0);
#else
   RooKeysPdf *HistSig = new RooKeysPdf("HistSig","HistSig",x,dataMC,RooKeysPdf::MirrorBoth);
   RooRealVar mean_gaus("mean_gaus","mean_gaus",0.1,-5,5);
   RooRealVar sigma_gaus("sigma_gaus","sigma_gaus",0.5,0.01,100);
   RooGaussian gauss("gauss","gauss",x,mean_gaus,sigma_gaus);
   RooAbsPdf *modelSig = new RooFFTConvPdf("modelSig","KeysPdf(*)Gauss",x,*HistSig,gauss);
#endif

   RooDstD0BG* modelBkg= new RooDstD0BG("modelBkg","modelBkg",x,dm0,cm,am,bm);

   RooRealVar NsigPRS("NsigPRS","NsigPRS",0.8*hPRS->Integral(),0,hPRS->Integral()),
		  NbkgPRS("NbkgPRS","NbkgPRS",0.2*hPRS->Integral(),0,hPRS->Integral()),
		  NsigMRS("NsigMRS","NsigMRS",0.8*hMRS->Integral(),0,hMRS->Integral()),
		  NbkgMRS("NbkgMRS","NbkgMRS",0.2*hMRS->Integral(),0,hMRS->Integral());

   RooAbsPdf *modelPRS = new RooAddPdf("modelPRS","modelPRS",RooArgList(*modelSig,*modelBkg),RooArgList(NsigPRS,NbkgPRS));
   RooAbsPdf *modelMRS = new RooAddPdf("modelMRS","modelMRS",RooArgList(*modelSig,*modelBkg),RooArgList(NsigMRS,NbkgMRS));

   RooAbsReal *nllDataPRS = modelPRS->createNLL(dataPRS,Extended());
   RooAbsReal *nllDataMRS = modelMRS->createNLL(dataMRS,Extended());
   RooAddition *nll = new RooAddition("nll","nll",RooArgSet(*nllDataPRS,*nllDataMRS));

   RooMinuit m(*nll);
   m.setVerbose(kFALSE);
   int migradstatus = m.migrad();
   int hessestatus = m.hesse();

   ofstream outFit(Form("results/"+year+polarity+"FIT__%d_%d.txt", ibin,jbin));
   outFit<<migradstatus<<"\t"<<hessestatus<<std::endl;

   RooPlot *MRSframe = x.frame();
   dataMRS.plotOn(MRSframe, MarkerSize(0.8));
   modelMRS->plotOn(MRSframe, Components("modelBkg"), LineColor(8));
   modelMRS->plotOn(MRSframe, Components("modelSig"), LineColor(2));
   modelMRS->plotOn(MRSframe, LineColor(4));

   RooHist* hMRSpull=MRSframe->pullHist();
   hMRSpull->SetFillStyle(3001);
   RooPlot* MRSpull=x.frame();
   MRSpull->addPlotable(hMRSpull,"l3");
   MRSpull->SetTitle("");
   MRSpull->GetYaxis()->SetLabelSize(0.20);
   MRSpull->GetYaxis()->SetNdivisions(206);

   RooPlot *PRSframe = x.frame();
   dataPRS.plotOn(PRSframe, MarkerSize(0.8));
   modelPRS->plotOn(PRSframe, Components("modelBkg"), LineColor(8));
   modelPRS->plotOn(PRSframe, Components("modelSig"), LineColor(2));
   modelPRS->plotOn(PRSframe, LineColor(4));

   RooHist* hPRSpull=PRSframe->pullHist();
   hPRSpull->SetFillStyle(3001);
   RooPlot* PRSpull=x.frame();
   PRSpull->addPlotable(hPRSpull,"l3");
   PRSpull->SetTitle("");
   PRSpull->GetYaxis()->SetLabelSize(0.20);
   PRSpull->GetYaxis()->SetNdivisions(206);

   gROOT->Reset();
   TCanvas *c4 = new TCanvas("c4","c4");
   c4->Divide(1,2,0,0,0);
   c4->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   gPad->SetLogy();
   TLatex *TeXM = new TLatex(0.7,0.8, "#splitline{#pi^{-} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{#splitline{p_{Det}#in(%d,%d) GeV}{p_{Infer}#in(%d,%d) GeV}}}",
		(int)ipmin,(int)ipmax,(int)jpmin,(int)jpmax));
   TeXM->SetNDC();
   TeXM->SetTextSize(0.07);
   MRSframe->addObject(TeXM);
   MRSframe->Draw();

   c4->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   MRSpull->Draw();

   TCanvas *c5 = new TCanvas("c5","c5");
   c5->Divide(1,2,0,0,0);
   c5->cd(2);
   gPad->SetTopMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.02,0.97,0.77);
   gPad->SetLogy();
   TLatex *TeXP = new TLatex(0.7,0.8, "#splitline{#pi^{+} in "+year+" Mag"+polarity+"}{#splitline{ RS Data}"
	   +TString::Format("{#splitline{p_{Det}#in(%d,%d) GeV}{p_{Infer}#in(%d,%d) GeV}}}",
		(int)ipmin,(int)ipmax,(int)jpmin,(int)jpmax));
   TeXP->SetNDC();
   TeXP->SetTextSize(0.07);
   PRSframe->addObject(TeXP);
   PRSframe->Draw();

   c5->cd(1);
   gPad->SetBottomMargin(0);
   gPad->SetLeftMargin(0.15);
   gPad->SetPad(0.03,0.77,0.97,0.97);
   PRSpull->Draw();

   c4->Print(t.FIG+"syst_signalshape_full_"+year+"_"+polarity+Form("_Bin%d_%d",ibin,jbin)+"_PIM.pdf");
   c5->Print(t.FIG+"syst_signalshape_full_"+year+"_"+polarity+Form("_Bin%d_%d",ibin,jbin)+"_PIP.pdf");
   c4->Print("results/full_"+polarity+"_PIM.pdf");
   c5->Print("results/full_"+polarity+"_PIP.pdf");

   ofstream outfile("results/para_full_"+polarity+".txt");
   outfile<<NbkgMRS.getVal()<< "\t";
   outfile<<NbkgPRS.getVal()<< "\t";
   outfile<<NsigMRS.getVal()<< "\t";
   outfile<<NsigPRS.getVal()<< "\t";
   outfile<<NsigMRS.getVal()<< "\t";
   outfile<<NsigPRS.getVal()<< "\t";

}

