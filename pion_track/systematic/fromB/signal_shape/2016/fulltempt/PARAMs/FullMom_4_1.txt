MagDown PARAMs START ====>
am, -5, -20, 10, 1
bm, -0.6, -20, 20, 0
cm, 11, 0, 200, 1
coeffsig, 0.3, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.4, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.0, 0, 10, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, -5, -20, 10, 1
bm, -0.6, -20, 20, 0
cm, 11, 0, 200, 1
coeffsig, 0.3, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.4, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.0, 0, 10, 0
MagUp PARAMs END <====
