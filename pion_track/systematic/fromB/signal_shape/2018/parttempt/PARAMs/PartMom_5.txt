MagDown PARAMs START ====>
mean, 137, 0, 148, 0
sigma, 2, 0, 4, 1
sigma2, 7, 0, 100, 1
acc_p, 3, 0, 5.5, 1
acc_s, 1, 0, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 3, -2, 10, 0
bm, -7, -20, 10, 0
cm, 3, -10, 10, 0
poly_a, 0.1, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, -0.2, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 138, 0, 148, 0
sigma, 0.6, -4, 4, 1
sigma2, 9, 0, 100, 1
acc_p, 2, 0, 5.5, 1
acc_s, 0.6, 0, 5.5, 1
offset, 139, 100, 150, 1
dm0, 139, 130, 145, 0
am, 4, -2, 10, 0
bm, -8, -20, 10, 0
cm, 4, -10, 10, 0
poly_a, -0.2, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, 0.2, -1, 1, 0
MagUp PARAMs END <====
