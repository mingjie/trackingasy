MagDown PARAMs START ====>
mean, 135, 0, 148, 0
sigma, 3, 0, 10, 0
sigma2, 8, 0, 20, 0
acc_p, 3, 0, 100, 0
acc_s, 1, 0.1, 1.5, 1
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 4, -2, 10, 0
bm, -8, -20, 10, 0
cm, 4, -10, 10, 0
poly_a, -0.2, -1, 1, 0
poly_b, -0.1, -1, 1, 0
poly_c, 0.1, -1, 1, 0
MagDown PARAMs END <====
MagUp PARAMs START ====>
mean, 137, 0, 148, 0
sigma, 2, 0, 10, 0
sigma2, 8, 0, 20, 0
acc_p, 3, 0, 100, 0
acc_s, 0.7, 0.1, 1.5, 0
offset, 139, 130, 150, 0
dm0, 139, 130, 145, 0
am, 4, -2, 10, 0
bm, -8, -20, 10, 0
cm, 4, -10, 10, 0
poly_a, -0.2, -1, 1, 0
poly_b, -0.2, -1, 1, 0
poly_c, 0.1, -1, 1, 0
MagUp PARAMs END <====
