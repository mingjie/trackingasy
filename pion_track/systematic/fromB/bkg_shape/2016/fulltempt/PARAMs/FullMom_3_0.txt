MagDown PARAMs START ====>
am, 5.4, -10, 10, 1
bm, -7.8, -20, 0, 1
cm, 0.3, -10, 200, 1
coeffsig, 0.99, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.9, 0, 10, 1
sigma2, 0.8, 0, 10, 1
sigma3, 3.5, 0, 10, 1
MagDown PARAMs END <====
MagUp PARAMs START ====>
am, 2, -10, 10, 0
bm, -3.5, -20, 0, 0
cm, 5, -10, 200, 0
coeffsig, 0.1, 0, 1, 0
dm0, 139.4, 130, 150, 0
mean, 145.5, 138, 159, 0
sigma1, 0.5, 0, 10, 0
sigma2, 0.5, 0, 10, 0
sigma3, 1.1, 0, 10, 0
MagUp PARAMs END <====
