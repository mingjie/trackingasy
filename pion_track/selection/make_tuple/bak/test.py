"""
This options file demonstrates how to run a stripping line
from a specific stripping version on a local MC DST file
It is based on the minimal DaVinci DecayTreeTuple example
"""

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
    EventNodeKiller,
    ProcStatusCheck,
    DaVinci,
    DecayTreeTuple
)
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *
import shelve


from Configurables import FilterInTrees, FilterDesktop
from Configurables import CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand


muons = DataOnDemand(Location = "Phys/StdAllnoPIDsMuons/Particles")
pions = DataOnDemand(Location = "Phys/StdAllnoPIDsPions/Particles")
kaons = DataOnDemand(Location = "Phys/StdAllnoPIDsKaons/Particles")

stream = 'AllStreams'
line = 'CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piRS'
tesLoc = '/Event/{0}/Phys/{1}/Particles'.format(stream, line)

d0_from_b0 = FilterInTrees('d0_from_b0_filter', Code="('K-' == ABSID)")
d0_from_b0_sel = Selection("d0_from_b0_sel",
      Algorithm=d0_from_b0,
      RequiredSelections=[DataOnDemand(Location=tesLoc)])
'''
d0_particles = CombineParticles('d0_particles'
      ,DecayDescriptor="[D0 -> K- pi+ pi+]cc"
      ,CombinationCut="AALL"
      ,MotherCut="ALL"
      #DaughtersCuts= {
      #   "K-" : "(PT > 250.0 *MeV) & (TRGHOSTPROB < 0.35) & (PIDK > 6.0)",
      #   "pi+": "(PT > 200.0*MeV) & (TRGHOSTPROB < 0.35) & (PIDe < 99.0)& (PIDK < 10.0)"
      #   },
      ,Inputs = [d0_from_b0]
      )
reald0_sel = Selection('reald0_sel',
      Algorithm=reald0,
      RequiredSelections=[d0_from_b0_sel])
'''

tuple_Full_RS = DecayTreeTuple('tuple_Full_RS')
# The output is placed directly into Phys, so we only need to
# define the stripping line here
tuple_Full_RS.Inputs = ['Phys/d0_from_b0_sel/Particles']
#tuple_Full_RS.Decay = "[B0 -> ^(D_s+ -> ^(D~0 -> ^K+ ^pi- ^pi-) ^pi+) ^(K*(892)+ -> ^mu+ ^pi-)]CC"
#tuple_Full_RS.Decay = "[B_s0 -> ^D_s+ ^K*(892)+]CC"
tuple_Full_RS.Decay = "[K-]CC"

'''
SeqPreselLb2JpsiKP = GaudiSequencer("SeqPreselLb2JpsiKP")
SeqPreselLb2JpsiKP.Members+=[d0_from_b0_sel, tuple_Full_RS]
'''

# Configure DaVinci

# Important: The selection sequence needs to be inserted into
# the Gaudi sequence for the stripping to run
#DaVinci().UserAlgorithms += [tuple_Full_RS]
DaVinci().UserAlgorithms += [tuple_Full_RS]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'Tuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
#DaVinci().CondDBtag = 'sim-20161124-2-vc-md100'
#DaVinci().DDDBtag = 'dddb-20150724'

# Use the local input data
IOHelper().inputFiles([
'/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00103234/0000/00103234_00000808_7.AllStreams.dst'
,'/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00103234/0000/00103234_00000788_7.AllStreams.dst'
], clear=True)
