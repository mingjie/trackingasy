"""
Submit Bc->JpsiMuNu options 
"""

myApp = GaudiExec()
myApp.directory = "/afs/cern.ch/user/x/xuyuan/cmtuser/DaVinciDev_v41r4p4"
myApp.options = ['Bc2JpsiMu.py']

data14543010down=BKQuery('/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14543010/ALLSTREAMS.DST', dqflag=['OK']).getDataset()
data14543010up=BKQuery('/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14543010/ALLSTREAMS.DST', dqflag=['OK']).getDataset()
data14543005down=BKQuery('/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/14543005/ALLSTREAMS.DST', dqflag=['OK']).getDataset()
data14543005up=BKQuery('/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/14543005/ALLSTREAMS.DST', dqflag=['OK']).getDataset()


j14543010down = Job(application = myApp)
j14543010down.application.platform = 'x86_64-slc6-gcc49-opt'
j14543010down.name='14543010D'
j14543010down.inputdata = data14543010down
j14543010down.splitter=SplitByFiles(filesPerJob=1)
j14543010down.outputfiles=[DiracFile("Tuple.root")]
j14543010down.backend=Dirac()
j14543010down.parallel_submit=True
#j14543010down.do_auto_resubmit = True
j14543010down.submit()

j14543010up = Job(application = myApp)
j14543010up.application.platform = 'x86_64-slc6-gcc49-opt'
j14543010up.name='14543010U'
j14543010up.inputdata = data14543010up
j14543010up.splitter=SplitByFiles(filesPerJob=1)
j14543010up.outputfiles=[DiracFile("Tuple.root")]
j14543010up.backend=Dirac()
j14543010up.parallel_submit=True
#j14543010up.do_auto_resubmit = True
j14543010up.submit()

j14543005down = Job(application = myApp)
j14543005down.application.platform = 'x86_64-slc6-gcc49-opt'
j14543005down.name='14543005D'
j14543005down.inputdata = data14543005down
j14543005down.splitter=SplitByFiles(filesPerJob=1)
j14543005down.outputfiles=[DiracFile("Tuple.root")]
j14543005down.backend=Dirac()
j14543005down.parallel_submit=True
#j14543005down.do_auto_resubmit = True
j14543005down.submit()

j14543005up = Job(application = myApp)
j14543005up.application.platform = 'x86_64-slc6-gcc49-opt'
j14543005up.name='14543005U'
j14543005up.inputdata = data14543005up
j14543005up.splitter=SplitByFiles(filesPerJob=1)
j14543005up.outputfiles=[DiracFile("Tuple.root")]
j14543005up.backend=Dirac()
j14543005up.parallel_submit=True
#j14543005up.do_auto_resubmit = True
j14543005up.submit()

