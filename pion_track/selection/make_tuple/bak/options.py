"""
This options file demonstrates how to run a stripping line
from a specific stripping version on a local MC DST file
It is based on the minimal DaVinci DecayTreeTuple example
"""

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
    EventNodeKiller,
    ProcStatusCheck,
    DaVinci,
    DecayTreeTuple
)
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *
import shelve


# Node killer: remove the previous Stripping
event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

# Build a new stream called 'CustomStream' that only
# contains the desired line
strip = 'stripping34'
config_db = strippingConfiguration(strip)
config = dict(config_db) # need to do this since the config_db is read-only
config['CharmFromBSemiForHadronAsy']['CONFIG']['K_PIDKMin'] = -1000
config['CharmFromBSemiForHadronAsy']['CONFIG']['Pi_PIDKMax'] = 1000
config['CharmFromBSemiForHadronAsy']['CONFIG']['Slowpi_PIDKMax'] = 1000

config_db_updated = shelve.open('tmp_stripping_config.db')
config_db_updated.update(config)

#streams = buildStreams(stripping=strippingConfiguration(strip),
#                       archive=strippingArchive(strip))
streams = buildStreams(stripping=config_db_updated,
                       archive=strippingArchive(strip))

line = 'CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piRS'

custom_stream = StrippingStream('CustomStream')
custom_line = 'Stripping'+line 
location = 'Phys/{0}/Particles'.format(line)
#location = '/Event/Phys/{0}/Particles'.format(line)

for stream in streams:
    for sline in stream.lines:
        if sline.name() == custom_line:
            custom_stream.appendLines([sline])

# Create the actual Stripping configurable
filterBadEvents = ProcStatusCheck()

sc = StrippingConf(Streams=[custom_stream],
                   MaxCandidates=2000,
                   AcceptBadEvents=False,
                   BadEventSelection=filterBadEvents)

from Configurables import FilterInTrees, FilterDesktop
from Configurables import CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand

'''
d0_from_b0 = FilterInTrees('d0_from_b0_filter', Code="('D0' == ABSID)")
d0_from_b0_sel = Selection("d0_from_b0_sel",
      Algorithm=d0_from_b0,
      RequiredSelections=[DataOnDemand(Location=location)])
kst_from_b0 = FilterInTrees('kst_from_b0_filter', Code="('K*(892)+' == ABSID)")
kst_from_b0_sel = Selection("kst_from_b0_sel",
      Algorithm=kst_from_b0,
      RequiredSelections=[DataOnDemand(Location=location)])
Pions = DataOnDemand('Phys/StdAllNoPIDsPions/Particles')

reald0 = CombineParticles('d0_particles',
      DecayDescriptor="[D_s- -> D0 pi-]cc",
      CombinationCut="ADAMASS('D0') < 300*MeV",
      MotherCut='(VFASPF(VCHI2/VDOF)< 9)')
reald0_sel = Selection('reald0_sel',
      Algorithm=reald0,
      RequiredSelections=[d0_from_b0_sel, Pions])
realb0 = CombineParticles('b0_particles',
      DecayDescriptor="[B_s0 -> D_s- K*(892)+]cc",
      CombinationCut="ADAMASS('B0') < 300*MeV",
      MotherCut='(VFASPF(VCHI2/VDOF)< 9)')
realb0_sel = Selection('realb0_sel',
      Algorithm=realb0,
      RequiredSelections=[reald0_sel, kst_from_b0_sel])
'''

muons = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
pions = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
kaons = DataOnDemand(Location = "Phys/StdAllNoPIDsKaons/Particles")

reald0 = CombineParticles('d0_particles',
      DecayDescriptor="[D0 -> K- pi+ pi- pi+]cc",
      CombinationCut="ADAMASS('D0') < 300*MeV",
      MotherCut='(VFASPF(VCHI2/VDOF)< 9)')
reald0_sel = Selection('reald0_sel',
      Algorithm=reald0,
      RequiredSelections=[pions, kaons])
      #RequiredSelections=[DataOnDemand('Phys/StdAllNoPIDsPions/Particles')
         #,DataOnDemand(Location=location)])


# Create an ntuple to capture D*+ decays from the StrippingLine line
tuple_Partial_RS = DecayTreeTuple('tuple_Partial_RS')
# The output is placed directly into Phys, so we only need to
# define the stripping line here
tuple_Partial_RS.Inputs = [location]
tuple_Partial_RS.Decay = "([B0 -> ^(D~0 -> ^K+ ^pi- ^pi-) ^(K*(892)+ -> ^mu+ ^pi-)]CC) || ([B0 -> ^(D~0 -> ^K+ ^pi- ^pi+) ^(K*(892)+ -> ^mu+ ^pi-)]CC) "

tuple_Full_RS = DecayTreeTuple('tuple_Full_RS')
# The output is placed directly into Phys, so we only need to
# define the stripping line here
tuple_Full_RS.Inputs = ['Phys/reald0_sel/Particles']
#tuple_Full_RS.Decay = "[B0 -> ^(D_s+ -> ^(D~0 -> ^K+ ^pi- ^pi-) ^pi+) ^(K*(892)+ -> ^mu+ ^pi-)]CC"
#tuple_Full_RS.Decay = "[B_s0 -> ^D_s+ ^K*(892)+]CC"
#tuple_Full_RS.Decay = "[B_s0 -> ^(B0 -> ^(D~0 -> ^K+ ^pi- ^pi-) ^(K*(892)+ -> ^mu+ ^pi-)) ^pi+]CC"
tuple_Full_RS.Decay = "[D0 -> K- pi+ pi- pi+]CC"

# Configure DaVinci

# Important: The selection sequence needs to be inserted into
# the Gaudi sequence for the stripping to run
DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])
#DaVinci().UserAlgorithms += [tuple_Full_RS]
DaVinci().UserAlgorithms += [tuple_Partial_RS, tuple_Full_RS]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'Tuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = 5000
#DaVinci().CondDBtag = 'sim-20161124-2-vc-md100'
#DaVinci().DDDBtag = 'dddb-20150724'

# Use the local input data
IOHelper().inputFiles([
'/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00103234/0000/00103234_00000808_7.AllStreams.dst'
,'/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00103234/0000/00103234_00000788_7.AllStreams.dst'
], clear=True)
