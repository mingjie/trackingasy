from Gaudi.Configuration import *
from Configurables import DaVinci, DecayTreeTuple, FilterDesktop, CombineParticles, MCDecayTreeTuple, TupleToolDecayTreeFitter, TupleToolTISTOS, OfflineVertexFitter, GaudiSequencer, TupleToolMCTruth
from Configurables import GaudiSequencer, OfflineVertexFitter, DecayTreeTuple, TupleToolDecay 
from Configurables import TupleToolTrigger, TupleToolDecay, TupleToolTISTOS, TupleToolRecoStats, TupleToolVtxIsoln

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
import shelve

mtl = [
	"L0MuonDecision",
	"L0DiMuonDecision",
	"Hlt1TrackAllL0Decision",
	"Hlt1TrackMuonDecision",
	"Hlt1DiMuonHighMassDecision",
	"Hlt2DiMuonJPsiDecision",
	"Hlt2TopoMu2BodyBBDTDecision",
	"Hlt2TopoMu3BodyBBDTDecision",
	"Hlt2MuonFromHLT1Decision",
	"Hlt2TFBc2JpsiMuXDecision",
	"Hlt2TFBc2JpsiMuXSignalDecision",
	"Hlt2DiMuonDecision",
	"Hlt2DiMuonJPsiHighPTDecision",
	"Hlt2DiMuonPsi2SDecision",
	"Hlt2DiMuonPsi2SHighPTDecision",
	"Hlt2DiMuonDetachedDecision",
	"Hlt2DiMuonDetachedHeavyDecision",
	"Hlt2DiMuonDetachedJPsiDecision",
	"Hlt2TriMuonDetachedDecision",
	"Hlt2DiMuonAndMuonDecision"
	]

toollist = [
	"TupleToolEventInfo",
	"TupleToolTISTOS",
	"TupleToolGeometry",
	"TupleToolKinematic",
	"TupleToolTrackInfo",
	"TupleToolPid",
	"TupleToolANNPID",
	"TupleToolDira",
	"TupleToolAngles",
	"TupleToolPrimaries",
	"TupleToolDecayTreeFitter",
	"TupleToolPropertime",
	"TupleToolVtxIsoln",
	"TupleToolMCBackgroundInfo",
	"TupleToolMCTruth",
	"TupleToolRecoStats"
	]

stripping = 'stripping34'
#get the configuration dictionary from the database
config_db = strippingConfiguration(stripping)
config = dict(config_db) # need to do this since the config_db is read-only
#config['CharmFromBSemiForHadronAsy']['CONFIG']['K_PIDKMin'] = -1000
#config['CharmFromBSemiForHadronAsy']['CONFIG']['Pi_PIDKMax'] = 1000
#config['CharmFromBSemiForHadronAsy']['CONFIG']['Slowpi_PIDKMax'] = 1000

config_db_updated = shelve.open('tmp_stripping_config.db')
config_db_updated.update(config)
#get the line builders from the archive
archive = strippingArchive(stripping)
myWG = "Semileptonic"

#streams = buildStreams(stripping = config_db_updated, archive = archive)
streams = buildStreams(stripping = config_db_updated, archive = archive, WGs = myWG)

line = 'CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piRS' 
custom_stream = StrippingStream('CustomStream')
custom_line = 'Stripping'+line

for stream in streams:
   for line in stream.lines:
	if line.name() == custom_line:
	   custom_stream.appendLines([line])

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [custom_stream],
      MaxCandidates = 2000,
      AcceptBadEvents = False,
      BadEventSelection = filterBadEvents,
      TESPrefix = 'Strip' )

from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections())
sr.OnlyPositive = False
location = ''

# Need to remove stripping banks (if stripping has previously been run)
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]

mct = MCDecayTreeTuple('mct')
mct.Decay = "[B0 ==> ^(D*(2010)- ==> ^(D~0 ==> ^K+ ^pi- ^pi- ^pi+) ^pi-) ^mu+ ^nu_mu]CC"
mct.Branches = {
	"B"      : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) pi-) mu+ nu_mu]CC"
	,"Dst"   : "[B0 ==> ^(D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) pi-) mu+ nu_mu]CC"
	,"D0"    : "[B0 ==> (D*(2010)- ==> ^(D~0 ==> K+ pi- pi- pi+) pi-) mu+ nu_mu]CC"
	,"DKp"   : "[B0 ==> (D*(2010)- ==> (D~0 ==> ^K+ pi- pi- pi+) pi-) mu+ nu_mu]CC"
	,"Dpi1"  : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ ^pi- pi- pi+) pi-) mu+ nu_mu]CC"
	,"Dpi2"  : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- ^pi- pi+) pi-) mu+ nu_mu]CC"
	,"PartPi": "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- ^pi+) pi-) mu+ nu_mu]CC"
	,"pis"   : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) ^pi-) mu+ nu_mu]CC"
	,"mu"    : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) pi-) ^mu+ nu_mu]CC"
	,"nu"    : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) pi-) mu+ ^nu_mu]CC"
      }
mctl=[ 'TupleToolRecoStats','MCTupleToolAngles', 'MCTupleToolHierarchy', 'MCTupleToolKinematic', 'MCTupleToolPrimaries', 'MCTupleToolReconstructed', "MCTupleToolInteractions" ]
mct.ToolList=mctl 

from Configurables import TupleToolDecayTreeFitter
from Configurables import LoKi__Hybrid__TupleTool

from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand

#location = 'Phys/CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piRS/Particles'
#location = '/Event/Phys/CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piRS/Particles'
location = '/Event/AllStreams/Phys/CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piRS/Particles'
#location = '/Event/CharmCompleteEvent/Phys/CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piRS/Particles'
tupRS = DecayTreeTuple( 'tupRS' )
tupRS.Inputs = [ '/Event/Phys/{0}/Particles'.format(line) ]
#tupRS.Inputs = [ location ]
tupRS.ReFitPVs = False
tupRS.Decay = "[B0 -> ^(D~0 -> ^K+ ^pi- ^pi-) ^(K*(892)+ -> ^mu+ ^pi-)]CC"
tupRS.Branches = {
	"B"      : "[B0 -> (D~0 -> K+ pi- pi-) (K*(892)+ -> mu+ pi-) ]CC"
	,"D0"    : "[B0 -> ^(D~0 -> K+ pi- pi-) (K*(892)+ -> mu+ pi-) ]CC"
	,"DKp"   : "[B0 -> (D~0 -> ^K+ pi- pi-) (K*(892)+ -> mu+ pi-) ]CC"
	,"Dpi1"  : "[B0 -> (D~0 -> K+ ^pi- pi-) (K*(892)+ -> mu+ pi-) ]CC"
	,"Dpi2"  : "[B0 -> (D~0 -> K+ pi- ^pi-) (K*(892)+ -> mu+ pi-) ]CC"
	,"Kst"   : "[B0 -> (D~0 -> K+ pi- pi-) ^(K*(892)+ -> mu+ pi-) ]CC"
	,"mu"    : "[B0 -> (D~0 -> K+ pi- pi-) (K*(892)+ -> ^mu+ pi-) ]CC"
	,"pis"   : "[B0 -> (D~0 -> K+ pi- pi-) (K*(892)+ -> mu+ ^pi-) ]CC"
	}

from Configurables import LoKi__Hybrid__EvtTupleTool, LoKi__Hybrid__TupleTool
from DecayTreeTuple.Configuration import *

LoKi_B = LoKi__Hybrid__TupleTool("LoKi_B")
LoKi_B.Preambulo += ["PFUNA = LoKi.Particles.PFunA" ]
LoKi_B.Variables=  {
	"AMAXDOCA" : "PFUNA(AMAXDOCA('LoKi::DistanceCalculator'))"
	,"ADOCACHI2max": "LoKi.Particles.PFunA(ADOCACHI2('LoKi::TrgDistanceCalculator'))"
	,"BPVVDCHI2" : "BPVVDCHI2"
	,"Best_PV_CORRM" : "BPVCORRM"
	,"PHI" : "PHI"
	,"CharmMuETA" : "ETA"
	,"LV01"         : "LV01"
	,"LV02"         : "LV02"
	}

LoKi_Mu = LoKi__Hybrid__TupleTool("LoKi_Mu")
LoKi_Mu.Variables =  {
	"NSHAREDMU" : "NSHAREDMU"
	}

for particle in ["B", "D0", "DKp", "Dpi1", "Dpi2", "Kst", "mu", "pis"]:
   tupRS.addTool(TupleToolDecay, name = particle)

tuples = [ tupRS ]

for tup in tuples:
   tup.ToolList = toollist 
   for particle in [ tup.B, tup.D0 ]:
      particle.ToolList+=[ "TupleToolTISTOS" ]
      particle.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
      particle.TupleToolTISTOS.Verbose=True
      particle.TupleToolTISTOS.VerboseL0   = True
      particle.TupleToolTISTOS.VerboseHlt1 = True
      particle.TupleToolTISTOS.VerboseHlt2 = True
      from Configurables import TriggerTisTos
      particle.TupleToolTISTOS.addTool(TriggerTisTos())
      particle.TupleToolTISTOS.TriggerTisTos.TOSFracMuon = 0.
      particle.TupleToolTISTOS.TriggerTisTos.TOSFracEcal = 0.
      particle.TupleToolTISTOS.TriggerTisTos.TOSFracHcal = 0.
      particle.TupleToolTISTOS.TriggerTisTos.PropertiesPrint = True
      particle.TupleToolTISTOS.TriggerList = mtl
   tup.B.addTool( LoKi_B )
   tup.B.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_B"]
   tup.mu.addTool( LoKi_Mu )
   tup.mu.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Mu"]

from Configurables import DecayTreeTuple, MCMatchObjP2MCRelator 
import DecayTreeTuple.Configuration 
default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations') 
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc] 

MCTruth = TupleToolMCTruth()
MCTruth.ToolList =  [
	"MCTupleToolAngles"
	, "MCTupleToolHierarchy"
	, "MCTupleToolKinematic"
	, "MCTupleToolReconstructed"
	]
MCTruth.addTool(MCMatchObjP2MCRelator) 
MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs 
tupRS.addTool(MCTruth)

SeqPhys = GaudiSequencer("SeqPhys")

from Configurables import CheckPV
checkpv = CheckPV()

SeqPhys.Members += [checkpv,tupRS]

from Configurables import DaVinci
DaVinci().HistogramFile = "DVHisto.root"    # Histogram file
DaVinci().TupleFile = "Tuple.root"           #Tuple file
DaVinci().EvtMax = -1  # Number of events
DaVinci().PrintFreq = 10000
DaVinci().DataType = "2018"                    # Default is "DC06"
DaVinci().InputType = 'DST'
DaVinci().Simulation   = True                  # It's MC

#all the algorimthm that will be processed
DaVinci().UserAlgorithms += [ SeqPhys, mct, tupRS]
DaVinci().Lumi = False

#DaVinci().DDDBtag = "dddb-20170721-3"
#DaVinci().CondDBtag = "sim-20170721-2-vc-md100"
DaVinci().appendToMainSequence([eventNodeKiller])
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([sr])

from Gaudi.Configuration import *
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles(['/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00103234/0000/00103234_00000808_7.AllStreams.dst'], clear=True) #11576030

