from Configurables import FilterInTrees, EventNodeKiller
from PhysSelPython.Wrappers import Selection, DataOnDemand

from Configurables import DaVinci
from GaudiConf import IOHelper

# Load algorithms
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from Configurables import TupleToolFitMissPion
from Configurables import TupleToolFitRecoPion
from DecayTreeTuple.Configuration import *

# Load input particles
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons as Kaons
# Load Selection objects
from PhysConf.Selections import CombineSelection, FilterSelection
from PhysConf.Selections import SelectionSequence

from Configurables import CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
      EventNodeKiller,
      ProcStatusCheck,
      DaVinci,
      DecayTreeTuple
      )
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *

import shelve

toollist = [ 
      "TupleToolEventInfo",
      "TupleToolTISTOS",
      "TupleToolGeometry",
      "TupleToolKinematic",
      "TupleToolTrackInfo",
      "TupleToolPid",
      "TupleToolANNPID",
      "TupleToolDira",
      "TupleToolAngles",
      "TupleToolPrimaries",
      "TupleToolDecayTreeFitter",
      "TupleToolPropertime",
      "TupleToolVtxIsoln",
      "TupleToolMCBackgroundInfo",
      "TupleToolMCTruth",
      "TupleToolRecoStats"
      ]  

mtl = [
      "L0MuonDecision",
      "L0HadronDecision",
      "Hlt1TrackAllL0Decision",
      "Hlt1TrackMuonDecision",
      "Hlt2SingleMuonDecision",
      "Hlt2TopoMu2BodyBBDTDecision",
      "Hlt2TopoMu3BodyBBDTDecision",
      "Hlt2TopoMu4BodyBBDTDecision"
      ]

stream = 'CharmCompleteEvent'
line = 'CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piWS'
location = '/Event/{0}/Phys/{1}/Particles'.format(stream, line)

Pions = DataOnDemand('Phys/StdAllNoPIDsPions/Particles')

D0_from_strip = FilterInTrees('D0_from_strip_filter', Code="('D0' == ABSID)")
D0_from_strip_sel = Selection("D0_from_strip_sel",
                            Algorithm=D0_from_strip,
                            RequiredSelections=[DataOnDemand(Location=location)])
Kst_from_strip = FilterInTrees('Kst_from_strip_filter', Code="('K*(892)+' == ABSID)")
Kst_from_strip_sel = Selection("Kst_from_strip_sel",
                            Algorithm=Kst_from_strip,
                            RequiredSelections=[DataOnDemand(Location=location)])

pion_from_D0 = FilterInTrees('pion_from_D0_filter', Code="('pi-' == ABSID)")
pion_from_D0_sel = Selection("pion_from_D0_sel",
                            Algorithm=pion_from_D0,
                            RequiredSelections=[D0_from_strip_sel])
kaon_from_D0 = FilterInTrees('kaon_from_D0_filter', Code="('K+' == ABSID)")
kaon_from_D0_sel = Selection("kaon_from_D0_sel",
                            Algorithm=kaon_from_D0,
                            RequiredSelections=[D0_from_strip_sel])
pion_from_strip = FilterInTrees('pion_from_strip_filter', Code="('pi-' == ABSID)")
pion_from_strip_sel = Selection("pion_from_strip_sel",
                            Algorithm=pion_from_strip,
                            RequiredSelections=[Kst_from_strip_sel])
muon_from_strip = FilterInTrees('muon_from_strip_filter', Code="('mu+' == ABSID)")
muon_from_strip_sel = Selection("muon_from_strip_sel",
                            Algorithm=muon_from_strip,
                            RequiredSelections=[Kst_from_strip_sel])


d0_comb = "(ADAMASS('D0') < 100*MeV)"
d0_mother = ('(VFASPF(VCHI2/VDOF)< 9)')
dst_comb = "(ADAMASS('D*(2010)+') < 100*MeV)"
dst_mother = ('(VFASPF(VCHI2/VDOF)< 9)')
pion_daughter = "(P>2000.0) & (PT > 250.0 )& (TRCHI2DOF < 3.0)& (TRGHOSTPROB < 0.35)& (MIPCHI2DV(PRIMARY)> 4.0) & (PIDK< 10.0)" 
b0_comb = "(AM > 2200.0) & (AM < 8000.0) & (ADOCACHI2CUT( 10, ''))"
b0_mother = ('(VFASPF(VCHI2/VDOF)< 9)')

real_PartialDst_sel = CombineSelection('Sel_PartDst',[D0_from_strip_sel, pion_from_strip_sel],
    DecayDescriptors=['[D*(2010)+ -> D0 pi+]cc','[D*(2010)+ -> D0 pi-]cc'],
    CombinationCut="AALL",MotherCut=dst_mother,
    DaughtersCuts = {"pi+" : "ALL"}
)
real_PartialB_sel = CombineSelection('Sel_PartB0',[real_PartialDst_sel, muon_from_strip_sel],
    DecayDescriptors=['[B~0 -> D*(2010)+ mu-]cc','[B~0 -> D*(2010)+ mu+]cc'],
    CombinationCut="AALL",MotherCut=dst_mother,
    DaughtersCuts = {"mu+" : pion_daughter}
)

#Combine D and pions into Dstar
real_d0_sel = CombineSelection('Sel_D0',[D0_from_strip_sel, Pions],
    DecayDescriptors=['[D+ -> D0 pi+]cc', '[D+ -> D0 pi-]cc'],
    CombinationCut=d0_comb,MotherCut=d0_mother,
    DaughtersCuts = {"pi+" : pion_daughter}
)

#Combine D and pions into Dstar
real_dst_sel = CombineSelection('Sel_Dst',[real_d0_sel, pion_from_strip_sel],
    DecayDescriptors=['[D*(2010)+ -> D+ pi+]cc', '[D*(2010)+ -> D+ pi-]cc'],
    CombinationCut=dst_comb,MotherCut=dst_mother
)

#Combine D and pions into Dstar
real_b0_sel = CombineSelection('Sel_B0',[real_dst_sel, muon_from_strip_sel],
    DecayDescriptors=['[B_s0 -> D*(2010)+ mu-]cc', '[B_s0 -> D*(2010)+ mu+]cc'],
    CombinationCut=b0_comb, MotherCut=b0_mother
)

dstar_seq = SelectionSequence('D0Seq', TopSelection=real_b0_sel)
partialdstar_seq = SelectionSequence('PartialD0Seq', TopSelection=real_PartialB_sel)

# Create an ntuple

tuple_partial_0 = DecayTreeTuple('TuplePartial0')
tuple_partial_1 = DecayTreeTuple('TuplePartial1')
tuple_partial_2 = DecayTreeTuple('TuplePartial2')
tuple_partial_3 = DecayTreeTuple('TuplePartial3')
tuple_partial_4 = DecayTreeTuple('TuplePartial4')
tuple_partial_5 = DecayTreeTuple('TuplePartial5')
tuple_partial_6 = DecayTreeTuple('TuplePartial6')
tuple_partial_7 = DecayTreeTuple('TuplePartial7')
tuple_partial_0.Inputs = partialdstar_seq.outputLocations()
tuple_partial_1.Inputs = partialdstar_seq.outputLocations()
tuple_partial_2.Inputs = partialdstar_seq.outputLocations()
tuple_partial_3.Inputs = partialdstar_seq.outputLocations()
tuple_partial_4.Inputs = partialdstar_seq.outputLocations()
tuple_partial_5.Inputs = partialdstar_seq.outputLocations()
tuple_partial_6.Inputs = partialdstar_seq.outputLocations()
tuple_partial_7.Inputs = partialdstar_seq.outputLocations()
tuple_partial_0.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K- ^pi- ^pi-) ^pi+) ^mu-]CC)'
tuple_partial_1.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K+ ^pi+ ^pi+) ^pi+) ^mu-]CC)'
tuple_partial_2.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K+ ^pi+ ^pi-) ^pi+) ^mu-]CC)'
tuple_partial_3.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^pi-) ^pi+) ^mu-]CC)'
tuple_partial_4.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K- ^pi- ^pi-) ^pi-) ^mu-]CC)'
tuple_partial_5.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K+ ^pi+ ^pi+) ^pi-) ^mu-]CC)'
tuple_partial_6.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K+ ^pi+ ^pi-) ^pi-) ^mu-]CC)'
tuple_partial_7.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^pi-) ^pi-) ^mu-]CC)'
tuple_partial_0.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi- pi-) pi+) mu-]CC) '
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K- pi- pi-) pi+) mu-]CC) '
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K- pi- pi-) pi+) mu-]CC) '
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K- pi- pi-) pi+) mu-]CC) '
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- ^pi- pi-) pi+) mu-]CC) '
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi- ^pi-) pi+) mu-]CC) '
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi- pi-) ^pi+) mu-]CC) '
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi- pi-) pi+) ^mu-]CC) '}
tuple_partial_1.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi+) pi+) mu-]CC) '
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K+ pi+ pi+) pi+) mu-]CC) '
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K+ pi+ pi+) pi+) mu-]CC) '
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K+ pi+ pi+) pi+) mu-]CC) '
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ ^pi+ pi+) pi+) mu-]CC) '
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ ^pi+) pi+) mu-]CC) '
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi+) ^pi+) mu-]CC) '
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi+) pi+) ^mu-]CC) '}
tuple_partial_2.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi-) pi+) mu-]CC) '
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K+ pi+ pi-) pi+) mu-]CC) '
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K+ pi+ pi-) pi+) mu-]CC) '
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K+ pi+ pi-) pi+) mu-]CC) '
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ ^pi+ pi-) pi+) mu-]CC) '
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ ^pi-) pi+) mu-]CC) '
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi-) ^pi+) mu-]CC) '
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi-) pi+) ^mu-]CC) '}
tuple_partial_3.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi- pi-) pi+) mu-]CC) '
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K+ pi- pi-) pi+) mu-]CC) '
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K+ pi- pi-) pi+) mu-]CC) '
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K+ pi- pi-) pi+) mu-]CC) '
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ ^pi- pi-) pi+) mu-]CC) '
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi- ^pi-) pi+) mu-]CC) '
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi- pi-) ^pi+) mu-]CC) '
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi- pi-) pi+) ^mu-]CC) '}
tuple_partial_4.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi- pi-) pi-) mu-]CC) '
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K- pi- pi-) pi-) mu-]CC) '
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K- pi- pi-) pi-) mu-]CC) '
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K- pi- pi-) pi-) mu-]CC) '
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- ^pi- pi-) pi-) mu-]CC) '
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi- ^pi-) pi-) mu-]CC) '
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi- pi-) ^pi-) mu-]CC) '
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi- pi-) pi-) ^mu-]CC) '}
tuple_partial_5.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi+) pi-) mu-]CC) '
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K+ pi+ pi+) pi-) mu-]CC) '
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K+ pi+ pi+) pi-) mu-]CC) '
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K+ pi+ pi+) pi-) mu-]CC) '
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ ^pi+ pi+) pi-) mu-]CC) '
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ ^pi+) pi-) mu-]CC) '
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi+) ^pi-) mu-]CC) '
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi+) pi-) ^mu-]CC) '}
tuple_partial_6.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi-) pi-) mu-]CC) '
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K+ pi+ pi-) pi-) mu-]CC) '
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K+ pi+ pi-) pi-) mu-]CC) '
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K+ pi+ pi-) pi-) mu-]CC) '
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ ^pi+ pi-) pi-) mu-]CC) '
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ ^pi-) pi-) mu-]CC) '
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi-) ^pi-) mu-]CC) '
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi+ pi-) pi-) ^mu-]CC) '}
tuple_partial_7.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi- pi-) pi-) mu-]CC) '
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K+ pi- pi-) pi-) mu-]CC) '
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K+ pi- pi-) pi-) mu-]CC) '
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K+ pi- pi-) pi-) mu-]CC) '
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ ^pi- pi-) pi-) mu-]CC) '
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi- ^pi-) pi-) mu-]CC) '
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi- pi-) ^pi-) mu-]CC) '
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K+ pi- pi-) pi-) ^mu-]CC) '}

tuple_full_0 = DecayTreeTuple('TupleFull0')
tuple_full_1 = DecayTreeTuple('TupleFull1')
tuple_full_2 = DecayTreeTuple('TupleFull2')
tuple_full_3 = DecayTreeTuple('TupleFull3')
tuple_full_4 = DecayTreeTuple('TupleFull4')
tuple_full_5 = DecayTreeTuple('TupleFull5')
tuple_full_6 = DecayTreeTuple('TupleFull6')
tuple_full_7 = DecayTreeTuple('TupleFull7')
tuple_full_8 = DecayTreeTuple('TupleFull8')
tuple_full_9 = DecayTreeTuple('TupleFull9')
tuple_full_10 = DecayTreeTuple('TupleFull10')
tuple_full_11 = DecayTreeTuple('TupleFull11')
tuple_full_12 = DecayTreeTuple('TupleFull12')
tuple_full_13 = DecayTreeTuple('TupleFull13')
tuple_full_14 = DecayTreeTuple('TupleFull14')
tuple_full_15 = DecayTreeTuple('TupleFull15')
tuple_full_0.Inputs = dstar_seq.outputLocations()
tuple_full_1.Inputs = dstar_seq.outputLocations()
tuple_full_2.Inputs = dstar_seq.outputLocations()
tuple_full_3.Inputs = dstar_seq.outputLocations()
tuple_full_4.Inputs = dstar_seq.outputLocations()
tuple_full_5.Inputs = dstar_seq.outputLocations()
tuple_full_6.Inputs = dstar_seq.outputLocations()
tuple_full_7.Inputs = dstar_seq.outputLocations()
tuple_full_8.Inputs = dstar_seq.outputLocations()
tuple_full_9.Inputs = dstar_seq.outputLocations()
tuple_full_10.Inputs = dstar_seq.outputLocations()
tuple_full_11.Inputs = dstar_seq.outputLocations()
tuple_full_12.Inputs = dstar_seq.outputLocations()
tuple_full_13.Inputs = dstar_seq.outputLocations()
tuple_full_14.Inputs = dstar_seq.outputLocations()
tuple_full_15.Inputs = dstar_seq.outputLocations()
tuple_full_0.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi+ ^pi+) ^pi+) ^pi+) ^mu-]CC)'
tuple_full_1.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi- ^pi-) ^pi+) ^pi+) ^mu-]CC)'
tuple_full_2.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi+ ^pi-) ^pi-) ^pi+) ^mu-]CC)'
tuple_full_3.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi- ^pi-) ^pi-) ^pi+) ^mu-]CC)'
tuple_full_4.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K+ ^pi+ ^pi-) ^pi+) ^pi+) ^mu-]CC)'
tuple_full_5.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K+ ^pi+ ^pi+) ^pi+) ^pi+) ^mu-]CC)'
tuple_full_6.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K+ ^pi+ ^pi-) ^pi-) ^pi+) ^mu-]CC)'
tuple_full_7.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K+ ^pi+ ^pi+) ^pi-) ^pi+) ^mu-]CC)'
tuple_full_8.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi+ ^pi+) ^pi+) ^pi-) ^mu-]CC)'
tuple_full_9.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi- ^pi-) ^pi+) ^pi-) ^mu-]CC)'
tuple_full_10.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi+ ^pi-) ^pi-) ^pi-) ^mu-]CC)'
tuple_full_11.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi- ^pi-) ^pi-) ^pi-) ^mu-]CC)'
tuple_full_12.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K+ ^pi+ ^pi-) ^pi+) ^pi-) ^mu-]CC)'
tuple_full_13.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K+ ^pi+ ^pi+) ^pi+) ^pi-) ^mu-]CC)'
tuple_full_14.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K+ ^pi+ ^pi-) ^pi-) ^pi-) ^mu-]CC)'
tuple_full_15.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K+ ^pi+ ^pi+) ^pi-) ^pi-) ^mu-]CC)'

tuple_full_0.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ ^pi+) pi+) pi+ )mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) ^pi+) pi+ )mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi+) ^pi+ )mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi+) pi+) ^mu-]CC) '}
tuple_full_1.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi+) pi+ )mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi+) pi+ )mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi- pi-) pi+) pi+ )mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi- pi-) pi+) pi+ )mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi- pi-) pi+) pi+ )mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi- pi-) pi+) pi+ )mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- ^pi-) pi+) pi+ )mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) ^pi+) pi+ )mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi+) ^pi+ )mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi+) pi+) ^mu-]CC) '}
tuple_full_2.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ ^pi-) pi-) pi+ )mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) ^pi-) pi+ )mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi-) ^pi+ )mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi-) pi+) ^mu-]CC) '}
tuple_full_3.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi-) pi+ )mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi-) pi+ )mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi- pi-) pi-) pi+ )mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi- pi-) pi-) pi+ )mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi- pi-) pi-) pi+ )mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi- pi-) pi-) pi+ )mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- ^pi-) pi-) pi+ )mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) ^pi-) pi+ )mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi-) ^pi+ )mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi-) pi+) ^mu-]CC) '}
tuple_full_4.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi+) pi+ )mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi+) pi+ )mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K+ pi+ pi-) pi+) pi+ )mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K+ pi+ pi-) pi+) pi+ )mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K+ pi+ pi-) pi+) pi+ )mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ ^pi+ pi-) pi+) pi+ )mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ ^pi-) pi+) pi+ )mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) ^pi+) pi+ )mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi+) ^pi+ )mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi+) pi+) ^mu-]CC) '}
tuple_full_5.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K+ pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K+ pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K+ pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ ^pi+ pi+) pi+) pi+ )mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ ^pi+) pi+) pi+ )mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) ^pi+) pi+ )mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi+) ^pi+ )mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi+) pi+) ^mu-]CC) '}
tuple_full_6.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K+ pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K+ pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K+ pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ ^pi+ pi-) pi-) pi+ )mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ ^pi-) pi-) pi+ )mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) ^pi-) pi+ )mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi-) ^pi+ )mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi-) pi+) ^mu-]CC) '}
tuple_full_7.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi-) pi+ )mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi-) pi+ )mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K+ pi+ pi+) pi-) pi+ )mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K+ pi+ pi+) pi-) pi+ )mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K+ pi+ pi+) pi-) pi+ )mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ ^pi+ pi+) pi-) pi+ )mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ ^pi+) pi-) pi+ )mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) ^pi-) pi+ )mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi-) ^pi+ )mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi-) pi+) ^mu-]CC) '}
tuple_full_8.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi+) pi-) mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi+) pi-) mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi+ pi+) pi+) pi-) mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi+ pi+) pi+) pi-) mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi+ pi+) pi+) pi-) mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi+ pi+) pi+) pi-) mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ ^pi+) pi+) pi-) mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) ^pi+) pi-) mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi+) ^pi-) mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi+) pi-) ^mu-]CC) '}
tuple_full_9.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi+) pi-) mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi+) pi-) mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi- pi-) pi+) pi-) mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi- pi-) pi+) pi-) mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi- pi-) pi+) pi-) mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi- pi-) pi+) pi-) mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- ^pi-) pi+) pi-) mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) ^pi+) pi-) mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi+) ^pi-) mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi+) pi-) ^mu-]CC) '}
tuple_full_10.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi-) pi-) mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi-) pi-) mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi+ pi-) pi-) pi-) mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi+ pi-) pi-) pi-) mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi+ pi-) pi-) pi-) mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi+ pi-) pi-) pi-) mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ ^pi-) pi-) pi-) mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) ^pi-) pi-) mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi-) ^pi-) mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi-) pi-) ^mu-]CC) '}
tuple_full_11.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi-) pi-) mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi-) pi-) mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi- pi-) pi-) pi-) mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi- pi-) pi-) pi-) mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi- pi-) pi-) pi-) mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi- pi-) pi-) pi-) mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- ^pi-) pi-) pi-) mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) ^pi-) pi-) mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi-) ^pi-) mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi- pi-) pi-) pi-) ^mu-]CC) '}
tuple_full_12.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi+) pi-) mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi+) pi-) mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K+ pi+ pi-) pi+) pi-) mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K+ pi+ pi-) pi+) pi-) mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K+ pi+ pi-) pi+) pi-) mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ ^pi+ pi-) pi+) pi-) mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ ^pi-) pi+) pi-) mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) ^pi+) pi-) mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi+) ^pi-) mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi+) pi-) ^mu-]CC) '}
tuple_full_13.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi+) pi-) mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi+) pi-) mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K+ pi+ pi+) pi+) pi-) mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K+ pi+ pi+) pi+) pi-) mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K+ pi+ pi+) pi+) pi-) mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ ^pi+ pi+) pi+) pi-) mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ ^pi+) pi+) pi-) mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) ^pi+) pi-) mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi+) ^pi-) mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi+) pi-) ^mu-]CC) '}
tuple_full_14.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi-) pi-) mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi-) pi-) mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K+ pi+ pi-) pi-) pi-) mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K+ pi+ pi-) pi-) pi-) mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K+ pi+ pi-) pi-) pi-) mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ ^pi+ pi-) pi-) pi-) mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ ^pi-) pi-) pi-) mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) ^pi-) pi-) mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi-) ^pi-) mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi-) pi-) pi-) ^mu-]CC) '}
tuple_full_15.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi-) pi-) mu-]CC) '
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi-) pi-) mu-]CC) '
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K+ pi+ pi+) pi-) pi-) mu-]CC) '
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K+ pi+ pi+) pi-) pi-) mu-]CC) '
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K+ pi+ pi+) pi-) pi-) mu-]CC) '
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ ^pi+ pi+) pi-) pi-) mu-]CC) '
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ ^pi+) pi-) pi-) mu-]CC) '
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) ^pi-) pi-) mu-]CC) '
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi-) ^pi-) mu-]CC) '
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K+ pi+ pi+) pi-) pi-) ^mu-]CC) '}


tuples = [tuple_partial_0
      ,tuple_partial_1
      ,tuple_partial_2
      ,tuple_partial_3
      ,tuple_partial_4
      ,tuple_partial_5
      ,tuple_partial_6
      ,tuple_partial_7
      , tuple_full_0
      , tuple_full_1
      , tuple_full_2
      , tuple_full_3
      , tuple_full_4
      , tuple_full_5
      , tuple_full_6
      , tuple_full_7
      , tuple_full_8
      , tuple_full_9
      , tuple_full_10
      , tuple_full_11
      , tuple_full_12
      , tuple_full_13
      , tuple_full_14
      , tuple_full_15]

DaVinci().UserAlgorithms += [dstar_seq.sequence(),partialdstar_seq.sequence()]
for tup in tuples:
   tup.ToolList = toollist
   tup.addTool(TupleToolDecay, name = "Dst") 
   tup.Dst.ToolList += ["TupleToolFitMissPion"]
   DaVinci().UserAlgorithms += [tup]


# DaVinci configuration
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'Tuple.root'
DaVinci().PrintFreq = 5000
DaVinci().DataType = '2017'
DaVinci().Simulation = False
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1

#IOHelper().inputFiles([
#'/eos/lhcb/grid/prod/lhcb/LHCb/Collision18/CHARMCOMPLETEEVENT.DST/00077434/0000/00077434_00009962_1.charmcompleteevent.dst'
#], clear=True)
