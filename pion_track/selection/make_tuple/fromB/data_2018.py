from Configurables import FilterInTrees, EventNodeKiller
from PhysSelPython.Wrappers import Selection, DataOnDemand

from Configurables import DaVinci
from GaudiConf import IOHelper

# Load algorithms
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from Configurables import TupleToolFitMissPion
from Configurables import TupleToolFitRecoPion
from DecayTreeTuple.Configuration import *

# Load input particles
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons as Kaons
# Load Selection objects
from PhysConf.Selections import CombineSelection, FilterSelection
from PhysConf.Selections import SelectionSequence

from Configurables import CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
      EventNodeKiller,
      ProcStatusCheck,
      DaVinci,
      DecayTreeTuple
      )
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *

import shelve

toollist = [ 
      "TupleToolEventInfo",
      "TupleToolTISTOS",
      "TupleToolGeometry",
      "TupleToolKinematic",
      "TupleToolTrackInfo",
      "TupleToolPid",
      "TupleToolANNPID",
      "TupleToolDira",
      "TupleToolAngles",
      "TupleToolPrimaries",
      "TupleToolDecayTreeFitter",
      "TupleToolPropertime",
      "TupleToolVtxIsoln",
      "TupleToolMCBackgroundInfo",
      "TupleToolMCTruth",
      "TupleToolRecoStats"
      ]  

mtl = [
      "L0MuonDecision",
      "L0HadronDecision",
      "Hlt1TrackAllL0Decision",
      "Hlt1TrackMuonDecision",
      "Hlt2SingleMuonDecision",
      "Hlt2TopoMu2BodyBBDTDecision",
      "Hlt2TopoMu3BodyBBDTDecision",
      "Hlt2TopoMu4BodyBBDTDecision"
      ]

stream = 'CharmCompleteEvent'
line = 'CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piRS'
location = '/Event/{0}/Phys/{1}/Particles'.format(stream, line)

Pions = DataOnDemand('Phys/StdAllNoPIDsPions/Particles')

D0_from_strip = FilterInTrees('D0_from_strip_filter', Code="('D0' == ABSID)")
D0_from_strip_sel = Selection("D0_from_strip_sel",
                            Algorithm=D0_from_strip,
                            RequiredSelections=[DataOnDemand(Location=location)])
Kst_from_strip = FilterInTrees('Kst_from_strip_filter', Code="('K*(892)+' == ABSID)")
Kst_from_strip_sel = Selection("Kst_from_strip_sel",
                            Algorithm=Kst_from_strip,
                            RequiredSelections=[DataOnDemand(Location=location)])

pion_from_D0 = FilterInTrees('pion_from_D0_filter', Code="('pi-' == ABSID)")
pion_from_D0_sel = Selection("pion_from_D0_sel",
                            Algorithm=pion_from_D0,
                            RequiredSelections=[D0_from_strip_sel])
kaon_from_D0 = FilterInTrees('kaon_from_D0_filter', Code="('K+' == ABSID)")
kaon_from_D0_sel = Selection("kaon_from_D0_sel",
                            Algorithm=kaon_from_D0,
                            RequiredSelections=[D0_from_strip_sel])
pion_from_strip = FilterInTrees('pion_from_strip_filter', Code="('pi-' == ABSID)")
pion_from_strip_sel = Selection("pion_from_strip_sel",
                            Algorithm=pion_from_strip,
                            RequiredSelections=[Kst_from_strip_sel])
muon_from_strip = FilterInTrees('muon_from_strip_filter', Code="('mu+' == ABSID)")
muon_from_strip_sel = Selection("muon_from_strip_sel",
                            Algorithm=muon_from_strip,
                            RequiredSelections=[Kst_from_strip_sel])

d0_comb = "(ADAMASS('D0') < 100*MeV)"
d0_mother = ('(VFASPF(VCHI2/VDOF)< 9)')
dst_comb = "(ADAMASS('D*(2010)+') < 100*MeV)"
dst_mother = ('(VFASPF(VCHI2/VDOF)< 9)')
pion_daughter = "(P>2000.0) & (PT > 250.0 )& (TRCHI2DOF < 3.0)& (TRGHOSTPROB < 0.35)& (MIPCHI2DV(PRIMARY)> 4.0) & (PIDK< 10.0)" 
b0_comb = "(AM > 2200.0) & (AM < 8000.0) & (ADOCACHI2CUT( 10, ''))"
b0_mother = ('(VFASPF(VCHI2/VDOF)< 9)')

real_PartialDst_sel = CombineSelection('Sel_PartDst',[D0_from_strip_sel, pion_from_strip_sel],
    DecayDescriptors=['[D*(2010)+ -> D0 pi+]cc'],
    CombinationCut="AALL",MotherCut=dst_mother,
    DaughtersCuts = {"pi+" : "ALL"}
)
real_PartialB_sel = CombineSelection('Sel_PartB0',[real_PartialDst_sel, muon_from_strip_sel],
    DecayDescriptors=['[B~0 -> D*(2010)+ mu-]cc'],
    CombinationCut="AALL",MotherCut=dst_mother,
    DaughtersCuts = {"mu+" : pion_daughter}
)

#Combine D and pions into Dstar
real_d0_sel = CombineSelection('Sel_D0',[D0_from_strip_sel, Pions],
    DecayDescriptors=['[D+ -> D0 pi+]cc', '[D+ -> D0 pi-]cc'],
    CombinationCut=d0_comb,MotherCut=d0_mother,
    DaughtersCuts = {"pi+" : pion_daughter}
)

#Combine D and pions into Dstar
real_dst_sel = CombineSelection('Sel_Dst',[real_d0_sel, pion_from_strip_sel],
    DecayDescriptors=['[D*(2010)+ -> D+ pi+]cc'],
    CombinationCut=dst_comb,MotherCut=dst_mother
)

#Combine D and pions into Dstar
real_b0_sel = CombineSelection('Sel_B0',[real_dst_sel, muon_from_strip_sel],
    DecayDescriptor='[B_s0 -> D*(2010)+ mu-]cc',
    CombinationCut=b0_comb, MotherCut=b0_mother
)

dstar_seq = SelectionSequence('D0Seq', TopSelection=real_b0_sel)
partialdstar_seq = SelectionSequence('PartialD0Seq', TopSelection=real_PartialB_sel)

# Create an ntuple
tuple_partial_RS = DecayTreeTuple('TuplePartialRS')
tuple_partial_RS.Inputs = partialdstar_seq.outputLocations()
tuple_partial_RS.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi-) ^pi+) ^mu-]CC) || ([B~0 -> ^(D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi+) ^pi+) ^mu-]CC)'
tuple_partial_RS.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi+ pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- pi+ pi+) pi+) mu-]CC)'
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K- pi+ pi-) pi+) mu-]CC) || ([B~0 -> ^(D*(2010)+ -> (D0 -> K- pi+ pi+) pi+) mu-]CC)'
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K- pi+ pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> ^(D0 -> K- pi+ pi+) pi+) mu-]CC)'
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K- pi+ pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> ^K- pi+ pi+) pi+) mu-]CC)'
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- ^pi+ pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- ^pi+ pi+) pi+) mu-]CC)'
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi+ ^pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- pi+ ^pi+) pi+) mu-]CC)'
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi+ pi-) ^pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- pi+ pi+) ^pi+) mu-]CC)'
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi+ pi-) pi+) ^mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- pi+ pi+) pi+) ^mu-]CC)'
}


tuple_full_RS = DecayTreeTuple('TupleFullRS')
tuple_full_RS.Inputs = dstar_seq.outputLocations()
tuple_full_RS.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi+ ^pi-) ^pi+) ^pi+) ^mu-]CC) || ([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi+ ^pi+) ^pi-) ^pi+) ^mu-]CC)'
tuple_full_RS.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi+ pi+) pi-) pi+) mu-]CC)'
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ ^pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ ^pi+) pi-) pi+) mu-]CC)'
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) ^pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) ^pi-) pi+) mu-]CC)'
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) ^pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi-) ^pi+) mu-]CC)'
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) pi+) ^mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi-) pi+) ^mu-]CC)'
}

tuples = [tuple_full_RS, tuple_partial_RS]

for tup in tuples:
   tup.ToolList = toollist
tuple_partial_RS.addTool(TupleToolDecay, name = "Dst")
tuple_partial_RS.Dst.ToolList += ["TupleToolFitMissPion"]
tuple_full_RS.addTool(TupleToolDecay, name = "Dst")
tuple_full_RS.Dst.ToolList += ["TupleToolFitRecoPion"]

DaVinci().UserAlgorithms += [dstar_seq.sequence(), partialdstar_seq.sequence(), tuple_full_RS, tuple_partial_RS]

# DaVinci configuration
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'Tuple.root'
DaVinci().PrintFreq = 5000
DaVinci().DataType = '2018'
DaVinci().Simulation = False
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1

#IOHelper().inputFiles([
#'/eos/lhcb/grid/prod/lhcb/LHCb/Collision18/CHARMCOMPLETEEVENT.DST/00077434/0000/00077434_00009962_1.charmcompleteevent.dst'
#], clear=True)
