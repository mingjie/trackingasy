"""
Submit Bc->JpsiMuNu options 
"""
AppDirectory = "/afs/cern.ch/work/x/xuyuan/Asls/DaVinci/test/20210602/DaVinciDev_v45r1"

MgFd = ['Down', 'Up']
DtStr = [
      ['18','18','34']
      ]

for mag in range(len(MgFd)): 
   for strip in range(len(DtStr)):
      bkPath = '/MC/20{0}/Beam6500GeV-20{0}-Mag{1}-Nu1.6-25ns-Pythia8/Sim09h/Trig0x617d18a4/Reco{0}/Turbo05-WithTurcal/Stripping{3}NoPrescalingFlagged/11576030/ALLSTREAMS.DST'.format(DtStr[strip][0], MgFd[mag], DtStr[strip][1], DtStr[strip][2])
      jobname = DtStr[strip][0]+MgFd[mag]
      optfile = 'mc_20{0}.py'.format(DtStr[strip][0])
      output  = 'Tuple.root'
      print(bkPath, jobname, optfile)

      bkdata = BKQuery(bkPath, dqflag=['OK']).getDataset()

      myApp = GaudiExec()
      myApp.directory = AppDirectory

      mySplitter = SplitByFiles()
      #mySplitter.filesPerJob = 1
      #mySplitter.maxFiles = 3
      mySplitter.filesPerJob = 15
      mySplitter.maxFiles = -1

      myjob = Job(name=jobname)
      myjob.application = myApp
      myjob.application.options = [optfile]
      myjob.application.platform = 'x86_64-centos7-gcc8-opt' 
      myjob.inputdata = bkdata
      #myjob.backend=Local()
      #myjob.backend=Interactive()
      myjob.backend=Dirac()
      myjob.splitter = mySplitter
      #myjob.outputfiles = [LocalFile('*.root'),LocalFile('stdout')]
      myjob.outputfiles = [DiracFile(output)]
      myjob.submit()

