from Configurables import FilterInTrees, EventNodeKiller
from PhysSelPython.Wrappers import Selection, DataOnDemand

from Configurables import DaVinci
from GaudiConf import IOHelper

# Load algorithms
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from Configurables import TupleToolFitMissPion
from Configurables import TupleToolFitRecoPion
from DecayTreeTuple.Configuration import *

# Load input particles
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons as Kaons
# Load Selection objects
from PhysConf.Selections import CombineSelection, FilterSelection
from PhysConf.Selections import SelectionSequence

from Configurables import CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
      EventNodeKiller,
      ProcStatusCheck,
      DaVinci,
      DecayTreeTuple
      )
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *

import shelve

toollist = [ 
      "TupleToolEventInfo",
      "TupleToolTISTOS",
      "TupleToolGeometry",
      "TupleToolKinematic",
      "TupleToolTrackInfo",
      "TupleToolPid",
      "TupleToolANNPID",
      "TupleToolDira",
      "TupleToolAngles",
      "TupleToolPrimaries",
      "TupleToolDecayTreeFitter",
      "TupleToolPropertime",
      "TupleToolVtxIsoln",
      "TupleToolMCBackgroundInfo",
      "TupleToolMCTruth",
      "TupleToolRecoStats"
      ]  
mctl=[ 'TupleToolRecoStats','MCTupleToolAngles', 'MCTupleToolHierarchy', 'MCTupleToolKinematic', 'MCTupleToolPrimaries', 'MCTupleToolReconstructed', "MCTupleToolInteractions" ]

mtl = [
      "L0MuonDecision",
      "L0HadronDecision",
      "Hlt1TrackAllL0Decision",
      "Hlt1TrackMuonDecision",
      "Hlt2SingleMuonDecision",
      "Hlt2TopoMu2BodyBBDTDecision",
      "Hlt2TopoMu3BodyBBDTDecision",
      "Hlt2TopoMu4BodyBBDTDecision"
      ]

stream = 'AllStreams'
line = 'CharmFromBSemiForHadronAsy_B2DstarMuD0toK2piRS'
tesLoc = '/Event/{0}/Phys/{1}/Particles'.format(stream, line)

# Node killer: remove the previous Stripping
event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

# Build a new stream called 'CustomStream' that only
# contains the desired line
strip = 'stripping34'
config_db = strippingConfiguration(strip)
config = dict(config_db) # need to do this since the config_db is read-only
config['CharmFromBSemiForHadronAsy']['CONFIG']['K_PIDKMin'] = -1000
config['CharmFromBSemiForHadronAsy']['CONFIG']['Pi_PIDKMax'] = 1000
config['CharmFromBSemiForHadronAsy']['CONFIG']['Slowpi_PIDKMax'] = 1000

config_db_updated = shelve.open('tmp_stripping_config.db')
config_db_updated.update(config)

#streams = buildStreams(stripping=strippingConfiguration(strip),
#                       archive=strippingArchive(strip))
streams = buildStreams(stripping=config_db_updated,
                       archive=strippingArchive(strip))

custom_stream = StrippingStream('CustomStream')
custom_line = 'Stripping'+line 
location = 'Phys/{0}/Particles'.format(line)

for stream in streams:
   for sline in stream.lines:
      if sline.name() == custom_line:
         custom_stream.appendLines([sline])

# Create the actual Stripping configurable
filterBadEvents = ProcStatusCheck()

sc = StrippingConf(Streams=[custom_stream],
      MaxCandidates=2000,
      AcceptBadEvents=False,
      BadEventSelection=filterBadEvents)

Pions = DataOnDemand('Phys/StdAllNoPIDsPions/Particles')

D0_from_strip = FilterInTrees('D0_from_strip_filter', Code="('D0' == ABSID)")
D0_from_strip_sel = Selection("D0_from_strip_sel",
                            Algorithm=D0_from_strip,
                            RequiredSelections=[DataOnDemand(Location=location)])
Kst_from_strip = FilterInTrees('Kst_from_strip_filter', Code="('K*(892)+' == ABSID)")
Kst_from_strip_sel = Selection("Kst_from_strip_sel",
                            Algorithm=Kst_from_strip,
                            RequiredSelections=[DataOnDemand(Location=location)])

pion_from_D0 = FilterInTrees('pion_from_D0_filter', Code="('pi-' == ABSID)")
pion_from_D0_sel = Selection("pion_from_D0_sel",
                            Algorithm=pion_from_D0,
                            RequiredSelections=[D0_from_strip_sel])
kaon_from_D0 = FilterInTrees('kaon_from_D0_filter', Code="('K+' == ABSID)")
kaon_from_D0_sel = Selection("kaon_from_D0_sel",
                            Algorithm=kaon_from_D0,
                            RequiredSelections=[D0_from_strip_sel])
pion_from_strip = FilterInTrees('pion_from_strip_filter', Code="('pi-' == ABSID)")
pion_from_strip_sel = Selection("pion_from_strip_sel",
                            Algorithm=pion_from_strip,
                            RequiredSelections=[Kst_from_strip_sel])
muon_from_strip = FilterInTrees('muon_from_strip_filter', Code="('mu+' == ABSID)")
muon_from_strip_sel = Selection("muon_from_strip_sel",
                            Algorithm=muon_from_strip,
                            RequiredSelections=[Kst_from_strip_sel])

d0_comb = "(ADAMASS('D0') < 100*MeV)"
d0_mother = ('(VFASPF(VCHI2/VDOF)< 9)')
dst_comb = "(ADAMASS('D*(2010)+') < 100*MeV)"
dst_mother = ('(VFASPF(VCHI2/VDOF)< 9)')
pion_daughter = "(P>2000.0) & (PT > 250.0 )& (TRCHI2DOF < 3.0)& (TRGHOSTPROB < 0.35)& (MIPCHI2DV(PRIMARY)> 4.0)" 
b0_comb = "(AM > 2200.0) & (AM < 8000.0) & (ADOCACHI2CUT( 10, ''))"
b0_mother = ('(VFASPF(VCHI2/VDOF)< 9)')


real_PartialDst_sel = CombineSelection('Sel_PartDst',[D0_from_strip_sel, pion_from_strip_sel],
    DecayDescriptors=['[D*(2010)+ -> D0 pi+]cc'],
    CombinationCut="AALL",MotherCut=dst_mother,
    DaughtersCuts = {"pi+" : "ALL"}
)
real_PartialB_sel = CombineSelection('Sel_PartB0',[real_PartialDst_sel, muon_from_strip_sel],
    DecayDescriptors=['[B~0 -> D*(2010)+ mu-]cc'],
    CombinationCut="AALL",MotherCut=dst_mother,
    DaughtersCuts = {"mu+" : pion_daughter}
)

#Combine D and pions into Dstar
real_d0_sel = CombineSelection('Sel_D0',[D0_from_strip_sel, Pions],
    DecayDescriptors=['[D+ -> D0 pi+]cc', '[D+ -> D0 pi-]cc'],
    CombinationCut=d0_comb,MotherCut=d0_mother,
    DaughtersCuts = {"pi+" : pion_daughter}
)

#Combine D and pions into Dstar
real_dst_sel = CombineSelection('Sel_Dst',[real_d0_sel, pion_from_strip_sel],
    DecayDescriptors=['[D*(2010)+ -> D+ pi+]cc'],
    CombinationCut=dst_comb,MotherCut=dst_mother
)

#Combine D and pions into Dstar
real_b0_sel = CombineSelection('Sel_B0',[real_dst_sel, muon_from_strip_sel],
    DecayDescriptor='[B_s0 -> D*(2010)+ mu-]cc',
    CombinationCut=b0_comb, MotherCut=b0_mother
)

dstar_seq = SelectionSequence('D0Seq', TopSelection=real_b0_sel)
partialdstar_seq = SelectionSequence('PartialD0Seq', TopSelection=real_PartialB_sel)

# Create an ntuple
mct = MCDecayTreeTuple('mct')
mct.Decay = "[B0 ==> ^(D*(2010)- ==> ^(D~0 ==> ^K+ ^pi- ^pi- ^pi+) ^pi-) ^mu+ ^nu_mu]CC"
mct.Branches = {
    "B"      : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) pi-) mu+ nu_mu]CC"
    ,"Dst"   : "[B0 ==> ^(D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) pi-) mu+ nu_mu]CC"
    ,"D0"    : "[B0 ==> (D*(2010)- ==> ^(D~0 ==> K+ pi- pi- pi+) pi-) mu+ nu_mu]CC"
    ,"K"     : "[B0 ==> (D*(2010)- ==> (D~0 ==> ^K+ pi- pi- pi+) pi-) mu+ nu_mu]CC"
    ,"pi1"   : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ ^pi- pi- pi+) pi-) mu+ nu_mu]CC"
    ,"pi2"   : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- ^pi- pi+) pi-) mu+ nu_mu]CC"
    ,"pi3"   : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- ^pi+) pi-) mu+ nu_mu]CC"
    ,"PiS"   : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) ^pi-) mu+ nu_mu]CC"
    ,"mu"    : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) pi-) ^mu+ nu_mu]CC"
    ,"nu"    : "[B0 ==> (D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) pi-) mu+ ^nu_mu]CC"
    }
mct.ToolList=mctl


tuple_partial_RS = DecayTreeTuple('TuplePartialRS')
tuple_partial_RS.Inputs = partialdstar_seq.outputLocations()
tuple_partial_RS.Decay = '([B~0 -> ^( D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi-) ^pi+) ^mu-]CC) || ([B~0 -> ^(D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi+) ^pi+) ^mu-]CC)'
tuple_partial_RS.Branches = {
    "B"       : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi+ pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- pi+ pi+) pi+) mu-]CC)'
    ,"Dst"    : '([B~0 -> ^( D*(2010)+ -> (D0 -> K- pi+ pi-) pi+) mu-]CC) || ([B~0 -> ^(D*(2010)+ -> (D0 -> K- pi+ pi+) pi+) mu-]CC)'
    ,"D0"     : '([B~0 -> ( D*(2010)+ -> ^(D0 -> K- pi+ pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> ^(D0 -> K- pi+ pi+) pi+) mu-]CC)'
    ,"K"      : '([B~0 -> ( D*(2010)+ -> (D0 -> ^K- pi+ pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> ^K- pi+ pi+) pi+) mu-]CC)'
    ,"pi1"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- ^pi+ pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- ^pi+ pi+) pi+) mu-]CC)'
    ,"pi2"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi+ ^pi-) pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- pi+ ^pi+) pi+) mu-]CC)'
    ,"PiS"    : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi+ pi-) ^pi+) mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- pi+ pi+) ^pi+) mu-]CC)'
    ,"mu"     : '([B~0 -> ( D*(2010)+ -> (D0 -> K- pi+ pi-) pi+) ^mu-]CC) || ([B~0 -> (D*(2010)+ -> (D0 -> K- pi+ pi+) pi+) ^mu-]CC)'
}

tuple_full_RS = DecayTreeTuple('TupleFullRS')
tuple_full_RS.Inputs = dstar_seq.outputLocations()
tuple_full_RS.Decay = '([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi+ ^pi-) ^pi+) ^pi+) ^mu-]CC) || ([B_s0 -> ^(D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi+ ^pi+) ^pi-) ^pi+) ^mu-]CC)'
tuple_full_RS.Branches = {
    "B"       : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"Dst"    : '([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> ^(D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"D0"     : '([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> ^(D+ -> (D0 -> K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"Dfake"  : '([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> ^(D0 -> K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"K"      : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> ^K- pi+ pi+) pi-) pi+) mu-]CC)'
    ,"pi1"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi+ pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- ^pi+ pi+) pi-) pi+) mu-]CC)'
    ,"pi2"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ ^pi-) pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ ^pi+) pi-) pi+) mu-]CC)'
    ,"PiMiss" : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) ^pi+) pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) ^pi-) pi+) mu-]CC)'
    ,"PiS"    : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) ^pi+) mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi-) ^pi+) mu-]CC)'
    ,"mu"     : '([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) pi+) ^mu-]CC) || ([B_s0 -> (D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi+) pi-) pi+) ^mu-]CC)'
}

from Configurables import DecayTreeTuple, MCMatchObjP2MCRelator
import DecayTreeTuple.Configuration
default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

MCTruth = TupleToolMCTruth()
MCTruth.ToolList =  [
      "MCTupleToolAngles"
      , "MCTupleToolHierarchy"
      , "MCTupleToolKinematic"
      , "MCTupleToolReconstructed"
      ]
MCTruth.addTool(MCMatchObjP2MCRelator)
MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs

tuples = [tuple_full_RS, tuple_partial_RS]

for tup in tuples:
   tup.ToolList = toollist
   tup.addTool(MCTruth)
tuple_partial_RS.addTool(TupleToolDecay, name = "Dst")
tuple_partial_RS.Dst.ToolList += ["TupleToolFitMissPion"]
tuple_full_RS.addTool(TupleToolDecay, name = "Dst")
tuple_full_RS.Dst.ToolList += ["TupleToolFitRecoPion"]

DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])
DaVinci().UserAlgorithms += [dstar_seq.sequence(), partialdstar_seq.sequence(), tuple_full_RS, tuple_partial_RS, mct]

# DaVinci configuration
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'Tuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1

IOHelper().inputFiles([
'/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00103234/0000/00103234_00000808_7.AllStreams.dst'
,'/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00103234/0000/00103234_00000788_7.AllStreams.dst'
], clear=True)
