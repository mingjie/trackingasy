//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Feb 15 11:05:12 2022 by ROOT version 6.24/06
// from TTree DecayTree/DecayTree
// found on file: Tuple.root
//////////////////////////////////////////////////////////

#ifndef DecayTree_h
#define DecayTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class DecayTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxDst_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxDst_OWNPV_COV = 1;
   static constexpr Int_t kMaxpD0_COV = 1;
   static constexpr Int_t kMaxpPi_COV = 1;
   static constexpr Int_t kMaxVEta_COV = 1;
   static constexpr Int_t kMaxD0_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxD0_OWNPV_COV = 1;
   static constexpr Int_t kMaxD0_ORIVX_COV = 1;
   static constexpr Int_t kMaxDfake_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxDfake_OWNPV_COV = 1;
   static constexpr Int_t kMaxDfake_ORIVX_COV = 1;
   static constexpr Int_t kMaxK_OWNPV_COV = 1;
   static constexpr Int_t kMaxK_ORIVX_COV = 1;
   static constexpr Int_t kMaxpi1_OWNPV_COV = 1;
   static constexpr Int_t kMaxpi1_ORIVX_COV = 1;
   static constexpr Int_t kMaxpi2_OWNPV_COV = 1;
   static constexpr Int_t kMaxpi2_ORIVX_COV = 1;
   static constexpr Int_t kMaxPiMiss_OWNPV_COV = 1;
   static constexpr Int_t kMaxPiMiss_ORIVX_COV = 1;
   static constexpr Int_t kMaxPiS_OWNPV_COV = 1;
   static constexpr Int_t kMaxPiS_ORIVX_COV = 1;

   // Declaration of leaf types
   Double_t        Dst_DiraAngleError;
   Double_t        Dst_DiraCosError;
   Double_t        Dst_DiraAngle;
   Double_t        Dst_DiraCos;
   Double_t        Dst_ENDVERTEX_X;
   Double_t        Dst_ENDVERTEX_Y;
   Double_t        Dst_ENDVERTEX_Z;
   Double_t        Dst_ENDVERTEX_XERR;
   Double_t        Dst_ENDVERTEX_YERR;
   Double_t        Dst_ENDVERTEX_ZERR;
   Double_t        Dst_ENDVERTEX_CHI2;
   Int_t           Dst_ENDVERTEX_NDOF;
   Float_t         Dst_ENDVERTEX_COV_[3][3];
   Double_t        Dst_OWNPV_X;
   Double_t        Dst_OWNPV_Y;
   Double_t        Dst_OWNPV_Z;
   Double_t        Dst_OWNPV_XERR;
   Double_t        Dst_OWNPV_YERR;
   Double_t        Dst_OWNPV_ZERR;
   Double_t        Dst_OWNPV_CHI2;
   Int_t           Dst_OWNPV_NDOF;
   Float_t         Dst_OWNPV_COV_[3][3];
   Double_t        Dst_IP_OWNPV;
   Double_t        Dst_IPCHI2_OWNPV;
   Double_t        Dst_FD_OWNPV;
   Double_t        Dst_FDCHI2_OWNPV;
   Double_t        Dst_DIRA_OWNPV;
   Double_t        Dst_P;
   Double_t        Dst_PT;
   Double_t        Dst_PE;
   Double_t        Dst_PX;
   Double_t        Dst_PY;
   Double_t        Dst_PZ;
   Double_t        Dst_MM;
   Double_t        Dst_MMERR;
   Double_t        Dst_M;
   Int_t           Dst_ID;
   Double_t        Dst_TAU;
   Double_t        Dst_TAUERR;
   Double_t        Dst_TAUCHI2;
   Bool_t          Dst_L0Global_Dec;
   Bool_t          Dst_L0Global_TIS;
   Bool_t          Dst_L0Global_TOS;
   Bool_t          Dst_Hlt1Global_Dec;
   Bool_t          Dst_Hlt1Global_TIS;
   Bool_t          Dst_Hlt1Global_TOS;
   Bool_t          Dst_Hlt1Phys_Dec;
   Bool_t          Dst_Hlt1Phys_TIS;
   Bool_t          Dst_Hlt1Phys_TOS;
   Bool_t          Dst_Hlt2Global_Dec;
   Bool_t          Dst_Hlt2Global_TIS;
   Bool_t          Dst_Hlt2Global_TOS;
   Bool_t          Dst_Hlt2Phys_Dec;
   Bool_t          Dst_Hlt2Phys_TIS;
   Bool_t          Dst_Hlt2Phys_TOS;
   Double_t        Dst_NumVtxWithinChi2WindowOneTrack;
   Double_t        Dst_SmallestDeltaChi2OneTrack;
   Double_t        Dst_SmallestDeltaChi2MassOneTrack;
   Double_t        Dst_SmallestDeltaChi2TwoTracks;
   Double_t        Dst_SmallestDeltaChi2MassTwoTracks;
   Float_t         pD0_COV_[7][7];
   Float_t         pPi_COV_[7][7];
   Double_t        PD0_ini0;
   Double_t        PD0_err0;
   Double_t        PxMiss_Fit0;
   Double_t        PxMiss_Err0;
   Double_t        PyMiss_Fit0;
   Double_t        PyMiss_Err0;
   Double_t        PzMiss_Fit0;
   Double_t        PzMiss_Err0;
   Double_t        X_K2Pi0;
   Double_t        Y_K2Pi0;
   Double_t        Z_K2Pi0;
   Double_t        PX_K2Pi0;
   Double_t        PY_K2Pi0;
   Double_t        PZ_K2Pi0;
   Double_t        PE_K2Pi0;
   Double_t        PX_Pi_s0;
   Double_t        PY_Pi_s0;
   Double_t        PZ_Pi_s0;
   Double_t        X_PV0;
   Double_t        Y_PV0;
   Double_t        Z_PV0;
   Double_t        PD0_Fit;
   Double_t        PxMiss_Fit;
   Double_t        PyMiss_Fit;
   Double_t        PzMiss_Fit;
   Double_t        PD0_Err;
   Double_t        PxMiss_Err;
   Double_t        PyMiss_Err;
   Double_t        PzMiss_Err;
   Double_t        X_K2Pi;
   Double_t        Y_K2Pi;
   Double_t        Z_K2Pi;
   Double_t        PX_K2Pi;
   Double_t        PY_K2Pi;
   Double_t        PZ_K2Pi;
   Double_t        PE_K2Pi;
   Double_t        PX_Pi_s;
   Double_t        PY_Pi_s;
   Double_t        PZ_Pi_s;
   Double_t        X_PV;
   Double_t        Y_PV;
   Double_t        Z_PV;
   Double_t        Chi2_final;
   Double_t        PMiss_Fit;
   Double_t        PMiss_Err;
   Double_t        PtMiss_Fit;
   Double_t        PtMiss_Err;
   Int_t           PD0_suc;
   Double_t        D0_FD;
   Double_t        D0_FDErr;
   Int_t           N;
   Float_t         Chi2_array[300];   //[N]
   Float_t         VEta_COV_[13][13];
   Double_t        PD0_ini1;
   Double_t        PD0_ini2;
   Double_t        PD0_ini3;
   Double_t        D0_CosTheta;
   Double_t        D0_ENDVERTEX_X;
   Double_t        D0_ENDVERTEX_Y;
   Double_t        D0_ENDVERTEX_Z;
   Double_t        D0_ENDVERTEX_XERR;
   Double_t        D0_ENDVERTEX_YERR;
   Double_t        D0_ENDVERTEX_ZERR;
   Double_t        D0_ENDVERTEX_CHI2;
   Int_t           D0_ENDVERTEX_NDOF;
   Float_t         D0_ENDVERTEX_COV_[3][3];
   Double_t        D0_OWNPV_X;
   Double_t        D0_OWNPV_Y;
   Double_t        D0_OWNPV_Z;
   Double_t        D0_OWNPV_XERR;
   Double_t        D0_OWNPV_YERR;
   Double_t        D0_OWNPV_ZERR;
   Double_t        D0_OWNPV_CHI2;
   Int_t           D0_OWNPV_NDOF;
   Float_t         D0_OWNPV_COV_[3][3];
   Double_t        D0_IP_OWNPV;
   Double_t        D0_IPCHI2_OWNPV;
   Double_t        D0_FD_OWNPV;
   Double_t        D0_FDCHI2_OWNPV;
   Double_t        D0_DIRA_OWNPV;
   Double_t        D0_ORIVX_X;
   Double_t        D0_ORIVX_Y;
   Double_t        D0_ORIVX_Z;
   Double_t        D0_ORIVX_XERR;
   Double_t        D0_ORIVX_YERR;
   Double_t        D0_ORIVX_ZERR;
   Double_t        D0_ORIVX_CHI2;
   Int_t           D0_ORIVX_NDOF;
   Float_t         D0_ORIVX_COV_[3][3];
   Double_t        D0_FD_ORIVX;
   Double_t        D0_FDCHI2_ORIVX;
   Double_t        D0_DIRA_ORIVX;
   Double_t        D0_P;
   Double_t        D0_PT;
   Double_t        D0_PE;
   Double_t        D0_PX;
   Double_t        D0_PY;
   Double_t        D0_PZ;
   Double_t        D0_MM;
   Double_t        D0_MMERR;
   Double_t        D0_M;
   Int_t           D0_ID;
   Double_t        D0_TAU;
   Double_t        D0_TAUERR;
   Double_t        D0_TAUCHI2;
   Bool_t          D0_L0Global_Dec;
   Bool_t          D0_L0Global_TIS;
   Bool_t          D0_L0Global_TOS;
   Bool_t          D0_Hlt1Global_Dec;
   Bool_t          D0_Hlt1Global_TIS;
   Bool_t          D0_Hlt1Global_TOS;
   Bool_t          D0_Hlt1Phys_Dec;
   Bool_t          D0_Hlt1Phys_TIS;
   Bool_t          D0_Hlt1Phys_TOS;
   Bool_t          D0_Hlt2Global_Dec;
   Bool_t          D0_Hlt2Global_TIS;
   Bool_t          D0_Hlt2Global_TOS;
   Bool_t          D0_Hlt2Phys_Dec;
   Bool_t          D0_Hlt2Phys_TIS;
   Bool_t          D0_Hlt2Phys_TOS;
   Double_t        D0_NumVtxWithinChi2WindowOneTrack;
   Double_t        D0_SmallestDeltaChi2OneTrack;
   Double_t        D0_SmallestDeltaChi2MassOneTrack;
   Double_t        D0_SmallestDeltaChi2TwoTracks;
   Double_t        D0_SmallestDeltaChi2MassTwoTracks;
   Double_t        Dfake_CosTheta;
   Double_t        Dfake_ENDVERTEX_X;
   Double_t        Dfake_ENDVERTEX_Y;
   Double_t        Dfake_ENDVERTEX_Z;
   Double_t        Dfake_ENDVERTEX_XERR;
   Double_t        Dfake_ENDVERTEX_YERR;
   Double_t        Dfake_ENDVERTEX_ZERR;
   Double_t        Dfake_ENDVERTEX_CHI2;
   Int_t           Dfake_ENDVERTEX_NDOF;
   Float_t         Dfake_ENDVERTEX_COV_[3][3];
   Double_t        Dfake_OWNPV_X;
   Double_t        Dfake_OWNPV_Y;
   Double_t        Dfake_OWNPV_Z;
   Double_t        Dfake_OWNPV_XERR;
   Double_t        Dfake_OWNPV_YERR;
   Double_t        Dfake_OWNPV_ZERR;
   Double_t        Dfake_OWNPV_CHI2;
   Int_t           Dfake_OWNPV_NDOF;
   Float_t         Dfake_OWNPV_COV_[3][3];
   Double_t        Dfake_IP_OWNPV;
   Double_t        Dfake_IPCHI2_OWNPV;
   Double_t        Dfake_FD_OWNPV;
   Double_t        Dfake_FDCHI2_OWNPV;
   Double_t        Dfake_DIRA_OWNPV;
   Double_t        Dfake_ORIVX_X;
   Double_t        Dfake_ORIVX_Y;
   Double_t        Dfake_ORIVX_Z;
   Double_t        Dfake_ORIVX_XERR;
   Double_t        Dfake_ORIVX_YERR;
   Double_t        Dfake_ORIVX_ZERR;
   Double_t        Dfake_ORIVX_CHI2;
   Int_t           Dfake_ORIVX_NDOF;
   Float_t         Dfake_ORIVX_COV_[3][3];
   Double_t        Dfake_FD_ORIVX;
   Double_t        Dfake_FDCHI2_ORIVX;
   Double_t        Dfake_DIRA_ORIVX;
   Double_t        Dfake_P;
   Double_t        Dfake_PT;
   Double_t        Dfake_PE;
   Double_t        Dfake_PX;
   Double_t        Dfake_PY;
   Double_t        Dfake_PZ;
   Double_t        Dfake_MM;
   Double_t        Dfake_MMERR;
   Double_t        Dfake_M;
   Int_t           Dfake_ID;
   Double_t        Dfake_TAU;
   Double_t        Dfake_TAUERR;
   Double_t        Dfake_TAUCHI2;
   Bool_t          Dfake_L0Global_Dec;
   Bool_t          Dfake_L0Global_TIS;
   Bool_t          Dfake_L0Global_TOS;
   Bool_t          Dfake_Hlt1Global_Dec;
   Bool_t          Dfake_Hlt1Global_TIS;
   Bool_t          Dfake_Hlt1Global_TOS;
   Bool_t          Dfake_Hlt1Phys_Dec;
   Bool_t          Dfake_Hlt1Phys_TIS;
   Bool_t          Dfake_Hlt1Phys_TOS;
   Bool_t          Dfake_Hlt2Global_Dec;
   Bool_t          Dfake_Hlt2Global_TIS;
   Bool_t          Dfake_Hlt2Global_TOS;
   Bool_t          Dfake_Hlt2Phys_Dec;
   Bool_t          Dfake_Hlt2Phys_TIS;
   Bool_t          Dfake_Hlt2Phys_TOS;
   Double_t        Dfake_NumVtxWithinChi2WindowOneTrack;
   Double_t        Dfake_SmallestDeltaChi2OneTrack;
   Double_t        Dfake_SmallestDeltaChi2MassOneTrack;
   Double_t        Dfake_SmallestDeltaChi2TwoTracks;
   Double_t        Dfake_SmallestDeltaChi2MassTwoTracks;
   Double_t        K_MC12TuneV2_ProbNNe;
   Double_t        K_MC12TuneV2_ProbNNmu;
   Double_t        K_MC12TuneV2_ProbNNpi;
   Double_t        K_MC12TuneV2_ProbNNk;
   Double_t        K_MC12TuneV2_ProbNNp;
   Double_t        K_MC12TuneV2_ProbNNghost;
   Double_t        K_MC12TuneV3_ProbNNe;
   Double_t        K_MC12TuneV3_ProbNNmu;
   Double_t        K_MC12TuneV3_ProbNNpi;
   Double_t        K_MC12TuneV3_ProbNNk;
   Double_t        K_MC12TuneV3_ProbNNp;
   Double_t        K_MC12TuneV3_ProbNNghost;
   Double_t        K_MC12TuneV4_ProbNNe;
   Double_t        K_MC12TuneV4_ProbNNmu;
   Double_t        K_MC12TuneV4_ProbNNpi;
   Double_t        K_MC12TuneV4_ProbNNk;
   Double_t        K_MC12TuneV4_ProbNNp;
   Double_t        K_MC12TuneV4_ProbNNghost;
   Double_t        K_MC15TuneV1_ProbNNe;
   Double_t        K_MC15TuneV1_ProbNNmu;
   Double_t        K_MC15TuneV1_ProbNNpi;
   Double_t        K_MC15TuneV1_ProbNNk;
   Double_t        K_MC15TuneV1_ProbNNp;
   Double_t        K_MC15TuneV1_ProbNNghost;
   Double_t        K_CosTheta;
   Double_t        K_OWNPV_X;
   Double_t        K_OWNPV_Y;
   Double_t        K_OWNPV_Z;
   Double_t        K_OWNPV_XERR;
   Double_t        K_OWNPV_YERR;
   Double_t        K_OWNPV_ZERR;
   Double_t        K_OWNPV_CHI2;
   Int_t           K_OWNPV_NDOF;
   Float_t         K_OWNPV_COV_[3][3];
   Double_t        K_IP_OWNPV;
   Double_t        K_IPCHI2_OWNPV;
   Double_t        K_ORIVX_X;
   Double_t        K_ORIVX_Y;
   Double_t        K_ORIVX_Z;
   Double_t        K_ORIVX_XERR;
   Double_t        K_ORIVX_YERR;
   Double_t        K_ORIVX_ZERR;
   Double_t        K_ORIVX_CHI2;
   Int_t           K_ORIVX_NDOF;
   Float_t         K_ORIVX_COV_[3][3];
   Double_t        K_P;
   Double_t        K_PT;
   Double_t        K_PE;
   Double_t        K_PX;
   Double_t        K_PY;
   Double_t        K_PZ;
   Double_t        K_M;
   Int_t           K_ID;
   Double_t        K_PIDe;
   Double_t        K_PIDmu;
   Double_t        K_PIDK;
   Double_t        K_PIDp;
   Double_t        K_PIDd;
   Double_t        K_ProbNNe;
   Double_t        K_ProbNNk;
   Double_t        K_ProbNNp;
   Double_t        K_ProbNNpi;
   Double_t        K_ProbNNmu;
   Double_t        K_ProbNNd;
   Double_t        K_ProbNNghost;
   Bool_t          K_hasMuon;
   Bool_t          K_isMuon;
   Bool_t          K_hasRich;
   Bool_t          K_UsedRichAerogel;
   Bool_t          K_UsedRich1Gas;
   Bool_t          K_UsedRich2Gas;
   Bool_t          K_RichAboveElThres;
   Bool_t          K_RichAboveMuThres;
   Bool_t          K_RichAbovePiThres;
   Bool_t          K_RichAboveKaThres;
   Bool_t          K_RichAbovePrThres;
   Bool_t          K_hasCalo;
   Bool_t          K_L0Global_Dec;
   Bool_t          K_L0Global_TIS;
   Bool_t          K_L0Global_TOS;
   Bool_t          K_Hlt1Global_Dec;
   Bool_t          K_Hlt1Global_TIS;
   Bool_t          K_Hlt1Global_TOS;
   Bool_t          K_Hlt1Phys_Dec;
   Bool_t          K_Hlt1Phys_TIS;
   Bool_t          K_Hlt1Phys_TOS;
   Bool_t          K_Hlt2Global_Dec;
   Bool_t          K_Hlt2Global_TIS;
   Bool_t          K_Hlt2Global_TOS;
   Bool_t          K_Hlt2Phys_Dec;
   Bool_t          K_Hlt2Phys_TIS;
   Bool_t          K_Hlt2Phys_TOS;
   Int_t           K_TRACK_Type;
   Int_t           K_TRACK_Key;
   Double_t        K_TRACK_CHI2NDOF;
   Double_t        K_TRACK_PCHI2;
   Double_t        K_TRACK_MatchCHI2;
   Double_t        K_TRACK_GhostProb;
   Double_t        K_TRACK_CloneDist;
   Double_t        K_TRACK_Likelihood;
   Double_t        pi1_MC12TuneV2_ProbNNe;
   Double_t        pi1_MC12TuneV2_ProbNNmu;
   Double_t        pi1_MC12TuneV2_ProbNNpi;
   Double_t        pi1_MC12TuneV2_ProbNNk;
   Double_t        pi1_MC12TuneV2_ProbNNp;
   Double_t        pi1_MC12TuneV2_ProbNNghost;
   Double_t        pi1_MC12TuneV3_ProbNNe;
   Double_t        pi1_MC12TuneV3_ProbNNmu;
   Double_t        pi1_MC12TuneV3_ProbNNpi;
   Double_t        pi1_MC12TuneV3_ProbNNk;
   Double_t        pi1_MC12TuneV3_ProbNNp;
   Double_t        pi1_MC12TuneV3_ProbNNghost;
   Double_t        pi1_MC12TuneV4_ProbNNe;
   Double_t        pi1_MC12TuneV4_ProbNNmu;
   Double_t        pi1_MC12TuneV4_ProbNNpi;
   Double_t        pi1_MC12TuneV4_ProbNNk;
   Double_t        pi1_MC12TuneV4_ProbNNp;
   Double_t        pi1_MC12TuneV4_ProbNNghost;
   Double_t        pi1_MC15TuneV1_ProbNNe;
   Double_t        pi1_MC15TuneV1_ProbNNmu;
   Double_t        pi1_MC15TuneV1_ProbNNpi;
   Double_t        pi1_MC15TuneV1_ProbNNk;
   Double_t        pi1_MC15TuneV1_ProbNNp;
   Double_t        pi1_MC15TuneV1_ProbNNghost;
   Double_t        pi1_CosTheta;
   Double_t        pi1_OWNPV_X;
   Double_t        pi1_OWNPV_Y;
   Double_t        pi1_OWNPV_Z;
   Double_t        pi1_OWNPV_XERR;
   Double_t        pi1_OWNPV_YERR;
   Double_t        pi1_OWNPV_ZERR;
   Double_t        pi1_OWNPV_CHI2;
   Int_t           pi1_OWNPV_NDOF;
   Float_t         pi1_OWNPV_COV_[3][3];
   Double_t        pi1_IP_OWNPV;
   Double_t        pi1_IPCHI2_OWNPV;
   Double_t        pi1_ORIVX_X;
   Double_t        pi1_ORIVX_Y;
   Double_t        pi1_ORIVX_Z;
   Double_t        pi1_ORIVX_XERR;
   Double_t        pi1_ORIVX_YERR;
   Double_t        pi1_ORIVX_ZERR;
   Double_t        pi1_ORIVX_CHI2;
   Int_t           pi1_ORIVX_NDOF;
   Float_t         pi1_ORIVX_COV_[3][3];
   Double_t        pi1_P;
   Double_t        pi1_PT;
   Double_t        pi1_PE;
   Double_t        pi1_PX;
   Double_t        pi1_PY;
   Double_t        pi1_PZ;
   Double_t        pi1_M;
   Int_t           pi1_ID;
   Double_t        pi1_PIDe;
   Double_t        pi1_PIDmu;
   Double_t        pi1_PIDK;
   Double_t        pi1_PIDp;
   Double_t        pi1_PIDd;
   Double_t        pi1_ProbNNe;
   Double_t        pi1_ProbNNk;
   Double_t        pi1_ProbNNp;
   Double_t        pi1_ProbNNpi;
   Double_t        pi1_ProbNNmu;
   Double_t        pi1_ProbNNd;
   Double_t        pi1_ProbNNghost;
   Bool_t          pi1_hasMuon;
   Bool_t          pi1_isMuon;
   Bool_t          pi1_hasRich;
   Bool_t          pi1_UsedRichAerogel;
   Bool_t          pi1_UsedRich1Gas;
   Bool_t          pi1_UsedRich2Gas;
   Bool_t          pi1_RichAboveElThres;
   Bool_t          pi1_RichAboveMuThres;
   Bool_t          pi1_RichAbovePiThres;
   Bool_t          pi1_RichAboveKaThres;
   Bool_t          pi1_RichAbovePrThres;
   Bool_t          pi1_hasCalo;
   Bool_t          pi1_L0Global_Dec;
   Bool_t          pi1_L0Global_TIS;
   Bool_t          pi1_L0Global_TOS;
   Bool_t          pi1_Hlt1Global_Dec;
   Bool_t          pi1_Hlt1Global_TIS;
   Bool_t          pi1_Hlt1Global_TOS;
   Bool_t          pi1_Hlt1Phys_Dec;
   Bool_t          pi1_Hlt1Phys_TIS;
   Bool_t          pi1_Hlt1Phys_TOS;
   Bool_t          pi1_Hlt2Global_Dec;
   Bool_t          pi1_Hlt2Global_TIS;
   Bool_t          pi1_Hlt2Global_TOS;
   Bool_t          pi1_Hlt2Phys_Dec;
   Bool_t          pi1_Hlt2Phys_TIS;
   Bool_t          pi1_Hlt2Phys_TOS;
   Int_t           pi1_TRACK_Type;
   Int_t           pi1_TRACK_Key;
   Double_t        pi1_TRACK_CHI2NDOF;
   Double_t        pi1_TRACK_PCHI2;
   Double_t        pi1_TRACK_MatchCHI2;
   Double_t        pi1_TRACK_GhostProb;
   Double_t        pi1_TRACK_CloneDist;
   Double_t        pi1_TRACK_Likelihood;
   Double_t        pi2_MC12TuneV2_ProbNNe;
   Double_t        pi2_MC12TuneV2_ProbNNmu;
   Double_t        pi2_MC12TuneV2_ProbNNpi;
   Double_t        pi2_MC12TuneV2_ProbNNk;
   Double_t        pi2_MC12TuneV2_ProbNNp;
   Double_t        pi2_MC12TuneV2_ProbNNghost;
   Double_t        pi2_MC12TuneV3_ProbNNe;
   Double_t        pi2_MC12TuneV3_ProbNNmu;
   Double_t        pi2_MC12TuneV3_ProbNNpi;
   Double_t        pi2_MC12TuneV3_ProbNNk;
   Double_t        pi2_MC12TuneV3_ProbNNp;
   Double_t        pi2_MC12TuneV3_ProbNNghost;
   Double_t        pi2_MC12TuneV4_ProbNNe;
   Double_t        pi2_MC12TuneV4_ProbNNmu;
   Double_t        pi2_MC12TuneV4_ProbNNpi;
   Double_t        pi2_MC12TuneV4_ProbNNk;
   Double_t        pi2_MC12TuneV4_ProbNNp;
   Double_t        pi2_MC12TuneV4_ProbNNghost;
   Double_t        pi2_MC15TuneV1_ProbNNe;
   Double_t        pi2_MC15TuneV1_ProbNNmu;
   Double_t        pi2_MC15TuneV1_ProbNNpi;
   Double_t        pi2_MC15TuneV1_ProbNNk;
   Double_t        pi2_MC15TuneV1_ProbNNp;
   Double_t        pi2_MC15TuneV1_ProbNNghost;
   Double_t        pi2_CosTheta;
   Double_t        pi2_OWNPV_X;
   Double_t        pi2_OWNPV_Y;
   Double_t        pi2_OWNPV_Z;
   Double_t        pi2_OWNPV_XERR;
   Double_t        pi2_OWNPV_YERR;
   Double_t        pi2_OWNPV_ZERR;
   Double_t        pi2_OWNPV_CHI2;
   Int_t           pi2_OWNPV_NDOF;
   Float_t         pi2_OWNPV_COV_[3][3];
   Double_t        pi2_IP_OWNPV;
   Double_t        pi2_IPCHI2_OWNPV;
   Double_t        pi2_ORIVX_X;
   Double_t        pi2_ORIVX_Y;
   Double_t        pi2_ORIVX_Z;
   Double_t        pi2_ORIVX_XERR;
   Double_t        pi2_ORIVX_YERR;
   Double_t        pi2_ORIVX_ZERR;
   Double_t        pi2_ORIVX_CHI2;
   Int_t           pi2_ORIVX_NDOF;
   Float_t         pi2_ORIVX_COV_[3][3];
   Double_t        pi2_P;
   Double_t        pi2_PT;
   Double_t        pi2_PE;
   Double_t        pi2_PX;
   Double_t        pi2_PY;
   Double_t        pi2_PZ;
   Double_t        pi2_M;
   Int_t           pi2_ID;
   Double_t        pi2_PIDe;
   Double_t        pi2_PIDmu;
   Double_t        pi2_PIDK;
   Double_t        pi2_PIDp;
   Double_t        pi2_PIDd;
   Double_t        pi2_ProbNNe;
   Double_t        pi2_ProbNNk;
   Double_t        pi2_ProbNNp;
   Double_t        pi2_ProbNNpi;
   Double_t        pi2_ProbNNmu;
   Double_t        pi2_ProbNNd;
   Double_t        pi2_ProbNNghost;
   Bool_t          pi2_hasMuon;
   Bool_t          pi2_isMuon;
   Bool_t          pi2_hasRich;
   Bool_t          pi2_UsedRichAerogel;
   Bool_t          pi2_UsedRich1Gas;
   Bool_t          pi2_UsedRich2Gas;
   Bool_t          pi2_RichAboveElThres;
   Bool_t          pi2_RichAboveMuThres;
   Bool_t          pi2_RichAbovePiThres;
   Bool_t          pi2_RichAboveKaThres;
   Bool_t          pi2_RichAbovePrThres;
   Bool_t          pi2_hasCalo;
   Bool_t          pi2_L0Global_Dec;
   Bool_t          pi2_L0Global_TIS;
   Bool_t          pi2_L0Global_TOS;
   Bool_t          pi2_Hlt1Global_Dec;
   Bool_t          pi2_Hlt1Global_TIS;
   Bool_t          pi2_Hlt1Global_TOS;
   Bool_t          pi2_Hlt1Phys_Dec;
   Bool_t          pi2_Hlt1Phys_TIS;
   Bool_t          pi2_Hlt1Phys_TOS;
   Bool_t          pi2_Hlt2Global_Dec;
   Bool_t          pi2_Hlt2Global_TIS;
   Bool_t          pi2_Hlt2Global_TOS;
   Bool_t          pi2_Hlt2Phys_Dec;
   Bool_t          pi2_Hlt2Phys_TIS;
   Bool_t          pi2_Hlt2Phys_TOS;
   Int_t           pi2_TRACK_Type;
   Int_t           pi2_TRACK_Key;
   Double_t        pi2_TRACK_CHI2NDOF;
   Double_t        pi2_TRACK_PCHI2;
   Double_t        pi2_TRACK_MatchCHI2;
   Double_t        pi2_TRACK_GhostProb;
   Double_t        pi2_TRACK_CloneDist;
   Double_t        pi2_TRACK_Likelihood;
   Double_t        PiMiss_MC12TuneV2_ProbNNe;
   Double_t        PiMiss_MC12TuneV2_ProbNNmu;
   Double_t        PiMiss_MC12TuneV2_ProbNNpi;
   Double_t        PiMiss_MC12TuneV2_ProbNNk;
   Double_t        PiMiss_MC12TuneV2_ProbNNp;
   Double_t        PiMiss_MC12TuneV2_ProbNNghost;
   Double_t        PiMiss_MC12TuneV3_ProbNNe;
   Double_t        PiMiss_MC12TuneV3_ProbNNmu;
   Double_t        PiMiss_MC12TuneV3_ProbNNpi;
   Double_t        PiMiss_MC12TuneV3_ProbNNk;
   Double_t        PiMiss_MC12TuneV3_ProbNNp;
   Double_t        PiMiss_MC12TuneV3_ProbNNghost;
   Double_t        PiMiss_MC12TuneV4_ProbNNe;
   Double_t        PiMiss_MC12TuneV4_ProbNNmu;
   Double_t        PiMiss_MC12TuneV4_ProbNNpi;
   Double_t        PiMiss_MC12TuneV4_ProbNNk;
   Double_t        PiMiss_MC12TuneV4_ProbNNp;
   Double_t        PiMiss_MC12TuneV4_ProbNNghost;
   Double_t        PiMiss_MC15TuneV1_ProbNNe;
   Double_t        PiMiss_MC15TuneV1_ProbNNmu;
   Double_t        PiMiss_MC15TuneV1_ProbNNpi;
   Double_t        PiMiss_MC15TuneV1_ProbNNk;
   Double_t        PiMiss_MC15TuneV1_ProbNNp;
   Double_t        PiMiss_MC15TuneV1_ProbNNghost;
   Double_t        PiMiss_CosTheta;
   Double_t        PiMiss_OWNPV_X;
   Double_t        PiMiss_OWNPV_Y;
   Double_t        PiMiss_OWNPV_Z;
   Double_t        PiMiss_OWNPV_XERR;
   Double_t        PiMiss_OWNPV_YERR;
   Double_t        PiMiss_OWNPV_ZERR;
   Double_t        PiMiss_OWNPV_CHI2;
   Int_t           PiMiss_OWNPV_NDOF;
   Float_t         PiMiss_OWNPV_COV_[3][3];
   Double_t        PiMiss_IP_OWNPV;
   Double_t        PiMiss_IPCHI2_OWNPV;
   Double_t        PiMiss_ORIVX_X;
   Double_t        PiMiss_ORIVX_Y;
   Double_t        PiMiss_ORIVX_Z;
   Double_t        PiMiss_ORIVX_XERR;
   Double_t        PiMiss_ORIVX_YERR;
   Double_t        PiMiss_ORIVX_ZERR;
   Double_t        PiMiss_ORIVX_CHI2;
   Int_t           PiMiss_ORIVX_NDOF;
   Float_t         PiMiss_ORIVX_COV_[3][3];
   Double_t        PiMiss_P;
   Double_t        PiMiss_PT;
   Double_t        PiMiss_PE;
   Double_t        PiMiss_PX;
   Double_t        PiMiss_PY;
   Double_t        PiMiss_PZ;
   Double_t        PiMiss_M;
   Int_t           PiMiss_ID;
   Double_t        PiMiss_PIDe;
   Double_t        PiMiss_PIDmu;
   Double_t        PiMiss_PIDK;
   Double_t        PiMiss_PIDp;
   Double_t        PiMiss_PIDd;
   Double_t        PiMiss_ProbNNe;
   Double_t        PiMiss_ProbNNk;
   Double_t        PiMiss_ProbNNp;
   Double_t        PiMiss_ProbNNpi;
   Double_t        PiMiss_ProbNNmu;
   Double_t        PiMiss_ProbNNd;
   Double_t        PiMiss_ProbNNghost;
   Bool_t          PiMiss_hasMuon;
   Bool_t          PiMiss_isMuon;
   Bool_t          PiMiss_hasRich;
   Bool_t          PiMiss_UsedRichAerogel;
   Bool_t          PiMiss_UsedRich1Gas;
   Bool_t          PiMiss_UsedRich2Gas;
   Bool_t          PiMiss_RichAboveElThres;
   Bool_t          PiMiss_RichAboveMuThres;
   Bool_t          PiMiss_RichAbovePiThres;
   Bool_t          PiMiss_RichAboveKaThres;
   Bool_t          PiMiss_RichAbovePrThres;
   Bool_t          PiMiss_hasCalo;
   Bool_t          PiMiss_L0Global_Dec;
   Bool_t          PiMiss_L0Global_TIS;
   Bool_t          PiMiss_L0Global_TOS;
   Bool_t          PiMiss_Hlt1Global_Dec;
   Bool_t          PiMiss_Hlt1Global_TIS;
   Bool_t          PiMiss_Hlt1Global_TOS;
   Bool_t          PiMiss_Hlt1Phys_Dec;
   Bool_t          PiMiss_Hlt1Phys_TIS;
   Bool_t          PiMiss_Hlt1Phys_TOS;
   Bool_t          PiMiss_Hlt2Global_Dec;
   Bool_t          PiMiss_Hlt2Global_TIS;
   Bool_t          PiMiss_Hlt2Global_TOS;
   Bool_t          PiMiss_Hlt2Phys_Dec;
   Bool_t          PiMiss_Hlt2Phys_TIS;
   Bool_t          PiMiss_Hlt2Phys_TOS;
   Int_t           PiMiss_TRACK_Type;
   Int_t           PiMiss_TRACK_Key;
   Double_t        PiMiss_TRACK_CHI2NDOF;
   Double_t        PiMiss_TRACK_PCHI2;
   Double_t        PiMiss_TRACK_MatchCHI2;
   Double_t        PiMiss_TRACK_GhostProb;
   Double_t        PiMiss_TRACK_CloneDist;
   Double_t        PiMiss_TRACK_Likelihood;
   Double_t        PiS_MC12TuneV2_ProbNNe;
   Double_t        PiS_MC12TuneV2_ProbNNmu;
   Double_t        PiS_MC12TuneV2_ProbNNpi;
   Double_t        PiS_MC12TuneV2_ProbNNk;
   Double_t        PiS_MC12TuneV2_ProbNNp;
   Double_t        PiS_MC12TuneV2_ProbNNghost;
   Double_t        PiS_MC12TuneV3_ProbNNe;
   Double_t        PiS_MC12TuneV3_ProbNNmu;
   Double_t        PiS_MC12TuneV3_ProbNNpi;
   Double_t        PiS_MC12TuneV3_ProbNNk;
   Double_t        PiS_MC12TuneV3_ProbNNp;
   Double_t        PiS_MC12TuneV3_ProbNNghost;
   Double_t        PiS_MC12TuneV4_ProbNNe;
   Double_t        PiS_MC12TuneV4_ProbNNmu;
   Double_t        PiS_MC12TuneV4_ProbNNpi;
   Double_t        PiS_MC12TuneV4_ProbNNk;
   Double_t        PiS_MC12TuneV4_ProbNNp;
   Double_t        PiS_MC12TuneV4_ProbNNghost;
   Double_t        PiS_MC15TuneV1_ProbNNe;
   Double_t        PiS_MC15TuneV1_ProbNNmu;
   Double_t        PiS_MC15TuneV1_ProbNNpi;
   Double_t        PiS_MC15TuneV1_ProbNNk;
   Double_t        PiS_MC15TuneV1_ProbNNp;
   Double_t        PiS_MC15TuneV1_ProbNNghost;
   Double_t        PiS_CosTheta;
   Double_t        PiS_OWNPV_X;
   Double_t        PiS_OWNPV_Y;
   Double_t        PiS_OWNPV_Z;
   Double_t        PiS_OWNPV_XERR;
   Double_t        PiS_OWNPV_YERR;
   Double_t        PiS_OWNPV_ZERR;
   Double_t        PiS_OWNPV_CHI2;
   Int_t           PiS_OWNPV_NDOF;
   Float_t         PiS_OWNPV_COV_[3][3];
   Double_t        PiS_IP_OWNPV;
   Double_t        PiS_IPCHI2_OWNPV;
   Double_t        PiS_ORIVX_X;
   Double_t        PiS_ORIVX_Y;
   Double_t        PiS_ORIVX_Z;
   Double_t        PiS_ORIVX_XERR;
   Double_t        PiS_ORIVX_YERR;
   Double_t        PiS_ORIVX_ZERR;
   Double_t        PiS_ORIVX_CHI2;
   Int_t           PiS_ORIVX_NDOF;
   Float_t         PiS_ORIVX_COV_[3][3];
   Double_t        PiS_P;
   Double_t        PiS_PT;
   Double_t        PiS_PE;
   Double_t        PiS_PX;
   Double_t        PiS_PY;
   Double_t        PiS_PZ;
   Double_t        PiS_M;
   Int_t           PiS_ID;
   Double_t        PiS_PIDe;
   Double_t        PiS_PIDmu;
   Double_t        PiS_PIDK;
   Double_t        PiS_PIDp;
   Double_t        PiS_PIDd;
   Double_t        PiS_ProbNNe;
   Double_t        PiS_ProbNNk;
   Double_t        PiS_ProbNNp;
   Double_t        PiS_ProbNNpi;
   Double_t        PiS_ProbNNmu;
   Double_t        PiS_ProbNNd;
   Double_t        PiS_ProbNNghost;
   Bool_t          PiS_hasMuon;
   Bool_t          PiS_isMuon;
   Bool_t          PiS_hasRich;
   Bool_t          PiS_UsedRichAerogel;
   Bool_t          PiS_UsedRich1Gas;
   Bool_t          PiS_UsedRich2Gas;
   Bool_t          PiS_RichAboveElThres;
   Bool_t          PiS_RichAboveMuThres;
   Bool_t          PiS_RichAbovePiThres;
   Bool_t          PiS_RichAboveKaThres;
   Bool_t          PiS_RichAbovePrThres;
   Bool_t          PiS_hasCalo;
   Bool_t          PiS_L0Global_Dec;
   Bool_t          PiS_L0Global_TIS;
   Bool_t          PiS_L0Global_TOS;
   Bool_t          PiS_Hlt1Global_Dec;
   Bool_t          PiS_Hlt1Global_TIS;
   Bool_t          PiS_Hlt1Global_TOS;
   Bool_t          PiS_Hlt1Phys_Dec;
   Bool_t          PiS_Hlt1Phys_TIS;
   Bool_t          PiS_Hlt1Phys_TOS;
   Bool_t          PiS_Hlt2Global_Dec;
   Bool_t          PiS_Hlt2Global_TIS;
   Bool_t          PiS_Hlt2Global_TOS;
   Bool_t          PiS_Hlt2Phys_Dec;
   Bool_t          PiS_Hlt2Phys_TIS;
   Bool_t          PiS_Hlt2Phys_TOS;
   Int_t           PiS_TRACK_Type;
   Int_t           PiS_TRACK_Key;
   Double_t        PiS_TRACK_CHI2NDOF;
   Double_t        PiS_TRACK_PCHI2;
   Double_t        PiS_TRACK_MatchCHI2;
   Double_t        PiS_TRACK_GhostProb;
   Double_t        PiS_TRACK_CloneDist;
   Double_t        PiS_TRACK_Likelihood;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPV;
   Float_t         PVX[100];   //[nPV]
   Float_t         PVY[100];   //[nPV]
   Float_t         PVZ[100];   //[nPV]
   Float_t         PVXERR[100];   //[nPV]
   Float_t         PVYERR[100];   //[nPV]
   Float_t         PVZERR[100];   //[nPV]
   Float_t         PVCHI2[100];   //[nPV]
   Float_t         PVNDOF[100];   //[nPV]
   Float_t         PVNTRACKS[100];   //[nPV]
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;

   // List of branches
   TBranch        *b_Dst_DiraAngleError;   //!
   TBranch        *b_Dst_DiraCosError;   //!
   TBranch        *b_Dst_DiraAngle;   //!
   TBranch        *b_Dst_DiraCos;   //!
   TBranch        *b_Dst_ENDVERTEX_X;   //!
   TBranch        *b_Dst_ENDVERTEX_Y;   //!
   TBranch        *b_Dst_ENDVERTEX_Z;   //!
   TBranch        *b_Dst_ENDVERTEX_XERR;   //!
   TBranch        *b_Dst_ENDVERTEX_YERR;   //!
   TBranch        *b_Dst_ENDVERTEX_ZERR;   //!
   TBranch        *b_Dst_ENDVERTEX_CHI2;   //!
   TBranch        *b_Dst_ENDVERTEX_NDOF;   //!
   TBranch        *b_Dst_ENDVERTEX_COV_;   //!
   TBranch        *b_Dst_OWNPV_X;   //!
   TBranch        *b_Dst_OWNPV_Y;   //!
   TBranch        *b_Dst_OWNPV_Z;   //!
   TBranch        *b_Dst_OWNPV_XERR;   //!
   TBranch        *b_Dst_OWNPV_YERR;   //!
   TBranch        *b_Dst_OWNPV_ZERR;   //!
   TBranch        *b_Dst_OWNPV_CHI2;   //!
   TBranch        *b_Dst_OWNPV_NDOF;   //!
   TBranch        *b_Dst_OWNPV_COV_;   //!
   TBranch        *b_Dst_IP_OWNPV;   //!
   TBranch        *b_Dst_IPCHI2_OWNPV;   //!
   TBranch        *b_Dst_FD_OWNPV;   //!
   TBranch        *b_Dst_FDCHI2_OWNPV;   //!
   TBranch        *b_Dst_DIRA_OWNPV;   //!
   TBranch        *b_Dst_P;   //!
   TBranch        *b_Dst_PT;   //!
   TBranch        *b_Dst_PE;   //!
   TBranch        *b_Dst_PX;   //!
   TBranch        *b_Dst_PY;   //!
   TBranch        *b_Dst_PZ;   //!
   TBranch        *b_Dst_MM;   //!
   TBranch        *b_Dst_MMERR;   //!
   TBranch        *b_Dst_M;   //!
   TBranch        *b_Dst_ID;   //!
   TBranch        *b_Dst_TAU;   //!
   TBranch        *b_Dst_TAUERR;   //!
   TBranch        *b_Dst_TAUCHI2;   //!
   TBranch        *b_Dst_L0Global_Dec;   //!
   TBranch        *b_Dst_L0Global_TIS;   //!
   TBranch        *b_Dst_L0Global_TOS;   //!
   TBranch        *b_Dst_Hlt1Global_Dec;   //!
   TBranch        *b_Dst_Hlt1Global_TIS;   //!
   TBranch        *b_Dst_Hlt1Global_TOS;   //!
   TBranch        *b_Dst_Hlt1Phys_Dec;   //!
   TBranch        *b_Dst_Hlt1Phys_TIS;   //!
   TBranch        *b_Dst_Hlt1Phys_TOS;   //!
   TBranch        *b_Dst_Hlt2Global_Dec;   //!
   TBranch        *b_Dst_Hlt2Global_TIS;   //!
   TBranch        *b_Dst_Hlt2Global_TOS;   //!
   TBranch        *b_Dst_Hlt2Phys_Dec;   //!
   TBranch        *b_Dst_Hlt2Phys_TIS;   //!
   TBranch        *b_Dst_Hlt2Phys_TOS;   //!
   TBranch        *b_Dst_NumVtxWithinChi2WindowOneTrack;   //!
   TBranch        *b_Dst_SmallestDeltaChi2OneTrack;   //!
   TBranch        *b_Dst_SmallestDeltaChi2MassOneTrack;   //!
   TBranch        *b_Dst_SmallestDeltaChi2TwoTracks;   //!
   TBranch        *b_Dst_SmallestDeltaChi2MassTwoTracks;   //!
   TBranch        *b_pD0_COV_;   //!
   TBranch        *b_pPi_COV_;   //!
   TBranch        *b_PD0_ini0;   //!
   TBranch        *b_PD0_err0;   //!
   TBranch        *b_PxMiss_Fit0;   //!
   TBranch        *b_PxMiss_Err0;   //!
   TBranch        *b_PyMiss_Fit0;   //!
   TBranch        *b_PyMiss_Err0;   //!
   TBranch        *b_PzMiss_Fit0;   //!
   TBranch        *b_PzMiss_Err0;   //!
   TBranch        *b_X_K2Pi0;   //!
   TBranch        *b_Y_K2Pi0;   //!
   TBranch        *b_Z_K2Pi0;   //!
   TBranch        *b_PX_K2Pi0;   //!
   TBranch        *b_PY_K2Pi0;   //!
   TBranch        *b_PZ_K2Pi0;   //!
   TBranch        *b_PE_K2Pi0;   //!
   TBranch        *b_PX_Pi_s0;   //!
   TBranch        *b_PY_Pi_s0;   //!
   TBranch        *b_PZ_Pi_s0;   //!
   TBranch        *b_X_PV0;   //!
   TBranch        *b_Y_PV0;   //!
   TBranch        *b_Z_PV0;   //!
   TBranch        *b_PD0_Fit;   //!
   TBranch        *b_PxMiss_Fit;   //!
   TBranch        *b_PyMiss_Fit;   //!
   TBranch        *b_PzMiss_Fit;   //!
   TBranch        *b_PD0_Err;   //!
   TBranch        *b_PxMiss_Err;   //!
   TBranch        *b_PyMiss_Err;   //!
   TBranch        *b_PzMiss_Err;   //!
   TBranch        *b_X_K2Pi;   //!
   TBranch        *b_Y_K2Pi;   //!
   TBranch        *b_Z_K2Pi;   //!
   TBranch        *b_PX_K2Pi;   //!
   TBranch        *b_PY_K2Pi;   //!
   TBranch        *b_PZ_K2Pi;   //!
   TBranch        *b_PE_K2Pi;   //!
   TBranch        *b_PX_Pi_s;   //!
   TBranch        *b_PY_Pi_s;   //!
   TBranch        *b_PZ_Pi_s;   //!
   TBranch        *b_X_PV;   //!
   TBranch        *b_Y_PV;   //!
   TBranch        *b_Z_PV;   //!
   TBranch        *b_Chi2_final;   //!
   TBranch        *b_PMiss_Fit;   //!
   TBranch        *b_PMiss_Err;   //!
   TBranch        *b_PtMiss_Fit;   //!
   TBranch        *b_PtMiss_Err;   //!
   TBranch        *b_PD0_suc;   //!
   TBranch        *b_D0_FD;   //!
   TBranch        *b_D0_FDErr;   //!
   TBranch        *b_N;   //!
   TBranch        *b_Chi2_array;   //!
   TBranch        *b_VEta_COV_;   //!
   TBranch        *b_PD0_ini1;   //!
   TBranch        *b_PD0_ini2;   //!
   TBranch        *b_PD0_ini3;   //!
   TBranch        *b_D0_CosTheta;   //!
   TBranch        *b_D0_ENDVERTEX_X;   //!
   TBranch        *b_D0_ENDVERTEX_Y;   //!
   TBranch        *b_D0_ENDVERTEX_Z;   //!
   TBranch        *b_D0_ENDVERTEX_XERR;   //!
   TBranch        *b_D0_ENDVERTEX_YERR;   //!
   TBranch        *b_D0_ENDVERTEX_ZERR;   //!
   TBranch        *b_D0_ENDVERTEX_CHI2;   //!
   TBranch        *b_D0_ENDVERTEX_NDOF;   //!
   TBranch        *b_D0_ENDVERTEX_COV_;   //!
   TBranch        *b_D0_OWNPV_X;   //!
   TBranch        *b_D0_OWNPV_Y;   //!
   TBranch        *b_D0_OWNPV_Z;   //!
   TBranch        *b_D0_OWNPV_XERR;   //!
   TBranch        *b_D0_OWNPV_YERR;   //!
   TBranch        *b_D0_OWNPV_ZERR;   //!
   TBranch        *b_D0_OWNPV_CHI2;   //!
   TBranch        *b_D0_OWNPV_NDOF;   //!
   TBranch        *b_D0_OWNPV_COV_;   //!
   TBranch        *b_D0_IP_OWNPV;   //!
   TBranch        *b_D0_IPCHI2_OWNPV;   //!
   TBranch        *b_D0_FD_OWNPV;   //!
   TBranch        *b_D0_FDCHI2_OWNPV;   //!
   TBranch        *b_D0_DIRA_OWNPV;   //!
   TBranch        *b_D0_ORIVX_X;   //!
   TBranch        *b_D0_ORIVX_Y;   //!
   TBranch        *b_D0_ORIVX_Z;   //!
   TBranch        *b_D0_ORIVX_XERR;   //!
   TBranch        *b_D0_ORIVX_YERR;   //!
   TBranch        *b_D0_ORIVX_ZERR;   //!
   TBranch        *b_D0_ORIVX_CHI2;   //!
   TBranch        *b_D0_ORIVX_NDOF;   //!
   TBranch        *b_D0_ORIVX_COV_;   //!
   TBranch        *b_D0_FD_ORIVX;   //!
   TBranch        *b_D0_FDCHI2_ORIVX;   //!
   TBranch        *b_D0_DIRA_ORIVX;   //!
   TBranch        *b_D0_P;   //!
   TBranch        *b_D0_PT;   //!
   TBranch        *b_D0_PE;   //!
   TBranch        *b_D0_PX;   //!
   TBranch        *b_D0_PY;   //!
   TBranch        *b_D0_PZ;   //!
   TBranch        *b_D0_MM;   //!
   TBranch        *b_D0_MMERR;   //!
   TBranch        *b_D0_M;   //!
   TBranch        *b_D0_ID;   //!
   TBranch        *b_D0_TAU;   //!
   TBranch        *b_D0_TAUERR;   //!
   TBranch        *b_D0_TAUCHI2;   //!
   TBranch        *b_D0_L0Global_Dec;   //!
   TBranch        *b_D0_L0Global_TIS;   //!
   TBranch        *b_D0_L0Global_TOS;   //!
   TBranch        *b_D0_Hlt1Global_Dec;   //!
   TBranch        *b_D0_Hlt1Global_TIS;   //!
   TBranch        *b_D0_Hlt1Global_TOS;   //!
   TBranch        *b_D0_Hlt1Phys_Dec;   //!
   TBranch        *b_D0_Hlt1Phys_TIS;   //!
   TBranch        *b_D0_Hlt1Phys_TOS;   //!
   TBranch        *b_D0_Hlt2Global_Dec;   //!
   TBranch        *b_D0_Hlt2Global_TIS;   //!
   TBranch        *b_D0_Hlt2Global_TOS;   //!
   TBranch        *b_D0_Hlt2Phys_Dec;   //!
   TBranch        *b_D0_Hlt2Phys_TIS;   //!
   TBranch        *b_D0_Hlt2Phys_TOS;   //!
   TBranch        *b_D0_NumVtxWithinChi2WindowOneTrack;   //!
   TBranch        *b_D0_SmallestDeltaChi2OneTrack;   //!
   TBranch        *b_D0_SmallestDeltaChi2MassOneTrack;   //!
   TBranch        *b_D0_SmallestDeltaChi2TwoTracks;   //!
   TBranch        *b_D0_SmallestDeltaChi2MassTwoTracks;   //!
   TBranch        *b_Dfake_CosTheta;   //!
   TBranch        *b_Dfake_ENDVERTEX_X;   //!
   TBranch        *b_Dfake_ENDVERTEX_Y;   //!
   TBranch        *b_Dfake_ENDVERTEX_Z;   //!
   TBranch        *b_Dfake_ENDVERTEX_XERR;   //!
   TBranch        *b_Dfake_ENDVERTEX_YERR;   //!
   TBranch        *b_Dfake_ENDVERTEX_ZERR;   //!
   TBranch        *b_Dfake_ENDVERTEX_CHI2;   //!
   TBranch        *b_Dfake_ENDVERTEX_NDOF;   //!
   TBranch        *b_Dfake_ENDVERTEX_COV_;   //!
   TBranch        *b_Dfake_OWNPV_X;   //!
   TBranch        *b_Dfake_OWNPV_Y;   //!
   TBranch        *b_Dfake_OWNPV_Z;   //!
   TBranch        *b_Dfake_OWNPV_XERR;   //!
   TBranch        *b_Dfake_OWNPV_YERR;   //!
   TBranch        *b_Dfake_OWNPV_ZERR;   //!
   TBranch        *b_Dfake_OWNPV_CHI2;   //!
   TBranch        *b_Dfake_OWNPV_NDOF;   //!
   TBranch        *b_Dfake_OWNPV_COV_;   //!
   TBranch        *b_Dfake_IP_OWNPV;   //!
   TBranch        *b_Dfake_IPCHI2_OWNPV;   //!
   TBranch        *b_Dfake_FD_OWNPV;   //!
   TBranch        *b_Dfake_FDCHI2_OWNPV;   //!
   TBranch        *b_Dfake_DIRA_OWNPV;   //!
   TBranch        *b_Dfake_ORIVX_X;   //!
   TBranch        *b_Dfake_ORIVX_Y;   //!
   TBranch        *b_Dfake_ORIVX_Z;   //!
   TBranch        *b_Dfake_ORIVX_XERR;   //!
   TBranch        *b_Dfake_ORIVX_YERR;   //!
   TBranch        *b_Dfake_ORIVX_ZERR;   //!
   TBranch        *b_Dfake_ORIVX_CHI2;   //!
   TBranch        *b_Dfake_ORIVX_NDOF;   //!
   TBranch        *b_Dfake_ORIVX_COV_;   //!
   TBranch        *b_Dfake_FD_ORIVX;   //!
   TBranch        *b_Dfake_FDCHI2_ORIVX;   //!
   TBranch        *b_Dfake_DIRA_ORIVX;   //!
   TBranch        *b_Dfake_P;   //!
   TBranch        *b_Dfake_PT;   //!
   TBranch        *b_Dfake_PE;   //!
   TBranch        *b_Dfake_PX;   //!
   TBranch        *b_Dfake_PY;   //!
   TBranch        *b_Dfake_PZ;   //!
   TBranch        *b_Dfake_MM;   //!
   TBranch        *b_Dfake_MMERR;   //!
   TBranch        *b_Dfake_M;   //!
   TBranch        *b_Dfake_ID;   //!
   TBranch        *b_Dfake_TAU;   //!
   TBranch        *b_Dfake_TAUERR;   //!
   TBranch        *b_Dfake_TAUCHI2;   //!
   TBranch        *b_Dfake_L0Global_Dec;   //!
   TBranch        *b_Dfake_L0Global_TIS;   //!
   TBranch        *b_Dfake_L0Global_TOS;   //!
   TBranch        *b_Dfake_Hlt1Global_Dec;   //!
   TBranch        *b_Dfake_Hlt1Global_TIS;   //!
   TBranch        *b_Dfake_Hlt1Global_TOS;   //!
   TBranch        *b_Dfake_Hlt1Phys_Dec;   //!
   TBranch        *b_Dfake_Hlt1Phys_TIS;   //!
   TBranch        *b_Dfake_Hlt1Phys_TOS;   //!
   TBranch        *b_Dfake_Hlt2Global_Dec;   //!
   TBranch        *b_Dfake_Hlt2Global_TIS;   //!
   TBranch        *b_Dfake_Hlt2Global_TOS;   //!
   TBranch        *b_Dfake_Hlt2Phys_Dec;   //!
   TBranch        *b_Dfake_Hlt2Phys_TIS;   //!
   TBranch        *b_Dfake_Hlt2Phys_TOS;   //!
   TBranch        *b_Dfake_NumVtxWithinChi2WindowOneTrack;   //!
   TBranch        *b_Dfake_SmallestDeltaChi2OneTrack;   //!
   TBranch        *b_Dfake_SmallestDeltaChi2MassOneTrack;   //!
   TBranch        *b_Dfake_SmallestDeltaChi2TwoTracks;   //!
   TBranch        *b_Dfake_SmallestDeltaChi2MassTwoTracks;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_K_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_K_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_K_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_K_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_K_CosTheta;   //!
   TBranch        *b_K_OWNPV_X;   //!
   TBranch        *b_K_OWNPV_Y;   //!
   TBranch        *b_K_OWNPV_Z;   //!
   TBranch        *b_K_OWNPV_XERR;   //!
   TBranch        *b_K_OWNPV_YERR;   //!
   TBranch        *b_K_OWNPV_ZERR;   //!
   TBranch        *b_K_OWNPV_CHI2;   //!
   TBranch        *b_K_OWNPV_NDOF;   //!
   TBranch        *b_K_OWNPV_COV_;   //!
   TBranch        *b_K_IP_OWNPV;   //!
   TBranch        *b_K_IPCHI2_OWNPV;   //!
   TBranch        *b_K_ORIVX_X;   //!
   TBranch        *b_K_ORIVX_Y;   //!
   TBranch        *b_K_ORIVX_Z;   //!
   TBranch        *b_K_ORIVX_XERR;   //!
   TBranch        *b_K_ORIVX_YERR;   //!
   TBranch        *b_K_ORIVX_ZERR;   //!
   TBranch        *b_K_ORIVX_CHI2;   //!
   TBranch        *b_K_ORIVX_NDOF;   //!
   TBranch        *b_K_ORIVX_COV_;   //!
   TBranch        *b_K_P;   //!
   TBranch        *b_K_PT;   //!
   TBranch        *b_K_PE;   //!
   TBranch        *b_K_PX;   //!
   TBranch        *b_K_PY;   //!
   TBranch        *b_K_PZ;   //!
   TBranch        *b_K_M;   //!
   TBranch        *b_K_ID;   //!
   TBranch        *b_K_PIDe;   //!
   TBranch        *b_K_PIDmu;   //!
   TBranch        *b_K_PIDK;   //!
   TBranch        *b_K_PIDp;   //!
   TBranch        *b_K_PIDd;   //!
   TBranch        *b_K_ProbNNe;   //!
   TBranch        *b_K_ProbNNk;   //!
   TBranch        *b_K_ProbNNp;   //!
   TBranch        *b_K_ProbNNpi;   //!
   TBranch        *b_K_ProbNNmu;   //!
   TBranch        *b_K_ProbNNd;   //!
   TBranch        *b_K_ProbNNghost;   //!
   TBranch        *b_K_hasMuon;   //!
   TBranch        *b_K_isMuon;   //!
   TBranch        *b_K_hasRich;   //!
   TBranch        *b_K_UsedRichAerogel;   //!
   TBranch        *b_K_UsedRich1Gas;   //!
   TBranch        *b_K_UsedRich2Gas;   //!
   TBranch        *b_K_RichAboveElThres;   //!
   TBranch        *b_K_RichAboveMuThres;   //!
   TBranch        *b_K_RichAbovePiThres;   //!
   TBranch        *b_K_RichAboveKaThres;   //!
   TBranch        *b_K_RichAbovePrThres;   //!
   TBranch        *b_K_hasCalo;   //!
   TBranch        *b_K_L0Global_Dec;   //!
   TBranch        *b_K_L0Global_TIS;   //!
   TBranch        *b_K_L0Global_TOS;   //!
   TBranch        *b_K_Hlt1Global_Dec;   //!
   TBranch        *b_K_Hlt1Global_TIS;   //!
   TBranch        *b_K_Hlt1Global_TOS;   //!
   TBranch        *b_K_Hlt1Phys_Dec;   //!
   TBranch        *b_K_Hlt1Phys_TIS;   //!
   TBranch        *b_K_Hlt1Phys_TOS;   //!
   TBranch        *b_K_Hlt2Global_Dec;   //!
   TBranch        *b_K_Hlt2Global_TIS;   //!
   TBranch        *b_K_Hlt2Global_TOS;   //!
   TBranch        *b_K_Hlt2Phys_Dec;   //!
   TBranch        *b_K_Hlt2Phys_TIS;   //!
   TBranch        *b_K_Hlt2Phys_TOS;   //!
   TBranch        *b_K_TRACK_Type;   //!
   TBranch        *b_K_TRACK_Key;   //!
   TBranch        *b_K_TRACK_CHI2NDOF;   //!
   TBranch        *b_K_TRACK_PCHI2;   //!
   TBranch        *b_K_TRACK_MatchCHI2;   //!
   TBranch        *b_K_TRACK_GhostProb;   //!
   TBranch        *b_K_TRACK_CloneDist;   //!
   TBranch        *b_K_TRACK_Likelihood;   //!
   TBranch        *b_pi1_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_pi1_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_pi1_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_pi1_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_pi1_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_pi1_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_pi1_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_pi1_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_pi1_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_pi1_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_pi1_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_pi1_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_pi1_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_pi1_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_pi1_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_pi1_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_pi1_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_pi1_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_pi1_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_pi1_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_pi1_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_pi1_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_pi1_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_pi1_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_pi1_CosTheta;   //!
   TBranch        *b_pi1_OWNPV_X;   //!
   TBranch        *b_pi1_OWNPV_Y;   //!
   TBranch        *b_pi1_OWNPV_Z;   //!
   TBranch        *b_pi1_OWNPV_XERR;   //!
   TBranch        *b_pi1_OWNPV_YERR;   //!
   TBranch        *b_pi1_OWNPV_ZERR;   //!
   TBranch        *b_pi1_OWNPV_CHI2;   //!
   TBranch        *b_pi1_OWNPV_NDOF;   //!
   TBranch        *b_pi1_OWNPV_COV_;   //!
   TBranch        *b_pi1_IP_OWNPV;   //!
   TBranch        *b_pi1_IPCHI2_OWNPV;   //!
   TBranch        *b_pi1_ORIVX_X;   //!
   TBranch        *b_pi1_ORIVX_Y;   //!
   TBranch        *b_pi1_ORIVX_Z;   //!
   TBranch        *b_pi1_ORIVX_XERR;   //!
   TBranch        *b_pi1_ORIVX_YERR;   //!
   TBranch        *b_pi1_ORIVX_ZERR;   //!
   TBranch        *b_pi1_ORIVX_CHI2;   //!
   TBranch        *b_pi1_ORIVX_NDOF;   //!
   TBranch        *b_pi1_ORIVX_COV_;   //!
   TBranch        *b_pi1_P;   //!
   TBranch        *b_pi1_PT;   //!
   TBranch        *b_pi1_PE;   //!
   TBranch        *b_pi1_PX;   //!
   TBranch        *b_pi1_PY;   //!
   TBranch        *b_pi1_PZ;   //!
   TBranch        *b_pi1_M;   //!
   TBranch        *b_pi1_ID;   //!
   TBranch        *b_pi1_PIDe;   //!
   TBranch        *b_pi1_PIDmu;   //!
   TBranch        *b_pi1_PIDK;   //!
   TBranch        *b_pi1_PIDp;   //!
   TBranch        *b_pi1_PIDd;   //!
   TBranch        *b_pi1_ProbNNe;   //!
   TBranch        *b_pi1_ProbNNk;   //!
   TBranch        *b_pi1_ProbNNp;   //!
   TBranch        *b_pi1_ProbNNpi;   //!
   TBranch        *b_pi1_ProbNNmu;   //!
   TBranch        *b_pi1_ProbNNd;   //!
   TBranch        *b_pi1_ProbNNghost;   //!
   TBranch        *b_pi1_hasMuon;   //!
   TBranch        *b_pi1_isMuon;   //!
   TBranch        *b_pi1_hasRich;   //!
   TBranch        *b_pi1_UsedRichAerogel;   //!
   TBranch        *b_pi1_UsedRich1Gas;   //!
   TBranch        *b_pi1_UsedRich2Gas;   //!
   TBranch        *b_pi1_RichAboveElThres;   //!
   TBranch        *b_pi1_RichAboveMuThres;   //!
   TBranch        *b_pi1_RichAbovePiThres;   //!
   TBranch        *b_pi1_RichAboveKaThres;   //!
   TBranch        *b_pi1_RichAbovePrThres;   //!
   TBranch        *b_pi1_hasCalo;   //!
   TBranch        *b_pi1_L0Global_Dec;   //!
   TBranch        *b_pi1_L0Global_TIS;   //!
   TBranch        *b_pi1_L0Global_TOS;   //!
   TBranch        *b_pi1_Hlt1Global_Dec;   //!
   TBranch        *b_pi1_Hlt1Global_TIS;   //!
   TBranch        *b_pi1_Hlt1Global_TOS;   //!
   TBranch        *b_pi1_Hlt1Phys_Dec;   //!
   TBranch        *b_pi1_Hlt1Phys_TIS;   //!
   TBranch        *b_pi1_Hlt1Phys_TOS;   //!
   TBranch        *b_pi1_Hlt2Global_Dec;   //!
   TBranch        *b_pi1_Hlt2Global_TIS;   //!
   TBranch        *b_pi1_Hlt2Global_TOS;   //!
   TBranch        *b_pi1_Hlt2Phys_Dec;   //!
   TBranch        *b_pi1_Hlt2Phys_TIS;   //!
   TBranch        *b_pi1_Hlt2Phys_TOS;   //!
   TBranch        *b_pi1_TRACK_Type;   //!
   TBranch        *b_pi1_TRACK_Key;   //!
   TBranch        *b_pi1_TRACK_CHI2NDOF;   //!
   TBranch        *b_pi1_TRACK_PCHI2;   //!
   TBranch        *b_pi1_TRACK_MatchCHI2;   //!
   TBranch        *b_pi1_TRACK_GhostProb;   //!
   TBranch        *b_pi1_TRACK_CloneDist;   //!
   TBranch        *b_pi1_TRACK_Likelihood;   //!
   TBranch        *b_pi2_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_pi2_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_pi2_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_pi2_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_pi2_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_pi2_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_pi2_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_pi2_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_pi2_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_pi2_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_pi2_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_pi2_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_pi2_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_pi2_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_pi2_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_pi2_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_pi2_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_pi2_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_pi2_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_pi2_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_pi2_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_pi2_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_pi2_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_pi2_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_pi2_CosTheta;   //!
   TBranch        *b_pi2_OWNPV_X;   //!
   TBranch        *b_pi2_OWNPV_Y;   //!
   TBranch        *b_pi2_OWNPV_Z;   //!
   TBranch        *b_pi2_OWNPV_XERR;   //!
   TBranch        *b_pi2_OWNPV_YERR;   //!
   TBranch        *b_pi2_OWNPV_ZERR;   //!
   TBranch        *b_pi2_OWNPV_CHI2;   //!
   TBranch        *b_pi2_OWNPV_NDOF;   //!
   TBranch        *b_pi2_OWNPV_COV_;   //!
   TBranch        *b_pi2_IP_OWNPV;   //!
   TBranch        *b_pi2_IPCHI2_OWNPV;   //!
   TBranch        *b_pi2_ORIVX_X;   //!
   TBranch        *b_pi2_ORIVX_Y;   //!
   TBranch        *b_pi2_ORIVX_Z;   //!
   TBranch        *b_pi2_ORIVX_XERR;   //!
   TBranch        *b_pi2_ORIVX_YERR;   //!
   TBranch        *b_pi2_ORIVX_ZERR;   //!
   TBranch        *b_pi2_ORIVX_CHI2;   //!
   TBranch        *b_pi2_ORIVX_NDOF;   //!
   TBranch        *b_pi2_ORIVX_COV_;   //!
   TBranch        *b_pi2_P;   //!
   TBranch        *b_pi2_PT;   //!
   TBranch        *b_pi2_PE;   //!
   TBranch        *b_pi2_PX;   //!
   TBranch        *b_pi2_PY;   //!
   TBranch        *b_pi2_PZ;   //!
   TBranch        *b_pi2_M;   //!
   TBranch        *b_pi2_ID;   //!
   TBranch        *b_pi2_PIDe;   //!
   TBranch        *b_pi2_PIDmu;   //!
   TBranch        *b_pi2_PIDK;   //!
   TBranch        *b_pi2_PIDp;   //!
   TBranch        *b_pi2_PIDd;   //!
   TBranch        *b_pi2_ProbNNe;   //!
   TBranch        *b_pi2_ProbNNk;   //!
   TBranch        *b_pi2_ProbNNp;   //!
   TBranch        *b_pi2_ProbNNpi;   //!
   TBranch        *b_pi2_ProbNNmu;   //!
   TBranch        *b_pi2_ProbNNd;   //!
   TBranch        *b_pi2_ProbNNghost;   //!
   TBranch        *b_pi2_hasMuon;   //!
   TBranch        *b_pi2_isMuon;   //!
   TBranch        *b_pi2_hasRich;   //!
   TBranch        *b_pi2_UsedRichAerogel;   //!
   TBranch        *b_pi2_UsedRich1Gas;   //!
   TBranch        *b_pi2_UsedRich2Gas;   //!
   TBranch        *b_pi2_RichAboveElThres;   //!
   TBranch        *b_pi2_RichAboveMuThres;   //!
   TBranch        *b_pi2_RichAbovePiThres;   //!
   TBranch        *b_pi2_RichAboveKaThres;   //!
   TBranch        *b_pi2_RichAbovePrThres;   //!
   TBranch        *b_pi2_hasCalo;   //!
   TBranch        *b_pi2_L0Global_Dec;   //!
   TBranch        *b_pi2_L0Global_TIS;   //!
   TBranch        *b_pi2_L0Global_TOS;   //!
   TBranch        *b_pi2_Hlt1Global_Dec;   //!
   TBranch        *b_pi2_Hlt1Global_TIS;   //!
   TBranch        *b_pi2_Hlt1Global_TOS;   //!
   TBranch        *b_pi2_Hlt1Phys_Dec;   //!
   TBranch        *b_pi2_Hlt1Phys_TIS;   //!
   TBranch        *b_pi2_Hlt1Phys_TOS;   //!
   TBranch        *b_pi2_Hlt2Global_Dec;   //!
   TBranch        *b_pi2_Hlt2Global_TIS;   //!
   TBranch        *b_pi2_Hlt2Global_TOS;   //!
   TBranch        *b_pi2_Hlt2Phys_Dec;   //!
   TBranch        *b_pi2_Hlt2Phys_TIS;   //!
   TBranch        *b_pi2_Hlt2Phys_TOS;   //!
   TBranch        *b_pi2_TRACK_Type;   //!
   TBranch        *b_pi2_TRACK_Key;   //!
   TBranch        *b_pi2_TRACK_CHI2NDOF;   //!
   TBranch        *b_pi2_TRACK_PCHI2;   //!
   TBranch        *b_pi2_TRACK_MatchCHI2;   //!
   TBranch        *b_pi2_TRACK_GhostProb;   //!
   TBranch        *b_pi2_TRACK_CloneDist;   //!
   TBranch        *b_pi2_TRACK_Likelihood;   //!
   TBranch        *b_PiMiss_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_PiMiss_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_PiMiss_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_PiMiss_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_PiMiss_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_PiMiss_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_PiMiss_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_PiMiss_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_PiMiss_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_PiMiss_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_PiMiss_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_PiMiss_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_PiMiss_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_PiMiss_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_PiMiss_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_PiMiss_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_PiMiss_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_PiMiss_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_PiMiss_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_PiMiss_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_PiMiss_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_PiMiss_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_PiMiss_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_PiMiss_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_PiMiss_CosTheta;   //!
   TBranch        *b_PiMiss_OWNPV_X;   //!
   TBranch        *b_PiMiss_OWNPV_Y;   //!
   TBranch        *b_PiMiss_OWNPV_Z;   //!
   TBranch        *b_PiMiss_OWNPV_XERR;   //!
   TBranch        *b_PiMiss_OWNPV_YERR;   //!
   TBranch        *b_PiMiss_OWNPV_ZERR;   //!
   TBranch        *b_PiMiss_OWNPV_CHI2;   //!
   TBranch        *b_PiMiss_OWNPV_NDOF;   //!
   TBranch        *b_PiMiss_OWNPV_COV_;   //!
   TBranch        *b_PiMiss_IP_OWNPV;   //!
   TBranch        *b_PiMiss_IPCHI2_OWNPV;   //!
   TBranch        *b_PiMiss_ORIVX_X;   //!
   TBranch        *b_PiMiss_ORIVX_Y;   //!
   TBranch        *b_PiMiss_ORIVX_Z;   //!
   TBranch        *b_PiMiss_ORIVX_XERR;   //!
   TBranch        *b_PiMiss_ORIVX_YERR;   //!
   TBranch        *b_PiMiss_ORIVX_ZERR;   //!
   TBranch        *b_PiMiss_ORIVX_CHI2;   //!
   TBranch        *b_PiMiss_ORIVX_NDOF;   //!
   TBranch        *b_PiMiss_ORIVX_COV_;   //!
   TBranch        *b_PiMiss_P;   //!
   TBranch        *b_PiMiss_PT;   //!
   TBranch        *b_PiMiss_PE;   //!
   TBranch        *b_PiMiss_PX;   //!
   TBranch        *b_PiMiss_PY;   //!
   TBranch        *b_PiMiss_PZ;   //!
   TBranch        *b_PiMiss_M;   //!
   TBranch        *b_PiMiss_ID;   //!
   TBranch        *b_PiMiss_PIDe;   //!
   TBranch        *b_PiMiss_PIDmu;   //!
   TBranch        *b_PiMiss_PIDK;   //!
   TBranch        *b_PiMiss_PIDp;   //!
   TBranch        *b_PiMiss_PIDd;   //!
   TBranch        *b_PiMiss_ProbNNe;   //!
   TBranch        *b_PiMiss_ProbNNk;   //!
   TBranch        *b_PiMiss_ProbNNp;   //!
   TBranch        *b_PiMiss_ProbNNpi;   //!
   TBranch        *b_PiMiss_ProbNNmu;   //!
   TBranch        *b_PiMiss_ProbNNd;   //!
   TBranch        *b_PiMiss_ProbNNghost;   //!
   TBranch        *b_PiMiss_hasMuon;   //!
   TBranch        *b_PiMiss_isMuon;   //!
   TBranch        *b_PiMiss_hasRich;   //!
   TBranch        *b_PiMiss_UsedRichAerogel;   //!
   TBranch        *b_PiMiss_UsedRich1Gas;   //!
   TBranch        *b_PiMiss_UsedRich2Gas;   //!
   TBranch        *b_PiMiss_RichAboveElThres;   //!
   TBranch        *b_PiMiss_RichAboveMuThres;   //!
   TBranch        *b_PiMiss_RichAbovePiThres;   //!
   TBranch        *b_PiMiss_RichAboveKaThres;   //!
   TBranch        *b_PiMiss_RichAbovePrThres;   //!
   TBranch        *b_PiMiss_hasCalo;   //!
   TBranch        *b_PiMiss_L0Global_Dec;   //!
   TBranch        *b_PiMiss_L0Global_TIS;   //!
   TBranch        *b_PiMiss_L0Global_TOS;   //!
   TBranch        *b_PiMiss_Hlt1Global_Dec;   //!
   TBranch        *b_PiMiss_Hlt1Global_TIS;   //!
   TBranch        *b_PiMiss_Hlt1Global_TOS;   //!
   TBranch        *b_PiMiss_Hlt1Phys_Dec;   //!
   TBranch        *b_PiMiss_Hlt1Phys_TIS;   //!
   TBranch        *b_PiMiss_Hlt1Phys_TOS;   //!
   TBranch        *b_PiMiss_Hlt2Global_Dec;   //!
   TBranch        *b_PiMiss_Hlt2Global_TIS;   //!
   TBranch        *b_PiMiss_Hlt2Global_TOS;   //!
   TBranch        *b_PiMiss_Hlt2Phys_Dec;   //!
   TBranch        *b_PiMiss_Hlt2Phys_TIS;   //!
   TBranch        *b_PiMiss_Hlt2Phys_TOS;   //!
   TBranch        *b_PiMiss_TRACK_Type;   //!
   TBranch        *b_PiMiss_TRACK_Key;   //!
   TBranch        *b_PiMiss_TRACK_CHI2NDOF;   //!
   TBranch        *b_PiMiss_TRACK_PCHI2;   //!
   TBranch        *b_PiMiss_TRACK_MatchCHI2;   //!
   TBranch        *b_PiMiss_TRACK_GhostProb;   //!
   TBranch        *b_PiMiss_TRACK_CloneDist;   //!
   TBranch        *b_PiMiss_TRACK_Likelihood;   //!
   TBranch        *b_PiS_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_PiS_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_PiS_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_PiS_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_PiS_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_PiS_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_PiS_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_PiS_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_PiS_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_PiS_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_PiS_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_PiS_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_PiS_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_PiS_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_PiS_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_PiS_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_PiS_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_PiS_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_PiS_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_PiS_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_PiS_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_PiS_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_PiS_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_PiS_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_PiS_CosTheta;   //!
   TBranch        *b_PiS_OWNPV_X;   //!
   TBranch        *b_PiS_OWNPV_Y;   //!
   TBranch        *b_PiS_OWNPV_Z;   //!
   TBranch        *b_PiS_OWNPV_XERR;   //!
   TBranch        *b_PiS_OWNPV_YERR;   //!
   TBranch        *b_PiS_OWNPV_ZERR;   //!
   TBranch        *b_PiS_OWNPV_CHI2;   //!
   TBranch        *b_PiS_OWNPV_NDOF;   //!
   TBranch        *b_PiS_OWNPV_COV_;   //!
   TBranch        *b_PiS_IP_OWNPV;   //!
   TBranch        *b_PiS_IPCHI2_OWNPV;   //!
   TBranch        *b_PiS_ORIVX_X;   //!
   TBranch        *b_PiS_ORIVX_Y;   //!
   TBranch        *b_PiS_ORIVX_Z;   //!
   TBranch        *b_PiS_ORIVX_XERR;   //!
   TBranch        *b_PiS_ORIVX_YERR;   //!
   TBranch        *b_PiS_ORIVX_ZERR;   //!
   TBranch        *b_PiS_ORIVX_CHI2;   //!
   TBranch        *b_PiS_ORIVX_NDOF;   //!
   TBranch        *b_PiS_ORIVX_COV_;   //!
   TBranch        *b_PiS_P;   //!
   TBranch        *b_PiS_PT;   //!
   TBranch        *b_PiS_PE;   //!
   TBranch        *b_PiS_PX;   //!
   TBranch        *b_PiS_PY;   //!
   TBranch        *b_PiS_PZ;   //!
   TBranch        *b_PiS_M;   //!
   TBranch        *b_PiS_ID;   //!
   TBranch        *b_PiS_PIDe;   //!
   TBranch        *b_PiS_PIDmu;   //!
   TBranch        *b_PiS_PIDK;   //!
   TBranch        *b_PiS_PIDp;   //!
   TBranch        *b_PiS_PIDd;   //!
   TBranch        *b_PiS_ProbNNe;   //!
   TBranch        *b_PiS_ProbNNk;   //!
   TBranch        *b_PiS_ProbNNp;   //!
   TBranch        *b_PiS_ProbNNpi;   //!
   TBranch        *b_PiS_ProbNNmu;   //!
   TBranch        *b_PiS_ProbNNd;   //!
   TBranch        *b_PiS_ProbNNghost;   //!
   TBranch        *b_PiS_hasMuon;   //!
   TBranch        *b_PiS_isMuon;   //!
   TBranch        *b_PiS_hasRich;   //!
   TBranch        *b_PiS_UsedRichAerogel;   //!
   TBranch        *b_PiS_UsedRich1Gas;   //!
   TBranch        *b_PiS_UsedRich2Gas;   //!
   TBranch        *b_PiS_RichAboveElThres;   //!
   TBranch        *b_PiS_RichAboveMuThres;   //!
   TBranch        *b_PiS_RichAbovePiThres;   //!
   TBranch        *b_PiS_RichAboveKaThres;   //!
   TBranch        *b_PiS_RichAbovePrThres;   //!
   TBranch        *b_PiS_hasCalo;   //!
   TBranch        *b_PiS_L0Global_Dec;   //!
   TBranch        *b_PiS_L0Global_TIS;   //!
   TBranch        *b_PiS_L0Global_TOS;   //!
   TBranch        *b_PiS_Hlt1Global_Dec;   //!
   TBranch        *b_PiS_Hlt1Global_TIS;   //!
   TBranch        *b_PiS_Hlt1Global_TOS;   //!
   TBranch        *b_PiS_Hlt1Phys_Dec;   //!
   TBranch        *b_PiS_Hlt1Phys_TIS;   //!
   TBranch        *b_PiS_Hlt1Phys_TOS;   //!
   TBranch        *b_PiS_Hlt2Global_Dec;   //!
   TBranch        *b_PiS_Hlt2Global_TIS;   //!
   TBranch        *b_PiS_Hlt2Global_TOS;   //!
   TBranch        *b_PiS_Hlt2Phys_Dec;   //!
   TBranch        *b_PiS_Hlt2Phys_TIS;   //!
   TBranch        *b_PiS_Hlt2Phys_TOS;   //!
   TBranch        *b_PiS_TRACK_Type;   //!
   TBranch        *b_PiS_TRACK_Key;   //!
   TBranch        *b_PiS_TRACK_CHI2NDOF;   //!
   TBranch        *b_PiS_TRACK_PCHI2;   //!
   TBranch        *b_PiS_TRACK_MatchCHI2;   //!
   TBranch        *b_PiS_TRACK_GhostProb;   //!
   TBranch        *b_PiS_TRACK_CloneDist;   //!
   TBranch        *b_PiS_TRACK_Likelihood;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVX;   //!
   TBranch        *b_PVY;   //!
   TBranch        *b_PVZ;   //!
   TBranch        *b_PVXERR;   //!
   TBranch        *b_PVYERR;   //!
   TBranch        *b_PVZERR;   //!
   TBranch        *b_PVCHI2;   //!
   TBranch        *b_PVNDOF;   //!
   TBranch        *b_PVNTRACKS;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!

   DecayTree(TTree *tree=0);
   virtual ~DecayTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef DecayTree_cxx
DecayTree::DecayTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Tuple.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("Tuple.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("Tuple.root:/TupleFullRS");
      dir->GetObject("DecayTree",tree);

   }
   Init(tree);
}

DecayTree::~DecayTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t DecayTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t DecayTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void DecayTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Dst_DiraAngleError", &Dst_DiraAngleError, &b_Dst_DiraAngleError);
   fChain->SetBranchAddress("Dst_DiraCosError", &Dst_DiraCosError, &b_Dst_DiraCosError);
   fChain->SetBranchAddress("Dst_DiraAngle", &Dst_DiraAngle, &b_Dst_DiraAngle);
   fChain->SetBranchAddress("Dst_DiraCos", &Dst_DiraCos, &b_Dst_DiraCos);
   fChain->SetBranchAddress("Dst_ENDVERTEX_X", &Dst_ENDVERTEX_X, &b_Dst_ENDVERTEX_X);
   fChain->SetBranchAddress("Dst_ENDVERTEX_Y", &Dst_ENDVERTEX_Y, &b_Dst_ENDVERTEX_Y);
   fChain->SetBranchAddress("Dst_ENDVERTEX_Z", &Dst_ENDVERTEX_Z, &b_Dst_ENDVERTEX_Z);
   fChain->SetBranchAddress("Dst_ENDVERTEX_XERR", &Dst_ENDVERTEX_XERR, &b_Dst_ENDVERTEX_XERR);
   fChain->SetBranchAddress("Dst_ENDVERTEX_YERR", &Dst_ENDVERTEX_YERR, &b_Dst_ENDVERTEX_YERR);
   fChain->SetBranchAddress("Dst_ENDVERTEX_ZERR", &Dst_ENDVERTEX_ZERR, &b_Dst_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("Dst_ENDVERTEX_CHI2", &Dst_ENDVERTEX_CHI2, &b_Dst_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("Dst_ENDVERTEX_NDOF", &Dst_ENDVERTEX_NDOF, &b_Dst_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("Dst_ENDVERTEX_COV_", Dst_ENDVERTEX_COV_, &b_Dst_ENDVERTEX_COV_);
   fChain->SetBranchAddress("Dst_OWNPV_X", &Dst_OWNPV_X, &b_Dst_OWNPV_X);
   fChain->SetBranchAddress("Dst_OWNPV_Y", &Dst_OWNPV_Y, &b_Dst_OWNPV_Y);
   fChain->SetBranchAddress("Dst_OWNPV_Z", &Dst_OWNPV_Z, &b_Dst_OWNPV_Z);
   fChain->SetBranchAddress("Dst_OWNPV_XERR", &Dst_OWNPV_XERR, &b_Dst_OWNPV_XERR);
   fChain->SetBranchAddress("Dst_OWNPV_YERR", &Dst_OWNPV_YERR, &b_Dst_OWNPV_YERR);
   fChain->SetBranchAddress("Dst_OWNPV_ZERR", &Dst_OWNPV_ZERR, &b_Dst_OWNPV_ZERR);
   fChain->SetBranchAddress("Dst_OWNPV_CHI2", &Dst_OWNPV_CHI2, &b_Dst_OWNPV_CHI2);
   fChain->SetBranchAddress("Dst_OWNPV_NDOF", &Dst_OWNPV_NDOF, &b_Dst_OWNPV_NDOF);
   fChain->SetBranchAddress("Dst_OWNPV_COV_", Dst_OWNPV_COV_, &b_Dst_OWNPV_COV_);
   fChain->SetBranchAddress("Dst_IP_OWNPV", &Dst_IP_OWNPV, &b_Dst_IP_OWNPV);
   fChain->SetBranchAddress("Dst_IPCHI2_OWNPV", &Dst_IPCHI2_OWNPV, &b_Dst_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Dst_FD_OWNPV", &Dst_FD_OWNPV, &b_Dst_FD_OWNPV);
   fChain->SetBranchAddress("Dst_FDCHI2_OWNPV", &Dst_FDCHI2_OWNPV, &b_Dst_FDCHI2_OWNPV);
   fChain->SetBranchAddress("Dst_DIRA_OWNPV", &Dst_DIRA_OWNPV, &b_Dst_DIRA_OWNPV);
   fChain->SetBranchAddress("Dst_P", &Dst_P, &b_Dst_P);
   fChain->SetBranchAddress("Dst_PT", &Dst_PT, &b_Dst_PT);
   fChain->SetBranchAddress("Dst_PE", &Dst_PE, &b_Dst_PE);
   fChain->SetBranchAddress("Dst_PX", &Dst_PX, &b_Dst_PX);
   fChain->SetBranchAddress("Dst_PY", &Dst_PY, &b_Dst_PY);
   fChain->SetBranchAddress("Dst_PZ", &Dst_PZ, &b_Dst_PZ);
   fChain->SetBranchAddress("Dst_MM", &Dst_MM, &b_Dst_MM);
   fChain->SetBranchAddress("Dst_MMERR", &Dst_MMERR, &b_Dst_MMERR);
   fChain->SetBranchAddress("Dst_M", &Dst_M, &b_Dst_M);
   fChain->SetBranchAddress("Dst_ID", &Dst_ID, &b_Dst_ID);
   fChain->SetBranchAddress("Dst_TAU", &Dst_TAU, &b_Dst_TAU);
   fChain->SetBranchAddress("Dst_TAUERR", &Dst_TAUERR, &b_Dst_TAUERR);
   fChain->SetBranchAddress("Dst_TAUCHI2", &Dst_TAUCHI2, &b_Dst_TAUCHI2);
   fChain->SetBranchAddress("Dst_L0Global_Dec", &Dst_L0Global_Dec, &b_Dst_L0Global_Dec);
   fChain->SetBranchAddress("Dst_L0Global_TIS", &Dst_L0Global_TIS, &b_Dst_L0Global_TIS);
   fChain->SetBranchAddress("Dst_L0Global_TOS", &Dst_L0Global_TOS, &b_Dst_L0Global_TOS);
   fChain->SetBranchAddress("Dst_Hlt1Global_Dec", &Dst_Hlt1Global_Dec, &b_Dst_Hlt1Global_Dec);
   fChain->SetBranchAddress("Dst_Hlt1Global_TIS", &Dst_Hlt1Global_TIS, &b_Dst_Hlt1Global_TIS);
   fChain->SetBranchAddress("Dst_Hlt1Global_TOS", &Dst_Hlt1Global_TOS, &b_Dst_Hlt1Global_TOS);
   fChain->SetBranchAddress("Dst_Hlt1Phys_Dec", &Dst_Hlt1Phys_Dec, &b_Dst_Hlt1Phys_Dec);
   fChain->SetBranchAddress("Dst_Hlt1Phys_TIS", &Dst_Hlt1Phys_TIS, &b_Dst_Hlt1Phys_TIS);
   fChain->SetBranchAddress("Dst_Hlt1Phys_TOS", &Dst_Hlt1Phys_TOS, &b_Dst_Hlt1Phys_TOS);
   fChain->SetBranchAddress("Dst_Hlt2Global_Dec", &Dst_Hlt2Global_Dec, &b_Dst_Hlt2Global_Dec);
   fChain->SetBranchAddress("Dst_Hlt2Global_TIS", &Dst_Hlt2Global_TIS, &b_Dst_Hlt2Global_TIS);
   fChain->SetBranchAddress("Dst_Hlt2Global_TOS", &Dst_Hlt2Global_TOS, &b_Dst_Hlt2Global_TOS);
   fChain->SetBranchAddress("Dst_Hlt2Phys_Dec", &Dst_Hlt2Phys_Dec, &b_Dst_Hlt2Phys_Dec);
   fChain->SetBranchAddress("Dst_Hlt2Phys_TIS", &Dst_Hlt2Phys_TIS, &b_Dst_Hlt2Phys_TIS);
   fChain->SetBranchAddress("Dst_Hlt2Phys_TOS", &Dst_Hlt2Phys_TOS, &b_Dst_Hlt2Phys_TOS);
   fChain->SetBranchAddress("Dst_NumVtxWithinChi2WindowOneTrack", &Dst_NumVtxWithinChi2WindowOneTrack, &b_Dst_NumVtxWithinChi2WindowOneTrack);
   fChain->SetBranchAddress("Dst_SmallestDeltaChi2OneTrack", &Dst_SmallestDeltaChi2OneTrack, &b_Dst_SmallestDeltaChi2OneTrack);
   fChain->SetBranchAddress("Dst_SmallestDeltaChi2MassOneTrack", &Dst_SmallestDeltaChi2MassOneTrack, &b_Dst_SmallestDeltaChi2MassOneTrack);
   fChain->SetBranchAddress("Dst_SmallestDeltaChi2TwoTracks", &Dst_SmallestDeltaChi2TwoTracks, &b_Dst_SmallestDeltaChi2TwoTracks);
   fChain->SetBranchAddress("Dst_SmallestDeltaChi2MassTwoTracks", &Dst_SmallestDeltaChi2MassTwoTracks, &b_Dst_SmallestDeltaChi2MassTwoTracks);
   fChain->SetBranchAddress("pD0_COV_", pD0_COV_, &b_pD0_COV_);
   fChain->SetBranchAddress("pPi_COV_", pPi_COV_, &b_pPi_COV_);
   fChain->SetBranchAddress("PD0_ini0", &PD0_ini0, &b_PD0_ini0);
   fChain->SetBranchAddress("PD0_err0", &PD0_err0, &b_PD0_err0);
   fChain->SetBranchAddress("PxMiss_Fit0", &PxMiss_Fit0, &b_PxMiss_Fit0);
   fChain->SetBranchAddress("PxMiss_Err0", &PxMiss_Err0, &b_PxMiss_Err0);
   fChain->SetBranchAddress("PyMiss_Fit0", &PyMiss_Fit0, &b_PyMiss_Fit0);
   fChain->SetBranchAddress("PyMiss_Err0", &PyMiss_Err0, &b_PyMiss_Err0);
   fChain->SetBranchAddress("PzMiss_Fit0", &PzMiss_Fit0, &b_PzMiss_Fit0);
   fChain->SetBranchAddress("PzMiss_Err0", &PzMiss_Err0, &b_PzMiss_Err0);
   fChain->SetBranchAddress("X_K2Pi0", &X_K2Pi0, &b_X_K2Pi0);
   fChain->SetBranchAddress("Y_K2Pi0", &Y_K2Pi0, &b_Y_K2Pi0);
   fChain->SetBranchAddress("Z_K2Pi0", &Z_K2Pi0, &b_Z_K2Pi0);
   fChain->SetBranchAddress("PX_K2Pi0", &PX_K2Pi0, &b_PX_K2Pi0);
   fChain->SetBranchAddress("PY_K2Pi0", &PY_K2Pi0, &b_PY_K2Pi0);
   fChain->SetBranchAddress("PZ_K2Pi0", &PZ_K2Pi0, &b_PZ_K2Pi0);
   fChain->SetBranchAddress("PE_K2Pi0", &PE_K2Pi0, &b_PE_K2Pi0);
   fChain->SetBranchAddress("PX_Pi_s0", &PX_Pi_s0, &b_PX_Pi_s0);
   fChain->SetBranchAddress("PY_Pi_s0", &PY_Pi_s0, &b_PY_Pi_s0);
   fChain->SetBranchAddress("PZ_Pi_s0", &PZ_Pi_s0, &b_PZ_Pi_s0);
   fChain->SetBranchAddress("X_PV0", &X_PV0, &b_X_PV0);
   fChain->SetBranchAddress("Y_PV0", &Y_PV0, &b_Y_PV0);
   fChain->SetBranchAddress("Z_PV0", &Z_PV0, &b_Z_PV0);
   fChain->SetBranchAddress("PD0_Fit", &PD0_Fit, &b_PD0_Fit);
   fChain->SetBranchAddress("PxMiss_Fit", &PxMiss_Fit, &b_PxMiss_Fit);
   fChain->SetBranchAddress("PyMiss_Fit", &PyMiss_Fit, &b_PyMiss_Fit);
   fChain->SetBranchAddress("PzMiss_Fit", &PzMiss_Fit, &b_PzMiss_Fit);
   fChain->SetBranchAddress("PD0_Err", &PD0_Err, &b_PD0_Err);
   fChain->SetBranchAddress("PxMiss_Err", &PxMiss_Err, &b_PxMiss_Err);
   fChain->SetBranchAddress("PyMiss_Err", &PyMiss_Err, &b_PyMiss_Err);
   fChain->SetBranchAddress("PzMiss_Err", &PzMiss_Err, &b_PzMiss_Err);
   fChain->SetBranchAddress("X_K2Pi", &X_K2Pi, &b_X_K2Pi);
   fChain->SetBranchAddress("Y_K2Pi", &Y_K2Pi, &b_Y_K2Pi);
   fChain->SetBranchAddress("Z_K2Pi", &Z_K2Pi, &b_Z_K2Pi);
   fChain->SetBranchAddress("PX_K2Pi", &PX_K2Pi, &b_PX_K2Pi);
   fChain->SetBranchAddress("PY_K2Pi", &PY_K2Pi, &b_PY_K2Pi);
   fChain->SetBranchAddress("PZ_K2Pi", &PZ_K2Pi, &b_PZ_K2Pi);
   fChain->SetBranchAddress("PE_K2Pi", &PE_K2Pi, &b_PE_K2Pi);
   fChain->SetBranchAddress("PX_Pi_s", &PX_Pi_s, &b_PX_Pi_s);
   fChain->SetBranchAddress("PY_Pi_s", &PY_Pi_s, &b_PY_Pi_s);
   fChain->SetBranchAddress("PZ_Pi_s", &PZ_Pi_s, &b_PZ_Pi_s);
   fChain->SetBranchAddress("X_PV", &X_PV, &b_X_PV);
   fChain->SetBranchAddress("Y_PV", &Y_PV, &b_Y_PV);
   fChain->SetBranchAddress("Z_PV", &Z_PV, &b_Z_PV);
   fChain->SetBranchAddress("Chi2_final", &Chi2_final, &b_Chi2_final);
   fChain->SetBranchAddress("PMiss_Fit", &PMiss_Fit, &b_PMiss_Fit);
   fChain->SetBranchAddress("PMiss_Err", &PMiss_Err, &b_PMiss_Err);
   fChain->SetBranchAddress("PtMiss_Fit", &PtMiss_Fit, &b_PtMiss_Fit);
   fChain->SetBranchAddress("PtMiss_Err", &PtMiss_Err, &b_PtMiss_Err);
   fChain->SetBranchAddress("PD0_suc", &PD0_suc, &b_PD0_suc);
   fChain->SetBranchAddress("D0_FD", &D0_FD, &b_D0_FD);
   fChain->SetBranchAddress("D0_FDErr", &D0_FDErr, &b_D0_FDErr);
   fChain->SetBranchAddress("N", &N, &b_N);
   fChain->SetBranchAddress("Chi2_array", Chi2_array, &b_Chi2_array);
   fChain->SetBranchAddress("VEta_COV_", VEta_COV_, &b_VEta_COV_);
   fChain->SetBranchAddress("PD0_ini1", &PD0_ini1, &b_PD0_ini1);
   fChain->SetBranchAddress("PD0_ini2", &PD0_ini2, &b_PD0_ini2);
   fChain->SetBranchAddress("PD0_ini3", &PD0_ini3, &b_PD0_ini3);
   fChain->SetBranchAddress("D0_CosTheta", &D0_CosTheta, &b_D0_CosTheta);
   fChain->SetBranchAddress("D0_ENDVERTEX_X", &D0_ENDVERTEX_X, &b_D0_ENDVERTEX_X);
   fChain->SetBranchAddress("D0_ENDVERTEX_Y", &D0_ENDVERTEX_Y, &b_D0_ENDVERTEX_Y);
   fChain->SetBranchAddress("D0_ENDVERTEX_Z", &D0_ENDVERTEX_Z, &b_D0_ENDVERTEX_Z);
   fChain->SetBranchAddress("D0_ENDVERTEX_XERR", &D0_ENDVERTEX_XERR, &b_D0_ENDVERTEX_XERR);
   fChain->SetBranchAddress("D0_ENDVERTEX_YERR", &D0_ENDVERTEX_YERR, &b_D0_ENDVERTEX_YERR);
   fChain->SetBranchAddress("D0_ENDVERTEX_ZERR", &D0_ENDVERTEX_ZERR, &b_D0_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("D0_ENDVERTEX_CHI2", &D0_ENDVERTEX_CHI2, &b_D0_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("D0_ENDVERTEX_NDOF", &D0_ENDVERTEX_NDOF, &b_D0_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("D0_ENDVERTEX_COV_", D0_ENDVERTEX_COV_, &b_D0_ENDVERTEX_COV_);
   fChain->SetBranchAddress("D0_OWNPV_X", &D0_OWNPV_X, &b_D0_OWNPV_X);
   fChain->SetBranchAddress("D0_OWNPV_Y", &D0_OWNPV_Y, &b_D0_OWNPV_Y);
   fChain->SetBranchAddress("D0_OWNPV_Z", &D0_OWNPV_Z, &b_D0_OWNPV_Z);
   fChain->SetBranchAddress("D0_OWNPV_XERR", &D0_OWNPV_XERR, &b_D0_OWNPV_XERR);
   fChain->SetBranchAddress("D0_OWNPV_YERR", &D0_OWNPV_YERR, &b_D0_OWNPV_YERR);
   fChain->SetBranchAddress("D0_OWNPV_ZERR", &D0_OWNPV_ZERR, &b_D0_OWNPV_ZERR);
   fChain->SetBranchAddress("D0_OWNPV_CHI2", &D0_OWNPV_CHI2, &b_D0_OWNPV_CHI2);
   fChain->SetBranchAddress("D0_OWNPV_NDOF", &D0_OWNPV_NDOF, &b_D0_OWNPV_NDOF);
   fChain->SetBranchAddress("D0_OWNPV_COV_", D0_OWNPV_COV_, &b_D0_OWNPV_COV_);
   fChain->SetBranchAddress("D0_IP_OWNPV", &D0_IP_OWNPV, &b_D0_IP_OWNPV);
   fChain->SetBranchAddress("D0_IPCHI2_OWNPV", &D0_IPCHI2_OWNPV, &b_D0_IPCHI2_OWNPV);
   fChain->SetBranchAddress("D0_FD_OWNPV", &D0_FD_OWNPV, &b_D0_FD_OWNPV);
   fChain->SetBranchAddress("D0_FDCHI2_OWNPV", &D0_FDCHI2_OWNPV, &b_D0_FDCHI2_OWNPV);
   fChain->SetBranchAddress("D0_DIRA_OWNPV", &D0_DIRA_OWNPV, &b_D0_DIRA_OWNPV);
   fChain->SetBranchAddress("D0_ORIVX_X", &D0_ORIVX_X, &b_D0_ORIVX_X);
   fChain->SetBranchAddress("D0_ORIVX_Y", &D0_ORIVX_Y, &b_D0_ORIVX_Y);
   fChain->SetBranchAddress("D0_ORIVX_Z", &D0_ORIVX_Z, &b_D0_ORIVX_Z);
   fChain->SetBranchAddress("D0_ORIVX_XERR", &D0_ORIVX_XERR, &b_D0_ORIVX_XERR);
   fChain->SetBranchAddress("D0_ORIVX_YERR", &D0_ORIVX_YERR, &b_D0_ORIVX_YERR);
   fChain->SetBranchAddress("D0_ORIVX_ZERR", &D0_ORIVX_ZERR, &b_D0_ORIVX_ZERR);
   fChain->SetBranchAddress("D0_ORIVX_CHI2", &D0_ORIVX_CHI2, &b_D0_ORIVX_CHI2);
   fChain->SetBranchAddress("D0_ORIVX_NDOF", &D0_ORIVX_NDOF, &b_D0_ORIVX_NDOF);
   fChain->SetBranchAddress("D0_ORIVX_COV_", D0_ORIVX_COV_, &b_D0_ORIVX_COV_);
   fChain->SetBranchAddress("D0_FD_ORIVX", &D0_FD_ORIVX, &b_D0_FD_ORIVX);
   fChain->SetBranchAddress("D0_FDCHI2_ORIVX", &D0_FDCHI2_ORIVX, &b_D0_FDCHI2_ORIVX);
   fChain->SetBranchAddress("D0_DIRA_ORIVX", &D0_DIRA_ORIVX, &b_D0_DIRA_ORIVX);
   fChain->SetBranchAddress("D0_P", &D0_P, &b_D0_P);
   fChain->SetBranchAddress("D0_PT", &D0_PT, &b_D0_PT);
   fChain->SetBranchAddress("D0_PE", &D0_PE, &b_D0_PE);
   fChain->SetBranchAddress("D0_PX", &D0_PX, &b_D0_PX);
   fChain->SetBranchAddress("D0_PY", &D0_PY, &b_D0_PY);
   fChain->SetBranchAddress("D0_PZ", &D0_PZ, &b_D0_PZ);
   fChain->SetBranchAddress("D0_MM", &D0_MM, &b_D0_MM);
   fChain->SetBranchAddress("D0_MMERR", &D0_MMERR, &b_D0_MMERR);
   fChain->SetBranchAddress("D0_M", &D0_M, &b_D0_M);
   fChain->SetBranchAddress("D0_ID", &D0_ID, &b_D0_ID);
   fChain->SetBranchAddress("D0_TAU", &D0_TAU, &b_D0_TAU);
   fChain->SetBranchAddress("D0_TAUERR", &D0_TAUERR, &b_D0_TAUERR);
   fChain->SetBranchAddress("D0_TAUCHI2", &D0_TAUCHI2, &b_D0_TAUCHI2);
   fChain->SetBranchAddress("D0_L0Global_Dec", &D0_L0Global_Dec, &b_D0_L0Global_Dec);
   fChain->SetBranchAddress("D0_L0Global_TIS", &D0_L0Global_TIS, &b_D0_L0Global_TIS);
   fChain->SetBranchAddress("D0_L0Global_TOS", &D0_L0Global_TOS, &b_D0_L0Global_TOS);
   fChain->SetBranchAddress("D0_Hlt1Global_Dec", &D0_Hlt1Global_Dec, &b_D0_Hlt1Global_Dec);
   fChain->SetBranchAddress("D0_Hlt1Global_TIS", &D0_Hlt1Global_TIS, &b_D0_Hlt1Global_TIS);
   fChain->SetBranchAddress("D0_Hlt1Global_TOS", &D0_Hlt1Global_TOS, &b_D0_Hlt1Global_TOS);
   fChain->SetBranchAddress("D0_Hlt1Phys_Dec", &D0_Hlt1Phys_Dec, &b_D0_Hlt1Phys_Dec);
   fChain->SetBranchAddress("D0_Hlt1Phys_TIS", &D0_Hlt1Phys_TIS, &b_D0_Hlt1Phys_TIS);
   fChain->SetBranchAddress("D0_Hlt1Phys_TOS", &D0_Hlt1Phys_TOS, &b_D0_Hlt1Phys_TOS);
   fChain->SetBranchAddress("D0_Hlt2Global_Dec", &D0_Hlt2Global_Dec, &b_D0_Hlt2Global_Dec);
   fChain->SetBranchAddress("D0_Hlt2Global_TIS", &D0_Hlt2Global_TIS, &b_D0_Hlt2Global_TIS);
   fChain->SetBranchAddress("D0_Hlt2Global_TOS", &D0_Hlt2Global_TOS, &b_D0_Hlt2Global_TOS);
   fChain->SetBranchAddress("D0_Hlt2Phys_Dec", &D0_Hlt2Phys_Dec, &b_D0_Hlt2Phys_Dec);
   fChain->SetBranchAddress("D0_Hlt2Phys_TIS", &D0_Hlt2Phys_TIS, &b_D0_Hlt2Phys_TIS);
   fChain->SetBranchAddress("D0_Hlt2Phys_TOS", &D0_Hlt2Phys_TOS, &b_D0_Hlt2Phys_TOS);
   fChain->SetBranchAddress("D0_NumVtxWithinChi2WindowOneTrack", &D0_NumVtxWithinChi2WindowOneTrack, &b_D0_NumVtxWithinChi2WindowOneTrack);
   fChain->SetBranchAddress("D0_SmallestDeltaChi2OneTrack", &D0_SmallestDeltaChi2OneTrack, &b_D0_SmallestDeltaChi2OneTrack);
   fChain->SetBranchAddress("D0_SmallestDeltaChi2MassOneTrack", &D0_SmallestDeltaChi2MassOneTrack, &b_D0_SmallestDeltaChi2MassOneTrack);
   fChain->SetBranchAddress("D0_SmallestDeltaChi2TwoTracks", &D0_SmallestDeltaChi2TwoTracks, &b_D0_SmallestDeltaChi2TwoTracks);
   fChain->SetBranchAddress("D0_SmallestDeltaChi2MassTwoTracks", &D0_SmallestDeltaChi2MassTwoTracks, &b_D0_SmallestDeltaChi2MassTwoTracks);
   fChain->SetBranchAddress("Dfake_CosTheta", &Dfake_CosTheta, &b_Dfake_CosTheta);
   fChain->SetBranchAddress("Dfake_ENDVERTEX_X", &Dfake_ENDVERTEX_X, &b_Dfake_ENDVERTEX_X);
   fChain->SetBranchAddress("Dfake_ENDVERTEX_Y", &Dfake_ENDVERTEX_Y, &b_Dfake_ENDVERTEX_Y);
   fChain->SetBranchAddress("Dfake_ENDVERTEX_Z", &Dfake_ENDVERTEX_Z, &b_Dfake_ENDVERTEX_Z);
   fChain->SetBranchAddress("Dfake_ENDVERTEX_XERR", &Dfake_ENDVERTEX_XERR, &b_Dfake_ENDVERTEX_XERR);
   fChain->SetBranchAddress("Dfake_ENDVERTEX_YERR", &Dfake_ENDVERTEX_YERR, &b_Dfake_ENDVERTEX_YERR);
   fChain->SetBranchAddress("Dfake_ENDVERTEX_ZERR", &Dfake_ENDVERTEX_ZERR, &b_Dfake_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("Dfake_ENDVERTEX_CHI2", &Dfake_ENDVERTEX_CHI2, &b_Dfake_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("Dfake_ENDVERTEX_NDOF", &Dfake_ENDVERTEX_NDOF, &b_Dfake_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("Dfake_ENDVERTEX_COV_", Dfake_ENDVERTEX_COV_, &b_Dfake_ENDVERTEX_COV_);
   fChain->SetBranchAddress("Dfake_OWNPV_X", &Dfake_OWNPV_X, &b_Dfake_OWNPV_X);
   fChain->SetBranchAddress("Dfake_OWNPV_Y", &Dfake_OWNPV_Y, &b_Dfake_OWNPV_Y);
   fChain->SetBranchAddress("Dfake_OWNPV_Z", &Dfake_OWNPV_Z, &b_Dfake_OWNPV_Z);
   fChain->SetBranchAddress("Dfake_OWNPV_XERR", &Dfake_OWNPV_XERR, &b_Dfake_OWNPV_XERR);
   fChain->SetBranchAddress("Dfake_OWNPV_YERR", &Dfake_OWNPV_YERR, &b_Dfake_OWNPV_YERR);
   fChain->SetBranchAddress("Dfake_OWNPV_ZERR", &Dfake_OWNPV_ZERR, &b_Dfake_OWNPV_ZERR);
   fChain->SetBranchAddress("Dfake_OWNPV_CHI2", &Dfake_OWNPV_CHI2, &b_Dfake_OWNPV_CHI2);
   fChain->SetBranchAddress("Dfake_OWNPV_NDOF", &Dfake_OWNPV_NDOF, &b_Dfake_OWNPV_NDOF);
   fChain->SetBranchAddress("Dfake_OWNPV_COV_", Dfake_OWNPV_COV_, &b_Dfake_OWNPV_COV_);
   fChain->SetBranchAddress("Dfake_IP_OWNPV", &Dfake_IP_OWNPV, &b_Dfake_IP_OWNPV);
   fChain->SetBranchAddress("Dfake_IPCHI2_OWNPV", &Dfake_IPCHI2_OWNPV, &b_Dfake_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Dfake_FD_OWNPV", &Dfake_FD_OWNPV, &b_Dfake_FD_OWNPV);
   fChain->SetBranchAddress("Dfake_FDCHI2_OWNPV", &Dfake_FDCHI2_OWNPV, &b_Dfake_FDCHI2_OWNPV);
   fChain->SetBranchAddress("Dfake_DIRA_OWNPV", &Dfake_DIRA_OWNPV, &b_Dfake_DIRA_OWNPV);
   fChain->SetBranchAddress("Dfake_ORIVX_X", &Dfake_ORIVX_X, &b_Dfake_ORIVX_X);
   fChain->SetBranchAddress("Dfake_ORIVX_Y", &Dfake_ORIVX_Y, &b_Dfake_ORIVX_Y);
   fChain->SetBranchAddress("Dfake_ORIVX_Z", &Dfake_ORIVX_Z, &b_Dfake_ORIVX_Z);
   fChain->SetBranchAddress("Dfake_ORIVX_XERR", &Dfake_ORIVX_XERR, &b_Dfake_ORIVX_XERR);
   fChain->SetBranchAddress("Dfake_ORIVX_YERR", &Dfake_ORIVX_YERR, &b_Dfake_ORIVX_YERR);
   fChain->SetBranchAddress("Dfake_ORIVX_ZERR", &Dfake_ORIVX_ZERR, &b_Dfake_ORIVX_ZERR);
   fChain->SetBranchAddress("Dfake_ORIVX_CHI2", &Dfake_ORIVX_CHI2, &b_Dfake_ORIVX_CHI2);
   fChain->SetBranchAddress("Dfake_ORIVX_NDOF", &Dfake_ORIVX_NDOF, &b_Dfake_ORIVX_NDOF);
   fChain->SetBranchAddress("Dfake_ORIVX_COV_", Dfake_ORIVX_COV_, &b_Dfake_ORIVX_COV_);
   fChain->SetBranchAddress("Dfake_FD_ORIVX", &Dfake_FD_ORIVX, &b_Dfake_FD_ORIVX);
   fChain->SetBranchAddress("Dfake_FDCHI2_ORIVX", &Dfake_FDCHI2_ORIVX, &b_Dfake_FDCHI2_ORIVX);
   fChain->SetBranchAddress("Dfake_DIRA_ORIVX", &Dfake_DIRA_ORIVX, &b_Dfake_DIRA_ORIVX);
   fChain->SetBranchAddress("Dfake_P", &Dfake_P, &b_Dfake_P);
   fChain->SetBranchAddress("Dfake_PT", &Dfake_PT, &b_Dfake_PT);
   fChain->SetBranchAddress("Dfake_PE", &Dfake_PE, &b_Dfake_PE);
   fChain->SetBranchAddress("Dfake_PX", &Dfake_PX, &b_Dfake_PX);
   fChain->SetBranchAddress("Dfake_PY", &Dfake_PY, &b_Dfake_PY);
   fChain->SetBranchAddress("Dfake_PZ", &Dfake_PZ, &b_Dfake_PZ);
   fChain->SetBranchAddress("Dfake_MM", &Dfake_MM, &b_Dfake_MM);
   fChain->SetBranchAddress("Dfake_MMERR", &Dfake_MMERR, &b_Dfake_MMERR);
   fChain->SetBranchAddress("Dfake_M", &Dfake_M, &b_Dfake_M);
   fChain->SetBranchAddress("Dfake_ID", &Dfake_ID, &b_Dfake_ID);
   fChain->SetBranchAddress("Dfake_TAU", &Dfake_TAU, &b_Dfake_TAU);
   fChain->SetBranchAddress("Dfake_TAUERR", &Dfake_TAUERR, &b_Dfake_TAUERR);
   fChain->SetBranchAddress("Dfake_TAUCHI2", &Dfake_TAUCHI2, &b_Dfake_TAUCHI2);
   fChain->SetBranchAddress("Dfake_L0Global_Dec", &Dfake_L0Global_Dec, &b_Dfake_L0Global_Dec);
   fChain->SetBranchAddress("Dfake_L0Global_TIS", &Dfake_L0Global_TIS, &b_Dfake_L0Global_TIS);
   fChain->SetBranchAddress("Dfake_L0Global_TOS", &Dfake_L0Global_TOS, &b_Dfake_L0Global_TOS);
   fChain->SetBranchAddress("Dfake_Hlt1Global_Dec", &Dfake_Hlt1Global_Dec, &b_Dfake_Hlt1Global_Dec);
   fChain->SetBranchAddress("Dfake_Hlt1Global_TIS", &Dfake_Hlt1Global_TIS, &b_Dfake_Hlt1Global_TIS);
   fChain->SetBranchAddress("Dfake_Hlt1Global_TOS", &Dfake_Hlt1Global_TOS, &b_Dfake_Hlt1Global_TOS);
   fChain->SetBranchAddress("Dfake_Hlt1Phys_Dec", &Dfake_Hlt1Phys_Dec, &b_Dfake_Hlt1Phys_Dec);
   fChain->SetBranchAddress("Dfake_Hlt1Phys_TIS", &Dfake_Hlt1Phys_TIS, &b_Dfake_Hlt1Phys_TIS);
   fChain->SetBranchAddress("Dfake_Hlt1Phys_TOS", &Dfake_Hlt1Phys_TOS, &b_Dfake_Hlt1Phys_TOS);
   fChain->SetBranchAddress("Dfake_Hlt2Global_Dec", &Dfake_Hlt2Global_Dec, &b_Dfake_Hlt2Global_Dec);
   fChain->SetBranchAddress("Dfake_Hlt2Global_TIS", &Dfake_Hlt2Global_TIS, &b_Dfake_Hlt2Global_TIS);
   fChain->SetBranchAddress("Dfake_Hlt2Global_TOS", &Dfake_Hlt2Global_TOS, &b_Dfake_Hlt2Global_TOS);
   fChain->SetBranchAddress("Dfake_Hlt2Phys_Dec", &Dfake_Hlt2Phys_Dec, &b_Dfake_Hlt2Phys_Dec);
   fChain->SetBranchAddress("Dfake_Hlt2Phys_TIS", &Dfake_Hlt2Phys_TIS, &b_Dfake_Hlt2Phys_TIS);
   fChain->SetBranchAddress("Dfake_Hlt2Phys_TOS", &Dfake_Hlt2Phys_TOS, &b_Dfake_Hlt2Phys_TOS);
   fChain->SetBranchAddress("Dfake_NumVtxWithinChi2WindowOneTrack", &Dfake_NumVtxWithinChi2WindowOneTrack, &b_Dfake_NumVtxWithinChi2WindowOneTrack);
   fChain->SetBranchAddress("Dfake_SmallestDeltaChi2OneTrack", &Dfake_SmallestDeltaChi2OneTrack, &b_Dfake_SmallestDeltaChi2OneTrack);
   fChain->SetBranchAddress("Dfake_SmallestDeltaChi2MassOneTrack", &Dfake_SmallestDeltaChi2MassOneTrack, &b_Dfake_SmallestDeltaChi2MassOneTrack);
   fChain->SetBranchAddress("Dfake_SmallestDeltaChi2TwoTracks", &Dfake_SmallestDeltaChi2TwoTracks, &b_Dfake_SmallestDeltaChi2TwoTracks);
   fChain->SetBranchAddress("Dfake_SmallestDeltaChi2MassTwoTracks", &Dfake_SmallestDeltaChi2MassTwoTracks, &b_Dfake_SmallestDeltaChi2MassTwoTracks);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNe", &K_MC12TuneV2_ProbNNe, &b_K_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNmu", &K_MC12TuneV2_ProbNNmu, &b_K_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNpi", &K_MC12TuneV2_ProbNNpi, &b_K_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNk", &K_MC12TuneV2_ProbNNk, &b_K_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNp", &K_MC12TuneV2_ProbNNp, &b_K_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("K_MC12TuneV2_ProbNNghost", &K_MC12TuneV2_ProbNNghost, &b_K_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNe", &K_MC12TuneV3_ProbNNe, &b_K_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNmu", &K_MC12TuneV3_ProbNNmu, &b_K_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNpi", &K_MC12TuneV3_ProbNNpi, &b_K_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNk", &K_MC12TuneV3_ProbNNk, &b_K_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNp", &K_MC12TuneV3_ProbNNp, &b_K_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("K_MC12TuneV3_ProbNNghost", &K_MC12TuneV3_ProbNNghost, &b_K_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNe", &K_MC12TuneV4_ProbNNe, &b_K_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNmu", &K_MC12TuneV4_ProbNNmu, &b_K_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNpi", &K_MC12TuneV4_ProbNNpi, &b_K_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNk", &K_MC12TuneV4_ProbNNk, &b_K_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNp", &K_MC12TuneV4_ProbNNp, &b_K_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("K_MC12TuneV4_ProbNNghost", &K_MC12TuneV4_ProbNNghost, &b_K_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNe", &K_MC15TuneV1_ProbNNe, &b_K_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNmu", &K_MC15TuneV1_ProbNNmu, &b_K_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNpi", &K_MC15TuneV1_ProbNNpi, &b_K_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNk", &K_MC15TuneV1_ProbNNk, &b_K_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNp", &K_MC15TuneV1_ProbNNp, &b_K_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("K_MC15TuneV1_ProbNNghost", &K_MC15TuneV1_ProbNNghost, &b_K_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("K_CosTheta", &K_CosTheta, &b_K_CosTheta);
   fChain->SetBranchAddress("K_OWNPV_X", &K_OWNPV_X, &b_K_OWNPV_X);
   fChain->SetBranchAddress("K_OWNPV_Y", &K_OWNPV_Y, &b_K_OWNPV_Y);
   fChain->SetBranchAddress("K_OWNPV_Z", &K_OWNPV_Z, &b_K_OWNPV_Z);
   fChain->SetBranchAddress("K_OWNPV_XERR", &K_OWNPV_XERR, &b_K_OWNPV_XERR);
   fChain->SetBranchAddress("K_OWNPV_YERR", &K_OWNPV_YERR, &b_K_OWNPV_YERR);
   fChain->SetBranchAddress("K_OWNPV_ZERR", &K_OWNPV_ZERR, &b_K_OWNPV_ZERR);
   fChain->SetBranchAddress("K_OWNPV_CHI2", &K_OWNPV_CHI2, &b_K_OWNPV_CHI2);
   fChain->SetBranchAddress("K_OWNPV_NDOF", &K_OWNPV_NDOF, &b_K_OWNPV_NDOF);
   fChain->SetBranchAddress("K_OWNPV_COV_", K_OWNPV_COV_, &b_K_OWNPV_COV_);
   fChain->SetBranchAddress("K_IP_OWNPV", &K_IP_OWNPV, &b_K_IP_OWNPV);
   fChain->SetBranchAddress("K_IPCHI2_OWNPV", &K_IPCHI2_OWNPV, &b_K_IPCHI2_OWNPV);
   fChain->SetBranchAddress("K_ORIVX_X", &K_ORIVX_X, &b_K_ORIVX_X);
   fChain->SetBranchAddress("K_ORIVX_Y", &K_ORIVX_Y, &b_K_ORIVX_Y);
   fChain->SetBranchAddress("K_ORIVX_Z", &K_ORIVX_Z, &b_K_ORIVX_Z);
   fChain->SetBranchAddress("K_ORIVX_XERR", &K_ORIVX_XERR, &b_K_ORIVX_XERR);
   fChain->SetBranchAddress("K_ORIVX_YERR", &K_ORIVX_YERR, &b_K_ORIVX_YERR);
   fChain->SetBranchAddress("K_ORIVX_ZERR", &K_ORIVX_ZERR, &b_K_ORIVX_ZERR);
   fChain->SetBranchAddress("K_ORIVX_CHI2", &K_ORIVX_CHI2, &b_K_ORIVX_CHI2);
   fChain->SetBranchAddress("K_ORIVX_NDOF", &K_ORIVX_NDOF, &b_K_ORIVX_NDOF);
   fChain->SetBranchAddress("K_ORIVX_COV_", K_ORIVX_COV_, &b_K_ORIVX_COV_);
   fChain->SetBranchAddress("K_P", &K_P, &b_K_P);
   fChain->SetBranchAddress("K_PT", &K_PT, &b_K_PT);
   fChain->SetBranchAddress("K_PE", &K_PE, &b_K_PE);
   fChain->SetBranchAddress("K_PX", &K_PX, &b_K_PX);
   fChain->SetBranchAddress("K_PY", &K_PY, &b_K_PY);
   fChain->SetBranchAddress("K_PZ", &K_PZ, &b_K_PZ);
   fChain->SetBranchAddress("K_M", &K_M, &b_K_M);
   fChain->SetBranchAddress("K_ID", &K_ID, &b_K_ID);
   fChain->SetBranchAddress("K_PIDe", &K_PIDe, &b_K_PIDe);
   fChain->SetBranchAddress("K_PIDmu", &K_PIDmu, &b_K_PIDmu);
   fChain->SetBranchAddress("K_PIDK", &K_PIDK, &b_K_PIDK);
   fChain->SetBranchAddress("K_PIDp", &K_PIDp, &b_K_PIDp);
   fChain->SetBranchAddress("K_PIDd", &K_PIDd, &b_K_PIDd);
   fChain->SetBranchAddress("K_ProbNNe", &K_ProbNNe, &b_K_ProbNNe);
   fChain->SetBranchAddress("K_ProbNNk", &K_ProbNNk, &b_K_ProbNNk);
   fChain->SetBranchAddress("K_ProbNNp", &K_ProbNNp, &b_K_ProbNNp);
   fChain->SetBranchAddress("K_ProbNNpi", &K_ProbNNpi, &b_K_ProbNNpi);
   fChain->SetBranchAddress("K_ProbNNmu", &K_ProbNNmu, &b_K_ProbNNmu);
   fChain->SetBranchAddress("K_ProbNNd", &K_ProbNNd, &b_K_ProbNNd);
   fChain->SetBranchAddress("K_ProbNNghost", &K_ProbNNghost, &b_K_ProbNNghost);
   fChain->SetBranchAddress("K_hasMuon", &K_hasMuon, &b_K_hasMuon);
   fChain->SetBranchAddress("K_isMuon", &K_isMuon, &b_K_isMuon);
   fChain->SetBranchAddress("K_hasRich", &K_hasRich, &b_K_hasRich);
   fChain->SetBranchAddress("K_UsedRichAerogel", &K_UsedRichAerogel, &b_K_UsedRichAerogel);
   fChain->SetBranchAddress("K_UsedRich1Gas", &K_UsedRich1Gas, &b_K_UsedRich1Gas);
   fChain->SetBranchAddress("K_UsedRich2Gas", &K_UsedRich2Gas, &b_K_UsedRich2Gas);
   fChain->SetBranchAddress("K_RichAboveElThres", &K_RichAboveElThres, &b_K_RichAboveElThres);
   fChain->SetBranchAddress("K_RichAboveMuThres", &K_RichAboveMuThres, &b_K_RichAboveMuThres);
   fChain->SetBranchAddress("K_RichAbovePiThres", &K_RichAbovePiThres, &b_K_RichAbovePiThres);
   fChain->SetBranchAddress("K_RichAboveKaThres", &K_RichAboveKaThres, &b_K_RichAboveKaThres);
   fChain->SetBranchAddress("K_RichAbovePrThres", &K_RichAbovePrThres, &b_K_RichAbovePrThres);
   fChain->SetBranchAddress("K_hasCalo", &K_hasCalo, &b_K_hasCalo);
   fChain->SetBranchAddress("K_L0Global_Dec", &K_L0Global_Dec, &b_K_L0Global_Dec);
   fChain->SetBranchAddress("K_L0Global_TIS", &K_L0Global_TIS, &b_K_L0Global_TIS);
   fChain->SetBranchAddress("K_L0Global_TOS", &K_L0Global_TOS, &b_K_L0Global_TOS);
   fChain->SetBranchAddress("K_Hlt1Global_Dec", &K_Hlt1Global_Dec, &b_K_Hlt1Global_Dec);
   fChain->SetBranchAddress("K_Hlt1Global_TIS", &K_Hlt1Global_TIS, &b_K_Hlt1Global_TIS);
   fChain->SetBranchAddress("K_Hlt1Global_TOS", &K_Hlt1Global_TOS, &b_K_Hlt1Global_TOS);
   fChain->SetBranchAddress("K_Hlt1Phys_Dec", &K_Hlt1Phys_Dec, &b_K_Hlt1Phys_Dec);
   fChain->SetBranchAddress("K_Hlt1Phys_TIS", &K_Hlt1Phys_TIS, &b_K_Hlt1Phys_TIS);
   fChain->SetBranchAddress("K_Hlt1Phys_TOS", &K_Hlt1Phys_TOS, &b_K_Hlt1Phys_TOS);
   fChain->SetBranchAddress("K_Hlt2Global_Dec", &K_Hlt2Global_Dec, &b_K_Hlt2Global_Dec);
   fChain->SetBranchAddress("K_Hlt2Global_TIS", &K_Hlt2Global_TIS, &b_K_Hlt2Global_TIS);
   fChain->SetBranchAddress("K_Hlt2Global_TOS", &K_Hlt2Global_TOS, &b_K_Hlt2Global_TOS);
   fChain->SetBranchAddress("K_Hlt2Phys_Dec", &K_Hlt2Phys_Dec, &b_K_Hlt2Phys_Dec);
   fChain->SetBranchAddress("K_Hlt2Phys_TIS", &K_Hlt2Phys_TIS, &b_K_Hlt2Phys_TIS);
   fChain->SetBranchAddress("K_Hlt2Phys_TOS", &K_Hlt2Phys_TOS, &b_K_Hlt2Phys_TOS);
   fChain->SetBranchAddress("K_TRACK_Type", &K_TRACK_Type, &b_K_TRACK_Type);
   fChain->SetBranchAddress("K_TRACK_Key", &K_TRACK_Key, &b_K_TRACK_Key);
   fChain->SetBranchAddress("K_TRACK_CHI2NDOF", &K_TRACK_CHI2NDOF, &b_K_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("K_TRACK_PCHI2", &K_TRACK_PCHI2, &b_K_TRACK_PCHI2);
   fChain->SetBranchAddress("K_TRACK_MatchCHI2", &K_TRACK_MatchCHI2, &b_K_TRACK_MatchCHI2);
   fChain->SetBranchAddress("K_TRACK_GhostProb", &K_TRACK_GhostProb, &b_K_TRACK_GhostProb);
   fChain->SetBranchAddress("K_TRACK_CloneDist", &K_TRACK_CloneDist, &b_K_TRACK_CloneDist);
   fChain->SetBranchAddress("K_TRACK_Likelihood", &K_TRACK_Likelihood, &b_K_TRACK_Likelihood);
   fChain->SetBranchAddress("pi1_MC12TuneV2_ProbNNe", &pi1_MC12TuneV2_ProbNNe, &b_pi1_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("pi1_MC12TuneV2_ProbNNmu", &pi1_MC12TuneV2_ProbNNmu, &b_pi1_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("pi1_MC12TuneV2_ProbNNpi", &pi1_MC12TuneV2_ProbNNpi, &b_pi1_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("pi1_MC12TuneV2_ProbNNk", &pi1_MC12TuneV2_ProbNNk, &b_pi1_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("pi1_MC12TuneV2_ProbNNp", &pi1_MC12TuneV2_ProbNNp, &b_pi1_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("pi1_MC12TuneV2_ProbNNghost", &pi1_MC12TuneV2_ProbNNghost, &b_pi1_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("pi1_MC12TuneV3_ProbNNe", &pi1_MC12TuneV3_ProbNNe, &b_pi1_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("pi1_MC12TuneV3_ProbNNmu", &pi1_MC12TuneV3_ProbNNmu, &b_pi1_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("pi1_MC12TuneV3_ProbNNpi", &pi1_MC12TuneV3_ProbNNpi, &b_pi1_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("pi1_MC12TuneV3_ProbNNk", &pi1_MC12TuneV3_ProbNNk, &b_pi1_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("pi1_MC12TuneV3_ProbNNp", &pi1_MC12TuneV3_ProbNNp, &b_pi1_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("pi1_MC12TuneV3_ProbNNghost", &pi1_MC12TuneV3_ProbNNghost, &b_pi1_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("pi1_MC12TuneV4_ProbNNe", &pi1_MC12TuneV4_ProbNNe, &b_pi1_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("pi1_MC12TuneV4_ProbNNmu", &pi1_MC12TuneV4_ProbNNmu, &b_pi1_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("pi1_MC12TuneV4_ProbNNpi", &pi1_MC12TuneV4_ProbNNpi, &b_pi1_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("pi1_MC12TuneV4_ProbNNk", &pi1_MC12TuneV4_ProbNNk, &b_pi1_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("pi1_MC12TuneV4_ProbNNp", &pi1_MC12TuneV4_ProbNNp, &b_pi1_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("pi1_MC12TuneV4_ProbNNghost", &pi1_MC12TuneV4_ProbNNghost, &b_pi1_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("pi1_MC15TuneV1_ProbNNe", &pi1_MC15TuneV1_ProbNNe, &b_pi1_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("pi1_MC15TuneV1_ProbNNmu", &pi1_MC15TuneV1_ProbNNmu, &b_pi1_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("pi1_MC15TuneV1_ProbNNpi", &pi1_MC15TuneV1_ProbNNpi, &b_pi1_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("pi1_MC15TuneV1_ProbNNk", &pi1_MC15TuneV1_ProbNNk, &b_pi1_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("pi1_MC15TuneV1_ProbNNp", &pi1_MC15TuneV1_ProbNNp, &b_pi1_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("pi1_MC15TuneV1_ProbNNghost", &pi1_MC15TuneV1_ProbNNghost, &b_pi1_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("pi1_CosTheta", &pi1_CosTheta, &b_pi1_CosTheta);
   fChain->SetBranchAddress("pi1_OWNPV_X", &pi1_OWNPV_X, &b_pi1_OWNPV_X);
   fChain->SetBranchAddress("pi1_OWNPV_Y", &pi1_OWNPV_Y, &b_pi1_OWNPV_Y);
   fChain->SetBranchAddress("pi1_OWNPV_Z", &pi1_OWNPV_Z, &b_pi1_OWNPV_Z);
   fChain->SetBranchAddress("pi1_OWNPV_XERR", &pi1_OWNPV_XERR, &b_pi1_OWNPV_XERR);
   fChain->SetBranchAddress("pi1_OWNPV_YERR", &pi1_OWNPV_YERR, &b_pi1_OWNPV_YERR);
   fChain->SetBranchAddress("pi1_OWNPV_ZERR", &pi1_OWNPV_ZERR, &b_pi1_OWNPV_ZERR);
   fChain->SetBranchAddress("pi1_OWNPV_CHI2", &pi1_OWNPV_CHI2, &b_pi1_OWNPV_CHI2);
   fChain->SetBranchAddress("pi1_OWNPV_NDOF", &pi1_OWNPV_NDOF, &b_pi1_OWNPV_NDOF);
   fChain->SetBranchAddress("pi1_OWNPV_COV_", pi1_OWNPV_COV_, &b_pi1_OWNPV_COV_);
   fChain->SetBranchAddress("pi1_IP_OWNPV", &pi1_IP_OWNPV, &b_pi1_IP_OWNPV);
   fChain->SetBranchAddress("pi1_IPCHI2_OWNPV", &pi1_IPCHI2_OWNPV, &b_pi1_IPCHI2_OWNPV);
   fChain->SetBranchAddress("pi1_ORIVX_X", &pi1_ORIVX_X, &b_pi1_ORIVX_X);
   fChain->SetBranchAddress("pi1_ORIVX_Y", &pi1_ORIVX_Y, &b_pi1_ORIVX_Y);
   fChain->SetBranchAddress("pi1_ORIVX_Z", &pi1_ORIVX_Z, &b_pi1_ORIVX_Z);
   fChain->SetBranchAddress("pi1_ORIVX_XERR", &pi1_ORIVX_XERR, &b_pi1_ORIVX_XERR);
   fChain->SetBranchAddress("pi1_ORIVX_YERR", &pi1_ORIVX_YERR, &b_pi1_ORIVX_YERR);
   fChain->SetBranchAddress("pi1_ORIVX_ZERR", &pi1_ORIVX_ZERR, &b_pi1_ORIVX_ZERR);
   fChain->SetBranchAddress("pi1_ORIVX_CHI2", &pi1_ORIVX_CHI2, &b_pi1_ORIVX_CHI2);
   fChain->SetBranchAddress("pi1_ORIVX_NDOF", &pi1_ORIVX_NDOF, &b_pi1_ORIVX_NDOF);
   fChain->SetBranchAddress("pi1_ORIVX_COV_", pi1_ORIVX_COV_, &b_pi1_ORIVX_COV_);
   fChain->SetBranchAddress("pi1_P", &pi1_P, &b_pi1_P);
   fChain->SetBranchAddress("pi1_PT", &pi1_PT, &b_pi1_PT);
   fChain->SetBranchAddress("pi1_PE", &pi1_PE, &b_pi1_PE);
   fChain->SetBranchAddress("pi1_PX", &pi1_PX, &b_pi1_PX);
   fChain->SetBranchAddress("pi1_PY", &pi1_PY, &b_pi1_PY);
   fChain->SetBranchAddress("pi1_PZ", &pi1_PZ, &b_pi1_PZ);
   fChain->SetBranchAddress("pi1_M", &pi1_M, &b_pi1_M);
   fChain->SetBranchAddress("pi1_ID", &pi1_ID, &b_pi1_ID);
   fChain->SetBranchAddress("pi1_PIDe", &pi1_PIDe, &b_pi1_PIDe);
   fChain->SetBranchAddress("pi1_PIDmu", &pi1_PIDmu, &b_pi1_PIDmu);
   fChain->SetBranchAddress("pi1_PIDK", &pi1_PIDK, &b_pi1_PIDK);
   fChain->SetBranchAddress("pi1_PIDp", &pi1_PIDp, &b_pi1_PIDp);
   fChain->SetBranchAddress("pi1_PIDd", &pi1_PIDd, &b_pi1_PIDd);
   fChain->SetBranchAddress("pi1_ProbNNe", &pi1_ProbNNe, &b_pi1_ProbNNe);
   fChain->SetBranchAddress("pi1_ProbNNk", &pi1_ProbNNk, &b_pi1_ProbNNk);
   fChain->SetBranchAddress("pi1_ProbNNp", &pi1_ProbNNp, &b_pi1_ProbNNp);
   fChain->SetBranchAddress("pi1_ProbNNpi", &pi1_ProbNNpi, &b_pi1_ProbNNpi);
   fChain->SetBranchAddress("pi1_ProbNNmu", &pi1_ProbNNmu, &b_pi1_ProbNNmu);
   fChain->SetBranchAddress("pi1_ProbNNd", &pi1_ProbNNd, &b_pi1_ProbNNd);
   fChain->SetBranchAddress("pi1_ProbNNghost", &pi1_ProbNNghost, &b_pi1_ProbNNghost);
   fChain->SetBranchAddress("pi1_hasMuon", &pi1_hasMuon, &b_pi1_hasMuon);
   fChain->SetBranchAddress("pi1_isMuon", &pi1_isMuon, &b_pi1_isMuon);
   fChain->SetBranchAddress("pi1_hasRich", &pi1_hasRich, &b_pi1_hasRich);
   fChain->SetBranchAddress("pi1_UsedRichAerogel", &pi1_UsedRichAerogel, &b_pi1_UsedRichAerogel);
   fChain->SetBranchAddress("pi1_UsedRich1Gas", &pi1_UsedRich1Gas, &b_pi1_UsedRich1Gas);
   fChain->SetBranchAddress("pi1_UsedRich2Gas", &pi1_UsedRich2Gas, &b_pi1_UsedRich2Gas);
   fChain->SetBranchAddress("pi1_RichAboveElThres", &pi1_RichAboveElThres, &b_pi1_RichAboveElThres);
   fChain->SetBranchAddress("pi1_RichAboveMuThres", &pi1_RichAboveMuThres, &b_pi1_RichAboveMuThres);
   fChain->SetBranchAddress("pi1_RichAbovePiThres", &pi1_RichAbovePiThres, &b_pi1_RichAbovePiThres);
   fChain->SetBranchAddress("pi1_RichAboveKaThres", &pi1_RichAboveKaThres, &b_pi1_RichAboveKaThres);
   fChain->SetBranchAddress("pi1_RichAbovePrThres", &pi1_RichAbovePrThres, &b_pi1_RichAbovePrThres);
   fChain->SetBranchAddress("pi1_hasCalo", &pi1_hasCalo, &b_pi1_hasCalo);
   fChain->SetBranchAddress("pi1_L0Global_Dec", &pi1_L0Global_Dec, &b_pi1_L0Global_Dec);
   fChain->SetBranchAddress("pi1_L0Global_TIS", &pi1_L0Global_TIS, &b_pi1_L0Global_TIS);
   fChain->SetBranchAddress("pi1_L0Global_TOS", &pi1_L0Global_TOS, &b_pi1_L0Global_TOS);
   fChain->SetBranchAddress("pi1_Hlt1Global_Dec", &pi1_Hlt1Global_Dec, &b_pi1_Hlt1Global_Dec);
   fChain->SetBranchAddress("pi1_Hlt1Global_TIS", &pi1_Hlt1Global_TIS, &b_pi1_Hlt1Global_TIS);
   fChain->SetBranchAddress("pi1_Hlt1Global_TOS", &pi1_Hlt1Global_TOS, &b_pi1_Hlt1Global_TOS);
   fChain->SetBranchAddress("pi1_Hlt1Phys_Dec", &pi1_Hlt1Phys_Dec, &b_pi1_Hlt1Phys_Dec);
   fChain->SetBranchAddress("pi1_Hlt1Phys_TIS", &pi1_Hlt1Phys_TIS, &b_pi1_Hlt1Phys_TIS);
   fChain->SetBranchAddress("pi1_Hlt1Phys_TOS", &pi1_Hlt1Phys_TOS, &b_pi1_Hlt1Phys_TOS);
   fChain->SetBranchAddress("pi1_Hlt2Global_Dec", &pi1_Hlt2Global_Dec, &b_pi1_Hlt2Global_Dec);
   fChain->SetBranchAddress("pi1_Hlt2Global_TIS", &pi1_Hlt2Global_TIS, &b_pi1_Hlt2Global_TIS);
   fChain->SetBranchAddress("pi1_Hlt2Global_TOS", &pi1_Hlt2Global_TOS, &b_pi1_Hlt2Global_TOS);
   fChain->SetBranchAddress("pi1_Hlt2Phys_Dec", &pi1_Hlt2Phys_Dec, &b_pi1_Hlt2Phys_Dec);
   fChain->SetBranchAddress("pi1_Hlt2Phys_TIS", &pi1_Hlt2Phys_TIS, &b_pi1_Hlt2Phys_TIS);
   fChain->SetBranchAddress("pi1_Hlt2Phys_TOS", &pi1_Hlt2Phys_TOS, &b_pi1_Hlt2Phys_TOS);
   fChain->SetBranchAddress("pi1_TRACK_Type", &pi1_TRACK_Type, &b_pi1_TRACK_Type);
   fChain->SetBranchAddress("pi1_TRACK_Key", &pi1_TRACK_Key, &b_pi1_TRACK_Key);
   fChain->SetBranchAddress("pi1_TRACK_CHI2NDOF", &pi1_TRACK_CHI2NDOF, &b_pi1_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("pi1_TRACK_PCHI2", &pi1_TRACK_PCHI2, &b_pi1_TRACK_PCHI2);
   fChain->SetBranchAddress("pi1_TRACK_MatchCHI2", &pi1_TRACK_MatchCHI2, &b_pi1_TRACK_MatchCHI2);
   fChain->SetBranchAddress("pi1_TRACK_GhostProb", &pi1_TRACK_GhostProb, &b_pi1_TRACK_GhostProb);
   fChain->SetBranchAddress("pi1_TRACK_CloneDist", &pi1_TRACK_CloneDist, &b_pi1_TRACK_CloneDist);
   fChain->SetBranchAddress("pi1_TRACK_Likelihood", &pi1_TRACK_Likelihood, &b_pi1_TRACK_Likelihood);
   fChain->SetBranchAddress("pi2_MC12TuneV2_ProbNNe", &pi2_MC12TuneV2_ProbNNe, &b_pi2_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("pi2_MC12TuneV2_ProbNNmu", &pi2_MC12TuneV2_ProbNNmu, &b_pi2_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("pi2_MC12TuneV2_ProbNNpi", &pi2_MC12TuneV2_ProbNNpi, &b_pi2_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("pi2_MC12TuneV2_ProbNNk", &pi2_MC12TuneV2_ProbNNk, &b_pi2_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("pi2_MC12TuneV2_ProbNNp", &pi2_MC12TuneV2_ProbNNp, &b_pi2_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("pi2_MC12TuneV2_ProbNNghost", &pi2_MC12TuneV2_ProbNNghost, &b_pi2_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("pi2_MC12TuneV3_ProbNNe", &pi2_MC12TuneV3_ProbNNe, &b_pi2_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("pi2_MC12TuneV3_ProbNNmu", &pi2_MC12TuneV3_ProbNNmu, &b_pi2_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("pi2_MC12TuneV3_ProbNNpi", &pi2_MC12TuneV3_ProbNNpi, &b_pi2_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("pi2_MC12TuneV3_ProbNNk", &pi2_MC12TuneV3_ProbNNk, &b_pi2_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("pi2_MC12TuneV3_ProbNNp", &pi2_MC12TuneV3_ProbNNp, &b_pi2_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("pi2_MC12TuneV3_ProbNNghost", &pi2_MC12TuneV3_ProbNNghost, &b_pi2_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("pi2_MC12TuneV4_ProbNNe", &pi2_MC12TuneV4_ProbNNe, &b_pi2_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("pi2_MC12TuneV4_ProbNNmu", &pi2_MC12TuneV4_ProbNNmu, &b_pi2_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("pi2_MC12TuneV4_ProbNNpi", &pi2_MC12TuneV4_ProbNNpi, &b_pi2_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("pi2_MC12TuneV4_ProbNNk", &pi2_MC12TuneV4_ProbNNk, &b_pi2_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("pi2_MC12TuneV4_ProbNNp", &pi2_MC12TuneV4_ProbNNp, &b_pi2_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("pi2_MC12TuneV4_ProbNNghost", &pi2_MC12TuneV4_ProbNNghost, &b_pi2_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("pi2_MC15TuneV1_ProbNNe", &pi2_MC15TuneV1_ProbNNe, &b_pi2_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("pi2_MC15TuneV1_ProbNNmu", &pi2_MC15TuneV1_ProbNNmu, &b_pi2_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("pi2_MC15TuneV1_ProbNNpi", &pi2_MC15TuneV1_ProbNNpi, &b_pi2_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("pi2_MC15TuneV1_ProbNNk", &pi2_MC15TuneV1_ProbNNk, &b_pi2_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("pi2_MC15TuneV1_ProbNNp", &pi2_MC15TuneV1_ProbNNp, &b_pi2_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("pi2_MC15TuneV1_ProbNNghost", &pi2_MC15TuneV1_ProbNNghost, &b_pi2_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("pi2_CosTheta", &pi2_CosTheta, &b_pi2_CosTheta);
   fChain->SetBranchAddress("pi2_OWNPV_X", &pi2_OWNPV_X, &b_pi2_OWNPV_X);
   fChain->SetBranchAddress("pi2_OWNPV_Y", &pi2_OWNPV_Y, &b_pi2_OWNPV_Y);
   fChain->SetBranchAddress("pi2_OWNPV_Z", &pi2_OWNPV_Z, &b_pi2_OWNPV_Z);
   fChain->SetBranchAddress("pi2_OWNPV_XERR", &pi2_OWNPV_XERR, &b_pi2_OWNPV_XERR);
   fChain->SetBranchAddress("pi2_OWNPV_YERR", &pi2_OWNPV_YERR, &b_pi2_OWNPV_YERR);
   fChain->SetBranchAddress("pi2_OWNPV_ZERR", &pi2_OWNPV_ZERR, &b_pi2_OWNPV_ZERR);
   fChain->SetBranchAddress("pi2_OWNPV_CHI2", &pi2_OWNPV_CHI2, &b_pi2_OWNPV_CHI2);
   fChain->SetBranchAddress("pi2_OWNPV_NDOF", &pi2_OWNPV_NDOF, &b_pi2_OWNPV_NDOF);
   fChain->SetBranchAddress("pi2_OWNPV_COV_", pi2_OWNPV_COV_, &b_pi2_OWNPV_COV_);
   fChain->SetBranchAddress("pi2_IP_OWNPV", &pi2_IP_OWNPV, &b_pi2_IP_OWNPV);
   fChain->SetBranchAddress("pi2_IPCHI2_OWNPV", &pi2_IPCHI2_OWNPV, &b_pi2_IPCHI2_OWNPV);
   fChain->SetBranchAddress("pi2_ORIVX_X", &pi2_ORIVX_X, &b_pi2_ORIVX_X);
   fChain->SetBranchAddress("pi2_ORIVX_Y", &pi2_ORIVX_Y, &b_pi2_ORIVX_Y);
   fChain->SetBranchAddress("pi2_ORIVX_Z", &pi2_ORIVX_Z, &b_pi2_ORIVX_Z);
   fChain->SetBranchAddress("pi2_ORIVX_XERR", &pi2_ORIVX_XERR, &b_pi2_ORIVX_XERR);
   fChain->SetBranchAddress("pi2_ORIVX_YERR", &pi2_ORIVX_YERR, &b_pi2_ORIVX_YERR);
   fChain->SetBranchAddress("pi2_ORIVX_ZERR", &pi2_ORIVX_ZERR, &b_pi2_ORIVX_ZERR);
   fChain->SetBranchAddress("pi2_ORIVX_CHI2", &pi2_ORIVX_CHI2, &b_pi2_ORIVX_CHI2);
   fChain->SetBranchAddress("pi2_ORIVX_NDOF", &pi2_ORIVX_NDOF, &b_pi2_ORIVX_NDOF);
   fChain->SetBranchAddress("pi2_ORIVX_COV_", pi2_ORIVX_COV_, &b_pi2_ORIVX_COV_);
   fChain->SetBranchAddress("pi2_P", &pi2_P, &b_pi2_P);
   fChain->SetBranchAddress("pi2_PT", &pi2_PT, &b_pi2_PT);
   fChain->SetBranchAddress("pi2_PE", &pi2_PE, &b_pi2_PE);
   fChain->SetBranchAddress("pi2_PX", &pi2_PX, &b_pi2_PX);
   fChain->SetBranchAddress("pi2_PY", &pi2_PY, &b_pi2_PY);
   fChain->SetBranchAddress("pi2_PZ", &pi2_PZ, &b_pi2_PZ);
   fChain->SetBranchAddress("pi2_M", &pi2_M, &b_pi2_M);
   fChain->SetBranchAddress("pi2_ID", &pi2_ID, &b_pi2_ID);
   fChain->SetBranchAddress("pi2_PIDe", &pi2_PIDe, &b_pi2_PIDe);
   fChain->SetBranchAddress("pi2_PIDmu", &pi2_PIDmu, &b_pi2_PIDmu);
   fChain->SetBranchAddress("pi2_PIDK", &pi2_PIDK, &b_pi2_PIDK);
   fChain->SetBranchAddress("pi2_PIDp", &pi2_PIDp, &b_pi2_PIDp);
   fChain->SetBranchAddress("pi2_PIDd", &pi2_PIDd, &b_pi2_PIDd);
   fChain->SetBranchAddress("pi2_ProbNNe", &pi2_ProbNNe, &b_pi2_ProbNNe);
   fChain->SetBranchAddress("pi2_ProbNNk", &pi2_ProbNNk, &b_pi2_ProbNNk);
   fChain->SetBranchAddress("pi2_ProbNNp", &pi2_ProbNNp, &b_pi2_ProbNNp);
   fChain->SetBranchAddress("pi2_ProbNNpi", &pi2_ProbNNpi, &b_pi2_ProbNNpi);
   fChain->SetBranchAddress("pi2_ProbNNmu", &pi2_ProbNNmu, &b_pi2_ProbNNmu);
   fChain->SetBranchAddress("pi2_ProbNNd", &pi2_ProbNNd, &b_pi2_ProbNNd);
   fChain->SetBranchAddress("pi2_ProbNNghost", &pi2_ProbNNghost, &b_pi2_ProbNNghost);
   fChain->SetBranchAddress("pi2_hasMuon", &pi2_hasMuon, &b_pi2_hasMuon);
   fChain->SetBranchAddress("pi2_isMuon", &pi2_isMuon, &b_pi2_isMuon);
   fChain->SetBranchAddress("pi2_hasRich", &pi2_hasRich, &b_pi2_hasRich);
   fChain->SetBranchAddress("pi2_UsedRichAerogel", &pi2_UsedRichAerogel, &b_pi2_UsedRichAerogel);
   fChain->SetBranchAddress("pi2_UsedRich1Gas", &pi2_UsedRich1Gas, &b_pi2_UsedRich1Gas);
   fChain->SetBranchAddress("pi2_UsedRich2Gas", &pi2_UsedRich2Gas, &b_pi2_UsedRich2Gas);
   fChain->SetBranchAddress("pi2_RichAboveElThres", &pi2_RichAboveElThres, &b_pi2_RichAboveElThres);
   fChain->SetBranchAddress("pi2_RichAboveMuThres", &pi2_RichAboveMuThres, &b_pi2_RichAboveMuThres);
   fChain->SetBranchAddress("pi2_RichAbovePiThres", &pi2_RichAbovePiThres, &b_pi2_RichAbovePiThres);
   fChain->SetBranchAddress("pi2_RichAboveKaThres", &pi2_RichAboveKaThres, &b_pi2_RichAboveKaThres);
   fChain->SetBranchAddress("pi2_RichAbovePrThres", &pi2_RichAbovePrThres, &b_pi2_RichAbovePrThres);
   fChain->SetBranchAddress("pi2_hasCalo", &pi2_hasCalo, &b_pi2_hasCalo);
   fChain->SetBranchAddress("pi2_L0Global_Dec", &pi2_L0Global_Dec, &b_pi2_L0Global_Dec);
   fChain->SetBranchAddress("pi2_L0Global_TIS", &pi2_L0Global_TIS, &b_pi2_L0Global_TIS);
   fChain->SetBranchAddress("pi2_L0Global_TOS", &pi2_L0Global_TOS, &b_pi2_L0Global_TOS);
   fChain->SetBranchAddress("pi2_Hlt1Global_Dec", &pi2_Hlt1Global_Dec, &b_pi2_Hlt1Global_Dec);
   fChain->SetBranchAddress("pi2_Hlt1Global_TIS", &pi2_Hlt1Global_TIS, &b_pi2_Hlt1Global_TIS);
   fChain->SetBranchAddress("pi2_Hlt1Global_TOS", &pi2_Hlt1Global_TOS, &b_pi2_Hlt1Global_TOS);
   fChain->SetBranchAddress("pi2_Hlt1Phys_Dec", &pi2_Hlt1Phys_Dec, &b_pi2_Hlt1Phys_Dec);
   fChain->SetBranchAddress("pi2_Hlt1Phys_TIS", &pi2_Hlt1Phys_TIS, &b_pi2_Hlt1Phys_TIS);
   fChain->SetBranchAddress("pi2_Hlt1Phys_TOS", &pi2_Hlt1Phys_TOS, &b_pi2_Hlt1Phys_TOS);
   fChain->SetBranchAddress("pi2_Hlt2Global_Dec", &pi2_Hlt2Global_Dec, &b_pi2_Hlt2Global_Dec);
   fChain->SetBranchAddress("pi2_Hlt2Global_TIS", &pi2_Hlt2Global_TIS, &b_pi2_Hlt2Global_TIS);
   fChain->SetBranchAddress("pi2_Hlt2Global_TOS", &pi2_Hlt2Global_TOS, &b_pi2_Hlt2Global_TOS);
   fChain->SetBranchAddress("pi2_Hlt2Phys_Dec", &pi2_Hlt2Phys_Dec, &b_pi2_Hlt2Phys_Dec);
   fChain->SetBranchAddress("pi2_Hlt2Phys_TIS", &pi2_Hlt2Phys_TIS, &b_pi2_Hlt2Phys_TIS);
   fChain->SetBranchAddress("pi2_Hlt2Phys_TOS", &pi2_Hlt2Phys_TOS, &b_pi2_Hlt2Phys_TOS);
   fChain->SetBranchAddress("pi2_TRACK_Type", &pi2_TRACK_Type, &b_pi2_TRACK_Type);
   fChain->SetBranchAddress("pi2_TRACK_Key", &pi2_TRACK_Key, &b_pi2_TRACK_Key);
   fChain->SetBranchAddress("pi2_TRACK_CHI2NDOF", &pi2_TRACK_CHI2NDOF, &b_pi2_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("pi2_TRACK_PCHI2", &pi2_TRACK_PCHI2, &b_pi2_TRACK_PCHI2);
   fChain->SetBranchAddress("pi2_TRACK_MatchCHI2", &pi2_TRACK_MatchCHI2, &b_pi2_TRACK_MatchCHI2);
   fChain->SetBranchAddress("pi2_TRACK_GhostProb", &pi2_TRACK_GhostProb, &b_pi2_TRACK_GhostProb);
   fChain->SetBranchAddress("pi2_TRACK_CloneDist", &pi2_TRACK_CloneDist, &b_pi2_TRACK_CloneDist);
   fChain->SetBranchAddress("pi2_TRACK_Likelihood", &pi2_TRACK_Likelihood, &b_pi2_TRACK_Likelihood);
   fChain->SetBranchAddress("PiMiss_MC12TuneV2_ProbNNe", &PiMiss_MC12TuneV2_ProbNNe, &b_PiMiss_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("PiMiss_MC12TuneV2_ProbNNmu", &PiMiss_MC12TuneV2_ProbNNmu, &b_PiMiss_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("PiMiss_MC12TuneV2_ProbNNpi", &PiMiss_MC12TuneV2_ProbNNpi, &b_PiMiss_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("PiMiss_MC12TuneV2_ProbNNk", &PiMiss_MC12TuneV2_ProbNNk, &b_PiMiss_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("PiMiss_MC12TuneV2_ProbNNp", &PiMiss_MC12TuneV2_ProbNNp, &b_PiMiss_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("PiMiss_MC12TuneV2_ProbNNghost", &PiMiss_MC12TuneV2_ProbNNghost, &b_PiMiss_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("PiMiss_MC12TuneV3_ProbNNe", &PiMiss_MC12TuneV3_ProbNNe, &b_PiMiss_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("PiMiss_MC12TuneV3_ProbNNmu", &PiMiss_MC12TuneV3_ProbNNmu, &b_PiMiss_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("PiMiss_MC12TuneV3_ProbNNpi", &PiMiss_MC12TuneV3_ProbNNpi, &b_PiMiss_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("PiMiss_MC12TuneV3_ProbNNk", &PiMiss_MC12TuneV3_ProbNNk, &b_PiMiss_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("PiMiss_MC12TuneV3_ProbNNp", &PiMiss_MC12TuneV3_ProbNNp, &b_PiMiss_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("PiMiss_MC12TuneV3_ProbNNghost", &PiMiss_MC12TuneV3_ProbNNghost, &b_PiMiss_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("PiMiss_MC12TuneV4_ProbNNe", &PiMiss_MC12TuneV4_ProbNNe, &b_PiMiss_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("PiMiss_MC12TuneV4_ProbNNmu", &PiMiss_MC12TuneV4_ProbNNmu, &b_PiMiss_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("PiMiss_MC12TuneV4_ProbNNpi", &PiMiss_MC12TuneV4_ProbNNpi, &b_PiMiss_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("PiMiss_MC12TuneV4_ProbNNk", &PiMiss_MC12TuneV4_ProbNNk, &b_PiMiss_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("PiMiss_MC12TuneV4_ProbNNp", &PiMiss_MC12TuneV4_ProbNNp, &b_PiMiss_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("PiMiss_MC12TuneV4_ProbNNghost", &PiMiss_MC12TuneV4_ProbNNghost, &b_PiMiss_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("PiMiss_MC15TuneV1_ProbNNe", &PiMiss_MC15TuneV1_ProbNNe, &b_PiMiss_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("PiMiss_MC15TuneV1_ProbNNmu", &PiMiss_MC15TuneV1_ProbNNmu, &b_PiMiss_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("PiMiss_MC15TuneV1_ProbNNpi", &PiMiss_MC15TuneV1_ProbNNpi, &b_PiMiss_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("PiMiss_MC15TuneV1_ProbNNk", &PiMiss_MC15TuneV1_ProbNNk, &b_PiMiss_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("PiMiss_MC15TuneV1_ProbNNp", &PiMiss_MC15TuneV1_ProbNNp, &b_PiMiss_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("PiMiss_MC15TuneV1_ProbNNghost", &PiMiss_MC15TuneV1_ProbNNghost, &b_PiMiss_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("PiMiss_CosTheta", &PiMiss_CosTheta, &b_PiMiss_CosTheta);
   fChain->SetBranchAddress("PiMiss_OWNPV_X", &PiMiss_OWNPV_X, &b_PiMiss_OWNPV_X);
   fChain->SetBranchAddress("PiMiss_OWNPV_Y", &PiMiss_OWNPV_Y, &b_PiMiss_OWNPV_Y);
   fChain->SetBranchAddress("PiMiss_OWNPV_Z", &PiMiss_OWNPV_Z, &b_PiMiss_OWNPV_Z);
   fChain->SetBranchAddress("PiMiss_OWNPV_XERR", &PiMiss_OWNPV_XERR, &b_PiMiss_OWNPV_XERR);
   fChain->SetBranchAddress("PiMiss_OWNPV_YERR", &PiMiss_OWNPV_YERR, &b_PiMiss_OWNPV_YERR);
   fChain->SetBranchAddress("PiMiss_OWNPV_ZERR", &PiMiss_OWNPV_ZERR, &b_PiMiss_OWNPV_ZERR);
   fChain->SetBranchAddress("PiMiss_OWNPV_CHI2", &PiMiss_OWNPV_CHI2, &b_PiMiss_OWNPV_CHI2);
   fChain->SetBranchAddress("PiMiss_OWNPV_NDOF", &PiMiss_OWNPV_NDOF, &b_PiMiss_OWNPV_NDOF);
   fChain->SetBranchAddress("PiMiss_OWNPV_COV_", PiMiss_OWNPV_COV_, &b_PiMiss_OWNPV_COV_);
   fChain->SetBranchAddress("PiMiss_IP_OWNPV", &PiMiss_IP_OWNPV, &b_PiMiss_IP_OWNPV);
   fChain->SetBranchAddress("PiMiss_IPCHI2_OWNPV", &PiMiss_IPCHI2_OWNPV, &b_PiMiss_IPCHI2_OWNPV);
   fChain->SetBranchAddress("PiMiss_ORIVX_X", &PiMiss_ORIVX_X, &b_PiMiss_ORIVX_X);
   fChain->SetBranchAddress("PiMiss_ORIVX_Y", &PiMiss_ORIVX_Y, &b_PiMiss_ORIVX_Y);
   fChain->SetBranchAddress("PiMiss_ORIVX_Z", &PiMiss_ORIVX_Z, &b_PiMiss_ORIVX_Z);
   fChain->SetBranchAddress("PiMiss_ORIVX_XERR", &PiMiss_ORIVX_XERR, &b_PiMiss_ORIVX_XERR);
   fChain->SetBranchAddress("PiMiss_ORIVX_YERR", &PiMiss_ORIVX_YERR, &b_PiMiss_ORIVX_YERR);
   fChain->SetBranchAddress("PiMiss_ORIVX_ZERR", &PiMiss_ORIVX_ZERR, &b_PiMiss_ORIVX_ZERR);
   fChain->SetBranchAddress("PiMiss_ORIVX_CHI2", &PiMiss_ORIVX_CHI2, &b_PiMiss_ORIVX_CHI2);
   fChain->SetBranchAddress("PiMiss_ORIVX_NDOF", &PiMiss_ORIVX_NDOF, &b_PiMiss_ORIVX_NDOF);
   fChain->SetBranchAddress("PiMiss_ORIVX_COV_", PiMiss_ORIVX_COV_, &b_PiMiss_ORIVX_COV_);
   fChain->SetBranchAddress("PiMiss_P", &PiMiss_P, &b_PiMiss_P);
   fChain->SetBranchAddress("PiMiss_PT", &PiMiss_PT, &b_PiMiss_PT);
   fChain->SetBranchAddress("PiMiss_PE", &PiMiss_PE, &b_PiMiss_PE);
   fChain->SetBranchAddress("PiMiss_PX", &PiMiss_PX, &b_PiMiss_PX);
   fChain->SetBranchAddress("PiMiss_PY", &PiMiss_PY, &b_PiMiss_PY);
   fChain->SetBranchAddress("PiMiss_PZ", &PiMiss_PZ, &b_PiMiss_PZ);
   fChain->SetBranchAddress("PiMiss_M", &PiMiss_M, &b_PiMiss_M);
   fChain->SetBranchAddress("PiMiss_ID", &PiMiss_ID, &b_PiMiss_ID);
   fChain->SetBranchAddress("PiMiss_PIDe", &PiMiss_PIDe, &b_PiMiss_PIDe);
   fChain->SetBranchAddress("PiMiss_PIDmu", &PiMiss_PIDmu, &b_PiMiss_PIDmu);
   fChain->SetBranchAddress("PiMiss_PIDK", &PiMiss_PIDK, &b_PiMiss_PIDK);
   fChain->SetBranchAddress("PiMiss_PIDp", &PiMiss_PIDp, &b_PiMiss_PIDp);
   fChain->SetBranchAddress("PiMiss_PIDd", &PiMiss_PIDd, &b_PiMiss_PIDd);
   fChain->SetBranchAddress("PiMiss_ProbNNe", &PiMiss_ProbNNe, &b_PiMiss_ProbNNe);
   fChain->SetBranchAddress("PiMiss_ProbNNk", &PiMiss_ProbNNk, &b_PiMiss_ProbNNk);
   fChain->SetBranchAddress("PiMiss_ProbNNp", &PiMiss_ProbNNp, &b_PiMiss_ProbNNp);
   fChain->SetBranchAddress("PiMiss_ProbNNpi", &PiMiss_ProbNNpi, &b_PiMiss_ProbNNpi);
   fChain->SetBranchAddress("PiMiss_ProbNNmu", &PiMiss_ProbNNmu, &b_PiMiss_ProbNNmu);
   fChain->SetBranchAddress("PiMiss_ProbNNd", &PiMiss_ProbNNd, &b_PiMiss_ProbNNd);
   fChain->SetBranchAddress("PiMiss_ProbNNghost", &PiMiss_ProbNNghost, &b_PiMiss_ProbNNghost);
   fChain->SetBranchAddress("PiMiss_hasMuon", &PiMiss_hasMuon, &b_PiMiss_hasMuon);
   fChain->SetBranchAddress("PiMiss_isMuon", &PiMiss_isMuon, &b_PiMiss_isMuon);
   fChain->SetBranchAddress("PiMiss_hasRich", &PiMiss_hasRich, &b_PiMiss_hasRich);
   fChain->SetBranchAddress("PiMiss_UsedRichAerogel", &PiMiss_UsedRichAerogel, &b_PiMiss_UsedRichAerogel);
   fChain->SetBranchAddress("PiMiss_UsedRich1Gas", &PiMiss_UsedRich1Gas, &b_PiMiss_UsedRich1Gas);
   fChain->SetBranchAddress("PiMiss_UsedRich2Gas", &PiMiss_UsedRich2Gas, &b_PiMiss_UsedRich2Gas);
   fChain->SetBranchAddress("PiMiss_RichAboveElThres", &PiMiss_RichAboveElThres, &b_PiMiss_RichAboveElThres);
   fChain->SetBranchAddress("PiMiss_RichAboveMuThres", &PiMiss_RichAboveMuThres, &b_PiMiss_RichAboveMuThres);
   fChain->SetBranchAddress("PiMiss_RichAbovePiThres", &PiMiss_RichAbovePiThres, &b_PiMiss_RichAbovePiThres);
   fChain->SetBranchAddress("PiMiss_RichAboveKaThres", &PiMiss_RichAboveKaThres, &b_PiMiss_RichAboveKaThres);
   fChain->SetBranchAddress("PiMiss_RichAbovePrThres", &PiMiss_RichAbovePrThres, &b_PiMiss_RichAbovePrThres);
   fChain->SetBranchAddress("PiMiss_hasCalo", &PiMiss_hasCalo, &b_PiMiss_hasCalo);
   fChain->SetBranchAddress("PiMiss_L0Global_Dec", &PiMiss_L0Global_Dec, &b_PiMiss_L0Global_Dec);
   fChain->SetBranchAddress("PiMiss_L0Global_TIS", &PiMiss_L0Global_TIS, &b_PiMiss_L0Global_TIS);
   fChain->SetBranchAddress("PiMiss_L0Global_TOS", &PiMiss_L0Global_TOS, &b_PiMiss_L0Global_TOS);
   fChain->SetBranchAddress("PiMiss_Hlt1Global_Dec", &PiMiss_Hlt1Global_Dec, &b_PiMiss_Hlt1Global_Dec);
   fChain->SetBranchAddress("PiMiss_Hlt1Global_TIS", &PiMiss_Hlt1Global_TIS, &b_PiMiss_Hlt1Global_TIS);
   fChain->SetBranchAddress("PiMiss_Hlt1Global_TOS", &PiMiss_Hlt1Global_TOS, &b_PiMiss_Hlt1Global_TOS);
   fChain->SetBranchAddress("PiMiss_Hlt1Phys_Dec", &PiMiss_Hlt1Phys_Dec, &b_PiMiss_Hlt1Phys_Dec);
   fChain->SetBranchAddress("PiMiss_Hlt1Phys_TIS", &PiMiss_Hlt1Phys_TIS, &b_PiMiss_Hlt1Phys_TIS);
   fChain->SetBranchAddress("PiMiss_Hlt1Phys_TOS", &PiMiss_Hlt1Phys_TOS, &b_PiMiss_Hlt1Phys_TOS);
   fChain->SetBranchAddress("PiMiss_Hlt2Global_Dec", &PiMiss_Hlt2Global_Dec, &b_PiMiss_Hlt2Global_Dec);
   fChain->SetBranchAddress("PiMiss_Hlt2Global_TIS", &PiMiss_Hlt2Global_TIS, &b_PiMiss_Hlt2Global_TIS);
   fChain->SetBranchAddress("PiMiss_Hlt2Global_TOS", &PiMiss_Hlt2Global_TOS, &b_PiMiss_Hlt2Global_TOS);
   fChain->SetBranchAddress("PiMiss_Hlt2Phys_Dec", &PiMiss_Hlt2Phys_Dec, &b_PiMiss_Hlt2Phys_Dec);
   fChain->SetBranchAddress("PiMiss_Hlt2Phys_TIS", &PiMiss_Hlt2Phys_TIS, &b_PiMiss_Hlt2Phys_TIS);
   fChain->SetBranchAddress("PiMiss_Hlt2Phys_TOS", &PiMiss_Hlt2Phys_TOS, &b_PiMiss_Hlt2Phys_TOS);
   fChain->SetBranchAddress("PiMiss_TRACK_Type", &PiMiss_TRACK_Type, &b_PiMiss_TRACK_Type);
   fChain->SetBranchAddress("PiMiss_TRACK_Key", &PiMiss_TRACK_Key, &b_PiMiss_TRACK_Key);
   fChain->SetBranchAddress("PiMiss_TRACK_CHI2NDOF", &PiMiss_TRACK_CHI2NDOF, &b_PiMiss_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("PiMiss_TRACK_PCHI2", &PiMiss_TRACK_PCHI2, &b_PiMiss_TRACK_PCHI2);
   fChain->SetBranchAddress("PiMiss_TRACK_MatchCHI2", &PiMiss_TRACK_MatchCHI2, &b_PiMiss_TRACK_MatchCHI2);
   fChain->SetBranchAddress("PiMiss_TRACK_GhostProb", &PiMiss_TRACK_GhostProb, &b_PiMiss_TRACK_GhostProb);
   fChain->SetBranchAddress("PiMiss_TRACK_CloneDist", &PiMiss_TRACK_CloneDist, &b_PiMiss_TRACK_CloneDist);
   fChain->SetBranchAddress("PiMiss_TRACK_Likelihood", &PiMiss_TRACK_Likelihood, &b_PiMiss_TRACK_Likelihood);
   fChain->SetBranchAddress("PiS_MC12TuneV2_ProbNNe", &PiS_MC12TuneV2_ProbNNe, &b_PiS_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("PiS_MC12TuneV2_ProbNNmu", &PiS_MC12TuneV2_ProbNNmu, &b_PiS_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("PiS_MC12TuneV2_ProbNNpi", &PiS_MC12TuneV2_ProbNNpi, &b_PiS_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("PiS_MC12TuneV2_ProbNNk", &PiS_MC12TuneV2_ProbNNk, &b_PiS_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("PiS_MC12TuneV2_ProbNNp", &PiS_MC12TuneV2_ProbNNp, &b_PiS_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("PiS_MC12TuneV2_ProbNNghost", &PiS_MC12TuneV2_ProbNNghost, &b_PiS_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("PiS_MC12TuneV3_ProbNNe", &PiS_MC12TuneV3_ProbNNe, &b_PiS_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("PiS_MC12TuneV3_ProbNNmu", &PiS_MC12TuneV3_ProbNNmu, &b_PiS_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("PiS_MC12TuneV3_ProbNNpi", &PiS_MC12TuneV3_ProbNNpi, &b_PiS_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("PiS_MC12TuneV3_ProbNNk", &PiS_MC12TuneV3_ProbNNk, &b_PiS_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("PiS_MC12TuneV3_ProbNNp", &PiS_MC12TuneV3_ProbNNp, &b_PiS_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("PiS_MC12TuneV3_ProbNNghost", &PiS_MC12TuneV3_ProbNNghost, &b_PiS_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("PiS_MC12TuneV4_ProbNNe", &PiS_MC12TuneV4_ProbNNe, &b_PiS_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("PiS_MC12TuneV4_ProbNNmu", &PiS_MC12TuneV4_ProbNNmu, &b_PiS_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("PiS_MC12TuneV4_ProbNNpi", &PiS_MC12TuneV4_ProbNNpi, &b_PiS_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("PiS_MC12TuneV4_ProbNNk", &PiS_MC12TuneV4_ProbNNk, &b_PiS_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("PiS_MC12TuneV4_ProbNNp", &PiS_MC12TuneV4_ProbNNp, &b_PiS_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("PiS_MC12TuneV4_ProbNNghost", &PiS_MC12TuneV4_ProbNNghost, &b_PiS_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("PiS_MC15TuneV1_ProbNNe", &PiS_MC15TuneV1_ProbNNe, &b_PiS_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("PiS_MC15TuneV1_ProbNNmu", &PiS_MC15TuneV1_ProbNNmu, &b_PiS_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("PiS_MC15TuneV1_ProbNNpi", &PiS_MC15TuneV1_ProbNNpi, &b_PiS_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("PiS_MC15TuneV1_ProbNNk", &PiS_MC15TuneV1_ProbNNk, &b_PiS_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("PiS_MC15TuneV1_ProbNNp", &PiS_MC15TuneV1_ProbNNp, &b_PiS_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("PiS_MC15TuneV1_ProbNNghost", &PiS_MC15TuneV1_ProbNNghost, &b_PiS_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("PiS_CosTheta", &PiS_CosTheta, &b_PiS_CosTheta);
   fChain->SetBranchAddress("PiS_OWNPV_X", &PiS_OWNPV_X, &b_PiS_OWNPV_X);
   fChain->SetBranchAddress("PiS_OWNPV_Y", &PiS_OWNPV_Y, &b_PiS_OWNPV_Y);
   fChain->SetBranchAddress("PiS_OWNPV_Z", &PiS_OWNPV_Z, &b_PiS_OWNPV_Z);
   fChain->SetBranchAddress("PiS_OWNPV_XERR", &PiS_OWNPV_XERR, &b_PiS_OWNPV_XERR);
   fChain->SetBranchAddress("PiS_OWNPV_YERR", &PiS_OWNPV_YERR, &b_PiS_OWNPV_YERR);
   fChain->SetBranchAddress("PiS_OWNPV_ZERR", &PiS_OWNPV_ZERR, &b_PiS_OWNPV_ZERR);
   fChain->SetBranchAddress("PiS_OWNPV_CHI2", &PiS_OWNPV_CHI2, &b_PiS_OWNPV_CHI2);
   fChain->SetBranchAddress("PiS_OWNPV_NDOF", &PiS_OWNPV_NDOF, &b_PiS_OWNPV_NDOF);
   fChain->SetBranchAddress("PiS_OWNPV_COV_", PiS_OWNPV_COV_, &b_PiS_OWNPV_COV_);
   fChain->SetBranchAddress("PiS_IP_OWNPV", &PiS_IP_OWNPV, &b_PiS_IP_OWNPV);
   fChain->SetBranchAddress("PiS_IPCHI2_OWNPV", &PiS_IPCHI2_OWNPV, &b_PiS_IPCHI2_OWNPV);
   fChain->SetBranchAddress("PiS_ORIVX_X", &PiS_ORIVX_X, &b_PiS_ORIVX_X);
   fChain->SetBranchAddress("PiS_ORIVX_Y", &PiS_ORIVX_Y, &b_PiS_ORIVX_Y);
   fChain->SetBranchAddress("PiS_ORIVX_Z", &PiS_ORIVX_Z, &b_PiS_ORIVX_Z);
   fChain->SetBranchAddress("PiS_ORIVX_XERR", &PiS_ORIVX_XERR, &b_PiS_ORIVX_XERR);
   fChain->SetBranchAddress("PiS_ORIVX_YERR", &PiS_ORIVX_YERR, &b_PiS_ORIVX_YERR);
   fChain->SetBranchAddress("PiS_ORIVX_ZERR", &PiS_ORIVX_ZERR, &b_PiS_ORIVX_ZERR);
   fChain->SetBranchAddress("PiS_ORIVX_CHI2", &PiS_ORIVX_CHI2, &b_PiS_ORIVX_CHI2);
   fChain->SetBranchAddress("PiS_ORIVX_NDOF", &PiS_ORIVX_NDOF, &b_PiS_ORIVX_NDOF);
   fChain->SetBranchAddress("PiS_ORIVX_COV_", PiS_ORIVX_COV_, &b_PiS_ORIVX_COV_);
   fChain->SetBranchAddress("PiS_P", &PiS_P, &b_PiS_P);
   fChain->SetBranchAddress("PiS_PT", &PiS_PT, &b_PiS_PT);
   fChain->SetBranchAddress("PiS_PE", &PiS_PE, &b_PiS_PE);
   fChain->SetBranchAddress("PiS_PX", &PiS_PX, &b_PiS_PX);
   fChain->SetBranchAddress("PiS_PY", &PiS_PY, &b_PiS_PY);
   fChain->SetBranchAddress("PiS_PZ", &PiS_PZ, &b_PiS_PZ);
   fChain->SetBranchAddress("PiS_M", &PiS_M, &b_PiS_M);
   fChain->SetBranchAddress("PiS_ID", &PiS_ID, &b_PiS_ID);
   fChain->SetBranchAddress("PiS_PIDe", &PiS_PIDe, &b_PiS_PIDe);
   fChain->SetBranchAddress("PiS_PIDmu", &PiS_PIDmu, &b_PiS_PIDmu);
   fChain->SetBranchAddress("PiS_PIDK", &PiS_PIDK, &b_PiS_PIDK);
   fChain->SetBranchAddress("PiS_PIDp", &PiS_PIDp, &b_PiS_PIDp);
   fChain->SetBranchAddress("PiS_PIDd", &PiS_PIDd, &b_PiS_PIDd);
   fChain->SetBranchAddress("PiS_ProbNNe", &PiS_ProbNNe, &b_PiS_ProbNNe);
   fChain->SetBranchAddress("PiS_ProbNNk", &PiS_ProbNNk, &b_PiS_ProbNNk);
   fChain->SetBranchAddress("PiS_ProbNNp", &PiS_ProbNNp, &b_PiS_ProbNNp);
   fChain->SetBranchAddress("PiS_ProbNNpi", &PiS_ProbNNpi, &b_PiS_ProbNNpi);
   fChain->SetBranchAddress("PiS_ProbNNmu", &PiS_ProbNNmu, &b_PiS_ProbNNmu);
   fChain->SetBranchAddress("PiS_ProbNNd", &PiS_ProbNNd, &b_PiS_ProbNNd);
   fChain->SetBranchAddress("PiS_ProbNNghost", &PiS_ProbNNghost, &b_PiS_ProbNNghost);
   fChain->SetBranchAddress("PiS_hasMuon", &PiS_hasMuon, &b_PiS_hasMuon);
   fChain->SetBranchAddress("PiS_isMuon", &PiS_isMuon, &b_PiS_isMuon);
   fChain->SetBranchAddress("PiS_hasRich", &PiS_hasRich, &b_PiS_hasRich);
   fChain->SetBranchAddress("PiS_UsedRichAerogel", &PiS_UsedRichAerogel, &b_PiS_UsedRichAerogel);
   fChain->SetBranchAddress("PiS_UsedRich1Gas", &PiS_UsedRich1Gas, &b_PiS_UsedRich1Gas);
   fChain->SetBranchAddress("PiS_UsedRich2Gas", &PiS_UsedRich2Gas, &b_PiS_UsedRich2Gas);
   fChain->SetBranchAddress("PiS_RichAboveElThres", &PiS_RichAboveElThres, &b_PiS_RichAboveElThres);
   fChain->SetBranchAddress("PiS_RichAboveMuThres", &PiS_RichAboveMuThres, &b_PiS_RichAboveMuThres);
   fChain->SetBranchAddress("PiS_RichAbovePiThres", &PiS_RichAbovePiThres, &b_PiS_RichAbovePiThres);
   fChain->SetBranchAddress("PiS_RichAboveKaThres", &PiS_RichAboveKaThres, &b_PiS_RichAboveKaThres);
   fChain->SetBranchAddress("PiS_RichAbovePrThres", &PiS_RichAbovePrThres, &b_PiS_RichAbovePrThres);
   fChain->SetBranchAddress("PiS_hasCalo", &PiS_hasCalo, &b_PiS_hasCalo);
   fChain->SetBranchAddress("PiS_L0Global_Dec", &PiS_L0Global_Dec, &b_PiS_L0Global_Dec);
   fChain->SetBranchAddress("PiS_L0Global_TIS", &PiS_L0Global_TIS, &b_PiS_L0Global_TIS);
   fChain->SetBranchAddress("PiS_L0Global_TOS", &PiS_L0Global_TOS, &b_PiS_L0Global_TOS);
   fChain->SetBranchAddress("PiS_Hlt1Global_Dec", &PiS_Hlt1Global_Dec, &b_PiS_Hlt1Global_Dec);
   fChain->SetBranchAddress("PiS_Hlt1Global_TIS", &PiS_Hlt1Global_TIS, &b_PiS_Hlt1Global_TIS);
   fChain->SetBranchAddress("PiS_Hlt1Global_TOS", &PiS_Hlt1Global_TOS, &b_PiS_Hlt1Global_TOS);
   fChain->SetBranchAddress("PiS_Hlt1Phys_Dec", &PiS_Hlt1Phys_Dec, &b_PiS_Hlt1Phys_Dec);
   fChain->SetBranchAddress("PiS_Hlt1Phys_TIS", &PiS_Hlt1Phys_TIS, &b_PiS_Hlt1Phys_TIS);
   fChain->SetBranchAddress("PiS_Hlt1Phys_TOS", &PiS_Hlt1Phys_TOS, &b_PiS_Hlt1Phys_TOS);
   fChain->SetBranchAddress("PiS_Hlt2Global_Dec", &PiS_Hlt2Global_Dec, &b_PiS_Hlt2Global_Dec);
   fChain->SetBranchAddress("PiS_Hlt2Global_TIS", &PiS_Hlt2Global_TIS, &b_PiS_Hlt2Global_TIS);
   fChain->SetBranchAddress("PiS_Hlt2Global_TOS", &PiS_Hlt2Global_TOS, &b_PiS_Hlt2Global_TOS);
   fChain->SetBranchAddress("PiS_Hlt2Phys_Dec", &PiS_Hlt2Phys_Dec, &b_PiS_Hlt2Phys_Dec);
   fChain->SetBranchAddress("PiS_Hlt2Phys_TIS", &PiS_Hlt2Phys_TIS, &b_PiS_Hlt2Phys_TIS);
   fChain->SetBranchAddress("PiS_Hlt2Phys_TOS", &PiS_Hlt2Phys_TOS, &b_PiS_Hlt2Phys_TOS);
   fChain->SetBranchAddress("PiS_TRACK_Type", &PiS_TRACK_Type, &b_PiS_TRACK_Type);
   fChain->SetBranchAddress("PiS_TRACK_Key", &PiS_TRACK_Key, &b_PiS_TRACK_Key);
   fChain->SetBranchAddress("PiS_TRACK_CHI2NDOF", &PiS_TRACK_CHI2NDOF, &b_PiS_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("PiS_TRACK_PCHI2", &PiS_TRACK_PCHI2, &b_PiS_TRACK_PCHI2);
   fChain->SetBranchAddress("PiS_TRACK_MatchCHI2", &PiS_TRACK_MatchCHI2, &b_PiS_TRACK_MatchCHI2);
   fChain->SetBranchAddress("PiS_TRACK_GhostProb", &PiS_TRACK_GhostProb, &b_PiS_TRACK_GhostProb);
   fChain->SetBranchAddress("PiS_TRACK_CloneDist", &PiS_TRACK_CloneDist, &b_PiS_TRACK_CloneDist);
   fChain->SetBranchAddress("PiS_TRACK_Likelihood", &PiS_TRACK_Likelihood, &b_PiS_TRACK_Likelihood);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
   fChain->SetBranchAddress("PVX", PVX, &b_PVX);
   fChain->SetBranchAddress("PVY", PVY, &b_PVY);
   fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
   fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
   fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
   fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
   fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
   fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
   fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   Notify();
}

Bool_t DecayTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void DecayTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t DecayTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef DecayTree_cxx
