"""
This options file demonstrates how to run a stripping line
from a specific stripping version on a local MC DST file
It is based on the minimal DaVinci DecayTreeTuple example
"""

# Load input particles
from StandardParticles import StdLoosePions as Pions
from StandardParticles import StdLooseKaons as Kaons
from PhysConf.Selections import CombineSelection, FilterSelection
from PhysConf.Selections import SelectionSequence

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
    EventNodeKiller,
    ProcStatusCheck,
    DaVinci,
    DecayTreeTuple
)
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *

import shelve

# Node killer: remove the previous Stripping
event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

# Build a new stream called 'CustomStream' that only
# contains the desired line
strip = 'stripping28r2p1'

config_db = strippingConfiguration(strip)
config = dict(config_db) # need to do this since the config_db is read-only
print config['BXX']['CONFIG']
config['BXX']['CONFIG']['DzeroDaughterKaonPIDKMin'] = -1000
config['BXX']['CONFIG']['DzeroDaughterPionPIDKMax'] = 1000
config['BXX']['CONFIG']['DzeroDaughterPionPIDmuMax'] = 1000
config['BXX']['CONFIG']['DstarPionPIDKMax'] = 1000

config_db_updated = shelve.open('tmp_stripping_config.db')
config_db_updated.update(config)
streams = buildStreams(stripping=config_db,
                       archive=strippingArchive(strip))

#streams = buildStreams(stripping=strippingConfiguration(strip),
#                       archive=strippingArchive(strip))

line = 'DstRSwD02K2PiD0forBXXLine'

custom_stream = StrippingStream('CustomStream')
custom_line = 'Stripping'+line 

for stream in streams:
    for sline in stream.lines:
        if sline.name() == custom_line:
            custom_stream.appendLines([sline])

# Create the actual Stripping configurable
filterBadEvents = ProcStatusCheck()

sc = StrippingConf(Streams=[custom_stream],
                   MaxCandidates=2000,
                   AcceptBadEvents=False,
                   BadEventSelection=filterBadEvents)

d0_comb = '(AM > 400.0) & (AM < 8700.0)'
d0_mother = '(VFASPF(VCHI2/VDOF) < 25.0)' 
pion_daughter = '(TRCHI2DOF < 3.0)' 

d0_sel = CombineSelection('d0_sel',[Kaons, Pions],
    DecayDescriptors=['[D0 -> pi+ pi- K- pi+]cc'],
    CombinationCut=d0_comb,MotherCut=d0_mother,
    DaughtersCuts = {"pi+" : pion_daughter}
)
d0_seq = SelectionSequence('d0_seq', TopSelection=d0_sel)

# Create an ntuple to capture D*+ decays from the StrippingLine line
dtt = DecayTreeTuple('TuplePartialRS')
# The output is placed directly into Phys, so we only need to
# define the stripping line here
dtt.Inputs = d0_seq.outputLocations()
#dtt.Inputs = ['/Event/Phys/{0}/Particles'.format(line)]
dtt.Decay = '[D0 -> pi+ pi- K- pi+]CC'

# Configure DaVinci

# Important: The selection sequence needs to be inserted into
# the Gaudi sequence for the stripping to run
#DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])
DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'Tuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2016'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1

# Use the local input data
IOHelper().inputFiles([
'/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00062512/0000/00062512_00000012_7.AllStreams.dst'
,'/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00062512/0000/00062512_00000014_7.AllStreams.dst'
], clear=True)
