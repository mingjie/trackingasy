from Configurables import FilterInTrees, EventNodeKiller
from PhysSelPython.Wrappers import Selection, DataOnDemand

from Configurables import DaVinci
from GaudiConf import IOHelper

# Load algorithms
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from Configurables import TupleToolFitMissPion
from Configurables import TupleToolFitRecoPion
from DecayTreeTuple.Configuration import *

# Load input particles
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons as Kaons
# Load Selection objects
from PhysConf.Selections import CombineSelection, FilterSelection
from PhysConf.Selections import SelectionSequence

from Configurables import CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
      EventNodeKiller,
      ProcStatusCheck,
      DaVinci,
      DecayTreeTuple
      )
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *

import shelve

toollist = [ 
      "TupleToolEventInfo",
      "TupleToolTISTOS",
      "TupleToolGeometry",
      "TupleToolKinematic",
      "TupleToolTrackInfo",
      "TupleToolPid",
      "TupleToolANNPID",
      "TupleToolDira",
      "TupleToolAngles",
      "TupleToolPrimaries",
      "TupleToolDecayTreeFitter",
      "TupleToolPropertime",
      "TupleToolVtxIsoln",
      "TupleToolMCBackgroundInfo",
      "TupleToolMCTruth",
      "TupleToolRecoStats"
      ]  
mctl=[ 'TupleToolRecoStats','MCTupleToolAngles', 'MCTupleToolHierarchy', 'MCTupleToolKinematic', 'MCTupleToolPrimaries', 'MCTupleToolReconstructed', "MCTupleToolInteractions" ]

mtl = [
      "L0MuonDecision",
      "L0HadronDecision",
      "Hlt1TrackAllL0Decision",
      "Hlt1TrackMuonDecision",
      "Hlt2SingleMuonDecision",
      "Hlt2TopoMu2BodyBBDTDecision",
      "Hlt2TopoMu3BodyBBDTDecision",
      "Hlt2TopoMu4BodyBBDTDecision"
      ]

stream = 'AllStreams'
line = 'DstRSwD02K2PiD0forBXXLine'
tesLoc = '/Event/{0}/Phys/{1}/Particles'.format(stream, line)

# Node killer: remove the previous Stripping
event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

# Build a new stream called 'CustomStream' that only
# contains the desired line
strip = 'stripping28r2p1'

streams = buildStreams(stripping=strippingConfiguration(strip),
                       archive=strippingArchive(strip))

custom_stream = StrippingStream('CustomStream')
custom_line = 'Stripping'+line 
location = 'Phys/{0}/Particles'.format(line)

for stream in streams:
   for sline in stream.lines:
      if sline.name() == custom_line:
         custom_stream.appendLines([sline])

# Create the actual Stripping configurable
filterBadEvents = ProcStatusCheck()

sc = StrippingConf(Streams=[custom_stream],
      MaxCandidates=2000,
      AcceptBadEvents=False,
      BadEventSelection=filterBadEvents)

Pions = DataOnDemand('Phys/StdAllNoPIDsPions/Particles')

D0_from_strip = FilterInTrees('D0_from_strip_filter', Code="('D0' == ABSID)")
D0_from_strip_sel = Selection("D0_from_strip_sel",
                            Algorithm=D0_from_strip,
                            RequiredSelections=[DataOnDemand(Location=location)])
pion_from_strip = FilterInTrees('pion_from_strip_filter', Code="('pi+' == ABSID)")
pion_from_strip_sel = Selection("pion_from_strip_sel",
                            Algorithm=pion_from_strip,
                            RequiredSelections=[DataOnDemand(Location=location)])

d0_comb = "(ADAMASS('D0') < 100*MeV)"
d0_mother = ('(VFASPF(VCHI2/VDOF)< 9)')
dst_comb = "(ADAMASS('D*(2010)+') < 100*MeV)"
dst_mother = ('(VFASPF(VCHI2/VDOF)< 9)')
pion_daughter = "(P>2000.0) & (PT > 250.0 )& (TRCHI2DOF < 3.0)& (TRGHOSTPROB < 0.35)& (MIPCHI2DV(PRIMARY)> 4.0)" 
b0_comb = "(AM > 2200.0) & (AM < 8000.0) & (ADOCACHI2CUT( 10, ''))"
b0_mother = ('(VFASPF(VCHI2/VDOF)< 9)')

#Combine D and pions into Dstar
real_d0_sel = CombineSelection('Sel_D0',[D0_from_strip_sel, Pions],
    DecayDescriptors=['[D+ -> D0 pi+]cc'],
    CombinationCut=d0_comb,MotherCut=d0_mother,
    DaughtersCuts = {"pi+" : pion_daughter}
)

#Combine D and pions into Dstar
real_dst_sel = CombineSelection('Sel_Dst',[real_d0_sel, pion_from_strip_sel],
    DecayDescriptors=['[D*(2010)+ -> D+ pi+]cc'],
    CombinationCut=dst_comb,MotherCut=dst_mother
)

dstar_seq = SelectionSequence('D0Seq', TopSelection=real_dst_sel)

# Create an ntuple
mct = MCDecayTreeTuple('mct')
mct.Decay = "[D*(2010)- ==> ^(D~0 ==> ^K+ ^pi- ^pi- ^pi+) ^pi-]CC"
mct.Branches = {
    "Dst"   : "[D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) pi-]CC"
    ,"D0"    : "[D*(2010)- ==> ^(D~0 ==> K+ pi- pi- pi+) pi-]CC"
    ,"K"     : "[D*(2010)- ==> (D~0 ==> ^K+ pi- pi- pi+) pi-]CC"
    ,"pi1"   : "[D*(2010)- ==> (D~0 ==> K+ ^pi- pi- pi+) pi-]CC"
    ,"pi2"   : "[D*(2010)- ==> (D~0 ==> K+ pi- ^pi- pi+) pi-]CC"
    ,"pi3"   : "[D*(2010)- ==> (D~0 ==> K+ pi- pi- ^pi+) pi-]CC"
    ,"PiS"   : "[D*(2010)- ==> (D~0 ==> K+ pi- pi- pi+) ^pi-]CC"
    }
mct.ToolList=mctl

tuple_partial_RS = DecayTreeTuple('TuplePartialRS')
tuple_partial_RS.Inputs = [location]
tuple_partial_RS.Decay = '[D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi-) ^pi+]CC'
tuple_partial_RS.Branches = {
    "Dst"    : '[D*(2010)+ -> (D0 -> K- pi+ pi-) pi+]CC'
    ,"D0"     : '[D*(2010)+ -> ^(D0 -> K- pi+ pi-) pi+]CC'
    ,"K"      : '[D*(2010)+ -> (D0 -> ^K- pi+ pi-) pi+]CC'
    ,"pi1"    : '[D*(2010)+ -> (D0 -> K- ^pi+ pi-) pi+]CC'
    ,"pi2"    : '[D*(2010)+ -> (D0 -> K- pi+ ^pi-) pi+]CC'
    ,"PiS"    : '[D*(2010)+ -> (D0 -> K- pi+ pi-) ^pi+]CC'
}

tuple_full_RS = DecayTreeTuple('TupleFullRS')
tuple_full_RS.Inputs = dstar_seq.outputLocations()
tuple_full_RS.Decay = '[D*(2010)+ -> ^(D+ -> ^(D0 -> ^K- ^pi+ ^pi-) ^pi+) ^pi+]CC'
tuple_full_RS.Branches = {
    "Dst"    : '[D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) pi+]CC'
    ,"D0"     : '[D*(2010)+ -> ^(D+ -> (D0 -> K- pi+ pi-) pi+) pi+]CC'
    ,"Dfake"  : '[D*(2010)+ -> (D+ -> ^(D0 -> K- pi+ pi-) pi+) pi+]CC'
    ,"K"      : '[D*(2010)+ -> (D+ -> (D0 -> ^K- pi+ pi-) pi+) pi+]CC'
    ,"pi1"    : '[D*(2010)+ -> (D+ -> (D0 -> K- ^pi+ pi-) pi+) pi+]CC'
    ,"pi2"    : '[D*(2010)+ -> (D+ -> (D0 -> K- pi+ ^pi-) pi+) pi+]CC'
    ,"PiMiss" : '[D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) ^pi+) pi+]CC'
    ,"PiS"    : '[D*(2010)+ -> (D+ -> (D0 -> K- pi+ pi-) pi+) ^pi+]CC'
}

from Configurables import DecayTreeTuple, MCMatchObjP2MCRelator
import DecayTreeTuple.Configuration
default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

MCTruth = TupleToolMCTruth()
MCTruth.ToolList =  [
      "MCTupleToolAngles"
      , "MCTupleToolHierarchy"
      , "MCTupleToolKinematic"
      , "MCTupleToolReconstructed"
      ]
MCTruth.addTool(MCMatchObjP2MCRelator)
MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs

tuples = [tuple_full_RS, tuple_partial_RS]

for tup in tuples:
   tup.ToolList = toollist
tuple_partial_RS.addTool(TupleToolDecay, name = "Dst")
tuple_partial_RS.Dst.ToolList += ["TupleToolFitMissPion"]
tuple_full_RS.addTool(TupleToolDecay, name = "Dst")
tuple_full_RS.Dst.ToolList += ["TupleToolFitRecoPion"]

DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])
DaVinci().UserAlgorithms += [dstar_seq.sequence(),tuple_full_RS, tuple_partial_RS, mct]

# DaVinci configuration
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'Tuple.root'
DaVinci().PrintFreq = 5000
DaVinci().DataType = '2016'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = False
DaVinci().EvtMax = -1
DaVinci().CondDBtag = 'sim-20170721-2-vc-md100'
DaVinci().DDDBtag = 'dddb-20170721-3'

IOHelper().inputFiles([
   '/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00137293/0000/00137293_00000659_7.AllStreams.dst',
   '/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00137293/0000/00137293_00000660_7.AllStreams.dst',
   '/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00137293/0000/00137293_00000664_7.AllStreams.dst',
   #'/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/SEMILEPTONIC.DST/00153032/0000/00153032_00009989_1.semileptonic.dst',
   #'/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/SEMILEPTONIC.DST/00153032/0000/00153032_00009949_1.semileptonic.dst',
   #'/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/SEMILEPTONIC.DST/00153032/0000/00153032_00009884_1.semileptonic.dst',
   #'/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/SEMILEPTONIC.DST/00153032/0000/00153032_00009785_1.semileptonic.dst',
   #'/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/SEMILEPTONIC.DST/00153032/0000/00153032_00009708_1.semileptonic.dst',
#'/eos/lhcb/grid/prod/lhcb/validation/Collision16/SEMILEPTONIC.DST/00150263/0000/00150263_00000014_1.semileptonic.dst',
#'/eos/lhcb/grid/prod/lhcb/validation/Collision16/SEMILEPTONIC.DST/00150263/0000/00150263_00000049_1.semileptonic.dst',
#'/eos/lhcb/grid/prod/lhcb/validation/Collision16/SEMILEPTONIC.DST/00150263/0000/00150263_00000078_1.semileptonic.dst',
#'/eos/lhcb/grid/prod/lhcb/validation/Collision16/SEMILEPTONIC.DST/00150263/0000/00150263_00000094_1.semileptonic.dst',
#'/eos/lhcb/grid/prod/lhcb/validation/Collision16/SEMILEPTONIC.DST/00150263/0000/00150263_00000112_1.semileptonic.dst',
#'/eos/lhcb/grid/prod/lhcb/validation/Collision16/SEMILEPTONIC.DST/00150263/0000/00150263_00000173_1.semileptonic.dst'
], clear=True)
