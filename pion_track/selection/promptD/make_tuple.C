bool make_tuple(TString year="16", TString polarity="Down", TString type="RS", TString reco = "Full", int ibin = 0){

   const int nbin = 7;
   double bin[nbin+1] = {2,5,10,20,30,40,50,100};
   double pmin = bin[ibin], pmax = bin[ibin];

   TString jobid;

   if (year=="16"){
       jobid = (polarity.Contains("Down")) ? "213" : "216";
       if (type.Contains("WS")) {jobid = (polarity.Contains("Down")) ? "221" : "224";}
   } else if (year=="17"){
       jobid = (polarity.Contains("Down")) ? "214" : "217";
       if (type.Contains("WS")) {jobid = (polarity.Contains("Down")) ? "222" : "225";}
   } else if (year=="18"){
       jobid = (polarity.Contains("Down")) ? "215" : "218";
       if (type.Contains("WS")) {jobid = (polarity.Contains("Down")) ? "223" : "226";}
   }

   TString directory = "/eos/home-m/mingjie/Asls/SCA/GangaDATA/pionTrackAsy/" + jobid;
   std::cout<<"directory = "<<directory<<std::endl;
   TString tuple = (reco.Contains("Full")) ? "TupleFullRS" : "TuplePartialRS";

   int maxfile = 400;//can be 400;

   TChain *chain = new TChain();
   for (int i=0; i<maxfile; i++){
	TString file = directory + "/" + TString::Format("%d",i) + "/output/Tuple.root";
	if (TFile::Open(file)){
	   std::cout<<"find "<<file<<std::endl;
	   chain->Add(file + "/"+tuple+"/DecayTree");
	}
   }

   std::cout<<" all files loaded, now start to copy the tree"<<std::endl;
   TFile *output = new TFile("./results/20"+year+"Mag"+polarity+reco+type+".root","recreate");
   double  pion_mass = 139.57061, rho_mass = 775.26, D0_mass = 1864.83;
   double  alpha = 0.317, p0 = 2400, p1 = 418, p2 = 497, beta1 = 0.01397, beta2 = 0.01605;
   TString PiS_cut = "PiS_TRACK_CHI2NDOF<3&&PiS_PT>250&&PiS_IPCHI2_OWNPV<4&&PiS_IP_OWNPV<0.3&&PiS_PIDK<10";
   TString   K_cut = "K_TRACK_CHI2NDOF<3&&K_P>2000&&K_PT>400&&K_IPCHI2_OWNPV>4&&K_PIDK>4";
   TString pi1_cut = "pi1_TRACK_CHI2NDOF<3&&pi1_P>2000&&pi1_PT>400&&pi1_IPCHI2_OWNPV>4&&pi1_PIDK<10&&pi1_PIDmu<10";
   TString pi2_cut = "pi2_TRACK_CHI2NDOF<3&&pi2_P>2000&&pi2_PT>400&&pi2_IPCHI2_OWNPV>4&&pi2_PIDK<10&&pi2_PIDmu<10";
   TString  D0_cut = "D0_ENDVERTEX_CHI2/D0_ENDVERTEX_NDOF<6&&D0_FDCHI2_OWNPV>120&&D0_FD_OWNPV>4&&D0_DIRA_OWNPV>0.9997&&D0_IPCHI2_OWNPV<25&&D0_MM>1400&&D0_MM<1700&&D0_PT>3000"; 
   TString Dst_cut = "Dst_ENDVERTEX_CHI2/Dst_ENDVERTEX_NDOF<5&&"+TString::Format("fabs(sqrt(pow(pi1_PE+pi2_PE,2)-pow(pi1_PX+pi2_PX,2)-pow(pi1_PY+pi2_PY,2)-pow(pi1_PZ+pi2_PZ,2))-%g)<200&&Dst_MM-D0_MM-%g<40",rho_mass,pion_mass);

   TString offline_cut = "PMiss_Fit/PMiss_Err>2&&PtMiss_Fit/PtMiss_Err>2.5&&Dst_MM-D0_MM<175&&Dst_MM-D0_MM>139";
   if (reco.Contains("Full")){
	D0_cut = "Dfake_ENDVERTEX_CHI2/Dfake_ENDVERTEX_NDOF<6&&Dfake_FDCHI2_OWNPV>120&&Dfake_FD_OWNPV>4&&Dfake_DIRA_OWNPV>0.9997&&Dfake_IPCHI2_OWNPV<25&&Dfake_MM>1400&&Dfake_MM<1700&&Dfake_PT>3000"; 
	offline_cut = "D0_ENDVERTEX_CHI2/D0_ENDVERTEX_NDOF<6&&"+TString::Format("fabs(D0_MM-%g)<30&&",D0_mass);
	offline_cut += "PiMiss_P>2000&&PiMiss_PT>300&&PiMiss_TRACK_CHI2NDOF<4&&PiMiss_TRACK_CloneDist<=0&&";
#if 0 //Based on Arg 16 discuss, no more fiducial cut
	offline_cut += TString::Format("fabs(PiMiss_PX)<=%g*(PiMiss_PZ-%g)&&",alpha,p0);
	offline_cut += TString::Format("(fabs(PiMiss_PY/PiMiss_PZ)>=0.02 || (fabs(PiMiss_PY/PiMiss_PZ)<0.02&&fabs(PiMiss_PX)>%g-%g*PiMiss_PZ&&fabs(PiMiss_PX)<%g-%g*PiMiss_PZ))&&",p1,beta1,p2,beta2);
#endif
	offline_cut += "Dst_MM-D0_MM<160&&Dst_MM-D0_MM>138";
	//offline_cut += "PiMiss_PIDK<10&&Dst_MM-D0_MM<160&&Dst_MM-D0_MM>138";
   }

   TString preselection = PiS_cut + "&&" + K_cut + "&&" + pi1_cut + "&&" + pi2_cut + "&&" + D0_cut + "&&" + Dst_cut;

   chain->SetBranchStatus("*",0);
   TString particles[] = {"K","pi1","pi2","PiS","PiMiss","D0","Dfake","Dst"};
   for (auto part : particles){
	if (reco.Contains("Part")&&(part.Contains("PiMiss")||part.Contains("Dfake"))) continue;
	chain->SetBranchStatus(part+"_P",1);
	chain->SetBranchStatus(part+"_PT",1);
	chain->SetBranchStatus(part+"_PE",1);
	chain->SetBranchStatus(part+"_PX",1);
	chain->SetBranchStatus(part+"_PY",1);
	chain->SetBranchStatus(part+"_PZ",1);
	chain->SetBranchStatus(part+"_IPCHI2_OWNPV",1);
	chain->SetBranchStatus(part+"_ID",1);
	if (part.Contains("K")||part.Contains("pi")||part.Contains("Pi")){
	   chain->SetBranchStatus(part+"_IP_OWNPV",1);
	   chain->SetBranchStatus(part+"_TRACK_CHI2NDOF",1);
	   chain->SetBranchStatus(part+"_PIDK",1);
	   chain->SetBranchStatus(part+"_PIDmu",1);
           chain->SetBranchStatus(part+"_M", 1);
	}
	if (part.Contains("D")){
	   chain->SetBranchStatus(part+"_ENDVERTEX_CHI2",1);
	   chain->SetBranchStatus(part+"_ENDVERTEX_NDOF",1);
	   chain->SetBranchStatus(part+"_FDCHI2_OWNPV",1);
	   chain->SetBranchStatus(part+"_FD_OWNPV",1);
	   chain->SetBranchStatus(part+"_DIRA_OWNPV",1);
	   chain->SetBranchStatus(part+"_MM",1);
	}
   }
   chain->SetBranchStatus("*_Fit",1);
   chain->SetBranchStatus("*_Err",1);
   chain->SetBranchStatus("Chi2_final",1);
   chain->SetBranchStatus("PD0_suc",1);
   if (reco.Contains("Full")) chain->SetBranchStatus("PiMiss_TRACK_CloneDist",1);


   TTree *newtree = chain->CopyTree(offline_cut);

   newtree->Write();
   delete chain;
   output->Close();

   return true;
}
